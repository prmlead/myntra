﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using OfficeOpenXml;
using PRMServices.Models;
using MySql.Data.MySqlClient;
using PRMServices;

namespace PRMServices.Common
{

    public class SlotUtility
    {
        
        public static Response BookSlotsFromVendorAlertsToCustomer(int reqID, int vendorID, string sessionID, SlotBooking bookSlot)
        {

            Response response = new Response();
            PRMFwdReqService prmservices = new PRMFwdReqService();
            PRMServices prmservice = new PRMServices();
            
            string ScreenName = "";

            FwdRequirement reqVendor = prmservices.GetRequirementData(reqID, vendorID, sessionID);
            UserInfo customer = prmservice.GetUser(reqVendor.CustomerID, sessionID);
            UserInfo vendor = prmservice.GetUser(vendorID, sessionID);

            string subject = string.Empty;
            string bodyEMAIL = string.Empty;
            string bodySMS = string.Empty;
            string bodyTELEGRAM = string.Empty;
            string actionTelegaram = string.Empty;
            string slotAction = string.Empty;


            if (!bookSlot.IS_CUSTOMER) {

                ScreenName = "BOOK SLOT";
                reqVendor.Module = ScreenName;
                if (bookSlot.Slot_is_valid == 0)
                {
                    subject = "Vendor inspection slot has been Cancelled! Req ID:";
                    slotAction = "CANCELLED";
                }
                else if (bookSlot.Slot_is_valid == 1)
                {
                    subject = "Vendor inspection slot has been booked! Req ID:"+ reqID;
                    slotAction = "BOOKED";
                }

                bodyEMAIL = Utilities.GenerateEmailBody("BookSlotCustomerEMAIL");
                bodySMS = Utilities.GenerateEmailBody("BookSlotCustomerSMS");
                //bodyTELEGRAM = Utilities.GenerateEmailBody("BookSlotCustomerTemplateTELEGRAM");
                actionTelegaram = "SLOT BOOKED";

                bodyEMAIL = String.Format(bodyEMAIL, customer.FirstName, customer.LastName, reqVendor.RequirementID, reqVendor.Title, bookSlot.Slot_description,
                                          Utilities.ToLocal(bookSlot.Slot_Start_Time), Utilities.ToLocal(bookSlot.Slot_End_Time), vendor.FirstName, vendor.LastName, slotAction);
                Utilities.SendEmail(customer.Email + "," + customer.AltEmail,
                                      "ReqID: " + reqVendor.RequirementID + " - " + subject,
                                      bodyEMAIL, reqVendor.RequirementID, Convert.ToInt32(customer.UserID), reqVendor.Module,
                                      sessionID).ConfigureAwait(false);

                //bodySMS = String.Format(bodySMS, customer.FirstName, customer.LastName, reqVendor.RequirementID, reqVendor.Title, bookSlot.Slot_description,
                //                          Utilities.ToLocal(bookSlot.Slot_Start_Time), Utilities.ToLocal(bookSlot.Slot_End_Time), vendor.FirstName, vendor.LastName, slotAction);
                //bodySMS = bodySMS.Replace("<br/>", "");
                //Utilities.SendSMS(string.Empty, customer.PhoneNum + "," + customer.AltPhoneNum,
                //        System.Web.HttpUtility.UrlEncode(bodySMS.Split(new string[] { "Thank You" },
                //        StringSplitOptions.None)[0]), reqVendor.RequirementID, vendorID, reqVendor.Module,
                //        sessionID).ConfigureAwait(false);



                //   bodyTELEGRAM = string.Format(bodyTELEGRAM, reqVendor.RequirementID, actionTelegaram, vendor.Institution, customer.Institution, "");
                //  SendSlotTelegramAlerts(bodyTELEGRAM);


            }
            else if (bookSlot.IS_CUSTOMER) {

                ScreenName = "SLOT APPROVED";
                reqVendor.Module = ScreenName;
                if (bookSlot.Is_slot_approved == 1)
                {
                    subject = "Your slot booking is confirmed-Req ID:"+ reqID;
                    slotAction = "APPROVED";

                    bodyEMAIL = Utilities.GenerateEmailBody("ApproveSlotVendorEMAIL");
                    bodySMS = Utilities.GenerateEmailBody("ApproveSlotVendorSMS");
                    //bodyTELEGRAM = Utilities.GenerateEmailBody("BookSlotCustomerTemplateTELEGRAM");
                    actionTelegaram = "SLOT APPROVED/REJECTED";

                    bodyEMAIL = String.Format(bodyEMAIL, vendor.FirstName, vendor.LastName, reqVendor.RequirementID, reqVendor.Title, bookSlot.Slot_description,
                                              Utilities.ToLocal(bookSlot.Slot_Start_Time), Utilities.ToLocal(bookSlot.Slot_End_Time), customer.FirstName, customer.LastName, slotAction);
                    Utilities.SendEmail(vendor.Email + "," + vendor.AltEmail,
                                          "ReqID: " + reqVendor.RequirementID + " - " + subject,
                                          bodyEMAIL, reqVendor.RequirementID, vendorID, reqVendor.Module,
                                          sessionID).ConfigureAwait(false);

                    //bodySMS = String.Format(bodySMS, vendor.FirstName, vendor.LastName, reqVendor.RequirementID, reqVendor.Title, bookSlot.Slot_description,
                    //                          Utilities.ToLocal(bookSlot.Slot_Start_Time), Utilities.ToLocal(bookSlot.Slot_End_Time), customer.FirstName, customer.LastName, slotAction);
                    //bodySMS = bodySMS.Replace("<br/>", "");
                    //Utilities.SendSMS(string.Empty, vendor.PhoneNum + "," + vendor.AltPhoneNum,
                    //        System.Web.HttpUtility.UrlEncode(bodySMS.Split(new string[] { "Thank You" },
                    //        StringSplitOptions.None)[0]), reqVendor.RequirementID, vendorID, reqVendor.Module,
                    //        sessionID).ConfigureAwait(false);
                }
                else if (bookSlot.Is_slot_approved == 0)
                {
                    subject = "Your Slot Booking request is rejected Req ID:"+ reqID;
                    slotAction = "REJECTED";

                    bodyEMAIL = Utilities.GenerateEmailBody("RejectSlotVendorEMAIL");
                    bodySMS = Utilities.GenerateEmailBody("RejectSlotVendorSMS");
                    //bodyTELEGRAM = Utilities.GenerateEmailBody("BookSlotCustomerTemplateTELEGRAM");
                    actionTelegaram = "SLOT APPROVED/REJECTED";

                    bodyEMAIL = String.Format(bodyEMAIL, vendor.FirstName, vendor.LastName, reqVendor.RequirementID, reqVendor.Title, bookSlot.Slot_description,
                                              customer.FirstName, customer.LastName, slotAction);
                    Utilities.SendEmail(vendor.Email + "," + vendor.AltEmail,
                                          "ReqID: " + reqVendor.RequirementID + " - " + subject,
                                          bodyEMAIL, reqVendor.RequirementID, vendorID, reqVendor.Module,
                                          sessionID).ConfigureAwait(false);

                    //bodySMS = String.Format(bodySMS, vendor.FirstName, vendor.LastName, reqVendor.RequirementID, reqVendor.Title, bookSlot.Slot_description,
                    //                        customer.FirstName, customer.LastName, slotAction);
                    //bodySMS = bodySMS.Replace("<br/>", "");
                    //Utilities.SendSMS(string.Empty, vendor.PhoneNum + "," + vendor.AltPhoneNum,
                    //        System.Web.HttpUtility.UrlEncode(bodySMS.Split(new string[] { "Thank You" },
                    //        StringSplitOptions.None)[0]), reqVendor.RequirementID, vendorID, reqVendor.Module,
                    //        sessionID).ConfigureAwait(false);

                }



                //   bodyTELEGRAM = string.Format(bodyTELEGRAM, reqVendor.RequirementID, actionTelegaram, vendor.Institution, customer.Institution, "");

                //  SendSlotTelegramAlerts(bodyTELEGRAM);

            }

            return response;

        }
        
        public static Response SendSlotTelegramAlerts(string bodyTelegram)
        {
            Response response = new Response();
            bodyTelegram = bodyTelegram.Replace("<br/>", "");
            bodyTelegram = bodyTelegram.Replace("<br />", "");
            TelegramMsg tgMsg = new TelegramMsg();
            tgMsg.Message = bodyTelegram.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0];
            Utilities.SendTelegramMsg(tgMsg);
            return response;
        }
    }
}