﻿using Autofac;
using Autofac.Integration.Wcf;
using PRM.Core;
using PRM.Data;
using PRM.Services.Acl;
using PRM.Services.Common;
using PRM.Services.Configurations;
using PRM.Services.Logistics;
using PRM.Services.Masters.ContactDetails;
using PRM.Services.Masters.DeliveryLocations;
using PRM.Services.Masters.DeliveryTerms;
using PRM.Services.Masters.GeneralTerms;
using PRM.Services.Masters.PaymentTerms;
using PRM.Services.Messages;
using PRM.Services.RealTimePrices;
using PRM.Services.Requirements;
using PRM.Services.Vendors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRMServices.Infrastructure
{
    public static class AutofacConfiguration
    {
        private static IContainer _container;
        public static void Initialize()
        {
            var builder = new ContainerBuilder();


            builder.Register(c => c.Resolve<HttpContextBase>().Request)
     .As<HttpRequestBase>().InstancePerLifetimeScope();
            builder.Register(c => c.Resolve<HttpContextBase>().Response)
                 .As<HttpResponseBase>()
                 .InstancePerLifetimeScope();
            builder.Register(c => c.Resolve<HttpContextBase>().Server)
                 .As<HttpServerUtilityBase>()
                 .InstancePerLifetimeScope();
            builder.Register(c => c.Resolve<HttpContextBase>().Session)
                 .As<HttpSessionStateBase>()
                 .InstancePerLifetimeScope();

            builder.Register(c => new HttpContextWrapper(HttpContext.Current)).As<HttpContextBase>().InstancePerLifetimeScope();

            // Register the channel factory for the service. Make it
            // SingleInstance since you don't need a new one each time.


            builder.RegisterType<RegistrationFormService>().As<IRegistrationFormService>().InstancePerLifetimeScope();
            builder.RegisterType<RequirementFormService>().As<IRequirementFormService>().InstancePerLifetimeScope();

            builder.RegisterGeneric(typeof(DapperRepository<>)).As(typeof(IRepository<>)).InstancePerDependency();

            builder.RegisterType<DbProvider>().As<IDbProvider>().InstancePerLifetimeScope();

            builder.RegisterType<VendorService>().As<IVendorService>().InstancePerLifetimeScope();

            builder.RegisterType<EmailSender>().As<IEmailSender>().InstancePerLifetimeScope();

            builder.RegisterType<RealTimePriceWorkflowMessage>().As<IRealTimePriceWorkflowMessage>().InstancePerLifetimeScope();

            builder.RegisterType<RealTimePriceService>().As<IRealTimePriceService>().InstancePerLifetimeScope();
            builder.RegisterType<ExcelService>().As<IExcelService>().InstancePerLifetimeScope();
            builder.RegisterType<LogisticQuotationService>().As<ILogisticQuotationService>().InstancePerLifetimeScope();

            //Masters
            builder.RegisterType<DeliveryLocationService>().As<IDeliveryLocationService>().InstancePerLifetimeScope();
            builder.RegisterType<DeliveryTermService>().As<IDeliveryTermService>().InstancePerLifetimeScope();
            builder.RegisterType<PaymentTermService>().As<IPaymentTermService>().InstancePerLifetimeScope();
            builder.RegisterType<GeneralTermService>().As<IGeneralTermService>().InstancePerLifetimeScope();
            builder.RegisterType<ContactDetailService>().As<IContactDetailService>().InstancePerLifetimeScope();
            builder.RegisterType<AclMappingService>().As<IAclMappingService>().InstancePerLifetimeScope();
            builder.RegisterType<RequirementTemplateService>().As<IRequirementTemplateService>().InstancePerLifetimeScope();


            //Web Service
            builder.RegisterType<PRMSettingsService>().As<IPRMSettingsService>();

            builder.RegisterType<PRMVendorService>().As<IPRMVendorService>();
            builder.RegisterType<PRMLogisticQuotationService>().As<IPRMLogisticQuotationService>();
            builder.RegisterType<PRMRealTimePriceService>().As<IPRMRealTimePriceService>();
            builder.RegisterType<PRMMasterService>().As<IPRMMasterService>();
            builder.RegisterType<PRMRequirementService>().As<IPRMRequirementService>();


            //Infra
            builder.RegisterType<WebWorkContext>().As<IWorkContext>();

            _container = builder.Build();

            AutofacHostFactory.Container = _container;


        }

        public static IContainer Container
        {
            get
            {
                return _container;
            }
        }

    }
}