prmApp
    .config(function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
        $stateProvider

        //////////////
        //  STORE   //
        //////////////
        .state('stores', {
            url: '/stores',
            templateUrl: 'views/storeviews/stores.html'
        })
            .state('addnewstore', {
                url: '/addnewstore/:storeID',
                templateUrl: 'views/storeviews/addnewstore.html',
                params: { storeObj: null },
                params: { companyStores: null }
            })


            .state('storeitems', {
                url: '/storeitems/:storeID/:itemID',
                templateUrl: 'views/storeviews/storeitems.html',
                params: { storeObj: null },
            })
            .state('addnewstoreitem', {
                url: '/addnewstoreitem/:storeID/:itemID',
                templateUrl: 'views/storeviews/addnewstoreitem.html',
                params: { itemObj: null }
            })

            .state('storeitemproperties', {
                url: '/storeitemproperties/:compID/:itemID',
                templateUrl: 'views/storeviews/storeitemproperties.html',
                params: { itemObj: null }
            })

            .state('storeitemdetails', {
                url: '/storeitemdetails/:itemID',
                templateUrl: 'views/storeviews/storeitemdetails.html'
            })
            .state('storegin', {
                url: '/storegin/:storeID',
                templateUrl: 'views/storeviews/storegin.html',
                params: { itemsArr: null }
            })
            .state('storegrn', {
                url: '/storegrn/:storeID',
                templateUrl: 'views/storeviews/storegrn.html',
                params: { itemsArr: null }
            })
            .state('storeginhistory', {
                url: '/ginhistory/:storeID',
                templateUrl: 'views/storeviews/storeginhistory.html'
            })
            .state('storegrnhistory', {
                url: '/grnhistory/:storeID',
                templateUrl: 'views/storeviews/storegrnhistory.html'
            })
            .state('storegindetails', {
                url: '/gindetails/:storeID/:ginCode',
                templateUrl: 'views/storeviews/storegindetails.html',
                params: { ginObj: null }
            })
            .state('storegrndetails', {
                url: '/grndetails/:storeID/:grnCode',
                templateUrl: 'views/storeviews/storegrndetails.html',
                params: { grnObj: null }
            })

        
        /////////////////
        //  TECHEVAL   //
        /////////////////

        .state('managetecheval', {
            url: '/managetecheval',
            templateUrl: 'views/techevalviews/managetecheval.html'
        })
        .state('savequestionnaire', {
            url: '/savequestionnaire/:compID/:questionnaireID',
            templateUrl: 'views/techevalviews/savequestionnaire.html'
        })
        .state('techevalresponses', {
            url: '/techevalresponses/:reqID/:vendorID',
            templateUrl: 'views/techevalviews/techevalresponses.html',
            params: { techEvalObj: null }
        })

        .state('techeval', {
            url: '/techeval/:reqID/:evalID',
            templateUrl: 'views/techevalviews/techevaluation.html'
        })
    });