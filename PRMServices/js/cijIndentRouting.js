﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider

                .state('cij', {
                    url: '/cij/:cijID',
                    templateUrl: 'views/requestIndent/cij.html',
                })

                .state('replacementIndent', {
                    url: '/replacementIndent',
                    templateUrl: 'views/requestIndent/replacementIndent.html',
                })


                .state('materialsPurchaseIndent', {
                    url: '/materialsPurchaseIndent/:indentID',
                    templateUrl: 'views/requestIndent/materialsPurchaseIndent.html',
                })

                .state('cijList', {
                    url: '/cijList',
                    templateUrl: 'views/requestIndent/cijList.html',
                })

                .state('indentList', {
                    url: '/indentList',
                    templateUrl: 'views/requestIndent/indentList.html',
                })


                .state('materialPurchaseIndentView', {
                    url: '/materialPurchaseIndentView/:indentID',
                    templateUrl: 'views/requestIndent/materialPurchaseIndentView.html',
                })

                .state('cijView', {
                    url: '/cijView/:cijID',
                    templateUrl: 'views/requestIndent/cijView.html',
                })



                .state('myWorkflows', {
                    url: '/myWorkflows',
                    templateUrl: 'views/requestIndent/myWorkflows.html',
                })


                .state('myApprovals', {
                    url: '/myApprovals',
                    templateUrl: 'views/requestIndent/myApprovals.html'
                })



                .state('kimsLoginPage', {
                    url: '/kimsLoginPage',
                    templateUrl: 'views/requestIndent/kimsLoginPage.html',
                })




                .state('myIndents', {
                    url: '/myIndents',
                    templateUrl: 'views/requestIndent/myIndents.html'
                })


                .state('newCij', {
                    url: '/newCij/:cijID',
                    templateUrl: 'views/requestIndent/newCij.html'
                })

                .state('newIndent', {
                    url: '/newIndent/:indentID',
                    templateUrl: 'views/requestIndent/newIndent.html'
                })



        }]);