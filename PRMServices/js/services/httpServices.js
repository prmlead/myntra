prmApp
    .service('httpServices', ["$http", "$log", "store", function ($http, $log, store) {
        var httpServices = this;

        var _header = {
            'Content-Type': 'application/json',
        }

        if (store.get('currentUser') != undefined) {
            _header.cid = store.get('currentUser').companyId ;
            _header.uid = store.get('currentUser').userID;
        } else
        {
            _header.cid = 349;
        }


        if (store.get('sessionid') != undefined)
            _header.token = store.get('sessionid')





        httpServices.get = function (URL) {
            if (URL.indexOf('?') < 0) {
                URL = URL + '?'
            }
            var d = new Date();
            var timeToken = String(d.getFullYear()) + d.getMonth() + d.getDate() + d.getHours() + d.getMinutes() + d.getMilliseconds();



            return $http({
                method: 'GET',
                url: URL + '&token=' + timeToken,
                encodeURI: true,
                headers: _header
            }).then(function (response) {
                return response.data;
            });
        };

        httpServices.post = function (URL, params) {
            return $http({
                method: 'POST',
                url: URL,
                encodeURI: true,
                headers: _header,
                data: params
            }).then(function (response) {
                return response.data;
            });
        };

        return httpServices;
    }]);