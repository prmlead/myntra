prmApp
    .controller('dispatchTrackFormCtrl', ["$scope", "$state", "$window", "$stateParams", "userService", "auctionsService", "fileReader", "poService",
        function ($scope, $state, $window, $stateParams, userService, auctionsService, fileReader, poService) {
            $scope.reqID = $stateParams.reqID;
            $scope.poID = $stateParams.poID;
            $scope.dTID = $stateParams.dTID;

            $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;
            $scope.sessionID = userService.getUserToken();

            $scope.dispatchTrackObject = {};
            $scope.vendors = [];

            $scope.dtobject = [];

            $scope.dispatchFile = 0;
            $scope.recivedFile = 0;

            $scope.getDispatchTrack = function () {
                poService.getDispatchTrack($stateParams.poID, $stateParams.dTID)
                    .then(function (response) {
                        $scope.dtobject = response;
                        $scope.dispatchTrackObject = response[0];
                        $scope.purchaseID = $scope.dispatchTrackObject.poItemsEntity[0].purchaseID;
                        $scope.indentID = $scope.dispatchTrackObject.poItemsEntity[0].indentID;

                        $scope.dtobject.forEach(function (item, index) {
                            if (item.dispatchLink > 0) {
                                $scope.dispatchFile = item.dispatchLink;
                            }
                            if (item.dispatchLink > 0) {
                                $scope.recivedFile = item.recivedLink;
                            }
                        });

                        $scope.dispatchTrackObject.dispatchDate = new moment($scope.dispatchTrackObject.dispatchDate).format("DD-MM-YYYY");
                        if ($scope.dispatchTrackObject.dispatchDate.indexOf('-9999') > -1) {
                            $scope.dispatchTrackObject.dispatchDate = "";
                        }

                        $scope.dispatchTrackObject.recivedDate = new moment($scope.dispatchTrackObject.recivedDate).format("DD-MM-YYYY");
                        if ($scope.dispatchTrackObject.recivedDate.indexOf('-9999') > -1) {
                            $scope.dispatchTrackObject.recivedDate = "";
                        }


                    });
            }

            $scope.getDispatchTrack();

            $scope.formatDate = function (date) {
                return moment(date).format('MM/DD/YYYY');
            }

            $scope.SaveDispatchTrack = function (type) {

                $scope.dispatchTrackObject.dTID = $scope.dTID;

                $scope.dispatchTrackObject.sessionID = userService.getUserToken();

                var ts = moment($scope.dispatchTrackObject.dispatchDate, "DD-MM-YYYY").valueOf();
                var m = moment(ts);
                var deliveryDate = new Date(m);
                var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                $scope.dispatchTrackObject.dispatchDate = "/Date(" + milliseconds + "000+0530)/";

                var ts = moment($scope.dispatchTrackObject.recivedDate, "DD-MM-YYYY").valueOf();
                var m = moment(ts);
                var deliveryDate = new Date(m);
                var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                $scope.dispatchTrackObject.recivedDate = "/Date(" + milliseconds + "000+0530)/";

                if (type == 'DISPATCH') {
                    $scope.dispatchTrackObject.recivedDate = null;
                }



                var params = {
                    'dispatchtrack': $scope.dispatchTrackObject,
                    'requestType': type
                }
                poService.SaveDispatchTrack(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            swal({
                                title: "Done!",
                                text: "Saved Successfully.",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {
                                    //location.reload();
                                    $state.go('dispatchtrack', { reqID: $scope.reqID, poID: $scope.poID, vendorID: 0 })
                                });
                        }
                    })
            }


            $scope.getFile1 = function (id, doctype, ext) {
                $scope.progress = 0;
                $scope.file = $("#" + id)[0].files[0];
                $scope.docType = doctype + "." + ext;
                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {
                        if (id == "poFile") {
                            $scope.descPo.poFile = { "fileName": '', 'fileStream': null };
                            var bytearray = new Uint8Array(result);
                            $scope.descPo.poFile.fileStream = $.makeArray(bytearray);
                            $scope.descPo.poFile.fileName = $scope.file.name;
                        }

                    });
            };


            $scope.dispatchQuantityValidation = function () {
                $scope.dispatchTrackObject.poItemsEntity.forEach(function (item, index) {

                    item.dispatchErrormessage = '';
                    $scope.dispatchValidation = false;

                    if ((parseFloat(item.dispatchQuantity) + parseFloat(item.sumRecivedQuantity)) > parseFloat(item.vendorPOQuantity)) {
                        item.dispatchErrormessage = 'Dispatch Quantity Should be Less than or Equal to Po Quantity';
                        $scope.dispatchValidation = true;
                    }
                });
            }


            $scope.receiveQuantityValidation = function () {
                $scope.dispatchTrackObject.poItemsEntity.forEach(function (item, index) {

                    item.receiveErrormessage = '';
                    $scope.receivedValidation = false;

                    temprecivedQuantity = item.recivedQuantity;
                    tempreturnQuantity = item.returnQuantity;

                    if (isNaN(item.recivedQuantity) || item.recivedQuantity == undefined || item.recivedQuantity == "") {
                        item.recivedQuantity = 0;
                    }

                    if (isNaN(item.returnQuantity) || item.returnQuantity == undefined || item.returnQuantity == "") {
                        item.returnQuantity = 0;
                    }

                    if ((parseFloat(item.recivedQuantity) + parseFloat(item.returnQuantity)) > parseFloat(item.dispatchQuantity)) {
                        item.receiveErrormessage = 'Received Quantity + Return Quantity Should be Equal to Dispatch Quantity';
                        $scope.receivedValidation = true;
                    }

                    item.recivedQuantity = temprecivedQuantity;
                    item.returnQuantity = tempreturnQuantity;
                });
            }

            $scope.isExists = false;

            $scope.CheckUniqueIfExists = function (param, idtype) {

                $scope.isExists = false;

                $scope.params = {
                    param: param,
                    idtype: idtype,
                    sessionID: userService.getUserToken()
                }

                poService.CheckUniqueIfExists($scope.params)
                    .then(function (response) {
                        $scope.isExists = response;
                    });
            }


            $scope.cancelPO = function () {
                if ($scope.isCustomer) {
                    $state.go("po-list", { "reqID": $scope.reqID, "vendorID": $scope.dispatchTrackObject.poItemsEntity[0].vendorID, "poID": 0 });
                } else if (!$scope.isCustomer) {
                    $state.go("po-list", { "reqID": $scope.reqID, "vendorID": userService.getUserId(), "poID": 0 });
                }
            }


        }]);