﻿prmApp
    .controller('POListCtrl', ["$scope", "$state", "$stateParams", "$window", "userService", "growlService", "fileReader", "$log", "poService",
        function ($scope, $state, $stateParams, $window, userService, growlService, fileReader, $log, poService) {
            $scope.reqID = $stateParams.reqID;
            $scope.vendorID = $stateParams.vendorID;
            $scope.poID = $stateParams.poID;

            $scope.po = {};
            $scope.auctionVendors = [];

            $scope.sessionID = userService.getUserToken();
            $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;

            $scope.deliveryAddress = '';
            $scope.purchaseID = '';
            $scope.indentID = '';

            $scope.updateForAllItems = function () {

                $scope.po.listPOItems.forEach(function (item, index) {
                    item.expectedDeliveryDate = $scope.expectedDeliveryDate;
                    item.deliveryAddress = $scope.deliveryAddress;
                });

                //for (i = 0; i < $scope.po.listPOItems.length; i++) {
                //    $scope.po.listPOItems.expectedDeliveryDate = $scope.expectedDeliveryDate;
                //}
            }

            $scope.autoUpdateForAllItems = function () {

                $scope.po.listPOItems.forEach(function (item, index) {
                    item.purchaseID = $scope.purchaseID;
                    item.indentID = $scope.indentID;
                });

                //for (i = 0; i < $scope.po.listPOItems.length; i++) {
                //    $scope.po.listPOItems.expectedDeliveryDate = $scope.expectedDeliveryDate;
                //}
            }

            //listPOItems




            $scope.taxCalculation = function () {
                if ($scope.isCustomer == true) {
                    $scope.po.listPOItems.forEach(function (item, index) {

                        var tempRevUnitPrice = item.poPrice;
                        var tempCGst = item.cGst;
                        var tempSGst = item.sGst;
                        var tempIGst = item.iGst;

                        if (item.poPrice == undefined || item.poPrice <= 0) {
                            item.poPrice = 0;
                        };

                        item.PoTotalPrice = item.poPrice * item.vendorPOQuantity;

                        if (item.cGst == undefined || item.cGst <= 0) {
                            item.cGst = 0;
                        };
                        if (item.sGst == undefined || item.sGst <= 0) {
                            item.sGst = 0;
                        };
                        if (item.iGst == undefined || item.iGst <= 0) {
                            item.iGst = 0;
                        };

                        item.PoTotalPrice = item.PoTotalPrice + ((item.PoTotalPrice / 100) * (item.cGst + item.sGst + item.iGst));

                        item.poPrice = tempRevUnitPrice;
                        item.cGst = tempCGst
                        item.sGst = tempSGst;
                        item.iGst = tempIGst;

                        if (item.cGst < 0 || item.sGst < 0 || item.iGst < 0 || item.cGst == undefined || item.sGst == undefined || item.iGst == undefined || item.cGst > 100 || item.sGst > 100 || item.iGst > 100) {
                            $scope.gstValidation = true;
                        };
                    });
                }
                else if ($scope.isCustomer == false)
                {
                    $scope.po.listPOItems1.forEach(function (item, index) {

                        var tempRevUnitPrice = item.poPrice;
                        var tempCGst = item.cGst;
                        var tempSGst = item.sGst;
                        var tempIGst = item.iGst;

                        if (item.poPrice == undefined || item.poPrice <= 0) {
                            item.poPrice = 0;
                        };

                        item.PoTotalPrice = item.poPrice * item.vendorPOQuantity;

                        if (item.cGst == undefined || item.cGst <= 0) {
                            item.cGst = 0;
                        };
                        if (item.sGst == undefined || item.sGst <= 0) {
                            item.sGst = 0;
                        };
                        if (item.iGst == undefined || item.iGst <= 0) {
                            item.iGst = 0;
                        };

                        item.PoTotalPrice = item.PoTotalPrice + ((item.PoTotalPrice / 100) * (item.cGst + item.sGst + item.iGst));

                        item.poPrice = tempRevUnitPrice;
                        item.cGst = tempCGst
                        item.sGst = tempSGst;
                        item.iGst = tempIGst;

                        if (item.cGst < 0 || item.sGst < 0 || item.iGst < 0 || item.cGst == undefined || item.sGst == undefined || item.iGst == undefined || item.cGst > 100 || item.sGst > 100 || item.iGst > 100) {
                            $scope.gstValidation = true;
                        };
                    });
                }


            }






            $scope.GetVendorPoList = function () {
                poService.GetVendorPoList($scope.reqID, $scope.vendorID, $scope.poID)
                    .then(function (response) {
                        $scope.po = response;
                        //alert($scope.isCustomer);


                            $scope.po.listPOItems.forEach(function (item, index) {

                                $scope.purchaseID = item.purchaseID;
                                $scope.indentID = item.indentID;


                                item.expectedDeliveryDate = new moment(item.expectedDeliveryDate).format("DD-MM-YYYY");
                                if (item.expectedDeliveryDate.indexOf('-9999') > -1) {
                                    item.expectedDeliveryDate = "";
                                }
                            });

                        if ($scope.isCustomer == false)
                        {
                            $scope.po.listPOItems1 = [];
                            $scope.po.listPOItems.filter(function (item) {
                                $scope.purchaseID = item.purchaseID;
                                $scope.indentID = item.indentID;


                                item.expectedDeliveryDate = new moment(item.expectedDeliveryDate).format("DD-MM-YYYY");
                                if (item.expectedDeliveryDate.indexOf('-9999') > -1) {
                                    item.expectedDeliveryDate = "";
                                }
                                if (item.isPOToVendor == 1) {
                                    $scope.po.listPOItems1.push(item);
                                }


                            })
                        }
                        //else if ($scope.isCustomer == false) {
                        //    $scope.po.listPOItems.filter(function (item) {
                        //        if (item.isPOToVendor == 1) {
                        //        $scope.purchaseID = item.purchaseID;
                        //        $scope.indentID = item.indentID;
                        //        item.expectedDeliveryDate = new moment(item.expectedDeliveryDate).format("DD-MM-YYYY");
                        //            if (item.expectedDeliveryDate.indexOf('-9999') > -1)
                        //        {
                        //            item.expectedDeliveryDate = "";
                        //        }
                        //            $scope.po.listPOItems.push(item);
                        //        }

                        //    })
                        //}
                        
                        $scope.taxCalculation();

                    });
            }

            $scope.GetVendorPoList();

            this.UploadPO = 0;

            $scope.converToDate = function (date) {
                var fDate = new moment(date).format("DD-MM-YYYY");

                if (fDate.indexOf('-9999') > -1) {
                    fDate = "-";
                }

                return fDate;
            }


            $scope.postRequest = function () {

                $scope.po.sessionID = userService.getUserToken();


                $scope.po.listPOItems.forEach(function (item, index) {
                    var ts = moment(item.expectedDeliveryDate, "DD-MM-YYYY").valueOf();
                    var m = moment(ts);
                    var deliveryDate = new Date(m);
                    var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                    item.expectedDeliveryDate = "/Date(" + milliseconds + "000+0530)/";
                });


                var params = {
                    'vendorpo': $scope.po
                }
                poService.SaveVendorPOInfo(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            swal({
                                title: "Done!",
                                text: "Purchase Orders Generated Successfully.",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {
                                    location.reload();
                                });
                        }
                    });
            }
            $scope.goToDispatchTrack = function (poID, dTID) {
                //$state.go("reqTechSupport", { "reqId": $scope.reqId }, { reload: true, newtab: true });

                var url = $state.href("dispatchtrack", { reqID: $scope.reqID, "poID": poID, "dTID": dTID });
                window.open(url, '_self');
            }






            $scope.getFile1 = function (id, doctype, ext) {
                $scope.progress = 0;
                $scope.file = $("#" + id)[0].files[0];
                $scope.docType = doctype + "." + ext;
                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {
                        if (id == "poFile") {
                            $scope.po.poFile = { "fileName": '', 'fileStream': null };
                            var bytearray = new Uint8Array(result);
                            $scope.po.poFile.fileStream = $.makeArray(bytearray);
                            $scope.po.poFile.fileName = $scope.file.name;
                        }

                    });
            };


            $scope.goToVendorPo = function (item, poID) {
                var url = $state.href("po", { "reqID": $scope.reqID, "vendorID": $scope.vendorID, "poID": poID });
                window.open(url, '_self');
            }

            $scope.goToViewPo = function (item, poID) {
                var url = $state.href("viewpo", { "reqID": $scope.reqID, "vendorID": $scope.vendorID, "poID": poID });
                window.open(url, '_self');
            }


            $scope.goToDispatchTrackForm = function (poID, dTID,vendID) {
                //$state.go("reqTechSupport", { "reqId": $scope.reqId }, { reload: true, newtab: true });

                var url = $state.href("dtform", { reqID: $scope.reqID, "poID": poID, "dTID": dTID });
                window.open(url, '_self');
            }

            $scope.goTopaymentTrack = function (poID) {
                var url = $state.href("paymentTrack", { "reqID": $scope.reqID, "vendorID": $scope.vendorID, "poID": poID });
                window.open(url, '_self');
            }

        }]);