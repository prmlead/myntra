﻿
prmApp
    .controller('trainingsCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "$stateParams", "userService", "auctionsService", "fwdauctionsService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log", "reportingService",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, $stateParams, userService, auctionsService, fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService) {
          
            $scope.userId = userService.getUserId();
            $scope.template = $stateParams.template;
            $scope.isSuperUser = userService.getUserObj().isSuperUser;
            $scope.sessionId = userService.getUserToken();
            $scope.companyID = userService.getUserCompanyId();

        }]);