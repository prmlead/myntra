﻿
prmApp
    .controller('vendorLogisticsListCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "userService", "auctionsService",
        "fwdauctionsService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log", "reportingService", "logisticServices",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService,
            fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService, logisticServices) {

            var loginUserData = userService.getUserObj();

            $scope.isOTPVerified = loginUserData.isOTPVerified;
            $scope.isEmailOTPVerified = loginUserData.isEmailOTPVerified;

            $scope.formRequest = {};
            $scope.formRequest.isForwardBidding = false;
            $scope.myActiveLeads = [];
            $scope.currentPage = 1;
            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
            };

            $scope.maxSize = 8;
            $scope.totalLeads = 0;
            $scope.itemsPerPage = 8;
            $scope.myAuctionsLoaded = false;
            $scope.myAuctions = [];
            $scope.myActiveLeads = [];
            $scope.myAuctionsMessage = '';

            $scope.getAuctions = function () {
                logisticServices.getactiveleads({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.myActiveLeads = response;
                        if ($scope.myActiveLeads.length > 0) {
                            $scope.myAuctionsLoaded = true;
                            $scope.totalLeads = $scope.myActiveLeads.length;
                        } else {
                            $scope.totalLeads = 0;
                            $scope.myAuctionsLoaded = false;
                            $scope.myAuctionsMessage = "There are no auctions running right now for you.";
                        }
                    });
            };

            if ($scope.isOTPVerified && $scope.isEmailOTPVerified) {
                $scope.getAuctions();
            }

            $scope.goToVendorLogistic = function (id) {
                $state.go('vendorlogistic', { 'Id': id });
            };

        }]);
