prmApp

    .controller('viewprofileCtrl', ["$scope", "$http", "$state", "domain", "$filter", "$stateParams", "$timeout", "auctionsService", "userService", "SignalRFactory", "fileReader", "growlService",
        function ($scope, $http, $state, domain, $filter, $stateParams, $timeout, auctionsService, userService, SignalRFactory, fileReader, growlService) {
            $scope.userId = $stateParams.Id;
            $scope.userObj = {
                //logoURL: '../js/resources/images/chart.png'
            };
            this.userdetails = {};
            $scope.sessionid = userService.getUserToken();

            $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;

            $scope.subcategories = '';
            $scope.categories = '';


            /*getuserdetails START*/
            userService.getProfileDetails({ "userid": $scope.userId, "sessionid": userService.getUserToken() })
                .then(function (response) {
                    $scope.userObj = response;
                    //var data = response.establishedDate;
                    //var date = new Date(parseInt(data.substr(6)));
                    //$scope.userObj.establishedDate = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
                    $scope.userObj.establishedDate = userService.toLocalDateOnly(response.establishedDate);


                    for (i = 0; i < $scope.userObj.subcategories.length; i++) {

                        $scope.categories = '';

                        $scope.subcategories += $scope.userObj.subcategories[i].subcategory + "; ";

                    }

                    if ($scope.userObj.subcategories) {

                        $scope.userObj.subcategories.forEach(function (item, index) {

                            var n = false;
                            n = $scope.categories.includes(item.category + ";");
                            if (n == false) {
                                $scope.categories += item.category + "; ";
                            }
                        });
                    }


                    if ($scope.categories == '') {
                        $scope.categories = $scope.userObj.category + "; ";
                    }
                    $scope.userObj.createdOn = userService.toLocalDate($scope.userObj.createdOn);
                    $scope.userObj.aboutUs = $scope.userObj.aboutUs.replace(/\u000a/g, "<br />");
                    $scope.userObj.address = $scope.userObj.address.replace(/\u000a/g, "<br />");



                });
            /*getuserdetails END*/



            $scope.addnewdetailsobj = {
                alternateID: 0,
                altUserID: $scope.userId,
                userID: $scope.userId,
                firstName: '',
                lastName: '',
                department: '',
                designation: '',
                email: '',
                phoneNum: '',
                companyId: userService.getUserCompanyId(),
                companyName: $scope.userObj.companyName,
                isPrimary: 0,
                isValid: 1,
                sessionID: userService.getUserToken()
            }



            $scope.addNewDetails = function () {
                $scope.addnewdetailsobj.companyName = $scope.userObj.companyName;

                var params = {
                    "user": $scope.addnewdetailsobj
                };

                auctionsService.newDetails(params)
                    .then(function (response) {
                        if (response.errorMessage != "") {
                            growlService.growl(response.errorMessage, "inverse");
                        } else {
                            growlService.growl("Data Saved successfully.");
                            this.addUser = 0;
                            $state.reload();
                        }
                    });

            };

            $scope.getAlternateContacts = function () {
                auctionsService.getalternatecontacts($scope.userId, userService.getUserCompanyId(), userService.getUserToken())
                    .then(function (response) {
                        $scope.alternateContacts = response;
                    });
            }
            $scope.getAlternateContacts();

            $scope.isDelete = false;

            $scope.editAltContacts = function (param, isValid) {

                window.scrollTo(0, 150);

                //var elmnt = document.getElementById("contactInfo");
                //elmnt.scrollIntoView();


                $scope.isDelete = false;

                if (isValid == 1) {
                    $scope.isDelete = false;
                }
                else {
                    $scope.isDelete = true;
                }

                $scope.show = true;
                $scope.addnewdetailsobj.alternateID = param.alternateID;
                $scope.addnewdetailsobj.firstName = param.firstName;
                $scope.addnewdetailsobj.lastName = param.lastName;
                $scope.addnewdetailsobj.email = param.email;
                $scope.addnewdetailsobj.phoneNum = parseInt(param.phoneNum);
                $scope.addnewdetailsobj.department = param.department;
                $scope.addnewdetailsobj.designation = param.designation;
                $scope.addnewdetailsobj.isValid = isValid;
            }


            $scope.show = false;

            $scope.display = function (val) {

                $scope.addnewdetailsobj = {
                    alternateID: 0,
                    altUserID: $scope.userId,
                    userID: $scope.userId,
                    firstName: '',
                    lastName: '',
                    department: '',
                    designation: '',
                    lastName: '',
                    email: '',
                    phoneNum: '',
                    companyId: userService.getUserCompanyId(),
                    companyName: $scope.userObj.companyName,
                    isPrimary: 0,
                    isValid: 1,
                    sessionID: userService.getUserToken()
                }

                $scope.isDelete = false;
                $scope.show = val;
            }




            $scope.isGSTNumber = '';
            $scope.gstFile = 0;
            $scope.getUserCredentials = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getusercredentials?sessionid=' + userService.getUserToken() + "&userid=" + $scope.userId,
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    $scope.CredentialsResponce = response.data;
                    $scope.isGSTVerified = 0;
                    $scope.CredentialsResponce.forEach(function (item, index) {
                        if (item.fileType == 'STN') {
                            $scope.isGSTVerified = item.isVerified;
                            $scope.gstNumber = item.credentialID;
                            if (item.fileLink > 0) {
                                $scope.gstFile = item.fileLink;
                            }

                        }

                    })


                })
            };

            $scope.getUserCredentials();

            $scope.getuserinfo = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getuserinfo?sessionid=' + userService.getUserToken() + "&userid=" + $scope.userId,
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    $scope.userinfo = response.data;


                })
            };

            $scope.getuserinfo();

            $scope.onlineStatusObj = 'Not logged in';
            $scope.GetOnlineStatus = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getonlinestatus?sessionid=' + userService.getUserToken() + "&userid=" + $scope.userId,
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    $scope.onlineStatusObj = response.data;
                })
            };

            $scope.GetOnlineStatus();


            $scope.changeOnHover = true; // default test value
            $scope.maxValue = 5; // default test value
            $scope.ratingValue = 0; // default test value

            $scope.vendorProducts = [];
            $scope.GetVendorProducts = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getvendorproducts?sessionid=' + userService.getUserToken() + "&userid=" + $scope.userId,
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    $scope.vendorProducts = response.data;
                })
            };

            $scope.GetVendorProducts();

        }]);