﻿prmApp
    .controller('profileInfoCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "userService", "auctionsService", "fwdauctionsService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log", "reportingService",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService, fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService) {

            this.editPro = 0;

            $scope.logoFile = { "fileName": '', 'fileStream': "" };

            $scope.userDetails = {
                achievements: "",
                assocWithOEM: false,
                clients: "",
                establishedDate: "01-01-1970",
                aboutUs: "",
                logoFile: "",
                logoURL: "",
                products: "",
                strengths: "",
                responseTime: "",
                oemCompanyName: "",
                oemKnownSince: "",
                workingHours: "",
                files: [],
                directors: "",
                address: "",
                dateshow: 0

            };
           
            $scope.callGetUserDetails = function () {
                $log.info("IN GET USER DETAILS");
                userService.getProfileDetails({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                       $scope.userStatus = "registered";
                        if (response != undefined) {
                            $scope.userDetails = response;

                            if ($scope.userDetails.subcategories && $scope.userDetails.subcategories.length > 0) {
                                for (i = 0; i < $scope.userDetails.subcategories.length; i++) {
                                    $scope.subcategories += $scope.userDetails.subcategories[i].subcategory + ";";
                                }
                            }
                            //var data = response.establishedDate;
                            //var date = new Date(parseInt(data.substr(6)));
                            //console.log(date);
                            //$scope.userDetails.establishedDate = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
                            $scope.userDetails.establishedDate = userService.toLocalDateOnly(response.establishedDate);

                            var today = new Date();
                            var todayDate = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();
                            $scope.userDetails.dateshow = 0;
                            if ($scope.userDetails.establishedDate == todayDate) {
                                $scope.userDetails.dateshow = 1;
                            }

                            if (response.registrationScore > 89) {
                                $scope.userStatus = "Authorised";
                            }
                        }
                    });
            };

            $scope.callGetUserDetails();

            this.editMode = function () {
                this.editPro = 1;
            };

            this.UpdateUserProfileInfo = function (contact) {

                $log.info("IN UPDATE");

                var params = {};

                if ($scope.userDetails.assocWithOEM) {
                    if ($scope.userDetails.oemCompanyName == "" || $scope.userDetails.oemKnownSince == "" || $scope.userDetails.assocWithOEMFileName == "") {
                        growlService.growl("If Associated with OEM, please provide further details.", "inverse");
                        return false;
                    }
                }

                if (this.editPro == 1 && $scope.userDetails.aboutUs == "") {
                    growlService.growl("Please update your about us section", "inverse");
                    return false;
                }

                var ts = userService.toUTCTicks($scope.userDetails.establishedDate);
                var m = moment(ts);
                var auctionStartDate = new Date(m);
                var milliseconds = parseInt(auctionStartDate.getTime() / 1000.0);
                $scope.userDetails.establishedDate = "/Date(" + milliseconds + "000+0530)/";

                params = $scope.userDetails;

                if ($scope.logoFile != '' && $scope.logoFile != null) {
                    params.logoFile = $scope.logoFile;
                    params.logoURL += $scope.logoFile.fileName;
                }
                params.sessionID = userService.getUserToken();
                params.userID = userService.getUserId();
                params.errorMessage = "";
                params.subcategories = $scope.userDetails.subcategories;

                params = {
                    user: params
                }

                userService.UpdateUserProfileInfo(params)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            $scope.pwdObj = {
                                username: userService.getUserObj().username
                            };
                            $scope.callGetUserDetails();
                            swal("Done!", 'Data Saved Successfully.', 'success');
                        }
                        else {
                            $scope.callGetUserDetails();
                            swal("Error!", response.errorMessage, 'error');
                        }
                    });
                this.editPro = 0;
            };

            $scope.getFile1 = function (id, doctype, ext) {
                $scope.progress = 0;
                $scope.file = $("#" + id)[0].files[0];
                $scope.docType = doctype + "." + ext;
                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {
                        if (id == "storeLogo") {
                            var bytearray = new Uint8Array(result);
                            $scope.logoFile.fileStream = $.makeArray(bytearray);
                            $scope.logoFile.fileName = $scope.file.name;
                        }
                        if (id == "profileFile") {
                            var bytearray = new Uint8Array(result);
                            $scope.userDetails.profileFile = $.makeArray(bytearray);
                            $scope.userDetails.profileFileName = $scope.file.name;
                        }
                        else if (id == "assocWithOEMFile") {
                            var bytearray = new Uint8Array(result);
                            $scope.userDetails.assocWithOEMFile = $.makeArray(bytearray);
                            $scope.userDetails.assocWithOEMFileName = $scope.file.name;
                        }

                    });
            };
        }]);