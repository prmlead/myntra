﻿
prmApp
    .controller('profileReqsCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "userService", "auctionsService", "fwdauctionsService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log", "reportingService", "$stateParams",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService, fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService, $stateParams) {
            $scope.formRequest = {};
            $scope.formRequest.isForwardBidding = false;

            /*pagination code*/
            $scope.totalItems = 0;
            $scope.totalVendors = 0;
            $scope.totalSubuser = 0;
            $scope.totalInactiveVendors = 0;
            $scope.totalLeads = 0;
            $scope.currentPage = 1;
            $scope.currentPage2 = 1;
            $scope.itemsPerPage = 8;
            $scope.itemsPerPage2 = 8;
            $scope.maxSize = 8;
            $scope.myAuctions = [];
            $scope.categories = [];
            $scope.reqStatus = 'ALL';
            $scope.biddingType = 'ALL';
            $scope.category = 'ALL CATEGORIES';
            $scope.maxdate = moment().format('YYYY-MM-DD');
            $scope.mindate = moment().subtract(30, "days");
            $scope.dateFilter = {
                reportFromDate: '',
                reportToDate: ''
            };

            $scope.dateFilter.reportToDate = moment().format('YYYY-MM-DD');
            $scope.dateFilter.reportFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");
          
            $scope.isSuperUser = userService.getUserObj().isSuperUser;

            /* CLIENT STATUS MAPPING TO PRM STATUS */
            $scope.isCustomer = userService.getUserType();
            $scope.prmStatus = function (type, status) {
                return userService.NegotiationStatus(type, status);
            };
            /* CLIENT STATUS MAPPING TO PRM STATUS */

            // Clickable dashboard //
            $scope.NavigationFilters = $stateParams.filters;
            // Clickable dashboard //

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return userService.toLocalDate(dateBefore);
                }
            };

            $scope.pageChanged = function () {
                //$scope.getAuctions();
            };

            $scope.getAuctions = function (isfilter) {
                $log.info($scope.formRequest.isForwardBidding);
                if (!$scope.formRequest.isForwardBidding) {


                    if (isfilter) {
                        $scope.totalItems = 0;
                        $scope.currentPage = 1;
                        $scope.currentPage2 = 1;
                        $scope.itemsPerPage = 8;
                        $scope.itemsPerPage2 = 8;
                        $scope.maxSize = 8;
                        $scope.myAuctions = [];
                    }

                    auctionsService.getmyAuctions({ "userid": userService.getUserId(), "reqstatus": $scope.reqStatus, "sessionid": userService.getUserToken() })
                        .then(function (response) {
                            $scope.myAuctions1 = [];
                            //$scope.myAuctions1 = $scope.myAuctions.concat(response);
                            $scope.myAuctions1 = response;
                            $scope.myAuctions1 = $scope.myAuctions1.filter(function (auction) {
                                return (auction.biddingType !== 'TENDER');
                            });
                            $scope.myAuctions1.forEach(function (item, index) {
                                item.postedOn = $scope.GetDateconverted(item.postedOn);
                                item.quotationFreezTime = $scope.GetDateconverted(item.quotationFreezTime);
                                item.expStartTime = $scope.GetDateconverted(item.expStartTime);
                                item.startTime = $scope.GetDateconverted(item.startTime);
                                item.endTime = $scope.GetDateconverted(item.endTime);

                                if (String(item.startTime).includes('9999')) {
                                    item.startTime = '';
                                }

                                if (item.isLockReq == 1 && item.status == 'Negotiation Ended') {
                                    item.status = 'CLOSED';
                                }

                                if (item.status == 'UNCONFIRMED') {
                                    item.statusColor = 'text-warning';
                                }
                                else if (item.status == 'NOT STARTED') {
                                    item.statusColor = 'text-warning';
                                }
                                else if (item.status == 'STARTED') {
                                    item.statusColor = 'text-danger';
                                }
                                else if (item.status == 'Negotiation Ended') {
                                    item.statusColor = 'text-success';
                                }
                                else if (item.status == 'Negotiation Ended') {
                                    item.statusColor = 'text-success';
                                }
                                else {
                                    item.statusColor = '';
                                }


                                if (item.isRFP) {
                                    item.rfpStatusColor = 'orangered';
                                } else {
                                    item.rfpStatusColor = 'darkgreen';
                                }

                               

                                if (item.noOfVendorsInvited > 0 && item.noOfVendorsParticipated > 0 && item.noOfVendorsInvited == item.noOfVendorsParticipated && item.status != 'Negotiation Ended' && item.status != 'VENDOR_FINALISED' &&
                                    item.status != 'STARTED' && item.status != 'NOTSTARTED') {
                                    item.status = 'IN_REVIEW_WITH_MYNTRA';

                                }

                                if (item.status == "UNCONFIRMED" && (item.status != "NOTSTARTED" || item.status != "STARTED" || item.status != "Negotiation Ended")) {
                                    if (item.noOfVendorsInvited === 0) {
                                        item.status = "Saved_As_Draft";
                                    } 
                                }

                            })

                            $scope.myAuctions = $scope.myAuctions1;

                            //$scope.participatedVendors = $scope.auctionItem.auctionVendors.filter(function (vendor) {
                            //    return (vendor.runningPrice > 0 && vendor.companyName != 'PRICE_CAP');
                            //});

                            //if ($scope.myAuctions1 && $scope.myAuctions1.length > 0)
                            //{
                            //    $scope.totalItems = $scope.myAuctions1[0].totalCount;
                            //    for (var i = 0; i <= $scope.myAuctions1.length; i++) {
                            //        if ($scope.myAuctions1[i] && $scope.myAuctions1[i].requirementID > 0)
                            //        {
                            //            $scope.myAuctions[(($scope.currentPage - 1) * $scope.itemsPerPage) + i] = $scope.myAuctions1[i];
                            //        }

                            //    }
                            //}




                            if ($scope.myAuctions.length > 0) {
                                $scope.myAuctionsLoaded = true;
                                $scope.totalItems = $scope.myAuctions.length;
                            } else {
                                $scope.myAuctionsLoaded = false;
                                $scope.totalItems = 0;
                                $scope.myAuctionsMessage = "There are no auctions running right now for you.";
                            }



                            $scope.tempStatusFilter = angular.copy($scope.myAuctions);
                            $scope.tempCategoryFilter = angular.copy($scope.myAuctions);


                            // Clickable dashboard //

                            if ($scope.NavigationFilters && $scope.NavigationFilters.status) {
                                $scope.getStatusFilter($scope.NavigationFilters.status);
                                $scope.reqStatus = $scope.NavigationFilters.status;
                            }

                            // Clickable dashboard //

                        });
                }
                else {
                    fwdauctionsService.getmyAuctions({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                        .then(function (response) {
                            $scope.myAuctions = response;
                            if ($scope.myAuctions.length > 0) {
                                $scope.myAuctionsLoaded = true;
                                $scope.totalItems = $scope.myAuctions.length;
                            } else {
                                $scope.myAuctionsLoaded = false;
                                $scope.totalItems = 0;
                                $scope.myAuctionsMessage = "There are no auctions running right now for you.";
                            }
                        });
                }
            };

            $scope.getAuctions();

            $scope.GetRequirementsReport = function () {
                auctionsService.getrequirementsreport({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.reqReport = response;
                        alasql.fn.handleDate = function (date) {
                            return new moment(date).format("MM/DD/YYYY");
                        }
                        alasql('SELECT requirementID as RequirementID, title as RequirementTitle, handleDate(postedDate) as [PostedDate], status as Status, vendorID as VendorID, companyName as CompanyName, userPhone as Phone,userEmail as Email, itemID as [ItemID], productBrand as Brand, productQuantity as Quantity, productQuantityIn as [Units], costPrice as [InitialCostPrice], revCostPrice as [FinalCostPrice], netPrice as [InitialNetPrice], marginAmount as [InitialMarginAmount], unitDiscount as [MarginPercentage],revUnitDiscount as [RevisedMarginPercentage],unitMRP as [MRP],gst as [GST],savings as [Savings], handleDate(startTime) as [StartTime], status as [Status], othersBrands as [ManufacturerName] INTO XLSX(?,{headers:true,sheetid: "MarginTypeConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["MarginTypeConsolidatedReport.xlsx", $scope.reqReport]);
                    });

                //alasql('SELECT requirementID as [RequirementID],title as [Title],vendorID as [VendorID],companyName as [Company],userPhone as [Phone],userEmail as [Email],itemID as [ItemID],productBrand as [Brand],productQuantity as [Quantity], productQuantityIn as [Units],costPrice as [InitialCostPrice],revCostPrice as [FinalCostPrice],netPrice as [NetPrice],marginAmount as [InitialMarginAmount],unitDiscount as [InitialMargin],revUnitDiscount as [FinalMargin],unitMRP as [MRP],gst as [GST],maxInitMargin as [MaximumInitialMargin],maxFinalMargin as [MaximumFinalMargin],savings as [Savings] INTO XLSX(?,{headers:true,sheetid: "MarginTypeConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["MarginTypeConsolidatedReport.xslx", $scope.reqReport]);

            };
            
            $scope.goToReqReport = function (reqID) {
                    //$state.go("reports", { "reqID": reqID });

                    var url = $state.href('reports', { "reqID": reqID });
                    $window.open(url, '_blank');

            };
            $scope.companyUserID = userService.getUserCompanyId();
            $scope.getReport = function (name) {
                reportingService.downloadTemplate(name, $scope.companyUserID, $scope.dateFilter.reportFromDate, $scope.dateFilter.reportToDate, 0);
            }

            $scope.getSevenDaysReport = function (name) {
                var diff = (moment().diff(moment($scope.dateFilter.reportFromDate, 'YYYY-MM-DD'), 'days'));
                $scope.message = 'Download Reports will be available from ' + ' ' + $scope.dateFilter.reportFromDate + ' ' + 'to ' + ' ' + moment($scope.dateFilter.reportToDate).format("YYYY-MM-DD");
                swal({
                    title: "Are you sure?",
                    text: $scope.message,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "Yes, I am sure",
                    closeOnConfirm: true
                }, function () {
                    $scope.downloadReport();
                });
            }

            $scope.cloneRequirement = function (requirement) {
                let tempObj = {};
                let stateView = 'save-requirementAdv';
                if (requirement.biddingType && requirement.biddingType === 'TENDER') {
                    stateView = 'save-tender';
                }
                
                tempObj.cloneId = requirement.requirementID;
                $state.go(stateView, { 'Id': requirement.requirementID, reqObj: tempObj });
            };

            $scope.searchTable = function (str) {

                //$scope.category = 'ALL CATEGORIES';
                //$scope.reqStatus = 'ALL';
                //$scope.myAuctions = $scope.myAuctions1.filter(function (req) {
                //    return (String(req.requirementID).includes(str) == true || String(req.title).includes(str) == true || req.category[0].includes(str) == true );
                //});

                // RFQ Date Based Filter // 

                str = str.toLowerCase();
                $scope.category = 'ALL CATEGORIES';
                $scope.reqStatus = 'ALL';
                $scope.myAuctions = $scope.myAuctions1.filter(function (req) {
                    return (String(req.requirementID).includes(str) == true
                        || String(req.title.toLowerCase()).includes(str) == true
                        || String(req.userName.toLowerCase()).includes(str) == true
                        || String(req.postedOn).includes(str) == true
                        || req.category[0].toLowerCase().includes(str) == true);
                });

                // RFQ Date Based Filter //

                $scope.totalItems = $scope.myAuctions.length;
            };

            
            $scope.getCategories = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getcategories?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken(),
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data) {

                        if (response.data.length > 0) {
                            $scope.categories = _.uniq(_.map(response.data, 'category'));

                            $scope.categories.push('ALL CATEGORIES');

                            //categories.splice(categories.indexOf("ALL"), 1);
                            //categories.unshift('ALL');

                            $scope.categories = $scope.categories.filter(item => item !== "ALL CATEGORIES" && item !== "");
                            $scope.categories.unshift("ALL CATEGORIES");

                            //console.log($scope.categories);

                            $scope.categoriesdata = response.data;
                            $scope.showCategoryDropdown = true;
                        }
                    }
                }, function (result) {
                });

            };


            $scope.getCategories();

            $scope.getStatusFilter = function (filterVal) {
                $scope.filterArray = [];
                if (filterVal == 'ALL') {
                    $scope.myAuctions = $scope.myAuctions1;

                } else {
                    $scope.filterArray = $scope.tempStatusFilter.filter(function (item) {
                        return $scope.prmStatus($scope.isCustomer, item.status) === $scope.prmStatus($scope.isCustomer, filterVal);
                    });
                    $scope.myAuctions = $scope.filterArray;
                }

                $scope.totalItems = $scope.myAuctions.length;

            };

            $scope.getCategoryFilter = function (filterCategoryVal) {
                $scope.filterCatArray = [];
                if (filterCategoryVal == 'ALL CATEGORIES') {
                    $scope.myAuctions = $scope.myAuctions1;
                } else {
                    $scope.filterCatArray = $scope.tempCategoryFilter.filter(function (item) {
                        return item.category[0] == filterCategoryVal;
                    });
                    $scope.myAuctions = $scope.filterCatArray;

                }

                $scope.totalItems = $scope.myAuctions.length;
            };

            $scope.assignRFQ = {};
            $scope.assignedRFQS = {};
            $scope.subUsersTemp = [];
            $scope.REQ_ID = 0;

            $scope.showAccess = function (object) {
                $scope.subUsers.forEach(function (item, index) {
                    if (item.userID == object.objectID) {
                        if (object.entityID == 1) {
                            item.isValid2 = true;
                            item.isValid1 = false;
                        } else if (object.entityID == 0) {
                            item.isValid1 = true;
                            item.isValid2 = false;
                        }
                    }
                    if (item.userID != object.objectID) {
                        if (object.entityID == 1) {
                            item.isValid1 = false;
                            item.isValid2 = false;
                        } else if (object.entityID == 0) {
                            item.isValid1 = true;
                            item.isValid2 = false;
                        }
                    }
                });
            };

            $scope.selectedRFQ = function (RfqId) {
                $scope.subUsers = [];
                $scope.REQ_ID = RfqId;
                userService.getSubUsersData({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.subUsersTemp = response;
                        $scope.subUsersTemp.forEach(function (item, index) {
                            item.isValid1 = true;
                            item.isValid2 = false;
                            if (item.isValid === true) {
                                $scope.subUsers.push(item);
                            }
                        });

                        $scope.getRFQAssignUsers(RfqId);
                    });
            };

            $scope.assignRFQToUser = function (type, userId) {
                auctionsService.assignRFQToUser({ "userid": userId, "sessionid": userService.getUserToken(), "type": type, "reqid": $scope.REQ_ID, "assignedBy": userService.getUserId() })
                    .then(function (response) {
                        $scope.assignRFQ = response;
                        $scope.getRFQAssignUsers($scope.assignRFQ.roleID);
                    });
            };

            $scope.getRFQAssignUsers = function (RfqId) {
                auctionsService.getRFQAssignUsers(RfqId, userService.getUserToken(), userService.getUserId())
                    .then(function (response) {
                        $scope.assignedRFQS = response;
                        $scope.showAccess($scope.assignedRFQS);
                    });
            };

            $scope.tableColumns = [];
            $scope.downloadReport = function () {
                $scope.companyUserID = userService.getUserCompanyId();
                auctionsService.downloadReport({ "compId": +$scope.companyUserID, "userid": +userService.getUserId(), "type": $scope.isSuperUser ? 1:0, "fromdate": $scope.dateFilter.reportFromDate, "todate": $scope.dateFilter.reportToDate, "sessionId": userService.getUserToken() })
                    .then(function (response) {
                        if (response) {
                            var arr = JSON.parse(response);
                            if (arr && arr.length > 0) {
                                $scope.totalItems = arr[0].TOTAL_COUNT;
                                arr.forEach(a => delete a.TOTAL_COUNT);
                                $scope.tableColumnsTemp = angular.copy(_.keys(arr[0]));
                                $scope.tableColumnsTemp.forEach(function (item, index) {
                                    item = item.replaceAll("_", " ");
                                    var foundIndex = _.findIndex($scope.tableColumns, function (val) { return val === item; });
                                    if (foundIndex <= -1) {
                                        $scope.tableColumns.push(item);
                                    }
                                });
                                $scope.rows = arr;
                                arr.forEach(function (item, index) {
                                    var obj = angular.copy(_.values(item));
                                    if (obj) {
                                        item.tableValues = [];
                                        obj.forEach(function (value, valueIndex) {
                                            item.tableValues.push(value);
                                        });
                                    }
                                });


                                $scope.excelRows = [];
                                $scope.excelColumnsTemp = [];
                                $scope.excelColumns = [];
                                $scope.rows.forEach(function (rowItem, rowIndex) {
                                    var newObj = Object.assign({}, rowItem);
                                    delete newObj.tableValues;
                                    $scope.excelColumnsTemp.push(newObj);
                                });
                                if ($scope.excelColumnsTemp && $scope.excelColumnsTemp.length > 0) {
                                    Object.keys($scope.excelColumnsTemp[0]).forEach(function (key, index) {
                                        var value = key + ' as [' + key.replaceAll("_", " ") + ']';
                                        $scope.excelColumns.push(value);
                                    });
                                    $scope.excelColumns = $scope.excelColumns.join(',');

                                    $scope.excelColumnsTemp.forEach(function (item, index) {
                                        $scope.excelRows.push(item);
                                    });
                                }
                                execute();

                            } else {
                                swal("Status!", "No Records Found..!", "error");
                            }
                        }
                    });
            };

            function execute() {
                var mystyle = {
                    headers: true,
                    column: { style: { Font: { Color: "#FFFFFF" }, Interior: { Color: "#4F81BD", Pattern: "Solid" }, Alignment: { Horizontal: "Center" } } },
                    row: { style: { Alignment: { Horizontal: "Center" } } }
                };
                alasql('SELECT ' + $scope.excelColumns + ' INTO XLSXML("Reporting.xls",?) FROM ?', [mystyle, $scope.excelRows]);
                $scope.downloadExcel = false;
            }
           
        }]);