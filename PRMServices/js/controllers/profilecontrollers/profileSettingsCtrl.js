﻿prmApp
 .controller('profileSettingsCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "userService", "auctionsService", "fwdauctionsService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log", "reportingService",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService, fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService) {

            $scope.NegotiationSettingsValidationMessage = '';

            $scope.NegotiationSettings = [];

            $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';

            $scope.callGetUserDetails = function () {
                $log.info("IN GET USER DETAILS");
                userService.getProfileDetails({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        if (response != undefined) {
                            $scope.userDetails = response;

                            $scope.NegotiationSettings = $scope.userDetails.NegotiationSettings;
                            var duration = $scope.NegotiationSettings.negotiationDuration.split(" ", 4);
                            $scope.days = parseInt(duration[0]);
                            duration = duration[1];
                            duration = duration.split(":", 4);
                            $scope.hours = parseInt(duration[0]);
                            $scope.mins = parseInt(duration[1]);

                        }
                    });
            };

            $scope.callGetUserDetails();

            $scope.NegotiationTimeValidation = function (days, hours, mins, minReduction, rankComparision) {
                $scope.NegotiationSettingsValidationMessage = '';

                if (minReduction <= 0  || minReduction == undefined) {
                    $scope.NegotiationSettingsValidationMessage = 'Set Min 0.01 for Min.Amount reduction';
                    return;
                }

                if (rankComparision <= 0 || rankComparision == undefined) {
                    $scope.NegotiationSettingsValidationMessage = 'Set Min 0.01 for Rank Comparision price';
                    return;
                }

                if (minReduction >= 0 && rankComparision > minReduction) {
                    $scope.NegotiationSettingsValidationMessage = 'Please enter Valid Rank Comparision price less than Min. Amount reduction';
                    return;
                }
                if (days == undefined || days < 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Days';
                    return;
                }
                if (hours < 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours';
                    return;
                }
                if (mins < 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Minutes';
                    return;
                }
                if (mins >= 60 || mins == undefined) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Minutes';
                    return;
                }
                if (hours > 24 || hours == undefined) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours';
                    return;
                }
                if (hours == 24 && mins > 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours & Minutes';
                    return;
                }
                if (mins < 5 && hours == 0 && days == 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Min 5 Minutes';
                    return;
                }
            };

            $scope.saveNegotiationSettings = function () {
                $scope.NegotiationTimeValidation($scope.days, $scope.hours, $scope.mins, $scope.NegotiationSettings.minReductionAmount, $scope.NegotiationSettings.rankComparision);

                if ($scope.NegotiationSettingsValidationMessage == '') {
                    $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';
                    var params = {
                        "NegotiationSettings": {
                            "userID": userService.getUserId(),
                            "minReductionAmount": $scope.NegotiationSettings.minReductionAmount,
                            "rankComparision": $scope.NegotiationSettings.rankComparision,
                            "negotiationDuration": $scope.NegotiationSettings.negotiationDuration,
                            "sessionID": userService.getUserToken(),
                            "compInterest": $scope.NegotiationSettings.compInterest,
                        }
                    };
                    $http({
                        method: 'POST',
                        url: domain + 'savenegotiationsettings',
                        encodeURI: true,
                        headers: { 'Content-Type': 'application/json' },
                        data: params
                    }).then(function (response) {
                        if (response && response.data && (response.data.errorMessage == "" || response.data.errorMessage == null)) {
                            //$scope.NegotiationSettings = response.data;
                            growlService.growl('Successfully Saved', 'success');
                        }
                        else {
                            growlService.growl('Not Saved', 'inverse');
                        }
                    });
                }
            };

            $scope.goToCurrencyManagement = function () {
                var url = $state.href('currencyManagement', {});
                $window.open(url, '_blank');
            };

            $scope.goTocatergory = function () {            
                var url = $state.href('category', {});
                    $window.open(url, '_blank');
            };

            $scope.goToworkflow = function () {
                var url = $state.href('workflow', {});
                $window.open(url, '_blank');
            };

        }]);