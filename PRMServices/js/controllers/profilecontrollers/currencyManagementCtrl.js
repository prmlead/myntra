﻿prmApp

    .controller('currencyManagementCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "userService", "auctionsService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService, $http, domain, $rootScope, fileReader, $filter, $log) {

            $rootScope.vendorcurrencies = [];


            $scope.isCustomer = userService.getUserType();
            $scope.sessionid = userService.getUserToken();
            $scope.userID = userService.getUserId();
            $scope.userObj = {};
            $scope.selectedCategory = {};
            $scope.compID = userService.getUserCompanyId();
            $scope.addCurrency = false;
            $scope.currienciesAndRates = [];

            userService.getUserDataNoCache()
                .then(function (response) {
                    $scope.userObj = response;

                })

            $scope.currencyValues = {
                value: 0,
                rate: 0
            }

            $scope.currencyArray = [];
            $scope.currencyObj = {
                compID: 0,
                currency: '',
                factor: 0,
                startTime: '',
                endTime: ''
            }
           

            $scope.currienciesAndRates.push($scope.currencyValues);

            $scope.currencies = [];
            $scope.currenciesTemp = [];
            $scope.currenciesbyid = [];
            $scope.currenciesHistory = [];

            $scope.GetCurrencybyCompany = function () {
                auctionsService.GetCurrencybyCompany($scope.compID, $scope.sessionid)
                    .then(function (response) {
                        $scope.currenciesbyid = response;
                        $scope.currenciesbyid.forEach(function (currency) {

                            currency.startTime = userService.toLocalDate(currency.startTime);
                            currency.endTime = userService.toLocalDate(currency.endTime);

                        })


                    })
            };
            $scope.GetCurrencybyCompany();

            $scope.getKeyValuePairs = function (parameter) {
                auctionsService.getKeyValuePairs(parameter)
                    .then(function (response) {
                        if (parameter == "CURRENCY") {
                            $scope.currencyArray = [];
                            $scope.currencies = response;
                            $scope.currenciesTemp = angular.copy($scope.currencies);
                            
                        }
                        $scope.selectedCurrency = $filter('filter')($scope.currencies, { value: response.currency });
                        $scope.selectedCurrency = $scope.selectedCurrency[0];
                       // $scope.keyFunction(response);
                       
                    })
            };

           // $scope.getKeyValuePairs('CURRENCY');

            $scope.GetCurrencies = function (parameter) {
                auctionsService.GetCurrencies(parameter)
                    .then(function (response) {
                        if (parameter == "CURRENCY") {
                            $scope.currencyArray = [];
                            $scope.currencies = response;
                            $scope.currenciesTemp = angular.copy($scope.currencies);

                        }
                       

                    })
            };

            $scope.GetCurrencies('CURRENCY');
            

            $scope.keyFunction = function (currencies) {
                currencies.forEach(function (curr) {
                    $scope.keyDisplay = curr.key;
                })
            }
            
            $scope.AddCurrency = function () {

                $scope.currencyValues = {
                    value: 0,
                    rate: 0
                }

                $scope.currienciesAndRates.push($scope.currencyValues);
                
            };


            $scope.deleteCurrency = function (parentIndex, index) {
                if ($scope.formRequest.listRequirementItems[parentIndex] && $scope.formRequest.listRequirementItems[parentIndex].ExtraDimentions.length >= 1) {
                    $scope.formRequest.listRequirementItems[parentIndex].ExtraDimentions.splice(index, 1);
                };
            };

           
            $scope.currID = 0;

            $scope.CurrencyDefaultValidation = false;

            $scope.CurrencyDefault = function (currencyName) {

                if (currencyName == 'INR') {

                    $scope.currencyObj.factor = 1;
                    $scope.CurrencyDefaultValidation = true;
                   
                }
                else {
                    $scope.currencyObj.factor = 0;
                    $scope.CurrencyDefaultValidation = false;
                   
                }

            }

            //$scope.getcurrencies = function () {
            //    $scope.getKeyValuePairs('CURRENCY');
            //    $scope.currenciesTemp.forEach(function (curr) {
            //        $scope.currenciesbyid.forEach(function (ID) {
            //            if (ID.currency == curr.value) {
            //                var tempindex = $scope.currenciesTemp.indexOf(curr);
            //                if (tempindex > -1) {
            //                    $scope.currencies.splice(tempindex,1)
            //                }
                           
            //            }


            //        })
                    
            //    })
            //}
            $scope.isEditCurrency = false;
            $scope.editCurrency = function (currency) {
                $scope.currencyObj.currency = currency.currency;
                $scope.currencyObj.factor = currency.factor;
                $scope.currencyObj.startTime = currency.startTime;
                $scope.currencyObj.endTime = currency.endTime;
                $scope.isEditCurrency = true;
            }

           
            $scope.errorMessageDisplay = '';
            $scope.currencyStartValidation = $scope.currencyEndValidation = false;
            $scope.SaveCurrencyRates = function (currencyObj) {

                $scope.currencyStartValidation = $scope.currencyEndValidation = false;

                if (currencyObj.startTime == null || currencyObj.startTime == '') {
                    $scope.currencyStartValidation = true;
                    return false;
                }

                if (currencyObj.endTime == null || currencyObj.endTime == '') {
                    $scope.currencyEndValidation = true;
                    return false;
                }

                var ts = userService.toUTCTicks(currencyObj.startTime);
                var m = moment(ts);
                var validFrom = new Date(m);
                var milliseconds = parseInt(validFrom.getTime() / 1000.0);
                currencyObj.startFrom = "/Date(" + milliseconds + "000+0530)/";


                var ts = userService.toUTCTicks(currencyObj.endTime);
                var m = moment(ts);
                var validTo = new Date(m);
                var milliseconds = parseInt(validTo.getTime() / 1000.0);
                currencyObj.endFrom = "/Date(" + milliseconds + "000+0530)/";
                
                var params = {
                    "currencyName": currencyObj.currency,
                    "currencyRate": currencyObj.factor,
                    "compID": $scope.compID,
                    "startTime": currencyObj.startFrom,
                    "endTime": currencyObj.endFrom,
                    "sessionID": userService.getUserToken()
                };

                if (currencyObj.currency == null || currencyObj.currency == undefined || currencyObj.currency == ' ') {
                    growlService.growl("Please select currency", "inverse");
                    return;
                }

                if (currencyObj.factor == 0 || currencyObj.factor == undefined || currencyObj.factor == ' ') {
                    growlService.growl("Please enter factor value", "inverse");
                    return;
                }

                auctionsService.SaveCurrencyRates(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                           // growlService.growl(response.errorMessage, "inverse");
                            $scope.errorMessageDisplay = response.errorMessage;
                            console.log(response.errorMessage);
                        }
                        else {
                            growlService.growl("Saved Successfully", "success");
                            location.reload();
                        }
                    });

            };

            


        }]);