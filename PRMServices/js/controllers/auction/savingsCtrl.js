﻿prmApp
    .controller('savingsCtrl', ["$scope", "$state", "$stateParams", "userService", "auctionsService", "reportingService", function ($scope, $state, $stateParams, userService, auctionsService, reportingService) {
        $scope.id = $stateParams.Id;
        $scope.totalItemsMinPrice = 0;
        $scope.totalLeastBidderPrice = 0;
        $scope.totalInitialPrice = 0;
        $scope.sessionid = userService.getUserToken();

        $scope.getData = function () {
            auctionsService.getpricecomparison({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                .then(function (response) {
                    $scope.auctionItem = response.requirement;
                    $scope.priceCompObj = response;

                    $scope.totalItemsL1Price = 0;
                    $scope.totalItemsMinimunPrice = 0;
                    $scope.negotiationSavings = 0;
                    $scope.savingsByLeastBidder = 0;
                    $scope.savingsByItemMinPrice = 0;

                    $scope.L1CompanyName = '';
                    $scope.L1CompanyName = $scope.auctionItem.auctionVendors[0].companyName;


                    for (var i = 0; i < $scope.priceCompObj.priceCompareObject.length; i++) {
                        $scope.totalItemsL1Price += $scope.priceCompObj.priceCompareObject[i].leastBidderPrice;
                        $scope.totalItemsMinimunPrice += $scope.priceCompObj.priceCompareObject[i].minPrice;
                    }

                    $scope.negotiationSavings = $scope.auctionItem.savings;
                    $scope.savingsByLeastBidder = $scope.priceCompObj.minQuotationPrice - $scope.totalItemsL1Price;
                    $scope.savingsByItemMinPrice = $scope.priceCompObj.minQuotationPrice - $scope.totalItemsMinimunPrice;

                    $scope.additionalSavings = 0;
                    $scope.additionalSavings = $scope.totalItemsL1Price - $scope.totalItemsMinimunPrice;
                });
        };


        $scope.isReportGenerated = 0;

        $scope.GetReportsRequirement = function () {
            auctionsService.GetReportsRequirement({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                .then(function (response) {
                    $scope.reports = response;
                    $scope.getData();
                    $scope.isReportGenerated = 1;
                });
        };

        $scope.getData();

        $scope.enableExportToExcel = false;
        $scope.isValid = { selectAll: false };

        $scope.validateExcelOption = function (vendor, type, isChecked) {
            let isValid = false;
            $scope.enableExportToExcel = false;

            if (type === 'OVERALL') {

                $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                    item.isValidToExport = false;
                    if (isChecked) {
                        item.isValidToExport = true;
                    }
                });

                isValid = _.some($scope.auctionItem.auctionVendors, function (vendor) {
                    return vendor.isValidToExport;
                });

            } else {
                if (vendor && vendor.vendorID > 0) {
                    isValid = _.some($scope.auctionItem.auctionVendors, function (vendor) {
                        return vendor.isValidToExport;
                    });
                }

                if (isValid) {
                    $scope.isValid.selectAll = false;
                }
            }

            if (isValid) {
                $scope.enableExportToExcel = true;
            }
        };


        $scope.downloadReport = function () {
            let ids = _($scope.auctionItem.auctionVendors)
                .filter(item => item.isValidToExport)
                .map('vendorID')
                .value();
            reportingService.downloadTemplate("UNIT_PRICE_BIDDING_DATA" + "_" + +$stateParams.Id, userService.getUserId(), +$stateParams.Id, ids.join(','));
        };

    }]);