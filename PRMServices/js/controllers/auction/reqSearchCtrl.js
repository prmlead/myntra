﻿prmApp
    .controller('reqSearchCtrl', ["$scope", "$state", "$stateParams", "userService", "auctionsService",
        function ($scope, $state, $stateParams, userService, auctionsService) {


        $scope.CompanyLeads = {};

        $scope.searchstring = '';
        $scope.searchstype = 'TITLE';
        $scope.message = '';

        $scope.GetCompanyLeads = function () {
            $scope.message = '';
            if ($scope.searchstring.length < 3) {
                $scope.CompanyLeads = {};
                $scope.message = 'No Results';
            }
            if ($scope.searchstring.length > 2) {
                var params = { "userid": userService.getUserId(), "searchstring": $scope.searchstring, "searchtype": $scope.searchstype, "sessionid": userService.getUserToken() };
                auctionsService.GetCompanyLeads(params)
                    .then(function (response) {
                        $scope.CompanyLeads = response;

                        if (!$scope.CompanyLeads.length > 0) {
                            $scope.message = 'No Results';
                        }

                        $scope.CompanyLeads.forEach(function (item, index) {
                            item.postedOn = new moment(item.postedOn).format("DD-MM-YYYY HH:mm");
                        })
                    });
            }
        }

        $scope.GetCompanyLeads();

        if (!$scope.CompanyLeads.length > 0) {
            $scope.message = 'No Results';
        }

    }]);