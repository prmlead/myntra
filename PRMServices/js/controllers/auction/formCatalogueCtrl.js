prmApp
    // =========================================================================
    // COMMON FORMS
    // =========================================================================

    .controller('formCatalogueCtrl', ["$state", "$stateParams", "$scope", "auctionsService", "userService", "$http", "$window", "domain",
        "fileReader", "growlService", "$log", "$filter", "ngDialog", "techevalService", "fwdauctionsService", "catalogService", "prmCompanyService",
        "workflowService", "PRMPRServices", "PRMCustomFieldService", "$location", "$anchorScroll", "$timeout", "reportingService",
        function ($state, $stateParams, $scope, auctionsService, userService, $http, $window, domain,
            fileReader, growlService, $log, $filter, ngDialog, techevalService, fwdauctionsService, catalogService, prmCompanyService,
            workflowService, PRMPRServices, PRMCustomFieldService, $location, $anchorScroll, $timeout,reportingService) {
            $scope.showCatalogQuotationTemplate = true;
            $scope.userID = userService.getUserId();
            $scope.cloneId = ($stateParams.reqObj || {}).cloneId;
            $scope.isClone = ($stateParams.reqObj && $stateParams.reqObj.cloneId);
            $scope.prmFieldMappingTemplates = [];
            $scope.errorInfo = {};

            $scope.showAddProduct = true;
            $scope.enableAddProduct = true;
            $scope.AddProductRespId = 0;
            $scope.AddProductRespError = '';

            $scope.currentSystemDate = null;

            $scope.selectedTemplate = 'DEFAULT';
            $scope.prmFieldMappingDetails = {};
            $scope.subItemOptions = [];
            $scope.fabricSNo = 1;
            $scope.processSNo = 1;
            $scope.AddFabricObj = {
                //fabricSNo: $scope.fabricSNo++,
                FABRIC_ID: '',
                NAME: '',
                WEAVE_KNITS: '',
                PATTERN_TYPE: '',
                COMPOSITION: '',
                COUNT: 0,
                CONSTRUCTION: '',
                WT_GSM_GLM_OZ: 0,
                WIDTH: '',
                AW19_PRICE: 0,
                FINAL_SS20_PRICE: 0,
                CONTENT: '',
                CUTTABLE_WIDTH: '',
                SHRINKAGE: '',
                DESCRIPTION: '',
                IS_FABRIC_NOMINATED: 0,
                UOM: '',
                CONSUMPTION: 1,
                MILL: '',
                FILE_ID: '',
                FILE_NAME: '',
                FILE_IMAGE_ID: 0,
                NOMINATED_SUPPLIER: '',
                DESIGN_STYLE: '',
                TOTAL_SUB_ITEM_PRICE: 0,
                IS_NEW_OBJ: 0,
                ATTACH_ID: 0
            };
            $scope.AddFabricObjTemp = angular.copy($scope.AddFabricObj);

            $scope.AddProcessObj = {
                // processSNo: $scope.processSNo++,
                WASH_ID: '',
                PRINT_ID: '',
                EMB_ID: '',
                APP_ID: '',
                E_ID: '',
                PROCESS_CATEGORY: '',
                PROCESS_DISCRIPTION: '',
                WASH_TYPE: '',
                TYPE_OF_GARMENT: '',
                TYPE_OF_PRODUCT: '',
                COST_PER_GARMENT: 0,
                IS_PROCESS_NOMINATED: 0,
                UOM: '',
                UNIT_PRICE: 0,
                CONSUMPTION: 1,
                FILE_ID: '',
                FILE_NAME: '',
                FILE_IMAGE_ID: 0,
                NOMINATED_SUPPLIER: '',
                EMBROIDERY_NAME: '',
                APPLIQUE_NAME: '',
                EMBELLISHMENT_NAME: '',
                PROCESS_TYPE: '',
                COLORS: '',
                SIZES: '',
                COST: 0,
                PRINT_TYPE: '',
                TOTAL_SUB_ITEM_PRICE: 0,
                WASH_TYPES_PRICE: 0,
                IS_NEW_OBJ: 0,
                SelectedwashTypeArray: []

            };
            $scope.AddProcessObjTemp = $scope.AddProcessObj;

            $scope.AddAccessoriesObj = {
                // processSNo: $scope.processSNo++,
                SEWING_ID: '',
                PACK_ID: '',
                SEWING_NAME: '',
                PACKAGING_NAME: '',
                CATEGORY: '',
                POLYBAG: '',
                CARTON: 0,
                IS_SUNDRIES_NOMINATED: 0,
                UOM: '',
                UNIT_PRICE: 0,
                CONSUMPTION: 1,
                FILE_ID: '',
                FILE_NAME: '',
                FILE_IMAGE_ID: 0,
                NOMINATED_SUPPLIER: '',
                PHILOSPHY_LABLE: '',
                TYPE_OF_SUNDRIES: '',
                STYLING: '',
                ARTICLE_TYPE: '',
                BRANDS: '',
                GENDER: '',
                SEWING_COST: 0,
                PACKAGING_COST: 0,
                NOMINATED_SUPPLIER: '',
                TOTAL_SUB_ITEM_PRICE: 0,
                TYPE: 'SEWING',
                SEWING_CONSUMPTION: 1,
                PACKAGING_CONSUMPTION: 1,
                IS_NEW_OBJ: 0,
                isDisabled: true

            };


            // $scope.AddAccessoriesObj2 = angular.extend($scope.AddAccessoriesObj);
            //$scope.AddAccessoriesObj2 = JSON.parse(JSON.stringify($scope.AddAccessoriesObj));


            $scope.AddFabricArray = [];
            $scope.AddFabricArray.push($scope.AddFabricObj);

            $scope.AddProcessArray = [];
            $scope.AddProcessArray.push($scope.AddProcessObj);

            $scope.AddAccessoriesArray = [];
            $scope.AddAccessoriesArray.push($scope.AddAccessoriesObj);

            $scope.printColors = ["upto 3 colors", ">3 <6 colors"];

            $scope.printSizes3c = ['2"X2" (3C)', '5"X5"(3C)', '10"X 12"(3C)'];
            $scope.printSizes6c = ['2"X2" (6C)', '5"X5"(6C)', '10"X 12"(6C)'];
            $scope.Brand = ["DRESSBERRY", "ETHER", "HARVARD", "INVICTUS", "KOOK N KEECH", "M&H", "MR. BOWERBIRD", "ROADSTER", "Roadster high MRP", "SZTORI", "ROADSTER VISCOSE BLEND"];

            $scope.reqUOM = ["KG", "NUMBER", "METER"];
            $scope.subItemUOM = ["KG", "PIECES", "METER"];
            $scope.sunUOM = ["KG", "PIECES", "METER", "NUMBER"];

            $scope.seasons = ["SS", "AW"];
            $scope.ssMonth = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN"];
            $scope.awMonth = ["JUL", "AUG", "SEPT", "OCT", "NOV", "DEC"];

            $scope.widgetStates = {
                MIN: 0,
                MAX: 1,
                PIN: -1
            };

            if ($stateParams.Id) {
                $scope.stateParamsReqID = $stateParams.Id;
                $scope.postRequestLoding = true;
                $scope.subItemRequestLoding = true;
                $scope.showCatalogQuotationTemplate = false;
                $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
                $scope.desigID = userService.getSelectedUserDesigID();
                $scope.deptTypeID = userService.getSelectedUserDepartmentDesignation().deptTypeID;

            } else {
                $scope.stateParamsReqID = 0;
                $scope.postRequestLoding = false;
                $scope.subItemRequestLoding = false;
                $scope.showCatalogQuotationTemplate = true;
                $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
                $scope.desigID = userService.getSelectedUserDesigID();
                $scope.deptTypeID = userService.getSelectedUserDepartmentDesignation().deptTypeID;

            };
            $scope.itemPreviousPrice = {};
            $scope.itemPreviousPrice.lastPrice = -1;
            $scope.itemLastPrice = {};
            $scope.itemLastPrice.lastPrice = -1;
            $scope.bestPriceEnable = 0;
            $scope.bestLastPriceEnable = 0;
            $scope.companyItemUnits = [];
            $scope.customFieldList = [];
            $scope.selectedcustomFieldList = [];
            $scope.otherRequirementItems = {
                INSTALLATION_CHARGES: true,
                PACKAGING: true,
                FREIGHT_CHARGES: true
            };

            $scope.SelectedVendorsCount = 0;


            $scope.isTechEval = false;
            $scope.isForwardBidding = false;
            $scope.allCompanyVendors = [];
            $scope.selectedProducts = [];

            var curDate = new Date();
            var today = moment();
            var tomorrow = today.add('days', 1);
            //var dateObj = $('.datetimepicker').datetimepicker({
            //    format: 'DD/MM/YYYY',
            //    useCurrent: false,
            //    minDate: tomorrow,
            //    keepOpen: false
            //});
            $scope.subcategories = [];
            $scope.sub = {
                selectedSubcategories: [],
            }
            $scope.selectedCurrency = {};
            $scope.currencies = [];
            $scope.questionnaireList = [];

            //$scope.postRequestLoding = false;
            $scope.selectedSubcategories = [];

            $scope.companyCatalogueList = [];

            $scope.selectVendorShow = true;
            $scope.isEdit = false;
            //Input Slider
            this.nouisliderValue = 4;
            this.nouisliderFrom = 25;
            this.nouisliderTo = 80;
            this.nouisliderRed = 35;
            this.nouisliderBlue = 90;
            this.nouisliderCyan = 20;
            this.nouisliderAmber = 60;
            this.nouisliderGreen = 75;

            //Color Picker
            this.color = '#03A9F4';
            this.color2 = '#8BC34A';
            this.color3 = '#F44336';
            this.color4 = '#FFC107';

            $scope.Vendors = [];
            $scope.VendorsTemp = [];
            $scope.categories = [];
            $scope.selectedA = [];
            $scope.selectedB = [];
            $scope.showCategoryDropdown = false;
            $scope.checkVendorPhoneUniqueResult = false;
            $scope.checkVendorEmailUniqueResult = false;
            $scope.checkVendorPanUniqueResult = false;
            $scope.checkVendorTinUniqueResult = false;
            $scope.checkVendorStnUniqueResult = false;
            $scope.showFreeCreditsMsg = false;
            $scope.showNoFreeCreditsMsg = false;
            $scope.formRequest = {
                isTabular: true,
                isRFP: false,
                auctionVendors: [],
                listRequirementItems: [],
                isQuotationPriceLimit: false,
                quotationPriceLimit: 0,
                quotationFreezTime: '',
                deleteQuotations: false,
                expStartTimeName: '',
                isDiscountQuotation: 0,
                isRevUnitDiscountEnable: 0,
                multipleAttachments: [],
                contractStartTime: '',
                contractEndTime: '',
                isContract: false,
                isRFQ: 1,
                biddingType: 'REVERSE',
                deliveryTime: 'As per PO'
            };
            $scope.Vendors.city = "";
            $scope.Vendors.quotationUrl = "";
            $scope.vendorsLoaded = false;
            $scope.requirementAttachment = [];
            $scope.selectedProjectId = 0;
            $scope.selectedProject = {};
            $scope.branchProjects = [
                { PROJECT_ID: "RAW MATERIALS", PROJECT_CODE: "RAW MATERIALS" },
                { PROJECT_ID: "CAPEX", PROJECT_CODE: "CAPEX" },
                { PROJECT_ID: "SOLAR", PROJECT_CODE: "SOLAR" }
            ];
            $scope.TotalPrice = 0;
            $scope.changeProject = function () {
                console.log($scope.selectedProjectId);
                var filterdProjects = _.filter($scope.branchProjects, function (o) {
                    return o.PROJECT_ID == $scope.selectedProjectId;
                });
                if (filterdProjects && filterdProjects.length > 0) {
                    $scope.selectedProject = filterdProjects[0];
                }
            }

            $scope.sessionid = userService.getUserToken();

            $scope.selectedQuestionnaire == {}

            $scope.formRequest.indentID = 0;

            $scope.cijList = [];
            $scope.companyCurrencies = [];
            $scope.indentList = [];

            $scope.clearObjectValues = function (objToClear) {
                for (var member in objToClear) delete objToClear[member];
                return $scope.AddFabricArray;
            }

            $scope.isGetSundriesDisabled = false;

            /*region start WORKFLOW*/
            $scope.workflowList = [];
            $scope.itemWorkflow = [];
            $scope.workflowObj = {};
            $scope.formRequest.workflowID = 0;
            $scope.currentStep = 0;
            $scope.orderInfo = 0;
            $scope.assignToShow = '';
            $scope.isWorkflowCompleted = false;
            $scope.WorkflowModule = 'QUOTATION';
            $scope.disableWFSelection = false;
            /*region end WORKFLOW*/
            $scope.compID = userService.getUserCompanyId();
            $scope.GetCompanyCurrencies = function () {
                auctionsService.GetCompanyCurrencies($scope.compID, userService.getUserToken())
                    .then(function (response) {
                        $scope.companyCurrencies = response;
                        $scope.companyCurrencies.forEach(function (currency, index) {
                            currency.StartTimeLocal = new Date(parseInt(currency.startTime.substr(6)));
                            currency.EndTimeLocal = new Date(parseInt(currency.endTime.substr(6)));
                        });
                    });
            };

            $scope.GetCompanyCurrencies();

            $scope.subItemIDZ = [];

            $scope.reqData = [];
            $scope.reqDataTemp = [];
            $scope.washData = [];
            $scope.washDataTemp = [];
            $scope.printData = [];
            $scope.printDataTemp = [];

            $scope.embroideryData = [];
            $scope.embroideryDataTemp = [];

            $scope.appliqueData = [];
            $scope.appliqueDataTemp = [];

            $scope.EmbellishmentData = [];
            $scope.EmbellishmentDataTemp = [];

            $scope.fabricData = [];
            $scope.fabricDataTemp = [];
            $scope.fabricName = [];
            $scope.fabricDesignStyle = [];
            $scope.brands = [];
            $scope.article = [];
            $scope.gender = [];
            $scope.MILLSarray = [];
            $scope.SewingSundriesData = [];
            $scope.PackagingSundriesData = [];
            $scope.AddAccessoriesArrayTemp = [];
            $scope.AddAccessoriesArrayTemp2 = [];
            $scope.sundryTypes = [];
            $scope.showSundryNames = '';
            $scope.sundriesError = '';
            $scope.GetSundriesFromExcel = function (brand, gender, article, mainIdx) {

                if (brand == '' || brand == null || brand == undefined) {
                    growlService.growl("Please select Brand", "inverse");
                    return;
                } else if (gender == '' || gender == null || gender == undefined) {
                    growlService.growl("Please select Gender", "inverse");
                    return;
                } else if (article == '' || article == null || article == undefined) {
                    growlService.growl("Please select Article Type", "inverse");
                    return;
                }


                $scope.sundryTypes = [];
                $scope.showSundryNames = '';
                $scope.sundriesError = '';
                $scope.postRequestLoding = false;

                $scope.isGetSundriesDisabled = true;
                $scope.AddAccessoriesArray = [];
                $scope.AddAccessoriesArrayTemp = [];
                $scope.AddAccessoriesArrayTemp2 = [];
                $scope.SewingSundriesData = [];
                $scope.PackagingSundriesData = [];

                $scope.AddAccessoriesObjTemp = {};
                $scope.AddAccessoriesObjTemp2 = {};
                var sewingName = [];
                var packName = [];

                if ($scope.formRequest.listRequirementItems[mainIdx].AddAccessoriesArray.length > 0) {
                    $scope.formRequest.listRequirementItems[mainIdx].AddAccessoriesArray.forEach(function (sub,subIdx) {
                        $scope.clearFeildsByNotNominated(sub, subIdx, mainIdx, 'SUNDRIES', $scope.formRequest.listRequirementItems[mainIdx].cutMake,0,false);
                    })
                }

                $scope.formRequest.listRequirementItems[mainIdx].AddAccessoriesArray = [];
                $scope.formRequest.listRequirementItems.forEach(function (item, itemIdx) {
                    item.sundriesError = '';
                    if (itemIdx == mainIdx) {
                        createTempObject(itemIdx);
                        auctionsService.GetSewingSundriesFromExcel($scope.sessionid)
                            .then(function (response) {
                                // $scope.isGetSundriesDisabled = true;
                                item.isGetSundriesDisabled = true;
                                if (response) {

                                    // $scope.SewingSundriesData = [];

                                    $scope.SewingSundriesData = response;

                                    $scope.SewingSundriesData.forEach(function (sun, sunIdx) {
                                        sun.issundisabled = true;
                                        if (sun.BRANDS == brand && sun.ARTICLE_TYPE == article && sun.GENDER == gender) {
                                            sun.SEWING_CONSUMPTION = 1;
                                            $scope.AddAccessoriesArrayTemp.push(sun);

                                        }

                                    })
                                    //  $scope.clearFeildavalues($scope.AddAccessoriesObj, itemIdx);
                                    // $scope.AddAccessoriesObj = $scope.AddAccessoriesObj;
                                    $scope.AddAccessoriesArrayTemp.forEach(function (S) {
                                        if (S.TYPE == "SEWING") {

                                            sewingName.push(S.STYLING);
                                            $scope.AddAccessoriesObj.STYLING = sewingName.join(',');
                                            $scope.AddAccessoriesObj.SEWING_COST += S.SEWING_COST;
                                            $scope.AddAccessoriesObj.SEWING_ID = S.SEWING_ID;
                                            $scope.AddAccessoriesObj.ARTICLE_TYPE = S.ARTICLE_TYPE;
                                            $scope.AddAccessoriesObj.BRANDS = S.BRANDS;
                                            $scope.AddAccessoriesObj.GENDER = S.GENDER;
                                            $scope.AddAccessoriesObj.TYPE = S.TYPE;
                                            $scope.AddAccessoriesObj.IS_SUNDRIES_NOMINATED = S.IS_SUNDRIES_NOMINATED;
                                            $scope.AddAccessoriesObj.NOMINATED_SUPPLIER = S.NOMINATED_SUPPLIER;
                                            $scope.AddAccessoriesObj.TOTAL_SUB_ITEM_PRICE = S.TOTAL_SUB_ITEM_PRICE;
                                            $scope.AddAccessoriesObj.SEWING_CONSUMPTION = S.SEWING_CONSUMPTION;
                                            $scope.AddAccessoriesObj.isDisabled = true;

                                            if (S.IS_SUNDRIES_NOMINATED == 1 && S.SEWING_COST > 0) {
                                                $scope.AddAccessoriesObj.isPriceDisabled = true;
                                            } else {
                                                $scope.AddAccessoriesObj.isPriceDisabled = false;
                                            }

                                        }
                                    })
                                    if ($scope.AddAccessoriesObj.SEWING_ID) {
                                        $scope.AddAccessoriesArray.push($scope.AddAccessoriesObj);
                                    }


                                    //  $scope.formRequest.listRequirementItems[mainIdx].AddAccessoriesArray = $scope.AddAccessoriesArray;
                                    //  $scope.calculateTotalPrice($scope.formRequest.listRequirementItems[mainIdx].AddFabricArray, $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray, $scope.formRequest.listRequirementItems[mainIdx].AddAccessoriesArray, mainIdx, 'SUNDRIES');

                                    auctionsService.GetPackagingSundriesFromExcel($scope.sessionid)
                                        .then(function (response1) {
                                            //  $scope.isGetSundriesDisabled = true;
                                            item.isGetSundriesDisabled = true;
                                            $scope.PackagingSundriesData = response1;
                                            $scope.PackagingSundriesData.forEach(function (sun, sunIdx) {
                                                sun.issundisabled = true;
                                                if (sun.BRANDS == brand && sun.ARTICLE_TYPE == article && sun.GENDER == gender) {
                                                    sun.PACKAGING_CONSUMPTION = 1;
                                                    $scope.AddAccessoriesArrayTemp2.push(sun);
                                                }
                                            })


                                            $scope.AddAccessoriesObjTemp = $scope.AddAccessoriesObj2;
                                            $scope.AddAccessoriesArrayTemp2.forEach(function (S) {
                                                if (S.TYPE == "PACKAGING") {
                                                     packName.push(S.STYLING);
                                                    $scope.AddAccessoriesObjTemp.STYLING = packName.join(',');
                                                    $scope.AddAccessoriesObjTemp.PACKAGING_COST += S.PACKAGING_COST;
                                                    $scope.AddAccessoriesObjTemp.PACK_ID = S.PACK_ID;
                                                    $scope.AddAccessoriesObjTemp.ARTICLE_TYPE = S.ARTICLE_TYPE;
                                                    $scope.AddAccessoriesObjTemp.BRANDS = S.BRANDS;
                                                    $scope.AddAccessoriesObjTemp.GENDER = S.GENDER;
                                                    $scope.AddAccessoriesObjTemp.TYPE = S.TYPE;
                                                    $scope.AddAccessoriesObjTemp.IS_SUNDRIES_NOMINATED = S.IS_SUNDRIES_NOMINATED;
                                                    $scope.AddAccessoriesObjTemp.NOMINATED_SUPPLIER = S.NOMINATED_SUPPLIER;
                                                    $scope.AddAccessoriesObjTemp.TOTAL_SUB_ITEM_PRICE = S.TOTAL_SUB_ITEM_PRICE;
                                                    $scope.AddAccessoriesObjTemp.PACKAGING_CONSUMPTION = S.PACKAGING_CONSUMPTION;
                                                    $scope.AddAccessoriesObjTemp.isDisabled = true;

                                                    if (S.IS_SUNDRIES_NOMINATED == 1 && S.PACKAGING_COST > 0) {
                                                        $scope.AddAccessoriesObjTemp.isPriceDisabled = true;
                                                    } else {
                                                        $scope.AddAccessoriesObjTemp.isPriceDisabled = false;
                                                    }
                                                }
                                            })
                                            if ($scope.AddAccessoriesObjTemp.PACK_ID) {
                                                $scope.AddAccessoriesArray.push($scope.AddAccessoriesObjTemp);
                                            }


                                            //$scope.formRequest.listRequirementItems.forEach(function (item,itemIdx) {
                                            //    if (itemIdx == mainIdx) {
                                            //        item.AddAccessoriesArray = $scope.AddAccessoriesArray;

                                            //        if (item.AddAccessoriesArray.length > 0) {
                                            //            item.AddAccessoriesArray.forEach(function (sundries, sundriesIdx) {
                                            //                if (sundries.TYPE == "PACKAGING") {
                                            //                    $scope.calculateSubItemPrice('', '', sundries, sundriesIdx, mainIdx, 'PACKAGING');
                                            //                }
                                            //                if (sundries.TYPE == "SEWING") {
                                            //                    $scope.calculateSubItemPrice('', '', sundries, sundriesIdx, mainIdx, 'SEWING');
                                            //                }
                                            //            })
                                            //        }
                                            //    }
                                            //    $scope.isGetSundriesDisabled = false;
                                            //})

                                            $scope.formRequest.listRequirementItems[mainIdx].AddAccessoriesArray = $scope.AddAccessoriesArray;
                                            if ($scope.formRequest.listRequirementItems[mainIdx].AddAccessoriesArray.length > 0) {
                                                $scope.formRequest.listRequirementItems[mainIdx].AddAccessoriesArray.forEach(function (sundries, sundriesIdx) {

                                                    if (sundries.TYPE == "PACKAGING") {
                                                        $scope.sundryTypes.push(sundries.TYPE);
                                                        $scope.calculateSubItemPrice('', '', sundries, sundriesIdx, mainIdx, 'PACKAGING');
                                                    }
                                                })
                                                $scope.formRequest.listRequirementItems[mainIdx].AddAccessoriesArray.forEach(function (sundries1, sundriesIdx1) {
                                                    if (sundries1.TYPE == "SEWING") {
                                                        $scope.sundryTypes.push(sundries1.TYPE);
                                                        $scope.calculateSubItemPrice('', '', sundries1, sundriesIdx1, mainIdx, 'SEWING');
                                                    }
                                                })
                                            } else {
                                                $scope.calculateTotalPrice($scope.formRequest.listRequirementItems[mainIdx].AddFabricArray, $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray, $scope.formRequest.listRequirementItems[mainIdx].AddAccessoriesArray, mainIdx, 'SEWING', $scope.formRequest.listRequirementItems[mainIdx].cutMake);

                                                $scope.calculateTotalPrice($scope.formRequest.listRequirementItems[mainIdx].AddFabricArray, $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray, $scope.formRequest.listRequirementItems[mainIdx].AddAccessoriesArray, mainIdx, 'PACKAGING', $scope.formRequest.listRequirementItems[mainIdx].cutMake);
                                            }
                                           
                                            if ($scope.SewingSundriesData.length > 0 && $scope.PackagingSundriesData.length > 0) {
                                                // $scope.isGetSundriesDisabled = false;
                                                item.isGetSundriesDisabled = false;


                                               
                                            }
                                            var types = new Set($scope.sundryTypes);
                                            $scope.sundryTypes = Array.from(types);
                                            $scope.showSundryNames = $scope.sundryTypes.join(' and ');
                                        })

                                }

                            })
                    }
                })


            }

            // $scope.GetSundriesFromExcel();

            $scope.GetReqItemDataFromExcel = function () {
                $scope.mills = [];
                auctionsService.GetReqItemDataFromExcel($scope.sessionid)
                    .then(function (response) {

                        if (response) {
                            $scope.reqData = response;
                            $scope.reqDataTemp = response;

                            $scope.reqData.forEach(function (req) {
                                $scope.brands.push(req.BRANDS)
                                $scope.article.push(req.ARTICLE_TYPE)
                                $scope.gender.push(req.GENDER);
                            })
                            var B = new Set($scope.brands)
                            $scope.brands = Array.from(B)

                            var A = new Set($scope.article)
                            $scope.article = Array.from(A)


                            var G = new Set($scope.gender)
                            $scope.gender = Array.from(G)
                        }

                    })
            }
           // $scope.GetReqItemDataFromExcel(); 2020-01-21



            $scope.GetItemsDataFromExcel = function () {
                $scope.mills = [];
                auctionsService.GetItemsDataFromExcel(0, 0, $scope.sessionid)
                    .then(function (response) {
                        if (response) {
                            $scope.fabricData = response;
                            $scope.fabricDataTemp = angular.copy(response);

                            $scope.fabricData.forEach(function (fab) {
                                $scope.fabricName.push(fab.NAME)
                                $scope.fabricDesignStyle.push(fab.DESIGN_STYLE)
                                $scope.mills.push(fab.MILL);
                            })
                            var mySet = new Set($scope.mills)
                            $scope.MILLSarray = Array.from(mySet)
                            var mySet1 = new Set($scope.fabricName)
                            $scope.fabricName = Array.from(mySet1)
                            var DESIGN_STYLE = new Set($scope.fabricDesignStyle)
                            $scope.fabricDesignStyle = Array.from(DESIGN_STYLE)
                        }
                    });
            };

           // $scope.GetItemsDataFromExcel(); 2020-01-21

            $scope.processCategory = [];
            $scope.typeOfGarment = [];
            $scope.washTypes = [];
            $scope.GetWashDataFromExcel = function () {
                auctionsService.GetWashDataFromExcel($scope.sessionid)
                    .then(function (response) {

                        if (response) {
                            $scope.washData = response;
                            $scope.washDataTemp = response;

                            $scope.washData.forEach(function (wash) {
                                $scope.processCategory.push(wash.PROCESS_CATEGORY)
                                $scope.typeOfGarment.push(wash.TYPE_OF_GARMENT)
                            })
                            var procat = new Set($scope.processCategory)
                            $scope.processCategory = Array.from(procat)

                            var garment = new Set($scope.typeOfGarment)
                            $scope.typeOfGarment = Array.from(garment)


                        }

                    })
            }
           // $scope.GetWashDataFromExcel(); 2020-01-21
            $scope.printTypes = [];
            $scope.GetPrintDataFromExcel = function () {
                auctionsService.GetPrintDataFromExcel($scope.sessionid)
                    .then(function (response) {

                        if (response) {
                            $scope.printData = response;
                            $scope.printDataTemp = response;

                            $scope.printData.forEach(function (print) {
                                $scope.printTypes.push(print.PRINT_TYPE);
                            })
                            var PT = new Set($scope.printTypes);
                            $scope.printTypes = Array.from(PT);


                        }

                    })
            }
          //  $scope.GetPrintDataFromExcel(); 2020-01-21

            $scope.embNames = [];
            $scope.GetEmbroideryDataFromExcel = function () {
                auctionsService.GetEmbroideryDataFromExcel($scope.sessionid)
                    .then(function (response) {
                        if (response) {
                            $scope.embroideryData = response;
                            $scope.embroideryDataTemp = response;

                            $scope.embroideryData.forEach(function (emb) {
                                $scope.embNames.push(emb.EMBROIDERY_NAME);
                            })
                            var EN = new Set($scope.embNames);
                            $scope.embNames = Array.from(EN);
                        }
                    })
            }

           // $scope.GetEmbroideryDataFromExcel(); 2020 - 01 - 21

            $scope.appNames = [];
            $scope.GetAppliqueDataFromExcel = function () {
                auctionsService.GetAppliqueDataFromExcel($scope.sessionid)
                    .then(function (response) {
                        if (response) {
                            $scope.appliqueData = response;
                            $scope.appliqueDataTemp = response;

                            $scope.appliqueData.forEach(function (app) {
                                $scope.appNames.push(app.APPLIQUE_NAME);
                            })
                            var AN = new Set($scope.appNames);
                            $scope.appNames = Array.from(AN);
                        }
                    })
            }
           // $scope.GetAppliqueDataFromExcel(); 2020-01-21

            $scope.EmbellishmentNames = [];
            $scope.GetEmbellishmentData = function () {
                auctionsService.GetEmbellishmentData($scope.sessionid)
                    .then(function (response) {
                        if (response) {
                            $scope.EmbellishmentData = response;
                            $scope.EmbellishmentDataTemp = response;

                            $scope.EmbellishmentData.forEach(function (app) {
                                $scope.EmbellishmentNames.push(app.EMBELLISHMENT_NAME);
                            })
                            var AN = new Set($scope.EmbellishmentNames);
                            $scope.EmbellishmentNames = Array.from(AN);
                        }
                    })
            }
           // $scope.GetEmbellishmentData(); 2020-01-21

            $scope.populateFabricData = function (fabName, fabMill, fabStyle, fabIdx, mainIdx, isNominated) {

                $scope.fabricDataTemp = angular.copy($scope.fabricData);
                $scope.MILLSarray = [];
                $scope.fabricDesignStyle = [];
                // $scope.clearFeildavalues(item1.AddFabricArray[fabIdx]);

                $scope.formRequest.listRequirementItems.forEach(function (item1, item1Idx) {

                    if (item1Idx == mainIdx) {

                        
                        $scope.fabricDataTemp.forEach(function (fab) {

                            if (fab.NAME == fabName) {
                                $scope.MILLSarray.push(fab.MILL);
                                var mill = new Set($scope.MILLSarray)
                                $scope.MILLSarray = Array.from(mill)
                            }
                        })
                        $scope.fabricDataTemp.forEach(function (fab) {

                            if (fab.MILL == fabMill && fab.NAME == fabName) {
                                $scope.fabricDesignStyle.push(fab.DESIGN_STYLE);
                                var style = new Set($scope.fabricDesignStyle)
                                $scope.fabricDesignStyle = Array.from(style)
                            }
                        })
                        $scope.fabricDesignStyle.forEach(function (product) {
                            if ((String(product).toUpperCase().includes(fabMill) == true || String(product).toUpperCase().includes(fabStyle) == true) && ($scope.formRequest.listRequirementItems[mainIdx].AddFabricArray[fabIdx].DESIGN_STYLE || '').length > 0) {
                                $scope.formRequest.listRequirementItems[mainIdx].AddFabricArray[fabIdx].fabricError = '';
                               // $scope.postRequestLoding = false;
                            } else {
                                $scope.formRequest.listRequirementItems[mainIdx].AddFabricArray[fabIdx].fabricError = 'Please select Design/Style from dropdown only';
                               // $scope.postRequestLoding = true;
                            }
                        })
                        $scope.AddFabricArray = [];

                        $scope.fabricDataTemp.forEach(function (fab) {

                            if (fab.DESIGN_STYLE == fabStyle && fab.MILL == fabMill && fab.NAME == fabName) {

                                item1.AddFabricArray[fabIdx].FABRIC_ID = fab.FABRIC_ID;
                                fab.IS_FABRIC_NOMINATED = 1;
                                fab.CONSUMPTION = 1;
                                if ($scope.isEdit == true) {
                                    fab.IS_NEW_OBJ = 1;
                                    item1.AddFabricArray[fabIdx].IS_NEW_OBJ = 1;
                                }
                                if (fab.WT_GSM_GLM_OZ == "" || fab.WT_GSM_GLM_OZ == null || fab.WT_GSM_GLM_OZ == undefined) {
                                    fab.WT_GSM_GLM_OZ = 0;
                                }
                                if (fab.IS_FABRIC_NOMINATED == 1 && fab.FINAL_SS20_PRICE > 0) {
                                    fab.isDisabled = true;
                                } else {
                                    fab.isDisabled = false;
                                }
                                if (fab.AW19_PRICE == "0" || fab.AW19_PRICE == "" || fab.AW19_PRICE == undefined || fab.AW19_PRICE == null) {
                                    fab.AW19_PRICE = 0;
                                    fab.AW19_PRICE = parseFloat(fab.AW19_PRICE);
                                }
                                item1.AddFabricArray[fabIdx] = fab;
                            }
                        })

                        if (fabMill == '' || fabStyle == '') {

                            item1.AddFabricArray[fabIdx].FABRIC_ID = '';
                            item1.AddFabricArray[fabIdx].DESIGN_STYLE = '';
                            item1.AddFabricArray[fabIdx].WEAVE_KNITS = '';
                            item1.AddFabricArray[fabIdx].PATTERN_TYPE = '';
                            item1.AddFabricArray[fabIdx].COMPOSITION = '';
                            item1.AddFabricArray[fabIdx].COUNT = '';
                            item1.AddFabricArray[fabIdx].CONSTRUCTION = '';
                            item1.AddFabricArray[fabIdx].WT_GSM_GLM_OZ = 0;
                            item1.AddFabricArray[fabIdx].CUTTABLE_WIDTH = '';
                            item1.AddFabricArray[fabIdx].SHRINKAGE = '';
                            item1.AddFabricArray[fabIdx].DESCRIPTION = '';
                            if (isNominated == 0) {
                                item1.AddFabricArray[fabIdx].IS_FABRIC_NOMINATED = 0;
                            }
                            else {
                                item1.AddFabricArray[fabIdx].IS_FABRIC_NOMINATED = 1;
                            }
                            item1.AddFabricArray[fabIdx].NOMINATED_SUPPLIER = '';
                            item1.AddFabricArray[fabIdx].FINAL_SS20_PRICE = 0;
                            item1.AddFabricArray[fabIdx].CONSUMPTION = 1;
                            item1.AddFabricArray[fabIdx].AW19_PRICE = 0;
                        }
                        $scope.calculateSubItemPrice($scope.formRequest.listRequirementItems[mainIdx].AddFabricArray[fabIdx], '', '', fabIdx, mainIdx, 'FABRIC');

                    }

                })




            };

            $scope.printCost = 0;
            $scope.AddProcessObjResponseTemp = [];
            $scope.WTobj = {
                washType: '',
                isChecked: false
            }
            $scope.populateProcessData = function (proType, proCategory, proGarment, typeofarticle, processIdx, mainIdx) {

                if (proType == 'Wash') {
                    if (typeofarticle == '' || typeofarticle == null || typeofarticle == undefined) {
                        growlService.growl("Please select Article Type", "inverse");
                        return;
                    }
                    $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx].PROCESS_TYPE = proType;
                    $scope.typeOfGarment = [];
                    $scope.washTypes = [];
                    $scope.washdatatype = [];

                    $scope.washDataTemp.forEach(function (wash) {

                        if (wash.PROCESS_CATEGORY == proCategory) {
                            $scope.typeOfGarment.push(wash.TYPE_OF_GARMENT);
                            var tg = new Set($scope.typeOfGarment)
                            $scope.formRequest.listRequirementItems.forEach(function (item, itemIdx) {
                                if (itemIdx == mainIdx) {
                                    item.AddProcessArray.forEach(function (process, proIDX) {


                                        if (proIDX == processIdx) {
                                            process.selectedWashTypes = '';
                                            process.typeOfGarment = Array.from(tg);
                                            process.typeOfGarmentList = process.typeOfGarment.join(',');
                                        }
                                    })
                                }

                            })
                        }
                    })

                    //if (proGarment) {
                    //    $scope.washDataTemp.forEach(function (wash) {
                    //        if (wash.TYPE_OF_GARMENT == proGarment && wash.TYPE_OF_PRODUCT == typeofarticle && wash.PROCESS_CATEGORY == proCategory) {
                    //            $scope.washTypes.push(wash.WASH_TYPE);
                    //            var wt = new Set($scope.washTypes)
                    //            $scope.formRequest.listRequirementItems.forEach(function (item) {
                    //                item.AddProcessArray.forEach(function (process, proIDX) {
                    //                    //  process.washTypes = [];
                    //                    if (proIDX == processIdx) {
                    //                        process.washTypes = Array.from(wt);
                    //                        $scope.AddProcessObjResponseTemp = process.washTypes;
                    //                    }
                    //                })
                    //            })
                    //            $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx].WASH_ID = wash.WASH_ID;
                    //        }
                    //    })
                    //}

                    if (proGarment) {
                        $scope.washDataTemp.forEach(function (wash) {
                            if (wash.TYPE_OF_GARMENT == proGarment && wash.TYPE_OF_PRODUCT == typeofarticle && wash.PROCESS_CATEGORY == proCategory) {
                                $scope.washTypes.push(wash.WASH_TYPE);
                                var wt = new Set($scope.washTypes)
                                $scope.washTypes = Array.from(wt);
                                $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx].WASH_ID = wash.WASH_ID;
                            }

                        })
                        // $scope.washdatatype
                        $scope.washTypes.forEach(function (val, valIdx) {
                            if (val) {
                                $scope.WTobj = {
                                    washType: val,
                                    isChecked: false
                                }
                                $scope.washdatatype.push($scope.WTobj);
                            }
                        })
                        $scope.formRequest.listRequirementItems.forEach(function (item, itemIdx) {
                            if (itemIdx == mainIdx) {
                                item.AddProcessArray.forEach(function (process, proIDX) {
                                    //  process.washTypes = [];
                                    if (proIDX == processIdx) {
                                        //process.washTypes = Array.from(wt);
                                        process.washTypes = $scope.washdatatype;
                                        $scope.AddProcessObjResponseTemp = process.washTypes;
                                    }
                                })
                            }

                        })

                    }
                }

                if (proType == 'Print') {
                    $scope.printDataTemp.forEach(function (print) {
                        $scope.printCost = 0;
                        if (print.PRINT_TYPE == proCategory && print.COLORS == proGarment && print.SIZES == typeofarticle) {
                            $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx].COST_PER_GARMENT = 0;
                            $scope.printCost = (print.COST_PER_GARMENT);

                            $scope.formRequest.listRequirementItems.forEach(function (item, itemIdx) {
                                if (mainIdx == itemIdx) {
                                    if (item.isNonCoreProductCategory != 1) {
                                        item.AddProcessArray.forEach(function (process, proIDX) {
                                            process.printCost = 0;
                                            process.printCost = print.COST_PER_GARMENT;
                                            if (proIDX == processIdx) {

                                                if (process.IS_PROCESS_NOMINATED == 1 && print.COST_PER_GARMENT > 0) {
                                                    process.isDisabled = true;
                                                } else {
                                                    process.isDisabled = false;
                                                }

                                                $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx].PRINT_ID = print.PRINT_ID;
                                                $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx].COST_PER_GARMENT = process.printCost;
                                                $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx] = process;
                                            }
                                        })
                                    }
                                }

                            })
                            $scope.calculateSubItemPrice($scope.formRequest.listRequirementItems[mainIdx].AddFabricArray[processIdx], $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx], $scope.AddAccessoriesArray, processIdx, mainIdx, 'Print');
                            // $scope.calculateTotalPrice($scope.formRequest.listRequirementItems[mainIdx].AddFabricArray, $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray, $scope.AddAccessoriesArray, mainIdx,'PRINT');
                        }
                    })
                }

                if (proType == 'Embroidery') {
                    $scope.embroideryDataTemp.forEach(function (emb) {
                        $scope.embCost = 0;
                        if (emb.EMBROIDERY_NAME == proCategory) {
                            $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx].COST_PER_GARMENT = 0;
                            $scope.embCost = (emb.COST_PER_GARMENT);
                            $scope.formRequest.listRequirementItems.forEach(function (item, itemIdx) {
                                if (mainIdx == itemIdx) {
                                    if (item.isNonCoreProductCategory != 1) {
                                        item.AddProcessArray.forEach(function (process, proIDX) {
                                            process.embCost = 0;
                                            process.embCost = emb.COST_PER_GARMENT;
                                            if (proIDX == processIdx) {

                                                if (process.IS_PROCESS_NOMINATED == 1 && emb.COST_PER_GARMENT > 0) {
                                                    process.isDisabled = true;
                                                } else {
                                                    process.isDisabled = false;
                                                }

                                                $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx].EMB_ID = emb.EMB_ID;
                                                $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx].COST_PER_GARMENT = process.embCost;
                                                $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx] = process;
                                            }
                                        })
                                    }
                                }
                            })
                            $scope.calculateSubItemPrice($scope.formRequest.listRequirementItems[mainIdx].AddFabricArray[processIdx], $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx], $scope.AddAccessoriesArray, processIdx, mainIdx, 'Embroidery');
                        }
                    })
                }

                if (proType == 'Applique') {
                    $scope.appliqueDataTemp.forEach(function (app) {
                        $scope.appCost = 0;
                        if (app.APPLIQUE_NAME == proCategory) {
                            $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx].COST_PER_GARMENT = 0;
                            $scope.appCost = (app.COST_PER_GARMENT);
                            $scope.formRequest.listRequirementItems.forEach(function (item, itemIdx) {
                                if (mainIdx == itemIdx) {
                                    if (item.isNonCoreProductCategory != 1) {
                                        item.AddProcessArray.forEach(function (process, proIDX) {
                                            process.appCost = 0;
                                            process.appCost = app.COST_PER_GARMENT;
                                            if (proIDX == processIdx) {

                                                if (process.IS_PROCESS_NOMINATED == 1 && app.COST_PER_GARMENT > 0) {
                                                    process.isDisabled = true;
                                                } else {
                                                    process.isDisabled = false;
                                                }
                                                $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx].APP_ID = app.APP_ID;
                                                $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx].COST_PER_GARMENT = process.appCost;
                                                $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx] = process;
                                            }
                                        })
                                    }
                                }
                            })
                            $scope.calculateSubItemPrice($scope.formRequest.listRequirementItems[mainIdx].AddFabricArray[processIdx], $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx], $scope.AddAccessoriesArray, processIdx, mainIdx, 'Applique');
                        }
                    })
                }

                if (proType == 'Embellishment') {
                    $scope.EmbellishmentDataTemp.forEach(function (emb) {
                        $scope.embCost = 0;
                        if (emb.EMBELLISHMENT_NAME == proCategory) {
                            $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx].COST_PER_GARMENT = 0;
                            $scope.embCost = (emb.COST_PER_GARMENT);
                            $scope.formRequest.listRequirementItems.forEach(function (item, itemIdx) {
                                if (mainIdx == itemIdx) {
                                    if (item.isNonCoreProductCategory != 1) {
                                        item.AddProcessArray.forEach(function (process, proIDX) {
                                            process.embCost = 0;
                                            process.embCost = emb.COST_PER_GARMENT;
                                            if (proIDX == processIdx) {
                                                if (process.IS_PROCESS_NOMINATED == 1 && emb.COST_PER_GARMENT > 0) {
                                                    process.isDisabled = true;
                                                } else {
                                                    process.isDisabled = false;
                                                }

                                                $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx].E_ID = emb.E_ID;
                                                $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx].COST_PER_GARMENT = process.embCost;
                                                $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx] = process;
                                            }
                                        })
                                    }
                                }
                            })
                            $scope.calculateSubItemPrice($scope.formRequest.listRequirementItems[mainIdx].AddFabricArray[processIdx], $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx], $scope.AddAccessoriesArray, processIdx, mainIdx, 'Embellishment');
                        }
                    })
                }
            }


            $scope.priceWashArray = [];
            $scope.priceWashObj = {
                price: 0,
                index: -1,
                mainIdx: -1
            }
            // $scope.priceWashArray.push($scope.priceWashObj);
            $scope.TotalPricewash = 0;
            $scope.TotalPricewashTemp = 0;
            $scope.mainID = 0;
            $scope.subID = 0;
            $scope.fillValue = function (processObj, wash, isChecked, pc, tg, at, processIdx, mainIdx) {
                $scope.priceWashArray = [];
                if (isChecked) {
                    // $scope.SelectedwashTypeArray.push(wash);
                    $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx].SelectedwashTypeArray.push(wash);
                    $scope.washDataTemp.forEach(function (WT, WTIdx) {
                        if (pc == WT.PROCESS_CATEGORY && tg == WT.TYPE_OF_GARMENT && wash == WT.WASH_TYPE && at == WT.TYPE_OF_PRODUCT) {
                            processObj.WASH_TYPES_PRICE += WT.COST_PER_GARMENT;
                            // $scope.TotalPricewash += WT.COST_PER_GARMENT;
                            $scope.priceWashObj.price = $scope.TotalPricewash;
                            $scope.priceWashObj.index = processIdx;



                            $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx].WASH_TYPES_PRICE = processObj.WASH_TYPES_PRICE;
                            $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx].COST_PER_GARMENT = processObj.WASH_TYPES_PRICE;
                        }
                    })
                    processObj.washTypes.forEach(function (obj, objIdx) {
                        if (obj.isChecked == true) {
                            $scope.priceWashArray.push(obj.washType);
                        }
                    })
                    var arr = new Set($scope.priceWashArray)
                    $scope.priceWashArray = Array.from(arr);
                    $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx].selectedWashTypes = $scope.priceWashArray.join(' , ');
                    if ($scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx].selectedWashTypes.length > 0) {
                        $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx].washtTypeError = '';
                        $scope.searchKeyword = '';
                    }
                    $scope.calculateSubItemPrice($scope.formRequest.listRequirementItems[mainIdx].AddFabricArray[processIdx], $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx], $scope.AddAccessoriesArray, processIdx, mainIdx, 'Wash');

                } else {
                    $scope.priceWashArray = $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx].SelectedwashTypeArray;
                    var tempindex = $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx].SelectedwashTypeArray.indexOf(wash);
                    if (tempindex > -1) {
                        $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx].SelectedwashTypeArray.splice(tempindex, 1);
                        $scope.priceWashArray = $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx].SelectedwashTypeArray;
                        // $scope.priceWashArray .splice(tempindex, 1);
                        $scope.washDataTemp.forEach(function (WT, WTIdx) {
                            if (pc == WT.PROCESS_CATEGORY && tg == WT.TYPE_OF_GARMENT && wash == WT.WASH_TYPE && at == WT.TYPE_OF_PRODUCT) {
                                $scope.TotalPricewashTemp = processObj.WASH_TYPES_PRICE - WT.COST_PER_GARMENT;
                                processObj.WASH_TYPES_PRICE = $scope.TotalPricewashTemp;
                                $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx].WASH_TYPES_PRICE = processObj.WASH_TYPES_PRICE;
                                $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx].COST_PER_GARMENT = processObj.WASH_TYPES_PRICE;
                                $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx].TOTAL_SUB_ITEM_PRICE = processObj.WASH_TYPES_PRICE;
                            }
                        })
                        if ($scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx].SelectedwashTypeArray.length == 0) {
                            processObj.WASH_TYPES_PRICE = 0;
                            $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx].TOTAL_SUB_ITEM_PRICE = processObj.WASH_TYPES_PRICE;
                        }
                        $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx].selectedWashTypes = $scope.priceWashArray.join(' , ');
                    }
                    $scope.calculateSubItemPrice($scope.formRequest.listRequirementItems[mainIdx].AddFabricArray[processIdx], $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[processIdx], $scope.AddAccessoriesArray, processIdx, mainIdx, 'Wash');
                }
                // $scope.calculateTotalPrice($scope.formRequest.listRequirementItems[mainIdx].AddFabricArray, $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray, $scope.AddAccessoriesArray, mainIdx, 'WASH');
            }






            //prmCompanyService.getBranchProjects({ compid: $scope.compID, branchid: userService.getUserSelectedBranch(), projectid: 0, sessionid: userService.getUserToken() })
            //    .then(function (response) {
            //        $scope.branchProjects = response;
            //    });

            var subItemsArray = [];

            $scope.AddFabricArrayTemp = [];
            $scope.addFabric = function (requirementItemIndex) {
                $scope.AddFabricObj = {
                    FABRIC_ID: '', NAME: '', WEAVE_KNITS: '', PATTERN_TYPE: '', COMPOSITION: '',
                    COUNT: 0, CONSTRUCTION: '', WT_GSM_GLM_OZ: 0, WIDTH: '', AW19_PRICE: 0,
                    FINAL_SS20_PRICE: 0, CONTENT: '', CUTTABLE_WIDTH: '', SHRINKAGE: '', DESCRIPTION: '',
                    IS_FABRIC_NOMINATED: 1, UOM: '', CONSUMPTION: 1, MILL: '', FILE_ID: '', FILE_NAME: '', FILE_IMAGE_ID: 0,
                    NOMINATED_SUPPLIER: '', DESIGN_STYLE: '', TOTAL_SUB_ITEM_PRICE: 0, IS_NEW_OBJ: 0
                };
                if ($scope.isEdit == true) {
                    $scope.AddFabricObj.IS_NEW_OBJ = 1;
                }
                // $scope.clearFeildavalues();
                $scope.formRequest.listRequirementItems[requirementItemIndex].AddFabricArray.push($scope.AddFabricObj);


            }

            //$scope.addFabric = function (requirementItemIndex) {
            //    var emptyFabricObject = Object.assign({}, $scope.AddFabricObj);
            //    $scope.formRequest.listRequirementItems[requirementItemIndex].AddFabricArray.push(emptyFabricObject);
            //    $scope.clearObjectValues(emptyFabricObject);
            //}


            $scope.AddProcessArrayTemp = [];
            $scope.addProcess = function (requirementItemIndex) {
                $scope.AddProcessObj = {
                    WASH_ID: '', PRINT_ID: '', EMB_ID: '', APP_ID: '',E_ID: '', PROCESS_CATEGORY: '', PROCESS_DISCRIPTION: '', WASH_TYPE: '', TYPE_OF_GARMENT: '',
                    TYPE_OF_PRODUCT: '', COST_PER_GARMENT: 0, IS_PROCESS_NOMINATED: 1, UOM: '', UNIT_PRICE: 0,
                    CONSUMPTION: 1, FILE_ID: '', FILE_NAME: '', FILE_IMAGE_ID: 0, NOMINATED_SUPPLIER: '', EMBROIDERY_NAME: '', APPLIQUE_NAME: '', EMBELLISHMENT_NAME: '', PROCESS_TYPE: '', COLORS: '',
                    SIZES: '', COST: 0, PRINT_TYPE: '', TOTAL_SUB_ITEM_PRICE: 0, WASH_TYPES_PRICE: 0, IS_NEW_OBJ: 0,
                    SelectedwashTypeArray: []
                };
                if ($scope.isEdit == true) {
                    $scope.AddProcessObj.IS_NEW_OBJ = 1;
                }
                $scope.formRequest.listRequirementItems[requirementItemIndex].AddProcessArray.push($scope.AddProcessObj);
            }


            $scope.AddAccessoriesArrayTemp = [];
            $scope.addAccessories = function (requirementItemIndex, type) {
                $scope.type = '';
                if (type == 'SEWING') {
                    $scope.type = 'SEWING';
                } else if (type == 'PACKAGING') {
                    $scope.type = 'PACKAGING';
                }
                $scope.AddAccessoriesObj = {
                    SEWING_ID: '', PACK_ID: '', SEWING_NAME: '', PACKAGING_NAME: '', CATEGORY: '',
                    POLYBAG: '', CARTON: 0, IS_SUNDRIES_NOMINATED: 0, UOM: '', UNIT_PRICE: 0,
                    CONSUMPTION: 1, FILE_ID: '', FILE_NAME: '', FILE_IMAGE_ID: 0, NOMINATED_SUPPLIER: '', PHILOSPHY_LABLE: '', TYPE_OF_SUNDRIES: '',
                    STYLING: '', ARTICLE_TYPE: '', BRANDS: '', GENDER: '', SEWING_COST: 0,
                    PACKAGING_COST: 0, NOMINATED_SUPPLIER: '', TOTAL_SUB_ITEM_PRICE: 0, TYPE: $scope.type, SEWING_CONSUMPTION: 1, PACKAGING_CONSUMPTION: 1, IS_NEW_OBJ: 0
                };
                if ($scope.isEdit == true) {
                    $scope.AddAccessoriesObj.IS_NEW_OBJ = 1;
                }
                $scope.AddAccessoriesObj.BRANDS = $scope.formRequest.listRequirementItems[requirementItemIndex].BRANDS;
                $scope.AddAccessoriesObj.GENDER = $scope.formRequest.listRequirementItems[requirementItemIndex].GENDER;
                $scope.AddAccessoriesObj.isDisabled = true;


                $scope.formRequest.listRequirementItems[requirementItemIndex].AddAccessoriesArray.push($scope.AddAccessoriesObj);

                $scope.sundryType = '';
            }

            $scope.changeSundries = function (product, mainIDX) {
                if (product.ARTICLE_TYPE_TEMP == '' || product.GENDER_TEMP == '' || product.BRANDS_TEMP == '') {
                    $scope.formRequest.listRequirementItems[mainIDX].ARTICLE_TYPE_TEMP = product.ARTICLE_TYPE;
                    $scope.formRequest.listRequirementItems[mainIDX].GENDER_TEMP = product.GENDER;
                    $scope.formRequest.listRequirementItems[mainIDX].BRANDS_TEMP = product.BRANDS;
                }
                if (!($scope.formRequest.listRequirementItems[mainIDX].ARTICLE_TYPE_TEMP == '') && ($scope.formRequest.listRequirementItems[mainIDX].AddAccessoriesArray.length >= 1 || $scope.formRequest.listRequirementItems[mainIDX].AddAccessoriesArray.length == 0)) {
                    if (!($scope.formRequest.listRequirementItems[mainIDX].ARTICLE_TYPE_TEMP == $scope.formRequest.listRequirementItems[mainIDX].ARTICLE_TYPE)) {
                        $scope.formRequest.listRequirementItems[mainIDX].sundriesError = "Article is Revised: Refresh Sundries and Wash types accordingly for item " + (mainIDX + 1);
                        $scope.postRequestLoding = true;//Article is Revised: Refresh Sundries and Wash types accordingly.
                        $scope.formRequest.listRequirementItems[mainIDX].AddAccessoriesArray.forEach(function (sundries, sundriesIdx) {
                            if (sundries.TYPE == "PACKAGING") {
                                sundries.TOTAL_SUB_ITEM_PRICE = 0;
                                sundries.PACKAGING_COST = 0;
                                $scope.calculateSubItemPrice('', '', sundries, sundriesIdx, mainIDX, 'PACKAGING');
                            }
                        })
                        $scope.formRequest.listRequirementItems[mainIDX].AddAccessoriesArray.forEach(function (sundries1, sundriesIdx1) {
                            if (sundries1.TYPE == "SEWING") {
                                sundries1.TOTAL_SUB_ITEM_PRICE = 0;
                                sundries1.SEWING_COST = 0;
                                $scope.calculateSubItemPrice('', '', sundries1, sundriesIdx1, mainIDX, 'SEWING');
                            }
                        })

                        $scope.formRequest.listRequirementItems[mainIDX].AddProcessArray.forEach(function (pro, proIdx) {
                            if (pro.PROCESS_TYPE == 'Wash') {
                                $scope.clearFeildsByNotNominated(pro, proIdx, mainIDX, 'PROCESS', $scope.formRequest.listRequirementItems[mainIDX].cutMake, 1)
                            }
                        })


                        $scope.formRequest.listRequirementItems[mainIDX].AddAccessoriesArray = [];
                        $scope.calculateTotalPrice($scope.formRequest.listRequirementItems[mainIDX].AddFabricArray, $scope.formRequest.listRequirementItems[mainIDX].AddProcessArray, $scope.formRequest.listRequirementItems[mainIDX].AddAccessoriesArray, mainIDX, 'SEWING', $scope.formRequest.listRequirementItems[mainIDX].cutMake);
                        $scope.calculateTotalPrice($scope.formRequest.listRequirementItems[mainIDX].AddFabricArray, $scope.formRequest.listRequirementItems[mainIDX].AddProcessArray, $scope.formRequest.listRequirementItems[mainIDX].AddAccessoriesArray, mainIDX, 'PACKAGING', $scope.formRequest.listRequirementItems[mainIDX].cutMake);

                    }
                    $scope.formRequest.listRequirementItems[mainIDX].ARTICLE_TYPE_TEMP = product.ARTICLE_TYPE;
                }
                if (!($scope.formRequest.listRequirementItems[mainIDX].GENDER_TEMP == '') && ($scope.formRequest.listRequirementItems[mainIDX].AddAccessoriesArray.length >= 1 || $scope.formRequest.listRequirementItems[mainIDX].AddAccessoriesArray.length == 0)) {
                    if (!($scope.formRequest.listRequirementItems[mainIDX].GENDER_TEMP == $scope.formRequest.listRequirementItems[mainIDX].GENDER)) {
                        $scope.formRequest.listRequirementItems[mainIDX].sundriesError = "Please click on Get Sundries as you changed GENDER for item";
                        $scope.postRequestLoding = true;
                        $scope.formRequest.listRequirementItems[mainIDX].AddAccessoriesArray.forEach(function (sundries, sundriesIdx) {
                            if (sundries.TYPE == "PACKAGING") {
                                sundries.TOTAL_SUB_ITEM_PRICE = 0;
                                sundries.PACKAGING_COST = 0;

                                $scope.calculateSubItemPrice('', '', sundries, sundriesIdx, mainIDX, 'PACKAGING');
                            }
                        })
                        $scope.formRequest.listRequirementItems[mainIDX].AddAccessoriesArray.forEach(function (sundries1, sundriesIdx1) {
                            if (sundries1.TYPE == "SEWING") {
                                sundries1.TOTAL_SUB_ITEM_PRICE = 0;
                                sundries1.SEWING_COST = 0;
                                $scope.calculateSubItemPrice('', '', sundries1, sundriesIdx1, mainIDX, 'SEWING');
                            }
                        })
                        $scope.formRequest.listRequirementItems[mainIDX].AddAccessoriesArray = [];
                        $scope.calculateTotalPrice($scope.formRequest.listRequirementItems[mainIDX].AddFabricArray, $scope.formRequest.listRequirementItems[mainIDX].AddProcessArray, $scope.formRequest.listRequirementItems[mainIDX].AddAccessoriesArray, mainIDX, 'SEWING', $scope.formRequest.listRequirementItems[mainIDX].cutMake);
                        $scope.calculateTotalPrice($scope.formRequest.listRequirementItems[mainIDX].AddFabricArray, $scope.formRequest.listRequirementItems[mainIDX].AddProcessArray, $scope.formRequest.listRequirementItems[mainIDX].AddAccessoriesArray, mainIDX, 'PACKAGING', $scope.formRequest.listRequirementItems[mainIDX].cutMake);
                    }
                    $scope.formRequest.listRequirementItems[mainIDX].GENDER_TEMP = product.GENDER;
                }
                if ((!($scope.formRequest.listRequirementItems[mainIDX].BRANDS_TEMP == '')) && ($scope.formRequest.listRequirementItems[mainIDX].AddAccessoriesArray.length >= 1 || $scope.formRequest.listRequirementItems[mainIDX].AddAccessoriesArray.length == 0)) {
                    if (!($scope.formRequest.listRequirementItems[mainIDX].BRANDS_TEMP == $scope.formRequest.listRequirementItems[mainIDX].BRANDS)) {
                        $scope.formRequest.listRequirementItems[mainIDX].sundriesError = "Please click on Get Sundries as you changed BRAND for item";
                        $scope.postRequestLoding = true;
                        $scope.formRequest.listRequirementItems[mainIDX].AddAccessoriesArray.forEach(function (sundries, sundriesIdx) {
                            if (sundries.TYPE == "PACKAGING") {
                                sundries.TOTAL_SUB_ITEM_PRICE = 0;
                                sundries.PACKAGING_COST = 0;
                                $scope.calculateSubItemPrice('', '', sundries, sundriesIdx, mainIDX, 'PACKAGING');
                            }
                        })
                        $scope.formRequest.listRequirementItems[mainIDX].AddAccessoriesArray.forEach(function (sundries1, sundriesIdx1) {
                            if (sundries1.TYPE == "SEWING") {
                                sundries1.TOTAL_SUB_ITEM_PRICE = 0;
                                sundries1.SEWING_COST = 0;
                                $scope.calculateSubItemPrice('', '', sundries1, sundriesIdx1, mainIDX, 'SEWING');
                            }
                        })
                        $scope.formRequest.listRequirementItems[mainIDX].AddAccessoriesArray = [];
                        $scope.calculateTotalPrice($scope.formRequest.listRequirementItems[mainIDX].AddFabricArray, $scope.formRequest.listRequirementItems[mainIDX].AddProcessArray, $scope.formRequest.listRequirementItems[mainIDX].AddAccessoriesArray, mainIDX, 'SEWING', $scope.formRequest.listRequirementItems[mainIDX].cutMake);
                        $scope.calculateTotalPrice($scope.formRequest.listRequirementItems[mainIDX].AddFabricArray, $scope.formRequest.listRequirementItems[mainIDX].AddProcessArray, $scope.formRequest.listRequirementItems[mainIDX].AddAccessoriesArray, mainIDX, 'PACKAGING', $scope.formRequest.listRequirementItems[mainIDX].cutMake);
                    }
                    $scope.formRequest.listRequirementItems[mainIDX].BRANDS_TEMP = product.BRANDS;
                }

                if ($scope.formRequest.listRequirementItems[mainIDX].AddAccessoriesArray.length > 0) {
                    $scope.formRequest.listRequirementItems[mainIDX].AddAccessoriesArray.forEach(function (sub,subIdx) {

                        sub.BRANDS = product.BRANDS;
                        sub.GENDER = product.GENDER;

                    })
                }
            }

            auctionsService.GetCompanyConfiguration($scope.compID, "ITEM_UNITS", userService.getUserToken())
                .then(function (unitResponse) {
                    $scope.companyItemUnits = unitResponse;
                });

            //auctionsService.GetCompanyConfiguration($scope.compID, "SUB_ITEM_OPT", userService.getUserToken())
            //    .then(function (response) {
            //        $scope.subItemOptions = response;
            //    });

            $scope.showBestPrice = true;

            $scope.dispalyBestprices = function (val) {
                $scope.showBestPrice = val;
            }
            $scope.getPreviousItemPrice = function (itemDetails, index) {
                $scope.itemPreviousPrice = {};
                $scope.itemPreviousPrice.lastPrice = -1;
                $scope.bestPriceEnable = index;
                $log.info(itemDetails);
                itemDetails.sessionID = userService.getUserToken();
                itemDetails.compID = userService.getUserCompanyId();
                auctionsService.getPreviousItemPrice(itemDetails)
                    .then(function (response) {
                        if (response && response.errorMessage == '') {
                            $scope.itemPreviousPrice.lastPrice = Number(response.initialPrice);
                            $scope.itemPreviousPrice.lastPriceDate = userService.toLocalDate(response.currentTime);
                            $scope.itemPreviousPrice.lastPriceVendor = response.companyName;
                            $log.info($scope.itemPreviousPrice);
                        }
                    });
            };


            $scope.AddFabricObjResponse = {};
            $scope.AddProcessObjResponse = [];

            $scope.AddProcessPrintObjResponse = {};



            $scope.washTypeArray = [];
            $scope.SelectedwashTypeArray = [];
            $scope.SelectedwashTypeTemp = [];



            $scope.subItemValidation = function (prodName, index, fieldType, mainIdx) {

                if (prodName && prodName != '' && prodName.length > 0) {
                    prodName = String(prodName).toUpperCase();
                    if (fieldType == 'MILL') {
                        $scope.MILLSarray.forEach(function (product) {
                            if (String(product).toUpperCase().includes(prodName) == true && $scope.formRequest.listRequirementItems[mainIdx].AddFabricArray[index].MILL.length > 0) {
                                $scope.formRequest.listRequirementItems[mainIdx].AddFabricArray[index].fabricError = '';
                               // $scope.postRequestLoding = false;
                              //  $scope.subItemRequestLoding = false;
                            } else {
                                $scope.formRequest.listRequirementItems[mainIdx].AddFabricArray[index].fabricError = 'Please select MILL from dropdown only';
                               // $scope.postRequestLoding = true;
                              //  $scope.subItemRequestLoding = true;
                            }
                        })

                    }
                    if (fieldType == 'STYLE') {
                        $scope.fabricDesignStyle.forEach(function (product) {
                            if (String(product).toUpperCase().includes(prodName) == true && ($scope.formRequest.listRequirementItems[mainIdx].AddFabricArray[index].DESIGN_STYLE || '').length > 0) {
                                $scope.formRequest.listRequirementItems[mainIdx].AddFabricArray[index].fabricError = '';
                                $scope.postRequestLoding = false;
                            } else {
                                $scope.formRequest.listRequirementItems[mainIdx].AddFabricArray[index].fabricError = 'Please select Design/Style from dropdown only';
                                $scope.postRequestLoding = true;
                            }
                        })
                    }
                    if (fieldType == 'PRINT_TYPE') {
                        $scope.printTypes.forEach(function (product) {
                            if (String(product).toUpperCase().includes(prodName) == true && ($scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[index].DESIGN_STYLE || '').length > 0) {
                                $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[index].processError = '';
                               // $scope.postRequestLoding = false;
                               // $scope.subItemRequestLoding = false;
                            } else {
                                $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[index].processError = 'Please select Print Type from dropdown only';
                              //  $scope.postRequestLoding = true;
                               // $scope.subItemRequestLoding = true;
                            }
                        })

                    }
                    if (fieldType == 'EMBROIDERY_NAME') {
                        $scope.embNames.forEach(function (product) {
                            if (String(product).toUpperCase().includes(prodName) == true && ($scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[index].EMBROIDERY_NAME || '').length > 0) {
                                $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[index].processError = '';
                               // $scope.postRequestLoding = false;
                               // $scope.subItemRequestLoding = false;
                            } else {
                                $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[index].processError = 'Please select Embroidery Name from dropdown only';
                               // $scope.postRequestLoding = true;
                              //  $scope.subItemRequestLoding = true;
                            }
                        })
                    }
                    if (fieldType == 'APPLIQUE_NAME') {
                        $scope.appNames.forEach(function (product) {
                            if (String(product).toUpperCase().includes(prodName) == true && ($scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[index].APPLIQUE_NAME || '').length > 0) {
                                $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[index].processError = '';
                               // $scope.postRequestLoding = false;
                               // $scope.subItemRequestLoding = false;
                            } else {
                                $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[index].processError = 'Please select Applique Name from dropdown only';
                                //$scope.postRequestLoding = true;
                               // $scope.subItemRequestLoding = true;
                            }
                        })
                    }
                    if (fieldType == 'EMBELLISHMENT_NAME') {
                        $scope.EmbellishmentNames.forEach(function (product) {
                            if (String(product).toUpperCase().includes(prodName) == true && ($scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[index].EMBELLISHMENT_NAME || '').length > 0) {
                                $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[index].processError = '';
                               // $scope.postRequestLoding = false;
                               // $scope.subItemRequestLoding = false;
                            } else {
                                $scope.formRequest.listRequirementItems[mainIdx].AddProcessArray[index].processError = 'Please select Embellishment Name from dropdown only';
                               // $scope.postRequestLoding = true;
                              //  $scope.subItemRequestLoding = true;
                            }
                        })
                    }
                }
            }

            $scope.autofillSubProduct = function (prodName, index, fieldType, mainIdx) {
                $scope['ItemSelected_' + index] = false;
                var output = [];
                var output1 = [];
                var output2 = [];
                var output3 = [];
                var output4 = [];
                var output5 = [];
                if (prodName && prodName != '' && prodName.length > 0) {
                    prodName = String(prodName).toUpperCase();
                    if (fieldType == 'MILL') {
                        output = $scope.MILLSarray.filter(function (product) {

                            return (String(product).toUpperCase().includes(prodName) == true);

                        });
                    } else if (fieldType == 'STYLE') {
                        output1 = $scope.fabricDesignStyle.filter(function (product) {

                            return (String(product).toUpperCase().includes(prodName) == true);

                        });
                    }
                    else if (fieldType == 'PRINT_TYPE') {
                        output2 = $scope.printTypes.filter(function (product) {

                            return (String(product).toUpperCase().includes(prodName) == true);

                        });
                    } else if (fieldType == 'EMBROIDERY_NAME') {
                        output3 = $scope.embNames.filter(function (name) {
                            return (String(name).toUpperCase().includes(prodName) == true);
                        })
                    }
                    else if (fieldType == 'APPLIQUE_NAME') {
                        output4 = $scope.appNames.filter(function (name) {
                            return (String(name).toUpperCase().includes(prodName) == true);
                        })
                    }
                    else if (fieldType == 'EMBELLISHMENT_NAME') {
                        output5 = $scope.EmbellishmentNames.filter(function (name) {
                            return (String(name).toUpperCase().includes(prodName) == true);
                        })
                    }

                }
                if (fieldType == 'MILL') {
                    $scope["filterSubProducts_" + index] = output;
                } else if (fieldType == 'STYLE') {
                    $scope["filterSubProductsStyle_" + index] = output1;
                } else if (fieldType == 'PRINT_TYPE') {
                    $scope["filterSubProductsPrint_" + index] = output2;
                } else if (fieldType == 'EMBROIDERY_NAME') {
                    $scope["filterSubProductsEmbroidery_" + index] = output3;
                } else if (fieldType == 'APPLIQUE_NAME') {
                    $scope["filterSubProductsApplique_" + index] = output4;
                } else if (fieldType == 'EMBELLISHMENT_NAME') {
                    $scope["filterSubProductsEmbellishment_" + index] = output5;
                }

            }

            $scope.fillSubTextbox = function (selProd, index, prodIndex, fieldType) {
                $scope['ItemSelected_' + index] = true;
                if (fieldType == 'MILL') {
                    $scope.formRequest.listRequirementItems[prodIndex].AddFabricArray[index].MILL = selProd;
                    $scope.formRequest.listRequirementItems[prodIndex].AddFabricArray[index].fabricError = '';
                    
                    $scope.postRequestLoding = false;
                } else if (fieldType == 'STYLE') {
                    $scope.formRequest.listRequirementItems[prodIndex].AddFabricArray[index].DESIGN_STYLE = selProd;
                    $scope.formRequest.listRequirementItems[prodIndex].AddFabricArray[index].fabricError = '';
                    $scope.postRequestLoding = false;
                }
                else if (fieldType == 'PRINT_TYPE') {
                    $scope.formRequest.listRequirementItems[prodIndex].AddProcessArray[index].PRINT_TYPE = selProd;
                    $scope.formRequest.listRequirementItems[prodIndex].AddProcessArray[index].processError = '';
                    $scope.postRequestLoding = false;
                } else if (fieldType == 'EMBROIDERY_NAME') {
                    $scope.formRequest.listRequirementItems[prodIndex].AddProcessArray[index].EMBROIDERY_NAME = selProd;
                    $scope.formRequest.listRequirementItems[prodIndex].AddProcessArray[index].processError = '';
                    $scope.postRequestLoding = false;
                } else if (fieldType == 'APPLIQUE_NAME') {
                    $scope.formRequest.listRequirementItems[prodIndex].AddProcessArray[index].APPLIQUE_NAME = selProd;
                    $scope.formRequest.listRequirementItems[prodIndex].AddProcessArray[index].processError = '';
                    $scope.postRequestLoding = false;
                } else if (fieldType == 'EMBELLISHMENT_NAME') {
                    $scope.formRequest.listRequirementItems[prodIndex].AddProcessArray[index].EMBELLISHMENT_NAME = selProd;
                    $scope.formRequest.listRequirementItems[prodIndex].AddProcessArray[index].processError = '';
                    $scope.postRequestLoding = false;
                }

                $scope['filterSubProducts_' + index] = null;
                $scope['filterSubProductsStyle_' + index] = null;
                $scope['filterSubProductsPrint_' + index] = null;
                $scope['filterSubProductsEmbroidery_' + index] = null;
                $scope['filterSubProductsApplique_' + index] = null;
                $scope['filterSubProductsEmbellishment_' + index] = null;
            }



            $scope.searchTableWash = function (str, washTypes, subIdx, MainIdx) {
                $scope.formRequest.listRequirementItems[MainIdx].AddProcessArray[subIdx];
                str1 = str.toUpperCase();
                if ($scope.AddProcessObjResponseTemp.length == 0) {
                    $scope.formRequest.listRequirementItems[MainIdx].AddProcessArray[subIdx].washtTypeError = "No Wash types for this Article type (" + $scope.formRequest.listRequirementItems[MainIdx].ARTICLE_TYPE + "). Please select other.";
                    $scope.postRequestLoding = true;
                } else {
                    $scope.formRequest.listRequirementItems[MainIdx].AddProcessArray[subIdx].washTypes = $scope.AddProcessObjResponseTemp.filter(function (req) {
                        if (String(req.washType).toUpperCase().includes(str1) == true) {
                            $scope.formRequest.listRequirementItems[MainIdx].AddProcessArray[subIdx].washtTypeError = '';
                            $scope.postRequestLoding = false;
                            return (String(req.washType).toUpperCase().includes(str1) == true);
                        } else {
                            $scope.formRequest.listRequirementItems[MainIdx].AddProcessArray[subIdx].washtTypeError = 'Please select wash types from dropdown only';
                            $scope.postRequestLoding = true;
                        }

                    });
                }


            }

            $scope.clearWashType = function () {
                $scope.washTypeArray = [];
                $scope.SelectedwashTypeArray = [];
                $scope.SelectedwashTypeTemp = [];
            }

            //$scope.callFromExcel = function (type) {
            //    if (type == 'Wash') {
            //        $scope.GetWashDataFromExcel();
            //    }
            //}

            $scope.showTable = true;

            $scope.dispalyLastprices = function (val) {
                $scope.showTable = val;
            }

            $scope.GetLastPrice = function (itemDetails, index) {
                $scope.itemLastPrice = {};
                $scope.itemLastPrice.lastPrice = -1;
                $scope.bestLastPriceEnable = index;
                $log.info(itemDetails);
                itemDetails.sessionID = userService.getUserToken();
                itemDetails.compID = userService.getUserCompanyId();
                auctionsService.GetLastPrice(itemDetails)
                    .then(function (response) {
                        $scope.itemLastPrice = response;

                        $scope.itemLastPrice.forEach(function (item, index) {

                            item.currentTime = userService.toLocalDate(item.currentTime);
                        })
                    });
            };




            $scope.budgetValidate = function () {
                if ($scope.formRequest.budget != "" && (isNaN($scope.formRequest.budget) || $scope.formRequest.budget.indexOf('.') > -1)) {
                    $scope.postRequestLoding = false;
                    swal({
                        title: "Error!",
                        text: "Please enter valid budget, budget should be greater than 1,00,000.",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {

                        });

                    $scope.formRequest.budget = "";
                }
            };

            $scope.clickToOpen = function () {
                ngDialog.open({ template: 'login/termsAddNewReq.html', width: 1000, height: 500 });
            };

            $scope.changeCategory = function () {

                $scope.selectedSubCategoriesList = [];
                $scope.formRequest.auctionVendors = [];
                $scope.loadSubCategories();
                //$scope.getvendors();
            }

            $scope.getCreditCount = function () {
                userService.getProfileDetails({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.userDetails = response;
                        //$scope.selectedCurrency = $filter('filter')($scope.currencies, { value: response.currency });
                        //$scope.selectedCurrency = $scope.selectedCurrency[0];
                        if (response.creditsLeft) {
                            $scope.showFreeCreditsMsg = true;
                        } else {
                            $scope.showNoFreeCreditsMsg = true;
                        }
                    });
            }



            $scope.VendorsList = [];
            $scope.VendorsTempList1 = [];

            $scope.getvendors = function (catObj) {
                $scope.ShowDuplicateVendorsNames = [];
                $scope.VendorsList = [];
                $scope.VendorsTempList1 = [];
                $scope.vendorsLoaded = false;
                var category = '';

                if (catObj) {
                    category = catObj.catCode;

                    var params = { 'Categories': category, 'sessionID': userService.getUserToken(), 'uID': userService.getUserId(), evalID: $scope.isTechEval ? $scope.selectedQuestionnaire.evalID : 0 };
                    $http({
                        method: 'POST',
                        url: domain + 'getvendors',
                        encodeURI: true,
                        headers: { 'Content-Type': 'application/json' },
                        data: params
                    }).then(function (response) {
                        if (response && response.data) {
                            if (response.data.length > 0) {
                                $scope.VendorsList = response.data;
                                $scope.VendorsTempList = $scope.VendorsList;



                                $scope.Vendors.forEach(function (item, index) {
                                    $scope.VendorsTempList.forEach(function (item1, index1) {
                                        if (item.vendorID == item1.vendorID) {
                                            $scope.ShowDuplicateVendorsNames.push(item1);
                                            $scope.VendorsList.splice(index1, 1);
                                        }
                                    })
                                });

                                if ($scope.formRequest.auctionVendors.length > 0) {
                                    $scope.formRequest.auctionVendors.forEach(function (item1, index1) {
                                        $scope.VendorsTempList.forEach(function (item2, index2) {
                                            if (item1.vendorID == item2.vendorID) {
                                                $scope.ShowDuplicateVendorsNames.push(item2);
                                                $scope.VendorsList.splice(index2, 1);
                                            }
                                        });
                                    });
                                }


                                $scope.VendorsTempList1 = $scope.VendorsList;

                                if ($scope.ShowDuplicateVendorsNames.length > 0) {
                                    $scope.totalItems = $scope.ShowDuplicateVendorsNames.length;
                                    //swal({
                                    //    title: "Cancelled",
                                    //    type: "error",
                                    //    confirmButtonText: "Ok",
                                    //    allowOutsideClick: true,
                                    //    customClass: 'swal-wide'
                                    //});
                                    //$(".sweet-alert h2").html("oops...! Some vendors are already retrieved in products they are <h3 style='color:red'><div style='max-height: 400px;overflow-y: auto;overflow-x:scroll'>" + $scope.ShowDuplicateVendorsNames + "</div></h3> so vendors are not retrieved in the categories even though they are assigned.");
                                } else {
                                    $scope.totalItems = $scope.ShowDuplicateVendorsNames.length;
                                }

                            } else {
                                $scope.VendorsList = [];
                            }

                            if ($scope.searchCategoryVendorstring != '') {
                                $scope.searchingCategoryVendors($scope.searchCategoryVendorstring);
                            } else {
                                $scope.searchingCategoryVendors('');
                            }


                        } else {

                        }
                    }, function (result) {
                    });
                }
            };

            $scope.getReqQuestionnaire = function () {


            }

            $scope.getQuestionnaireList = function () {
                techevalService.getquestionnairelist(0)
                    .then(function (response) {
                        $scope.questionnaireList = $filter('filter')(response, { reqID: 0 });
                        if ($stateParams.Id && $stateParams.Id > 0) {
                            techevalService.getreqquestionnaire($stateParams.Id, 1)
                                .then(function (response) {
                                    $scope.selectedQuestionnaire = response;

                                    if ($scope.selectedQuestionnaire && $scope.selectedQuestionnaire.evalID > 0) {
                                        $scope.isTechEval = true;
                                    }

                                    $scope.questionnaireList.push($scope.selectedQuestionnaire);
                                })
                        }
                    })
            };



            // $scope.getQuestionnaireList();

            /*$scope.getvendorsbysubcat = function () {
                $scope.vendorsLoaded = false;
                var category = [];
    
                category.push($scope.sub.selectedSubcategories);
                //$scope.formRequest.category = category;
                if ($scope.formRequest.category != undefined) {
                    var params = { 'Categories': category, 'sessionID': userService.getUserToken(), 'uID': userService.getUserId() };
                    $http({
                        method: 'POST',
                        url: domain + 'getvendorsbycatnsubcat',
                        encodeURI: true,
                        headers: { 'Content-Type': 'application/json' },
                        data: params
                    }).then(function (response) {
                        if (response && response.data) {
                            if (response.data.length > 0) {
                                $scope.Vendors = response.data;
                                $scope.vendorsLoaded = true;
                                for (var j in $scope.formRequest.auctionVendors) {
                                    for (var i in $scope.Vendors) {
                                        if ($scope.Vendors[i].vendorName == $scope.formRequest.auctionVendors[j].vendorName) {
                                            $scope.Vendors.splice(i, 1);
                                        }
                                    }
                                }
                            }
                            //$scope.formRequest.auctionVendors =[];
                        } else {
                        }
                    }, function (result) {
                    });
                }
    
            };*/

            $scope.isRequirementPosted = 0;


            $scope.getCategories = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getcategories?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken(),
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data) {

                        if (response.data.length > 0) {
                            $scope.categories = _.uniq(_.map(response.data, 'category'));
                            $scope.categoriesdata = response.data;
                            $scope.showCategoryDropdown = true;
                        }
                    } else {
                    }
                }, function (result) {
                });

            };


            $scope.getCurrencies = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getkeyvaluepairs?parameter=CURRENCY',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data) {
                        if (response.data.length > 0) {
                            $scope.currencies = response.data;
                            if (!$scope.formRequest.currency) {
                                $scope.formRequest.currency = 'INR';
                            }

                            $scope.selectedCurrency = $filter('filter')($scope.currencies, { value: $scope.formRequest.currency });
                            $scope.selectedCurrency = $scope.selectedCurrency[0];

                            $scope.getCreditCount();
                        }
                    } else {
                    }
                }, function (result) {
                });
            };




            $scope.getData = function () {
                // formCatalog
                // $scope.getCategories();

                if ($stateParams.Id) {
                    var id = $stateParams.Id;
                    $scope.isEdit = true;

                    auctionsService.getrequirementdata({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), "userid": userService.getUserId() })
                        .then(function (response) {
                            $scope.selectedProjectId = response.projectId;
                            var category = response.category[0];
                            response.category = category;
                            response.taxes = parseInt(response.taxes);
                            //response.paymentTerms = parseInt(response.paymentTerms);
                            $scope.formRequest = response;
                            $scope.selectedCurrency = $filter('filter')($scope.currencies, { value: $scope.formRequest.currency });
                            $scope.selectedCurrency = $scope.selectedCurrency[0];

                            //$scope.getPRNumber($scope.formRequest.PR_ID);
                            $scope.itemSNo = $scope.formRequest.itemSNoCount;

                            $scope.formRequest.checkBoxEmail = true;
                            $scope.formRequest.checkBoxSms = true;
                            $scope.loadSubCategories();
                            $scope.isRequirementPosted = $scope.formRequest.auctionVendors.length;
                            if (!$scope.formRequest.multipleAttachments) {
                                $scope.formRequest.multipleAttachments = [];
                            }
                            $scope.formRequest.attFile = response.attachmentName;
                            if ($scope.formRequest.attFile != '' && $scope.formRequest.attFile != null && $scope.formRequest.attFile != undefined) {


                                var attchArray = $scope.formRequest.attFile.split(',');

                                attchArray.forEach(function (att, index) {

                                    var fileUpload = {
                                        fileStream: [],
                                        fileName: '',
                                        fileID: att
                                    };

                                    $scope.formRequest.multipleAttachments.push(fileUpload);
                                })

                            }


                            $scope.selectedSubcategories = response.subcategories.split(",");
                            for (i = 0; i < $scope.selectedSubcategories.length; i++) {
                                for (j = 0; j < $scope.subcategories.length; j++) {
                                    if ($scope.selectedSubcategories[i] == $scope.subcategories[j].subcategory) {
                                        $scope.subcategories[j].ticked = true;
                                    }
                                }
                            }
                            //$scope.getvendors();
                            $scope.selectSubcat();
                            $scope.formRequest.attFile = response.attachmentName;
                            $scope.formRequest.quotationFreezTime = userService.toLocalDate($scope.formRequest.quotationFreezTime);
                            //$scope.formRequest.quotationFreezTimeNew = userService.toLocalDate($scope.formRequest.quotationFreezTime);
                            //$scope.formRequest.urgency.push(urgency);

                            $scope.formRequest.expStartTime = userService.toLocalDate($scope.formRequest.expStartTime);

                            $scope.SelectedVendors = $scope.formRequest.auctionVendors;

                            $scope.SelectedVendorsCount = $scope.formRequest.auctionVendors.length;

                            let catalogItemIdTemplateMapping = {};
                            let requirementCatalogProductIds = _.chain($scope.formRequest.listRequirementItems).map('catalogueItemID').uniq().value();
                            console.log(requirementCatalogProductIds);
                            $scope.formRequest.listRequirementItems.forEach(function (item, itemIndex) {
                                item.widgetState = 0;
                                item.isNonCoreProductCategory = !item.isCoreProductCategory; //Try to clean up keep only one property
                                if (item.productQuotationTemplateJson && item.productQuotationTemplateJson != '' && item.productQuotationTemplateJson != null && item.productQuotationTemplateJson != undefined) {
                                    item.productQuotationTemplateArray = JSON.parse(item.productQuotationTemplateJson);
                                    item.productQuotationTemplate = JSON.parse(item.productQuotationTemplateJson);
                                    item.productQuotationTemplate = _.sortBy(item.productQuotationTemplate, function (o) {
                                        if (o.SORT_ORDER) { return o.SORT_ORDER } else { return o.T_ID };
                                    });
                                }
                                else {
                                    item.productQuotationTemplateArray = [];
                                }

                                if (item.productQuotationTemplate && item.productQuotationTemplate.length > 0) {
                                    item.productQuotationTemplate = item.productQuotationTemplate.filter(function (item, index) {
                                        return item.NAME != 'Total';
                                    });
                                }

                                //$scope.GetProductQuotationTemplate(item.catalogueItemID, itemIndex);

                                item.AddFabricArray = [];
                                item.AddProcessArray = [];
                                item.AddAccessoriesArray = [];

                                if (item.isNonCoreProductCategory == false) {

                                    item.subItemsTotalPrice = 0;

                                    if (item.apparelFabricJson && item.apparelFabricJson != '' && item.apparelFabricJson != null && item.apparelFabricJson != undefined) {
                                        //item.AddFabricArray = JSON.parse(item.apparelFabricJson);
                                       // item.subItemsTotalPrice += _.sumBy(item.AddFabricArray, function (fab) { return fab.TOTAL_SUB_ITEM_PRICE; });
                                      //  item.FabricSubTotalPrice = _.sumBy(item.AddFabricArray, function (fab) { return fab.TOTAL_SUB_ITEM_PRICE; });

                                    } else {
                                        item.AddFabricArray = [];
                                    }

                                    if (item.apparelProcessJson && item.apparelProcessJson != '' && item.apparelProcessJson != null && item.apparelProcessJson != undefined) {
                                       // item.AddProcessArray = JSON.parse(item.apparelProcessJson);
                                       // item.subItemsTotalPrice += _.sumBy(item.AddProcessArray, function (fab) { return fab.TOTAL_SUB_ITEM_PRICE; });
                                      //  item.ProcessSubTotalPrice = _.sumBy(item.AddProcessArray, function (fab) { return fab.TOTAL_SUB_ITEM_PRICE; });

                                        item.AddProcessArray.forEach(function (obj, objIdx) {
                                            if (obj.PROCESS_TYPE == "Wash") {
                                                obj.SelectedwashTypeArray = [];
                                                $scope.priceWashArray = [];
                                                obj.washTypes.forEach(function (obj1, objIdx) {
                                                    if (obj1.isChecked == true) {
                                                        $scope.priceWashArray.push(obj1.washType);
                                                        obj.SelectedwashTypeArray.push(obj1.washType);
                                                    }

                                                })
                                                obj.typeOfGarment = obj.typeOfGarmentList.split(',');
                                                obj.typeOfGarment.forEach(function (type) {
                                                    type = type.trim();
                                                    obj.TYPE_OF_GARMENT = obj.TYPE_OF_GARMENT.trim();
                                                    if (type == obj.TYPE_OF_GARMENT) {
                                                        obj.TYPE_OF_GARMENT = obj.TYPE_OF_GARMENT;
                                                    }
                                                })

                                                obj.selectedWashTypes = $scope.priceWashArray.join(' , ')
                                            }

                                        })


                                    } else {
                                        item.AddProcessArray = [];
                                    }

                                    if (item.apparelSundriesJson && item.apparelSundriesJson != '' && item.apparelSundriesJson != null && item.apparelSundriesJson != undefined) {
                                        //item.AddAccessoriesArray = JSON.parse(item.apparelSundriesJson);
                                      //  item.subItemsTotalPrice += _.sumBy(item.AddAccessoriesArray, function (fab) { return fab.TOTAL_SUB_ITEM_PRICE; });
                                      //  item.SundriesSubTotalPrice = _.sumBy(item.AddAccessoriesArray, function (fab) { return fab.TOTAL_SUB_ITEM_PRICE; });
                                    } else {
                                        item.AddAccessoriesArray = [];
                                    }

                                    if (item.cutMake >= 0) {
                                        item.subItemsTotalPrice += item.cutMake;
                                    }

                                    if (item.rejectionPerAmount > 0) {
                                        item.subItemsTotalPrice += parseFloat(item.rejectionPerAmount);
                                    }
                                    if (item.testing > 0) {
                                        item.subItemsTotalPrice += parseFloat(item.testing);
                                    }
                                    if (item.marginPerAmount > 0) {
                                        item.subItemsTotalPrice += parseFloat(item.marginPerAmount);
                                    }
                                    if (item.financeCharges > 0) {
                                        item.subItemsTotalPrice += parseFloat(item.financeCharges);
                                    }
                                    if (item.freightCharges > 0) {
                                        item.subItemsTotalPrice += parseFloat(item.freightCharges);
                                    }
                                    if (item.chaCharges > 0) {
                                        item.subItemsTotalPrice += parseFloat(item.chaCharges);
                                    }
                                }


                            });

                           

                            $scope.GetProductQuotationTemplateBulk(requirementCatalogProductIds);

                            $scope.postRequestLoding = false;
                            $scope.selectRequirementPRS();
                        });
                }

            };



            $scope.showSimilarNegotiationsButton = function (value, searchstring) {
                $scope.showSimilarNegotiations = value;
                if (!value) {
                    $scope.CompanyLeads = {};
                }
                if (value) {
                    if (searchstring.length < 3) {
                        $scope.CompanyLeads = {};
                    }
                    if (searchstring.length > 2) {
                        $scope.searchstring = searchstring;
                        $scope.GetCompanyLeads(searchstring);
                    }
                }
                return $scope.showSimilarNegotiations;
            }

            $scope.showSimilarNegotiations = false;

            $scope.CompanyLeads = {};

            $scope.searchstring = '';

            $scope.GetCompanyLeads = function (searchstring) {
                if (searchstring.length < 3) {
                    $scope.CompanyLeads = {};
                }
                if ($scope.showSimilarNegotiations && searchstring.length > 2) {
                    $scope.searchstring = searchstring;
                    var params = { "userid": userService.getUserId(), "searchstring": $scope.searchstring, "searchtype": 'Title', "sessionid": userService.getUserToken() };
                    auctionsService.GetCompanyLeads(params)
                        .then(function (response) {
                            $scope.CompanyLeads = response;
                            $scope.CompanyLeads.forEach(function (item, index) {
                                item.postedOn = userService.toLocalDate(item.postedOn);

                                item.endTime = userService.toLocalDate(item.endTime);
                                if (item.endTime.includes('1000') || item.endTime.includes('10000')) {
                                    item.endTime = '--';
                                } else {
                                    item.endTime = item.endTime;
                                }
                            })
                        });
                }
            }


            $scope.changeScheduledAuctionsLimit = function () {
                $scope.scheduledLimit = 8;
                $scope.getMiniItems();
            }

            $scope.loadSubCategories = function () {
                $scope.subcategories = _.filter($scope.categoriesdata, { category: $scope.formRequest.category });
                /*$scope.subcategories = _.map($scope.subcategories, 'subcategory');*/
            }

            $scope.selectedSubCategoriesList = [];

            $scope.selectSubcat = function (subcat) {

                $scope.selectedSubCategoriesList = [];

                if (!$scope.isEdit) {
                    $scope.formRequest.auctionVendors = [];
                }
                $scope.vendorsLoaded = false;
                var category = [];
                var count = 0;
                var succategory = "";
                $scope.sub.selectedSubcategories = $filter('filter')($scope.subcategories, { ticked: true });
                selectedcount = $scope.sub.selectedSubcategories.length;
                if (selectedcount > 0) {
                    succategory = _.map($scope.sub.selectedSubcategories, 'id');
                    category.push(succategory);

                    $scope.selectedSubCategoriesList = succategory;

                    //$scope.formRequest.category = category;
                    if ($scope.formRequest.category != undefined) {
                        var params = { 'Categories': succategory, 'sessionID': userService.getUserToken(), 'count': selectedcount, 'uID': userService.getUserId(), evalID: $scope.selectedQuestionnaire ? $scope.selectedQuestionnaire.evalID : 0 };
                        $http({
                            method: 'POST',
                            url: domain + 'getvendorsbycatnsubcat',
                            encodeURI: true,
                            headers: { 'Content-Type': 'application/json' },
                            data: params
                        }).then(function (response) {
                            if (response && response.data) {
                                if (response.data.length > 0) {
                                    $scope.Vendors = response.data;
                                    $scope.vendorsLoaded = true;
                                    for (var j in $scope.formRequest.auctionVendors) {
                                        for (var i in $scope.Vendors) {
                                            if ($scope.Vendors[i].vendorName == $scope.formRequest.auctionVendors[j].vendorName) {
                                                $scope.Vendors.splice(i, 1);
                                            }
                                        }
                                    }

                                    $scope.VendorsTemp = $scope.Vendors;
                                    $scope.searchVendors('');

                                }
                                //$scope.formRequest.auctionVendors =[];
                            } else {
                            }
                        }, function (result) {
                        });
                    }
                } else {
                    //$scope.getvendors();
                }

            }

            $scope.getData();

            $scope.checkVendorUniqueResult = function (idtype, inputvalue) {


                if (idtype == "PHONE") {
                    $scope.checkVendorPhoneUniqueResult = false;
                } else if (idtype == "EMAIL") {
                    $scope.checkVendorEmailUniqueResult = false;
                }
                else if (idtype == "PAN") {
                    $scope.checkVendorPanUniqueResult = false;
                }
                else if (idtype == "TIN") {
                    $scope.checkVendorTinUniqueResult = false;
                }
                else if (idtype == "STN") {
                    $scope.checkVendorStnUniqueResult = false;
                }

                if (inputvalue == "" || inputvalue == undefined) {
                    return false;
                }

                userService.checkUserUniqueResult(inputvalue, idtype).then(function (response) {
                    if (idtype == "PHONE") {
                        $scope.checkVendorPhoneUniqueResult = !response;
                    } else if (idtype == "EMAIL") {
                        $scope.checkVendorEmailUniqueResult = !response;
                    }
                    else if (idtype == "PAN") {
                        $scope.checkVendorPanUniqueResult = !response;
                    }
                    else if (idtype == "TIN") {
                        $scope.checkVendorTinUniqueResult = !response;
                    }
                    else if (idtype == "STN") {
                        $scope.checkVendorStnUniqueResult = !response;
                    }
                });
            };
            $scope.selectForA = function (item) {
                var index = $scope.selectedA.indexOf(item);
                if (index > -1) {
                    $scope.selectedA.splice(index, 1);
                } else {
                    $scope.selectedA.splice($scope.selectedA.length, 0, item);
                }
                for (i = 0; i < $scope.selectedA.length; i++) {
                    $scope.formRequest.auctionVendors.push($scope.selectedA[i]);
                    $scope.Vendors.splice($scope.Vendors.indexOf($scope.selectedA[i]), 1);
                    $scope.VendorsTemp.splice($scope.VendorsTemp.indexOf($scope.selectedA[i]), 1);
                }
                $scope.reset();
            }

            $scope.selectForB = function (item) {
                var index = $scope.selectedB.indexOf(item);
                if (index > -1) {
                    $scope.selectedB.splice(index, 1);
                } else {
                    $scope.selectedB.splice($scope.selectedA.length, 0, item);
                }
                for (i = 0; i < $scope.selectedB.length; i++) {
                    $scope.Vendors.push($scope.selectedB[i]);
                    $scope.VendorsTemp.push($scope.selectedB[i]);
                    $scope.formRequest.auctionVendors.splice($scope.formRequest.auctionVendors.indexOf($scope.selectedB[i]), 1);
                }
                $scope.reset();
            }

            $scope.AtoB = function () {

            }

            $scope.BtoA = function () {

            }

            $scope.reset = function () {
                $scope.selectedA = [];
                $scope.selectedB = [];
            }

            // $scope.getFile = function () {
            //     $scope.progress = 0;
            //     fileReader.readAsDataUrl($scope.file, $scope)
            //     .then(function(result) {
            //         $scope.formRequest.attachment = result;
            //     });
            // };

            $scope.multipleAttachments = [];

            $scope.getFile = function () {
                $scope.progress = 0;

                //$scope.file = $("#attachement")[0].files[0];
                $scope.multipleAttachments = $("#attachement")[0].files;

                $scope.multipleAttachments = Object.values($scope.multipleAttachments)


                $scope.multipleAttachments.forEach(function (item, index) {

                    fileReader.readAsDataUrl(item, $scope)
                        .then(function (result) {

                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };

                            var bytearray = new Uint8Array(result);

                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = item.name;

                            if (!$scope.formRequest.multipleAttachments) {
                                $scope.formRequest.multipleAttachments = [];
                            }

                            $scope.formRequest.multipleAttachments.push(fileUpload);

                        });

                })

            };


            $scope.newVendor = {};
            $scope.Attaachmentparams = {};
            $scope.deleteAttachment = function (reqid) {
                $scope.Attaachmentparams = {
                    reqID: reqid,
                    userID: userService.getUserId()
                }
                auctionsService.deleteAttachment($scope.Attaachmentparams)
                    .then(function (response) {
                        if (response.errorMessage != "") {
                            growlService.growl(response.errorMessage, "inverse");
                        } else {
                            growlService.growl("Attachment deleted Successfully", "inverse");
                            $scope.getData();
                        }
                    });
            }

            $scope.newVendor.panno = "";
            $scope.newVendor.vatNum = "";
            $scope.newVendor.serviceTaxNo = "";

            $scope.addVendor = function () {
                $scope.emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                $scope.mobileRegx = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
                $scope.panregx = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
                //$scope.emailRegx = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
                var addVendorValidationStatus = false;
                $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
                if ($scope.newVendor.companyName == "" || $scope.newVendor.companyName === undefined) {
                    $scope.companyvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.firstName == "" || $scope.newVendor.firstName === undefined) {
                    $scope.firstvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.lastName == "" || $scope.newVendor.lastName === undefined) {
                    $scope.lastvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.contactNum == "" || $scope.newVendor.contactNum === undefined || isNaN($scope.newVendor.contactNum)) {
                    $scope.contactvalidation = true;
                    addVendorValidationStatus = true;
                }
                else if ($scope.newVendor.contactNum.length != 10) {
                    $scope.contactvalidationlength = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.email == "" || $scope.newVendor.email === undefined) {
                    $scope.emailvalidation = true;
                    addVendorValidationStatus = true;
                }
                else if (!$scope.emailRegx.test($scope.newVendor.email)) {
                    $scope.emailregxvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.vendorcurrency == "" || $scope.newVendor.vendorcurrency === undefined) {
                    $scope.vendorcurrencyvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.panno != "" && $scope.newVendor.panno != undefined && !$scope.panregx.test($scope.newVendor.panno)) {
                    $scope.panregxvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.vatNum != "" && $scope.newVendor.vatNum != undefined && $scope.newVendor.vatNum.length != 11) {
                    $scope.tinvalidation = true;
                    addVendorValidationStatus = true;
                }

                if ($scope.newVendor.serviceTaxNo != "" && $scope.newVendor.serviceTaxNo != undefined && $scope.newVendor.serviceTaxNo.length != 15) {
                    $scope.stnvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.formRequest.category == "" || $scope.formRequest.category === undefined) {
                    $scope.categoryvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.checkVendorEmailUniqueResult || $scope.checkVendorEmailUniqueResult) {
                    addVendorValidationStatus = true;
                }
                if (addVendorValidationStatus) {
                    return false;
                }
                var vendCAtegories = [];
                $scope.newVendor.category = $scope.formRequest.category;
                vendCAtegories.push($scope.newVendor.category);
                var params = {
                    "register": {
                        "firstName": $scope.newVendor.firstName,
                        "lastName": $scope.newVendor.lastName,
                        "email": $scope.newVendor.email,
                        "phoneNum": $scope.newVendor.contactNum,
                        "username": $scope.newVendor.contactNum,
                        "password": $scope.newVendor.contactNum,
                        "companyName": $scope.newVendor.companyName ? $scope.newVendor.companyName : "",
                        "isOTPVerified": 0,
                        "category": $scope.newVendor.category,
                        "userType": "VENDOR",
                        "panNumber": ("panno" in $scope.newVendor) ? $scope.newVendor.panno : "",
                        "stnNumber": ("serviceTaxNo" in $scope.newVendor) ? $scope.newVendor.serviceTaxNo : "",
                        "vatNumber": ("vatNum" in $scope.newVendor) ? $scope.newVendor.vatNum : "",
                        "referringUserID": userService.getUserId(),
                        "knownSince": ("knownSince" in $scope.newVendor) ? $scope.newVendor.knownSince : "",
                        "errorMessage": "",
                        "sessionID": "",
                        "userID": 0,
                        "department": "",
                        "currency": $scope.newVendor.vendorcurrency.key,
                        "altPhoneNum": $scope.newVendor.altPhoneNum,
                        "altEmail": $scope.newVendor.altEmail,
                        "subcategories": $scope.selectedSubCategoriesList
                    }
                };
                $http({
                    method: 'POST',
                    url: domain + 'register',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                }).then(function (response) {
                    if ((response && response.data && response.data.errorMessage == "") || response.data.errorMessage == 'User assigned to Company.') {
                        $scope.formRequest.auctionVendors.push({ vendorName: $scope.newVendor.firstName + " " + $scope.newVendor.lastName, companyName: $scope.newVendor.companyName, vendorID: response.data.objectID });
                        $scope.newVendor = null;
                        $scope.newVendor = {};
                        //$scope.addVendorForm.$setPristine();
                        $scope.addVendorShow = false;
                        $scope.selectVendorShow = true;
                        $scope.newVendor = {};
                        $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
                        growlService.growl("Vendor Added Successfully.", 'inverse');
                    } else if (response && response.data && response.data.errorMessage) {
                        growlService.growl(response.data.errorMessage, 'inverse');
                    } else {
                        growlService.growl('Unexpected Error Occurred', 'inverse');
                    }
                });
            }

            //$scope.checkboxModel = {
            //    value1: true
            //};
            $scope.mapSubitems = function () {

            }


            $scope.formRequest.checkBoxEmail = true;
            $scope.formRequest.checkBoxSms = true;
            //$scope.postRequestLoding = false;
            $scope.formRequest.urgency = 'High (Will be Closed in 2 Days)';
            $scope.formRequest.deliveryLocation = '';
            $scope.formRequest.paymentTerms = '';
            $scope.formRequest.isSubmit = 0;


            $scope.titleValidation = $scope.attachmentNameValidation = false;
            $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = false;
            $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation = $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = $scope.questionnaireValidation = false;

            $scope.postRequest = function (isSubmit, pageNo, navigateToView, stage) {

                $scope.titleValidation = $scope.attachmentNameValidation = false;
                $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = false;
                $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation = $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = $scope.questionnaireValidation = false;

                $scope.postRequestLoding = true;
                $scope.formRequest.isSubmit = isSubmit;

                if (isSubmit == 1 || pageNo == 1) {
                    if ($scope.formRequest.title == null || $scope.formRequest.title == '') {
                        $scope.titleValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if (isSubmit == 1) {
                        if ($scope.formRequest.isTabular) {
                            $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                                if (!item.attachmentName || item.attachmentName == '') {
                                    $scope.attachmentNameValidation = true;
                                    return false;
                                }
                            })

                        }
                        if (!$scope.formRequest.isTabular) {
                            if ($scope.formRequest.description == null || $scope.formRequest.description == '') {
                                $scope.descriptionValidation = true;
                                $scope.postRequestLoding = false;
                                return false;
                            }
                        }
                    }
                }

                if (isSubmit == 1) {
                    if ($scope.formRequest.deliveryTime == null || $scope.formRequest.deliveryTime == '') {
                        $scope.deliveryTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    //if ($scope.formRequest.deliveryLocation == null || $scope.formRequest.deliveryLocation == '') {
                    //    $scope.deliveryLocationValidation = true;
                    //    $scope.postRequestLoding = false;
                    //    return false;
                    //}
                    //if ($scope.formRequest.paymentTerms == null || $scope.formRequest.paymentTerms == '') {
                    //    $scope.paymentTermsValidation = true;
                    //    $scope.postRequestLoding = false;
                    //    return false;
                    //}

                }

                if (isSubmit == 1) {
                    if ($scope.formRequest.urgency == null || $scope.formRequest.urgency == '') {
                        $scope.urgencyValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }

                    if (($scope.formRequest.quotationPriceLimit == null || $scope.formRequest.quotationPriceLimit == '' || $scope.formRequest.quotationPriceLimit <= '') && $scope.formRequest.isQuotationPriceLimit == true) {
                        $scope.quotationPriceLimitValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.isQuotationPriceLimit == false) {
                        $scope.formRequest.quotationPriceLimit = 0;
                    }
                    if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                        $scope.noOfQuotationRemindersValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                        $scope.remindersTimeIntervalValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.isTechEval == true && (!$scope.selectedQuestionnaire || $scope.selectedQuestionnaire.evalID <= 0)) {
                        $scope.questionnaireValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                        $scope.quotationFreezTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                }

                if (!$scope.postRequestLoding) {
                    return false;
                }


                if (pageNo != 4) {
                    $scope.textMessage = "Save as Draft.";
                }
                let isValidVendorCurrency = true;
                let inValidVendorCurrencyValue = '';
                $scope.formRequest.auctionVendors.forEach(function (vendor, itemIndexs) {
                    if (vendor && vendor.vendorCurrency) {
                        let selecVendorCurrencyTemp = _.filter($scope.companyCurrencies, function (currItem) { return currItem.currency === vendor.vendorCurrency; });
                        if (selecVendorCurrencyTemp && selecVendorCurrencyTemp.length > 0) {
                            if (selecVendorCurrencyTemp[0].StartTimeLocal > $scope.currentSystemDateOnly || selecVendorCurrencyTemp[0].EndTimeLocal < $scope.currentSystemDateOnly) {
                                if (!inValidVendorCurrencyValue.includes(vendor.vendorCurrency)) {
                                    inValidVendorCurrencyValue += vendor.vendorCurrency + ',';
                                }
                                if (isValidVendorCurrency) {
                                    isValidVendorCurrency = false;
                                }
                            }
                        }
                    }
                });

                
                if ($scope.formRequest.checkBoxEmail == true && $scope.formRequest.checkBoxSms == true && pageNo == 4) {
                    $scope.textMessage = "This will send an email invite to all the vendors selected above.";
                }
                else if ($scope.formRequest.checkBoxEmail == true && pageNo == 4) {
                    $scope.textMessage = "This will send an email invite to all the vendors selected above.";
                }
                else if ($scope.formRequest.checkBoxSms == true && pageNo == 4) {
                    //  $scope.textMessage = "This will send an SMS invite to all the vendors selected above.";
                    $scope.textMessage = "This will not send any communication to all the vendors selected above.";
                }
                else if (pageNo == 4) {
                    $scope.textMessage = "This will not send an EMAIL the vendors selected above.";
                }

                if ($scope.stateParamsReqID && !$scope.formRequest.deleteQuotations && pageNo == 4 && !$scope.isClone) {
                    $scope.textMessage = "Please select \"Request New Quotations\" to vendors if you modify any details in Requirement item specifications / Quantity. Click \"Cancel\" to select the option.";
                }

                $scope.formRequest.currency = $scope.selectedCurrency.value;
                $scope.formRequest.timeZoneID = 190;
                $scope.postRequestLoding = false;

                if (!isValidVendorCurrency) {
                    inValidVendorCurrencyValue = inValidVendorCurrencyValue.replace(/,\s*$/, "");
                    $scope.postRequestLoding = false;
                    swal({
                        title: "Error!",
                        title: "Selected vendor currency (" + inValidVendorCurrencyValue + ") conversion rate has expired, please validate.",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            return false;
                        });
                } else {
                    swal({
                        title: "Are you sure?",
                        text: $scope.textMessage,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#F44336",
                        confirmButtonText: "OK",
                        closeOnConfirm: true
                    }, function () {
                        $scope.postRequestLoding = true;
                        var ts = userService.toUTCTicks($scope.formRequest.quotationFreezTime);
                        var m = moment(ts);
                        var quotationDate = new Date(m);
                        var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                        $scope.formRequest.quotationFreezTime = "/Date(" + milliseconds + "000++530)/";
                        // this post request



                        var ts = userService.toUTCTicks($scope.formRequest.expStartTime);
                        var m = moment(ts);
                        var quotationDate = new Date(m);
                        var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                        $scope.formRequest.expStartTime = "/Date(" + milliseconds + "000+0530)/";

                        $scope.formRequest.requirementID = $stateParams.Id ? $stateParams.Id : -1;
                        $scope.formRequest.customerID = userService.getUserId();
                        $scope.formRequest.customerFirstname = userService.getFirstname();
                        $scope.formRequest.customerLastname = userService.getLastname();
                        $scope.formRequest.isClosed = "NOTSTARTED";
                        $scope.formRequest.endTime = "";
                        $scope.formRequest.sessionID = userService.getUserToken();
                        $scope.formRequest.subcategories = "";
                        $scope.formRequest.budget = 100000;
                        $scope.formRequest.custCompID = userService.getUserCompanyId();
                        $scope.formRequest.customerCompanyName = userService.getUserCompanyId();

                        //for (i = 0; i < $scope.sub.selectedSubcategories.length; i++) {
                        //    $scope.formRequest.subcategories += $scope.sub.selectedSubcategories[i].subcategory + ",";
                        //}
                        var category = [];
                        //category.push($scope.formRequest.category);
                        $scope.formRequest.category = category;

                        $scope.formRequest.listRequirementItems.forEach(function (item, itemIndexs) {
                            if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                                item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                                    subItem.dateCreated = "/Date(1561000200000+0530)/";
                                    subItem.dateModified = "/Date(1561000200000+0530)/";
                                })
                            }
                        });

                        if (!$scope.formRequest.isForwardBidding) {
                            $scope.formRequest.cloneID = 0;
                            if ($stateParams.reqObj && $stateParams.reqObj.cloneId && $stateParams.reqObj.cloneId !== "") {
                                $scope.formRequest.cloneID = $stateParams.reqObj.cloneId;
                                $scope.formRequest.auctionVendors = _.filter($scope.formRequest.auctionVendors, function (x) { return x.companyName !== 'PRICE_CAP'; });
                            }

                            if ($scope.PRList && $scope.PRList.length > 0) {
                                var selectedPRs = _.filter($scope.PRList, function (x) { return x.isSelected; });
                                var selectedPRList = [];
                                if (selectedPRs && selectedPRs.length > 0) {
                                    selectedPRs.forEach(function (selectedPR, itemIndexs) {
                                        selectedPRList.push(selectedPR.PR_ID);
                                    });

                                    $scope.formRequest.PR_ID = selectedPRList.join(',');
                                }
                            }

                            $scope.formRequest.listRequirementItems.forEach(function (product, index) {
                                subItemsArray = [];
                                $scope.sunItemsIdz = [];
                                product.subItemsIds = '';



                                //if (product.productQuotationTemplate && product.productQuotationTemplate.length > 0) {
                                //    product.productQuotationTemplate.forEach(function (subItem, index) {
                                //        if (subItem) {
                                //            $scope.saveSubItemOptions(subItem);
                                //        }
                                //    });
                                //}

                                if (product && product.isNonCoreProductCategory != 1) {
                                    if (product.AddFabricArray && product.AddFabricArray.length > 0) {
                                        product.AddFabricArray.forEach(function (fabric, fabIdx) {
                                            if (fabric.IS_FABRIC_NOMINATED == 1) {
                                                fabric.tempBulk = fabric.FINAL_SS20_PRICE;
                                                fabric.tempconsumption = fabric.CONSUMPTION;
                                            } else {
                                                fabric.tempBulk = 0;
                                                fabric.tempconsumption = 1;
                                            }
                                            $scope.QuotationTemplateObjTemp = {
                                                T_ID: 0,
                                                TEMP_ID: fabric.FABRIC_ID,
                                                TEMP_TYPE: 'FABRIC',
                                                BULK_PRICE: fabric.tempBulk,
                                                CONSUMPTION: fabric.tempconsumption,
                                                DESCRIPTION: fabric.NOMINATED_SUPPLIER,
                                                DESCRIPTION1: fabric.IS_FABRIC_NOMINATED,
                                                HAS_PRICE: 1,
                                                HAS_QUANTITY: 1,
                                                HAS_SPECIFICATION: 0,
                                                HAS_TAX: 0,
                                                IS_CALCULATED: 0,
                                                IS_VALID: 1,
                                                NAME: fabric.NAME,
                                                PRODUCT_ID: product.catalogueItemID,
                                                REV_BULK_PRICE: fabric.tempBulk,
                                                REV_CONSUMPTION: fabric.tempconsumption,
                                                REV_UNIT_PRICE: 0,
                                                UNIT_PRICE: 0,
                                                UOM: fabric.UOM,
                                                U_ID: userService.getUserId()
                                            }
                                            if (!$scope.isEdit) {
                                                subItemsArray.push($scope.QuotationTemplateObjTemp);
                                            }
                                            if (($scope.isEdit && fabric.IS_NEW_OBJ == 1) || $scope.isEdit) {
                                                subItemsArray.push($scope.QuotationTemplateObjTemp);
                                            }
                                            if (fabric.FABRIC_ID) {
                                                $scope.sunItemsIdz.push(fabric.FABRIC_ID);

                                            }
                                        })
                                    }
                                    if (product.AddProcessArray && product.AddProcessArray.length > 0) {
                                        product.AddProcessArray.forEach(function (process, proIdx) {
                                            process.tempName = '';
                                            process.tempNameID = 0;
                                            process.TEMP_TYPE = 'PROCESS';
                                            if (process.PROCESS_TYPE == "Wash") {
                                                if (process.IS_PROCESS_NOMINATED == 1) {
                                                    process.tempBulk = process.COST_PER_GARMENT;
                                                    process.tempconsumption = process.CONSUMPTION;
                                                } else {
                                                    process.tempBulk = 0;
                                                    process.tempconsumption = 1;
                                                }
                                                $scope.sunItemsIdz.push(process.WASH_ID);
                                                process.ID = process.WASH_ID;
                                                process.TEMP_TYPE = 'WASH';
                                                process.tempName = process.NOMINATED_SUPPLIER;
                                                process.tempNameID = process.IS_PROCESS_NOMINATED;
                                            }
                                            else if (process.PROCESS_TYPE == "Print") {
                                                if (process.IS_PROCESS_NOMINATED == 1) {
                                                    process.tempBulk = process.COST_PER_GARMENT;
                                                    process.tempconsumption = process.CONSUMPTION;
                                                } else {
                                                    process.tempBulk = 0;
                                                    process.tempconsumption = 1;
                                                }
                                                $scope.sunItemsIdz.push(process.PRINT_ID);
                                                process.ID = process.PRINT_ID;
                                                process.TEMP_TYPE = 'PRINT';
                                                process.tempName = process.NOMINATED_SUPPLIER;
                                                process.tempNameID = process.IS_PROCESS_NOMINATED;
                                            }
                                            else if (process.PROCESS_TYPE == "Embroidery") {
                                                if (process.IS_PROCESS_NOMINATED == 1) {
                                                    process.tempBulk = process.COST_PER_GARMENT;
                                                    process.tempconsumption = process.CONSUMPTION;
                                                } else {
                                                    process.tempBulk = 0;
                                                    process.tempconsumption = 1;
                                                }
                                                $scope.sunItemsIdz.push(process.EMB_ID);
                                                process.ID = process.EMB_ID;
                                                process.TEMP_TYPE = 'EMBROIDERY';
                                                process.tempName = process.NOMINATED_SUPPLIER;
                                                process.tempNameID = process.IS_PROCESS_NOMINATED;
                                            }
                                            else if (process.PROCESS_TYPE == "Applique") {
                                                if (process.IS_PROCESS_NOMINATED == 1) {
                                                    process.tempBulk = process.COST_PER_GARMENT;
                                                    process.tempconsumption = process.CONSUMPTION;
                                                } else {
                                                    process.tempBulk = 0;
                                                    process.tempconsumption = 1;
                                                }
                                                $scope.sunItemsIdz.push(process.APP_ID);
                                                process.ID = process.APP_ID;
                                                process.TEMP_TYPE = 'APPLIQUE';
                                                process.tempName = process.NOMINATED_SUPPLIER;
                                                process.tempNameID = process.IS_PROCESS_NOMINATED;
                                            }
                                            else if (process.PROCESS_TYPE == "Embellishment") {
                                                if (process.IS_PROCESS_NOMINATED == 1) {
                                                    process.tempBulk = process.COST_PER_GARMENT;
                                                    process.tempconsumption = process.CONSUMPTION;
                                                } else {
                                                    process.tempBulk = 0;
                                                    process.tempconsumption = 1;
                                                }
                                                $scope.sunItemsIdz.push(process.E_ID);
                                                process.ID = process.E_ID;
                                                process.TEMP_TYPE = 'EMBELLISHMENT';
                                                process.tempName = process.NOMINATED_SUPPLIER;
                                                process.tempNameID = process.IS_PROCESS_NOMINATED;
                                            }

                                            $scope.QuotationTemplateObjTemp = {
                                                T_ID: 0,
                                                TEMP_ID: process.ID,
                                                TEMP_TYPE: process.TEMP_TYPE,
                                                BULK_PRICE: process.tempBulk,
                                                CONSUMPTION: process.tempconsumption,
                                                DESCRIPTION: process.tempName,
                                                DESCRIPTION1: process.tempNameID,
                                                HAS_PRICE: 1,
                                                HAS_QUANTITY: 1,
                                                HAS_SPECIFICATION: 0,
                                                HAS_TAX: 0,
                                                IS_CALCULATED: 0,
                                                IS_VALID: 1,
                                                NAME: process.PROCESS_TYPE,
                                                PRODUCT_ID: product.catalogueItemID,
                                                REV_BULK_PRICE: process.tempBulk,
                                                REV_CONSUMPTION: process.tempconsumption,
                                                REV_UNIT_PRICE: 0,
                                                UNIT_PRICE: 0,
                                                UOM: process.UOM,
                                                U_ID: userService.getUserId()
                                            }
                                            if (!$scope.isEdit) {
                                                subItemsArray.push($scope.QuotationTemplateObjTemp);
                                            }
                                            if (($scope.isEdit && process.IS_NEW_OBJ == 1) || $scope.isEdit) {
                                                subItemsArray.push($scope.QuotationTemplateObjTemp);
                                            }
                                        })
                                    }
                                    if (product.AddAccessoriesArray && product.AddAccessoriesArray.length > 0) {
                                        product.AddAccessoriesArray.forEach(function (sun, sunIdx) {
                                            sun.tempName1 = '';
                                            sun.tempNameID1 = '';
                                            //if (sun.TYPE == 'PACKAGING') {
                                            //    sun.STYLING = 'PACKAGING';
                                            //}

                                            if (sun.TYPE == "SEWING") {
                                                if (sun.IS_SUNDRIES_NOMINATED == 1) {
                                                    sun.tempBulk = sun.SEWING_COST;
                                                    sun.tempconsumption = sun.SEWING_CONSUMPTION;
                                                } else {
                                                    sun.tempBulk = 0;
                                                    sun.tempconsumption = 1;
                                                }
                                                $scope.sunItemsIdz.push(sun.SEWING_ID);
                                                sun.ID = sun.SEWING_ID;
                                                sun.TEMP_TYPE = "SEWING";
                                                sun.tempName1 = sun.NOMINATED_SUPPLIER;
                                                sun.tempNameID = sun.IS_SUNDRIES_NOMINATED;
                                            } else if (sun.TYPE == "PACKAGING") {
                                                if (sun.IS_SUNDRIES_NOMINATED == 1) {
                                                    sun.tempBulk = sun.PACKAGING_COST;
                                                    sun.tempconsumption = sun.PACKAGING_CONSUMPTION;
                                                } else {
                                                    sun.tempBulk = 0;
                                                    sun.tempconsumption = 1;
                                                }
                                                $scope.sunItemsIdz.push(sun.PACK_ID);
                                                sun.ID = sun.PACK_ID;
                                                sun.TEMP_TYPE = "PACKAGING";
                                                sun.tempName1 = sun.NOMINATED_SUPPLIER;
                                                sun.tempNameID = sun.IS_SUNDRIES_NOMINATED;
                                            }
                                            $scope.QuotationTemplateObjTemp = {
                                                T_ID: 0,
                                                TEMP_ID: sun.ID,
                                                TEMP_TYPE: sun.TEMP_TYPE,
                                                BULK_PRICE: sun.tempBulk,
                                                CONSUMPTION: sun.tempconsumption,
                                                DESCRIPTION: sun.tempName1,
                                                DESCRIPTION1: sun.tempNameID,
                                                HAS_PRICE: 1,
                                                HAS_QUANTITY: 1,
                                                HAS_SPECIFICATION: 0,
                                                HAS_TAX: 0,
                                                IS_CALCULATED: 0,
                                                IS_VALID: 1,
                                                NAME: sun.STYLING,
                                                PRODUCT_ID: product.catalogueItemID,
                                                REV_BULK_PRICE: sun.tempBulk,
                                                REV_CONSUMPTION: sun.tempconsumption,
                                                REV_UNIT_PRICE: 0,
                                                UNIT_PRICE: 0,
                                                UOM: sun.UOM,
                                                U_ID: userService.getUserId()
                                            }
                                            if (!$scope.isEdit) {
                                                subItemsArray.push($scope.QuotationTemplateObjTemp);
                                            }
                                            if (($scope.isEdit && sun.IS_NEW_OBJ == 1) || $scope.isEdit) {
                                                subItemsArray.push($scope.QuotationTemplateObjTemp);
                                            }
                                        })
                                    }
                                    // product.productQuotationTemplateJson = JSON.stringify(subItemsArray);
                                    if (subItemsArray.length > 0) {
                                        subItemsArray.forEach(function (sub, subIdx) {
                                            if (product.productQuotationTemplate) {
                                                product.productQuotationTemplate.push(sub);
                                            }
                                            
                                        })
                                    }


                                    product.productQuotationTemplateJson = JSON.stringify(product.productQuotationTemplate);
                                    // product.productQuotationTemplateJsonCallBack = JSON.stringify(product.productQuotationTemplate);

                                    product.subItemsIds = $scope.sunItemsIdz.join(',');
                                }

                                if (product.isNonCoreProductCategory != 1) {
                                    //if (product.AddFabricArray && product.AddFabricArray != null && product.AddFabricArray != undefined && product.AddFabricArray.length > 0) {
                                    //    product.AddFabricArray.forEach(function (fab, Idx) {
                                    //        fab.currentTime = "/Date(1561000200000+0530)/";
                                    //    })
                                    //    product.apparelFabricJson = JSON.stringify(product.AddFabricArray);

                                    //}

                                    //if (product.AddProcessArray && product.AddProcessArray != null && product.AddProcessArray != undefined && product.AddProcessArray.length > 0) {
                                    //    product.AddProcessArray.forEach(function (fab, Idx) {
                                    //        fab.currentTime = "/Date(1561000200000+0530)/";
                                    //    })
                                    //    product.apparelProcessJson = JSON.stringify(product.AddProcessArray);

                                    //}

                                    //if (product.AddAccessoriesArray && product.AddAccessoriesArray != null && product.AddAccessoriesArray != undefined && product.AddAccessoriesArray.length > 0) {
                                    //    product.AddAccessoriesArray.forEach(function (fab, Idx) {
                                    //        fab.currentTime = "/Date(1561000200000+0530)/";
                                    //    })
                                    //    product.apparelSundriesJson = JSON.stringify(product.AddAccessoriesArray);

                                    //}
                                }
                                
                            });



                            auctionsService.postrequirementdata($scope.formRequest)
                                .then(function (response) {
                                    if (response.objectID != 0) {
                                        if ($scope.selectedProject) {
                                            $scope.selectedProject.sessionID = userService.getUserToken();
                                            $scope.selectedProject.requirement = {};
                                            $scope.selectedProject.requirement.requirementID = response.objectID;
                                            var param = { item: $scope.selectedProject };
                                            //prmCompanyService.SaveProjectRequirement(param).then(function (projecgtMappingResponse) {
                                            //    console.log("saving project mapping");
                                            //    console.log(projecgtMappingResponse);
                                            //    console.log("END saving project mapping");
                                            //});
                                        }

                                        $scope.SaveReqDepartments(response.objectID);
                                        $scope.saveRequirementCustomFields(response.objectID);
                                        //$scope.SaveReqDeptDesig(response.objectID);

                                        //$scope.DeleteRequirementTerms();
                                        //$scope.SaveRequirementTerms(response.objectID);

                                        if ($scope.selectedQuestionnaire != null && $scope.selectedQuestionnaire != undefined && $scope.selectedQuestionnaire.evalID != undefined) {
                                            techevalService.getquestionnaire($scope.selectedQuestionnaire.evalID, 1)
                                                .then(function (response1) {
                                                    $scope.selectedQuestionnaire = response1;

                                                    if ($scope.isTechEval) {
                                                        $scope.selectedQuestionnaire.reqID = response.objectID;
                                                    }
                                                    else {
                                                        $scope.selectedQuestionnaire.reqID = 0;
                                                    }
                                                    $scope.selectedQuestionnaire.sessionID = userService.getUserToken();
                                                    var params = {
                                                        questionnaire: $scope.selectedQuestionnaire
                                                    }
                                                    techevalService.assignquestionnaire(params)
                                                        .then(function (response) {
                                                        })
                                                })
                                        }
                                        if (stage) {
                                            $state.go('save-requirementAdv', { Id: response.objectID });
                                        }
                                        swal({
                                            title: "Done!",
                                            text: "Requirement Saved Successfully",
                                            type: "success",
                                            showCancelButton: false,
                                            confirmButtonColor: "#DD6B55",
                                            confirmButtonText: "Ok",
                                            closeOnConfirm: true
                                        },
                                            function () {
                                                //$scope.postRequestLoding = false;
                                                //$state.go('view-requirement');

                                                if (navigateToView && stage == undefined) {
                                                    $state.go('view-requirement', { 'Id': response.objectID });
                                                } else {
                                                    if ($scope.stateParamsReqID > 0 && stage == undefined) {
                                                        location.reload();
                                                    }
                                                    else {
                                                        if (stage == undefined) {
                                                            //$state.go('form.addnewrequirement', { 'Id': response.objectID });
                                                            $state.go('save-requirementAdv', { Id: response.objectID });
                                                        }
                                                    }
                                                }
                                            });
                                    }
                                });
                        } else {
                            fwdauctionsService.postrequirementdata($scope.formRequest)
                                .then(function (response) {
                                    if (response.objectID != 0) {

                                        //$scope.SaveReqDepartments(response.objectID);
                                        $scope.SaveReqDeptDesig(response.objectID);

                                        swal({
                                            title: "Done!",
                                            text: "Requirement Created Successfully",
                                            type: "success",
                                            showCancelButton: false,
                                            confirmButtonColor: "#DD6B55",
                                            confirmButtonText: "Ok",
                                            closeOnConfirm: true
                                        },
                                            function () {
                                                //$scope.postRequestLoding = false;
                                                //$state.go('view-requirement');
                                                $state.go('fwd-view-req', { 'Id': response.objectID });
                                            });
                                    }
                                });
                        }
                    });
                    //$scope.postRequestLoding = false;
                }

            };


            $scope.ItemFile = '';
            $scope.itemSNo = 1;
            $scope.ItemFileName = '';

            $scope.itemnumber = $scope.formRequest.listRequirementItems.length;

            $scope.requirementItems =
                {
                    productSNo: $scope.itemSNo++,
                    ItemID: 0,
                    productIDorName: '',
                    productNo: '',
                    productDescription: '',
                    processDescription: '',
                    fabricDescription: '',
                    ARTICLE_TYPE: '',
                    ARTICLE_TYPE_TEMP: '',
                    GENDER_TEMP: '',
                    GENDER: '',
                    BRANDS: '',
                    BRANDS_TEMP: '',
                    productQuantity: 0,
                    productBrand: '',
                    othersBrands: '',
                    isDeleted: 0,
                    itemAttachment: '',
                    attachmentName: '',
                    itemMinReduction: 0,
                    splitenabled: 0,
                    fromrange: 0,
                    torange: 0,
                    requiredQuantity: 0,
                    productCode: '',
                    AddFabricArray: [],
                    AddProcessArray: [],
                    AddAccessoriesArray: [],
                    subItemsTotalPrice: 0,
                    cutMake: 0,
                    fabricPrice: 0,
                    fabricquality : ''
                }

            $scope.formRequest.listRequirementItems.push($scope.requirementItems);

            //$scope.formRequest.listRequirementItems[0].AddFabricArray.push($scope.AddFabricObj);
            //$scope.formRequest.listRequirementItems[0].AddProcessArray.push($scope.AddProcessObj);
            //$scope.formRequest.listRequirementItems.AddAccessoriesArray.push($scope.requirementItems);

            $scope.AddSubitems = function () {
                $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                    item.AddFabricArray.push($scope.AddFabricObj);
                    item.AddProcessArray.push($scope.AddProcessObj);
                    item.AddAccessoriesArray.push($scope.AddAccessoriesObj);
                })
                //$scope.formRequest.listRequirementItems[0].AddProcessArray.push($scope.AddProcessObj);
                //$scope.formRequest.listRequirementItems.AddAccessoriesArray.push($scope.requirementItems);
            }
           // $scope.AddSubitems();
            $scope.AddItem = function (type) {
                $scope.AddFabricArray = [];
                $scope.AddProcessArray = [];
                $scope.AddAccessoriesArray = [];
                $window.scrollBy(0, 50);

                var listRequirementItemsTemp = $filter('filter')($scope.formRequest.listRequirementItems, { isDeleted: 0 });

                $scope.itemSNo = $scope.formRequest.listRequirementItems.length;
                if (listRequirementItemsTemp.length < 200) {
                    $scope.requirementItems =
                        {
                            productSNo: listRequirementItemsTemp.length + 1,
                            ItemID: 0,
                            productIDorName: '',
                            productNo: '',
                            productDescription: '',
                            processDescription: '',
                            fabricDescription: '',
                            ARTICLE_TYPE: '',
                            ARTICLE_TYPE_TEMP: '',
                            GENDER: '',
                            GENDER_TEMP: '',
                            BRANDS: '',
                            BRANDS_TEMP: '',
                            productQuantity: 0,
                            productBrand: '',
                            othersBrands: '',
                            isDeleted: 0,
                            itemAttachment: '',
                            attachmentName: '',
                            itemMinReduction: 0,
                            splitenabled: 0,
                            fromrange: 0,
                            torange: 0,
                            requiredQuantity: 0,
                            productCode: '',
                            AddFabricArray: [],
                            AddProcessArray: [],
                            AddAccessoriesArray: [],
                            isCoreProductCategory: 0,
                            subItemsTotalPrice: 0,
                            cutMake: 0,
                            FabricSubTotalPrice: 0,
                            ProcessSubTotalPrice: 0,
                            SundriesSubTotalPrice: 0,
                            fabricPrice: 0,
                            fabricquality: ''
                        }

                    $scope.formRequest.listRequirementItems.push($scope.requirementItems);

                    var sortedCollection = $scope.formRequest.listRequirementItems.sort(function (a, b) {
                        return a.isDeleted - b.isDeleted;

                    });

                    $scope.formRequest.listRequirementItems = sortedCollection;

                    //Widget Management
                    $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                        if (item.productSNo !== $scope.requirementItems.productSNo && item.widgetState !== -1 && item.productIDorName !== '') { //&& item.productCode !== ''
                            item.widgetState = 0;
                        }

                        //if (item.isNonCoreProductCategory != 1 && item.productSNo === $scope.requirementItems.productSNo && type != "NON_CORE") {
                        //    $scope.addFabric(index);
                        //    $scope.addProcess(index);
                        //    $scope.addAccessories(index, 'SEWING');
                        //}
                    });
                    //^Widget Management
                } else {
                    swal("Error!", "Cannot add more than 200 items", "error");
                    return;
                }
               
            };

            $scope.CloneItem = function (product, type) {
                var listRequirementItemsTemp = $filter('filter')($scope.formRequest.listRequirementItems, { isDeleted: 0 });
                if (listRequirementItemsTemp.length < 200) {
                    if (type == 'CLONE') {
                        var productCount = $scope.formRequest.listRequirementItems.length;
                        var NewproductCount = $scope.formRequest.listRequirementItems.length + 1;
                        var NewProduct = angular.copy(product);

                        var prodCount = _.maxBy($scope.formRequest.listRequirementItems, function (o) { return o.productSNo; })
                        NewProduct.productSNo = parseInt(prodCount.productSNo + 1);
                        NewProduct.itemID = 0;
                        //NewProduct.AddFabricArray.forEach(function (fab,Idx) {
                        //    fab.currentTime = "/Date(1561000200000+0530)/";
                        //})
                        //NewProduct.apparelFabricJson = JSON.stringify(NewProduct.AddFabricArray);

                        //NewProduct.AddProcessArray.forEach(function (fab, Idx) {
                        //    fab.currentTime = "/Date(1561000200000+0530)/";
                        //})
                        //NewProduct.apparelProcessJson = JSON.stringify(NewProduct.AddProcessArray);

                        //NewProduct.AddAccessoriesArray.forEach(function (fab, Idx) {
                        //    fab.currentTime = "/Date(1561000200000+0530)/";
                        //})
                        //NewProduct.apparelSundriesJson = JSON.stringify(NewProduct.AddAccessoriesArray);

                        $scope.formRequest.listRequirementItems.push(NewProduct);

                        var sortedCollection = $scope.formRequest.listRequirementItems.sort(function (a, b) {
                            return a.isDeleted - b.isDeleted;

                        });

                        $scope.formRequest.listRequirementItems = sortedCollection;
                    }
                } else {
                    swal("Error!", "Cannot add more than 200 items", "error");
                    return;
                }
                
            }

            $scope.handleWindowState = function (product, state) {
                // if (product.productCode !== '') { //item.productIDorName !== ''
                if (product.productIDorName !== '') { //item.productIDorName !== ''
                    product.widgetState = state;
                } else {
                    swal("Error!", "Item details cannot be empty", "error");
                }
            }

            $scope.AddOtherRequirementItem = function (nonCoreProduct) {
                var item = {};
                if (nonCoreProduct) {
                    var output = $scope.productsList.filter(function (product) {
                        return (String(product.prodCode).toUpperCase().includes(nonCoreProduct.prodCode.toUpperCase()) == true);
                    });
                    if (output) {
                        nonCoreProduct.doHide = true;
                        item = output[0];
                    }
                }

                if (item) {
                    var index = 0;
                    item.isNonCoreProductCategory = 1;
                    if (!$scope.formRequest.listRequirementItems[$scope.formRequest.listRequirementItems.length - 1].productCode) {
                        $scope.AddItem("NON_CORE");
                        index = $scope.formRequest.listRequirementItems.length - 1;

                    } else {
                        $scope.AddItem("NON_CORE");
                        index = $scope.formRequest.listRequirementItems.length - 1;
                    }

                    $scope.fillTextbox(item, index);
                }
            };

            $scope.deleteItem = function (product) {
                //$scope.formRequest.listRequirementItems.splice(SNo, 1);

                if (!$scope.isEdit) {
                    if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 1) {
                        $scope.formRequest.listRequirementItems = _.filter($scope.formRequest.listRequirementItems, function (x) { return x.productSNo !== product.productSNo; });
                    };
                } else {
                    product.isDeleted = 1;
                }

                var sortedCollection = $scope.formRequest.listRequirementItems.sort(function (a, b) {
                    return a.isDeleted - b.isDeleted; 
                       
                });
                

                $scope.formRequest.listRequirementItems = sortedCollection;

                $scope.nonCoreproductsList.forEach(function (item, index) {
                    if (item.prodNo === product.productNo) {
                        item.doHide = false;
                    }
                });

                if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length === 1
                    && $scope.formRequest.listRequirementItems[0].isNonCoreProductCategory === 1) {
                    location.reload();
                }
            };



            $scope.getFile1 = function (id, itemid, ext) {
                $scope.file = $("#" + id)[0].files[0];

                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {
                        if (id == "itemsAttachment") {
                            if (ext != "xlsx") {
                                swal("Error!", "File type should be XSLX. Please download the template and  fill values accordingly.", "error");
                                return;
                            }
                            var bytearray = new Uint8Array(result);
                            $scope.formRequest.itemsAttachment = $.makeArray(bytearray);
                            if (!$scope.isEdit) {
                                $scope.formRequest.listRequirementItems = [];
                            }

                            $scope.postRequest(0, 1, false);
                            //$scope.formRequest.itemsAttachmentName = $scope.file.name;
                        }

                        if (id == "requirementItemsSveAttachment") {
                            if (ext != "xlsx") {
                                swal("Error!", "File type should be XSLX. Please download the template and  fill values accordingly.", "error");
                                return;
                            }

                            var bytearray = new Uint8Array(result);
                            $scope.uploadRequirementItemsSaveExcel($.makeArray(bytearray));
                            $scope.file = [];
                            $scope.file.name = '';
                            return;
                        }

                        if (id != "itemsAttachment") {
                            var bytearray = new Uint8Array(result);
                            var arrayByte = $.makeArray(bytearray);
                            var ItemFileName = $scope.file.name;
                            var index = _.indexOf($scope.formRequest.listRequirementItems, _.find($scope.formRequest.listRequirementItems, function (o) { if (o.isDeleted == 0) { return o.productSNo == id; } }));
                            var obj = $scope.formRequest.listRequirementItems[index];
                            obj.itemAttachment = arrayByte;
                            obj.attachmentName = ItemFileName;
                            $scope.formRequest.listRequirementItems.splice(index, 1, obj);
                        }


                    });
            }

            $scope.getSubFile1 = function (id, itemid, ext, itemtype, mainid) {

                $scope.file = $("#" + id)[0].files[0];

                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {

                        if (itemtype == "fabricAttachment") {
                            var bytearray = new Uint8Array(result);
                            var arrayByte = $.makeArray(bytearray);
                            var ItemFileName = $scope.file.name;
                            var item = _.filter($scope.formRequest.listRequirementItems, function (o) { return o.productSNo == mainid; });
                            var obj_item = _.filter(item[0].AddFabricArray, function (i, Idx) { return Idx == itemid; });
                            var obj = obj_item[0];
                            obj.FILE_ID = arrayByte;
                            obj.FILE_NAME = ItemFileName;;
                        }

                        if (itemtype == "processAttachment") {
                            var bytearray = new Uint8Array(result);
                            var arrayByte = $.makeArray(bytearray);
                            var ItemFileName = $scope.file.name;
                            var item = _.filter($scope.formRequest.listRequirementItems, function (o) { return o.productSNo == mainid; });
                            var obj_item = _.filter(item[0].AddProcessArray, function (i, Idx) { return Idx == itemid; });
                            var obj = obj_item[0];
                            obj.FILE_ID = arrayByte;
                            obj.FILE_NAME = ItemFileName;;
                        }
                        if (itemtype == "sundriesAttachment") {
                            var bytearray = new Uint8Array(result);
                            var arrayByte = $.makeArray(bytearray);
                            var ItemFileName = $scope.file.name;
                            var item = _.filter($scope.formRequest.listRequirementItems, function (o) { return o.productSNo == mainid; });
                            var obj_item = _.filter(item[0].AddAccessoriesArray, function (i, Idx) { return Idx == itemid; });
                            var obj = obj_item[0];
                            obj.FILE_ID = arrayByte;
                            obj.FILE_NAME = ItemFileName;;
                        }
                    });
            }


            $scope.uploadRequirementItemsSaveExcel = function (fileContent) {
                var params = {
                    reqID: $scope.auctionItem ? $scope.auctionItem.requirementID : 0,
                    isrfp: $scope.formRequest ? $scope.formRequest.isRFP : false,
                    userID: userService.getUserId(),
                    compId: userService.getUserCompanyId(),
                    sessionID: userService.getUserToken(),
                    requirementItemsAttachment: fileContent,
                    templateid: $scope.selectedTemplate.TEMPLATE_ID ? $scope.selectedTemplate.TEMPLATE_ID : 0
                };
                auctionsService.uploadRequirementItemsSaveExcel(params)
                    .then(function (response) {
                        $("#requirementItemsSveAttachment").val(null);
                        if (response && response.length > 0 && response.length <= 200) {

                            if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                $scope.formRequest.listRequirementItems.forEach(function (reqItem, index) {
                                    if (!reqItem.isNonCoreProductCategory && reqItem.productCode) {
                                        var filteredReqItems = response.filter(function (excelItem) {
                                            return (reqItem.productId == excelItem.catalogueItemID || reqItem.catalogueItemID == excelItem.catalogueItemID);
                                        });

                                        if (filteredReqItems && filteredReqItems.length <= 0) {
                                            reqItem.isDeleted = 1;
                                        }
                                    }
                                });
                            }

                            response.forEach(function (item, index) {
                                let maxSnoItemNo = 1;
                                if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                    maxSnoItemNo = $scope.formRequest.listRequirementItems.length;
                                    maxSnoItem = _.maxBy($scope.formRequest.listRequirementItems, 'productSNo');
                                    maxSnoItemNo = maxSnoItem ? maxSnoItem.productSNo : $scope.formRequest.listRequirementItems.length;
                                }

                                $scope.itemnumber = maxSnoItemNo;
                                //let itemIndex = $scope.formRequest.listRequirementItems ? $scope.formRequest.listRequirementItems.length : 1;
                                let tempRequirementItem =
                                {
                                    productSNo: index+1,
                                    ItemID: 0,
                                    productIDorName: item.productIDorName,
                                    productNo: item.productNo,
                                    hsnCode: item.hsnCode,
                                    productDescription: item.productDescription,
                                    productQuantity: item.productQuantity,
                                    productQuantityIn: item.productQuantityIn,
                                    productBrand: item.productBrand,
                                    productDeliveryDetails: item.productDeliveryDetails,
                                    othersBrands: '',
                                    isDeleted: 0,
                                    productImageID: 0,
                                    attachmentName: '',
                                    //budgetID: 0,
                                    //bcInfo: '',
                                    productCode: item.productCode,
                                    splitenabled: 0,
                                    fromrange: 0,
                                    torange: 0,
                                    requiredQuantity: 0,
                                    I_LLP_DETAILS: "",
                                    itemAttachment: "",
                                    itemMinReduction: 0,
                                    ARTICLE_TYPE: item.ARTICLE_TYPE,
                                    ARTICLE_TYPE_TEMP: item.ARTICLE_TYPE,
                                    GENDER: item.GENDER,
                                    GENDER_TEMP: item.GENDER,
                                    BRANDS: item.BRANDS,
                                    BRANDS_TEMP: item.BRANDS,
                                    SEASON: item.SEASON,
                                    BUY_PLAN_MONTH: item.BUY_PLAN_MONTH,
                                    buyerTargetCost: item.buyerTargetCost,
                                    isCoreProductCategory: 1,
                                    productId: item.catalogueItemID,
                                    AddFabricArray: [],
                                    AddProcessArray: [],
                                    AddAccessoriesArray: [],
                                    catalogueItemID: item.catalogueItemID,
                                    fabricPrice: item.fabricPrice,
                                    fabricquality: item.fabricquality
                                };

                                //if ($scope.brands.length > 0) {
                                //    $scope.brands.forEach(function (b) {
                                //        if (item.BRANDS == b) {
                                //            tempRequirementItem.BRANDS = b;
                                //        }
                                        
                                //    })
                                //} 

                                if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                    var existingItem = $scope.formRequest.listRequirementItems.filter(function (currentItem) {
                                        //return (currentItem.productId == tempRequirementItem.productId || currentItem.catalogueItemID == tempRequirementItem.productId);
                                        return (currentItem.productCode == tempRequirementItem.productCode);
                                    });

                                    if (existingItem && existingItem.length > 0 && !$scope.formRequest.isRFP) {
                                        existingItem[0].productQuantity = tempRequirementItem.productQuantity;
                                        existingItem[0].isDeleted = 0;
                                    } else {
                                        $scope.formRequest.listRequirementItems.push(tempRequirementItem);
                                    }
                                } else {
                                    $scope.formRequest.listRequirementItems.push(tempRequirementItem);
                                }
                            });

                            if ($scope.formRequest && $scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                if (!$scope.formRequest.listRequirementItems[0].productCode) {
                                    $scope.formRequest.listRequirementItems[0].isDeleted = 1;
                                    $scope.formRequest.listRequirementItems.splice(0, 1);
                                }

                                $scope.formRequest.listRequirementItems = _.sortBy($scope.formRequest.listRequirementItems, function (o) {
                                    if (o.isNonCoreProductCategory) { return o.isNonCoreProductCategory; } else { return 0; }
                                });

                                let index = 0;
                                $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                                    if (!item.isDeleted && !item.isNonCoreProductCategory) {
                                        $scope.GetProductQuotationTemplate(item.productId, index, true);
                                    }
                                    //if (item.isNonCoreProductCategory != 1) {
                                    //    $scope.addFabric(index);
                                    //    $scope.addProcess(index);
                                    //    $scope.addAccessories(index, 'SEWING');
                                    //}
                                    index++;
                                });
                            }

                            swal("Verify", "Please verify and Submit the requirement!", "success");
                        } else {
                            swal("Error!", "Cannot add more than 200 items", "error");
                            return;
                        }
                    });
            };

            $scope.removeSpecialCharecter = function (val) {
                var value = '';
                if (val != null || val != undefined) {
                    val = val.replace(/\'/gi, " ");
                    val = val.replace(/\"/gi, " ");
                    val = val.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, " ");
                    val = val.replace(/(\r\n|\n|\r)/gm, " ");
                    val = val.replace(/\t/g, '');
                    value = val;
                }
                
                return value;
            }

            $scope.pageNo = 1;
            $scope.returnIfError = false;
            $scope.nextpage = function (pageNo) {
                $scope.returnIfError = false;
                $scope.tableValidation = false;
                $scope.tableValidationMsg = '';
                $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = departmentsValidation = false;
                $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation = $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = $scope.remindersTimeIntervalValidation = $scope.questionnaireValidation = false;

                $scope.titleValidation = $scope.attachmentNameValidation = false;
                $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = false;
                $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation = $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = $scope.questionnaireValidation = $scope.expNegotiationTimeValidation = false;

                if (pageNo == 1 || pageNo == 4) {
                    $scope.errorInfo = {};
                    if ($scope.reqDepartments == null || $scope.reqDepartments == undefined || $scope.reqDepartments.length == 0) {
                        $scope.GetReqDepartments();
                    }

                    if ($scope.currencies == null || $scope.currencies == undefined || $scope.currencies.length == 0) {
                        //$scope.getCurrencies();
                    }

                    if ($scope.formRequest.title == null || $scope.formRequest.title == '') {
                        $scope.titleValidation = true;
                        $scope.postRequestLoding = false;
                        scrollToElement('titleDiv');
                        return false;
                    }

                    var requirementItems = $scope.formRequest.listRequirementItems.filter(function (item) {
                        return !item.isDeleted;
                    });

                    if (!requirementItems || requirementItems.length <= 0) {
                        swal("Verify", "Cannot create requirement with no items.", "error");
                        return false;
                    }

                    if ($scope.formRequest.isTabular) {
                        $scope.selectedProducts = [];

                        $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                            let mainSectionId = 'sectionMain' + index;
                            if ($scope.tableValidation || item.isDeleted) {
                                return false;
                            }


                            var sno = parseInt(index) + 1;

                            item.fabricDescription = $scope.removeSpecialCharecter(item.fabricDescription);
                            //if (item.fabricDescription) {
                            //    item.fabricDescription = item.fabricDescription.replace(/\'/gi, " ");
                            //    item.fabricDescription = item.fabricDescription.replace(/\"/gi, " ");
                            //    item.fabricDescription = item.fabricDescription.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, " ");
                            //    item.fabricDescription = item.fabricDescription.replace(/(\r\n|\n|\r)/gm, " ");
                            //    item.fabricDescription = item.fabricDescription.replace(/\t/g, '');
                            //}
                            item.productDescription = $scope.removeSpecialCharecter(item.productDescription);
                            item.processDescription = $scope.removeSpecialCharecter(item.processDescription);
                            

                            if ((item.BRANDS == null || item.BRANDS == '') && item.isNonCoreProductCategory != 1) {
                                $scope.tableValidation = true;
                                $scope.tableValidationMsg = $scope.errorInfo[mainSectionId] = 'Please enter Brand for item: ' + sno;
                                scrollToElement(mainSectionId, item);
                                return;
                            }

                            if ((item.GENDER == null || item.GENDER == '') && item.isNonCoreProductCategory != 1) {
                                $scope.tableValidation = true;
                                $scope.tableValidationMsg = $scope.errorInfo[mainSectionId] = 'Please enter Gender for item: ' + sno;
                                scrollToElement(mainSectionId, item);
                                return;
                            }

                            if ((item.ARTICLE_TYPE == null || item.ARTICLE_TYPE == '') && item.isNonCoreProductCategory != 1) {
                                $scope.tableValidation = true;
                                $scope.tableValidationMsg = $scope.errorInfo[mainSectionId] = 'Please enter Article Type for item: ' + sno;
                                scrollToElement(mainSectionId, item);
                                return;
                            }

                            if (item.productCode == null || item.productCode == '') {
                                $scope.tableValidation = true;
                                $scope.tableValidationMsg = $scope.errorInfo[mainSectionId] = 'Please enter Article Number for item: ' + sno;
                                scrollToElement(mainSectionId, item);
                                return;
                            }

                            if (item.productNo == null || item.productNo == '') {
                                $scope.tableValidation = true;
                                $scope.tableValidationMsg = $scope.errorInfo[mainSectionId] = 'Please enter Style ID for item: ' + sno;
                                scrollToElement(mainSectionId, item);
                                return;
                            }

                            if (item.productQuantity <= 0 && item.isNonCoreProductCategory != 1) {
                                $scope.tableValidation = true;
                                $scope.tableValidationMsg = $scope.errorInfo[mainSectionId] = 'Please enter Quantity for item: ' + sno;
                                scrollToElement(mainSectionId, item);
                                return;
                            } else
                                if (item.productQuantity > 0 && item.isNonCoreProductCategory != 1) {
                                    if (item.productQuantity < 150) {
                                        $scope.tableValidation = true;
                                        $scope.tableValidationMsg = $scope.errorInfo[mainSectionId] = 'Please enter Quantity above 150 for item: ' + sno;
                                        scrollToElement(mainSectionId, item);
                                        return;
                                    }
                                }

                            if ((item.productQuantityIn == null || item.productQuantityIn == '') && item.isNonCoreProductCategory != 1) {
                                $scope.tableValidation = true;
                                $scope.tableValidationMsg = $scope.errorInfo[mainSectionId] = 'Please enter Units for item: ' + sno;
                                scrollToElement(mainSectionId, item);
                                return;
                            }


                            //if ((item.fabricPrice <= 0 || !item.fabricPrice) && item.isNonCoreProductCategory != 1) {
                            //    $scope.tableValidation = true;
                            //    $scope.tableValidationMsg = $scope.errorInfo[mainSectionId] = 'Please enter Fabric Price for item: ' + sno;
                            //    scrollToElement(mainSectionId, item);
                            //    return;
                            //}


                            //if ((item.fabricquality == null || item.fabricquality == '') && item.isNonCoreProductCategory != 1) {
                            //    $scope.tableValidation = true;
                            //    $scope.tableValidationMsg = $scope.errorInfo[mainSectionId] = 'Please enter Fabric Quality for item: ' + sno;
                            //    scrollToElement(mainSectionId, item);
                            //    return;
                            //}

                            if (item.isNonCoreProductCategory != 1 && 1==2) {
                                if (item.AddFabricArray) {
                                    $scope.errorInfo = {};
                                    item.AddFabricArray.forEach(function (fab, fabIdx) {
                                        //if ($scope.errorInfo && Object.keys($scope.errorInfo).length > 0) {
                                        //    return false;
                                        //}

                                        //if (fab.fabricError != "" && fab.fabricError != undefined) {
                                        //    //swal("Error!", fab.fabricError, "error");
                                        //    $scope.returnIfError = true;
                                        //    return false;
                                        //}
                                        let fabricSectionName = 'sectionFabric-' + index + '-' + fabIdx;
                                        fabIdx = fabIdx + 1;

                                        fab.DESCRIPTION = $scope.removeSpecialCharecter(fab.DESCRIPTION);
                                        fab.NOMINATED_SUPPLIER = $scope.removeSpecialCharecter(fab.NOMINATED_SUPPLIER);
                                        if (fab.IS_FABRIC_NOMINATED == 0) {
                                            fab.MILL = $scope.removeSpecialCharecter(fab.MILL);
                                            fab.DESIGN_STYLE = $scope.removeSpecialCharecter(fab.DESIGN_STYLE);
                                            fab.WEAVE_KNITS = $scope.removeSpecialCharecter(fab.WEAVE_KNITS);
                                            fab.PATTERN_TYPE = $scope.removeSpecialCharecter(fab.PATTERN_TYPE);
                                            fab.CONSTRUCTION = $scope.removeSpecialCharecter(fab.CONSTRUCTION);
                                            fab.COMPOSITION = $scope.removeSpecialCharecter(fab.COMPOSITION);
                                        }
                                        
                                        
                                        
                                        //if (fab.NAME == null || fab.NAME == '' || fab.NAME == undefined) {
                                        //    $scope.tableValidation = true;
                                        //    $scope.tableValidationMsg = $scope.errorInfo[fabricSectionName] = 'Please select Fabric Type for fabric item ' + fabIdx + ' in product ' + sno;
                                        //} else

                                        //    if (fab.MILL == null || fab.MILL == '' || fab.MILL == undefined) {
                                        //        $scope.tableValidation = true;
                                        //        $scope.tableValidationMsg = $scope.errorInfo[fabricSectionName] = 'Please enter Mill for fabric item ' + fabIdx + ' in product ' + sno;
                                        //    } else

                                        //        if (fab.DESIGN_STYLE == null || fab.DESIGN_STYLE == '' || fab.DESIGN_STYLE == undefined) {
                                        //            $scope.tableValidation = true;
                                        //            $scope.tableValidationMsg = $scope.errorInfo[fabricSectionName] = 'Please enter Design/Style for fabric item ' + fabIdx + ' in product ' + sno;
                                        //        } else

                                        //            if (fab.UOM == null || fab.UOM == '' || fab.UOM == undefined) {
                                        //                $scope.tableValidation = true;
                                        //                $scope.tableValidationMsg = $scope.errorInfo[fabricSectionName] = 'Please enter UOM for fabric item ' + fabIdx + ' in product ' + sno;
                                        //            }
                                    });

                                    //if ($scope.errorInfo && Object.keys($scope.errorInfo).length > 0) {
                                    //    scrollToElement(Object.keys($scope.errorInfo)[0], item);
                                    //    return;
                                    //}
                                }

                                if (item.AddProcessArray) {
                                    $scope.errorInfo = {};
                                    item.AddProcessArray.forEach(function (fab, fabIdx) {
                                        //if ($scope.errorInfo && Object.keys($scope.errorInfo).length > 0) {
                                        //    return false;
                                        //}
                                        //if (fab.processError != "" && fab.processError != undefined) {
                                        //    //swal("Error!", fab.fabricError, "error");
                                        //    $scope.returnIfError = true;
                                        //    return false;
                                        //}
                                        let processSectionName = 'sectionProcess-' + index + '-' + fabIdx;
                                        fabIdx = fabIdx + 1;

                                        fab.PROCESS_DISCRIPTION = $scope.removeSpecialCharecter(fab.PROCESS_DISCRIPTION);
                                        fab.EMBROIDERY_NAME = $scope.removeSpecialCharecter(fab.EMBROIDERY_NAME);
                                        fab.APPLIQUE_NAME = $scope.removeSpecialCharecter(fab.APPLIQUE_NAME);
                                        fab.EMBELLISHMENT_NAME = $scope.removeSpecialCharecter(fab.EMBELLISHMENT_NAME);
                                        fab.NOMINATED_SUPPLIER = $scope.removeSpecialCharecter(fab.NOMINATED_SUPPLIER);
                                        
                                        //if (fab.PROCESS_TYPE == null || fab.PROCESS_TYPE == '' || fab.PROCESS_TYPE == undefined) {
                                        //    $scope.tableValidation = true;
                                        //    $scope.tableValidationMsg = $scope.errorInfo[processSectionName] = 'Please enter Process Type for process item ' + fabIdx + ' in product ' + sno;
                                        //}

                                        //if (fab.PROCESS_TYPE == 'Wash') {
                                        //    if (fab.PROCESS_CATEGORY == null || fab.PROCESS_CATEGORY == '' || fab.PROCESS_CATEGORY == undefined) {
                                        //        $scope.tableValidation = true;
                                        //        $scope.tableValidationMsg = $scope.errorInfo[processSectionName] = 'Please select Process Category for process item ' + fabIdx + ' in product ' + sno;
                                        //    } else

                                        //        if (fab.TYPE_OF_GARMENT == null || fab.TYPE_OF_GARMENT == '' || fab.TYPE_OF_GARMENT == undefined) {
                                        //            $scope.tableValidation = true;
                                        //            $scope.tableValidationMsg = $scope.errorInfo[processSectionName] = 'Please select Type of Garment for process item ' + fabIdx + ' in product ' + sno;
                                        //        } else

                                        //            if (fab.SelectedwashTypeArray <= 0) {
                                        //                $scope.tableValidation = true;
                                        //                $scope.tableValidationMsg = $scope.errorInfo[processSectionName] = 'Please select Wash Types for process item ' + fabIdx + ' in product ' + sno;
                                        //            } else

                                        //                if (fab.UOM == null || fab.UOM == '' || fab.UOM == undefined) {
                                        //                    $scope.tableValidation = true;
                                        //                    $scope.tableValidationMsg = $scope.errorInfo[processSectionName] = 'Please enter UOM for process item ' + fabIdx + ' in product ' + sno;
                                        //                }

                                        //}

                                        //if (fab.PROCESS_TYPE == 'Print') {
                                        //    if (fab.PRINT_TYPE == null || fab.PRINT_TYPE == '' || fab.PRINT_TYPE == undefined) {
                                        //        $scope.tableValidation = true;
                                        //        $scope.tableValidationMsg = $scope.errorInfo[processSectionName] = 'Please select Print Type for process item ' + fabIdx + ' in product ' + sno;
                                        //    } else

                                        //        if (fab.COLORS == null || fab.COLORS == '' || fab.COLORS == undefined) {
                                        //            $scope.tableValidation = true;
                                        //            $scope.tableValidationMsg = $scope.errorInfo[processSectionName] = 'Please select Colors for process item ' + fabIdx + ' in product ' + sno;
                                        //        } else

                                        //            if (fab.SIZES == null || fab.SIZES == '' || fab.SIZES == undefined) {
                                        //                $scope.tableValidation = true;
                                        //                $scope.tableValidationMsg = $scope.errorInfo[processSectionName] = 'Please select Sizes for process item ' + fabIdx + ' in product ' + sno;
                                        //            } else
                                        //                if (fab.UOM == null || fab.UOM == '' || fab.UOM == undefined) {
                                        //                    $scope.tableValidation = true;
                                        //                    $scope.tableValidationMsg = $scope.errorInfo[processSectionName] = 'Please enter UOM for process item ' + fabIdx + ' in product ' + sno;
                                        //                }
                                        //}
                                        //if (fab.PROCESS_TYPE == 'Embroidery') {
                                        //    if (fab.EMBROIDERY_NAME == null || fab.EMBROIDERY_NAME == '' || fab.EMBROIDERY_NAME == undefined) {
                                        //        $scope.tableValidation = true;
                                        //        $scope.tableValidationMsg = $scope.errorInfo[processSectionName] = 'Please enter Embroidery Name for process item ' + fabIdx + ' in product ' + sno;
                                        //    } else
                                        //        if (fab.UOM == null || fab.UOM == '' || fab.UOM == undefined) {
                                        //            $scope.tableValidation = true;
                                        //            $scope.tableValidationMsg = $scope.errorInfo[processSectionName] = 'Please enter UOM for process item ' + fabIdx + ' in product ' + sno;
                                        //        }
                                        //}
                                        //if (fab.PROCESS_TYPE == 'Applique') {
                                        //    if (fab.APPLIQUE_NAME == null || fab.APPLIQUE_NAME == '' || fab.APPLIQUE_NAME == undefined) {
                                        //        $scope.tableValidation = true;
                                        //        $scope.tableValidationMsg = $scope.errorInfo[processSectionName] = 'Please enter Applique Name for process item ' + fabIdx + ' in product ' + sno;
                                        //    } else
                                        //        if (fab.UOM == null || fab.UOM == '' || fab.UOM == undefined) {
                                        //            $scope.tableValidation = true;
                                        //            $scope.tableValidationMsg = $scope.errorInfo[processSectionName] = 'Please enter UOM for process item ' + fabIdx + ' in product ' + sno;
                                        //        }
                                        //}
                                        //if (fab.PROCESS_TYPE == 'Embellishment') {
                                        //    if (fab.EMBELLISHMENT_NAME == null || fab.EMBELLISHMENT_NAME == '' || fab.EMBELLISHMENT_NAME == undefined) {
                                        //        $scope.tableValidation = true;
                                        //        $scope.tableValidationMsg = $scope.errorInfo[processSectionName] = 'Please enter Embellishment Name for process item ' + fabIdx + ' in product ' + sno;
                                        //    } else
                                        //        if (fab.UOM == null || fab.UOM == '' || fab.UOM == undefined) {
                                        //            $scope.tableValidation = true;
                                        //            $scope.tableValidationMsg = $scope.errorInfo[processSectionName] = 'Please enter UOM for process item ' + fabIdx + ' in product ' + sno;
                                        //        }
                                        //}
                                    });

                                    //if ($scope.errorInfo && Object.keys($scope.errorInfo).length > 0) {
                                    //    scrollToElement(Object.keys($scope.errorInfo)[0], item);
                                    //    return;
                                    //}
                                }

                                if (item.AddAccessoriesArray) {
                                    $scope.errorInfo = {};
                                    item.AddAccessoriesArray.forEach(function (fab, fabIdx) {
                                        //if ($scope.errorInfo && Object.keys($scope.errorInfo).length > 0) {
                                        //    return false;
                                        //}

                                        fab.STYLING = $scope.removeSpecialCharecter(fab.STYLING);
                                        fab.NOMINATED_SUPPLIER = $scope.removeSpecialCharecter(fab.NOMINATED_SUPPLIER);
                                        
                                        let sundriesSectionName = 'sectionSundries-' + index + '-' + fabIdx;
                                        fabIdx = fabIdx + 1;
                                        //if (fab.TYPE == "SEWING") {
                                        //    if (fab.UOM == null || fab.UOM == '' || fab.UOM == undefined) {
                                        //        $scope.tableValidation = true;
                                        //        $scope.tableValidationMsg = $scope.errorInfo[sundriesSectionName] = 'Please enter UOM for sewing item: ' + fabIdx;
                                        //    }
                                        //}

                                        //if (fab.TYPE == "PACKAGING") {
                                        //    if (fab.UOM == null || fab.UOM == '' || fab.UOM == undefined) {
                                        //        $scope.tableValidation = true;
                                        //        $scope.tableValidationMsg = $scope.errorInfo[sundriesSectionName] = 'Please enter UOM for packaging item: ' + fabIdx;
                                        //    }
                                        //}
                                    });

                                    //if ($scope.errorInfo && Object.keys($scope.errorInfo).length > 0) {
                                    //    scrollToElement(Object.keys($scope.errorInfo)[0], item);
                                    //    return;
                                    //}
                                }
                            }

                            

                            //if (item.productIDorName == null || item.productIDorName == '') {
                            //    $scope.tableValidation = true;
                            //    $scope.tableValidationMsg = $scope.errorInfo[mainSectionId] = 'Please enter Article Number/Name for item: ' + sno;
                            //    scrollToElement(mainSectionId, item);
                            //    return;
                            //}
                            else {
                                $scope.selectedProducts.push(item.catalogueItemID);

                                subItemsArray = [];

                                //var productQuotationTemplateFiltered = 
                                var productQuotationTemplateFiltered = _.filter(item.productQuotationTemplate, function (o) {
                                    return o.IS_VALID == 1;
                                });
                                if (subItemsArray && subItemsArray != null && subItemsArray != undefined && subItemsArray.length > 0) {
                                    subItemsArray.forEach(function (subitems, subitemsindex) {
                                        productQuotationTemplateFiltered.push(subitems);
                                    })
                                }

                                if (productQuotationTemplateFiltered != null && productQuotationTemplateFiltered != undefined && productQuotationTemplateFiltered.length > 0) {
                                    item.productQuotationTemplateJson = JSON.stringify(productQuotationTemplateFiltered);
                                    //  item.productQuotationTemplateJsonCallBack = JSON.stringify(productQuotationTemplateFiltered);
                                } else {
                                    item.productQuotationTemplateJson = '';
                                }

                                if ($scope.isEdit) {
                                    item.productQuotationTemplate = [];
                                    if (!item.isNonCoreProductCategory || item.isCoreProductCategory) {
                                        $scope.GetProductQuotationTemplate(item.catalogueItemID, index, true);
                                    } else {
                                        $scope.formRequest.listRequirementItems = _.sortBy($scope.formRequest.listRequirementItems, function (o) {
                                            if (o.isNonCoreProductCategory) { return o.isNonCoreProductCategory; } else { return 0; }
                                        });
                                    }
                                }

                            }


                            if (item.productQuantity <= 0 || item.productQuantity == undefined || item.productQuantity == '') {
                                $scope.tableValidation = true;
                                $scope.tableValidationMsg = $scope.errorInfo[mainSectionId] = 'Please enter Quantity for item: ' + sno;
                                scrollToElement(mainSectionId, item);
                                return;
                            }


                            if ($scope.formRequest.isSplitEnabled == true) {
                                if (item.fromrange == null || item.fromrange == '' || item.fromrange <= 0) {
                                    $scope.tableValidation = true;
                                    $scope.tableValidationMsg = 'Please enter From Range for item: ' + sno;
                                    return;
                                }
                                if (item.torange == null || item.torange == '' || item.torange <= 0) {
                                    $scope.tableValidation = true;
                                    $scope.tableValidationMsg = 'Please enter To Range for item: ' + sno;
                                    return;
                                }

                                if (parseFloat(item.fromrange) > parseFloat(item.torange)) {
                                    $scope.tableValidation = true;
                                    $scope.tableValidationMsg = 'Please make sure that To Range is higher than From Range for item: ' + sno;
                                    return;
                                }
                            }
                        });

                        if ($scope.returnIfError) {
                            swal("Error!", "Please verify the given details", "error");
                            return;
                        }


                        if ($scope.tableValidation) {
                            return false;
                        }

                    }
                    if (!$scope.formRequest.isTabular) {
                        if ($scope.formRequest.description == null || $scope.formRequest.description == '') {
                            $scope.descriptionValidation = true;
                            $scope.postRequestLoding = false;
                            return false;
                        }
                    }


                    var phn = '';
                    if ($scope.userObj && $scope.userObj.phoneNum) {
                        phn = $scope.userObj.phoneNum;
                        $scope.formRequest.phoneNum = phn;
                        $scope.formRequest.contactDetails = phn;
                    }
                    else {
                        $scope.formRequest.phoneNum = '';
                        $scope.formRequest.contactDetails = '';
                    }



                }

                if (pageNo == 2 || pageNo == 4) {



                    if ($scope.questionnaireList == null || $scope.questionnaireList == undefined || $scope.questionnaireList.length == 0) {
                        $scope.getQuestionnaireList();
                    }

                    if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                        var d = new Date();
                        d.setHours(18, 00, 00);
                        d = new moment(d).format("DD-MM-YYYY HH:mm");
                        $scope.formRequest.quotationFreezTime = d;

                    }


                    if ($scope.formRequest.expStartTime == null || $scope.formRequest.expStartTime == '') {
                        var dt = new Date();
                        var tomorrowNoon = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate() + 1, 12, 0, 0);
                        tomorrowNoon = new moment(tomorrowNoon).format("DD-MM-YYYY HH:mm");
                        $scope.formRequest.expStartTime = tomorrowNoon;

                    }

                    if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                        $scope.formRequest.noOfQuotationReminders = 3;
                    }
                    if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                        $scope.formRequest.remindersTimeInterval = 2;
                    }
                    if ($scope.formRequest.deliveryTime == null || $scope.formRequest.deliveryTime == '') {
                        $scope.deliveryTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    //if ($scope.formRequest.deliveryLocation == null || $scope.formRequest.deliveryLocation == '') {
                    //    $scope.deliveryLocationValidation = true;
                    //    $scope.postRequestLoding = false;
                    //    return false;
                    //}
                    //if ($scope.formRequest.paymentTerms == null || $scope.formRequest.paymentTerms == '') {
                    //    $scope.paymentTermsValidation = true;
                    //    $scope.postRequestLoding = false;
                    //    return false;
                    //}


                    if ($scope.reqDepartments.length > 0) {

                        $scope.departmentsValidation = true;

                        $scope.reqDepartments.forEach(function (item, index) {
                            if (item.isValid == true || $scope.noDepartments == true)
                                $scope.departmentsValidation = false;
                        });

                        if ($scope.departmentsValidation == true) {
                            return false;
                        };
                    }

                }

                if (pageNo == 3 || pageNo == 4) {

                    //if ($scope.categories == null || $scope.categories == undefined || $scope.categories.length == 0) {
                    //    $scope.getCategories();
                    //}
                    $scope.getproductvendors();

                    if ($scope.formRequest.urgency == null || $scope.formRequest.urgency == '') {
                        $scope.urgencyValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }


                    if (($scope.formRequest.quotationPriceLimit == null || $scope.formRequest.quotationPriceLimit == '' || $scope.formRequest.quotationPriceLimit <= 0) && $scope.formRequest.isQuotationPriceLimit == true) {
                        $scope.quotationPriceLimitValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                        $scope.noOfQuotationRemindersValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                        $scope.remindersTimeIntervalValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.isTechEval == true && (!$scope.selectedQuestionnaire || $scope.selectedQuestionnaire.evalID <= 0)) {
                        $scope.questionnaireValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                        $scope.quotationFreezTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }

                    var ts = moment($scope.formRequest.quotationFreezTime, "DD-MM-YYYY HH:mm").valueOf();
                    var m = moment(ts);
                    var quotationFreezTime = new Date(m);

                    var ts = moment($scope.formRequest.expStartTime, "DD-MM-YYYY HH:mm").valueOf();
                    var m = moment(ts);
                    var expStartTime = new Date(m);

                    auctionsService.getdate()
                        .then(function (GetDateResponse) {
                            //var CurrentDate = moment(new Date(parseInt(GetDateResponse.substr(6))));
                            var CurrentDateToLocal = userService.toLocalDate(GetDateResponse);
                            var ts = moment(CurrentDateToLocal, "DD-MM-YYYY HH:mm").valueOf();
                            var m = moment(ts);
                            var deliveryDate = new Date(m);
                            var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                            var CurrentDateToTicks = "/Date(" + milliseconds + "000+0530)/";
                            var CurrentDate = moment(new Date(parseInt(CurrentDateToTicks.substr(6))));
                            $scope.currentSystemDate = CurrentDate;

                            var CurrentDateOnlyLocal = userService.toLocalDate(GetDateResponse);
                            var ts1 = moment(CurrentDateOnlyLocal, "DD-MM-YYYY").valueOf();
                            var m1 = moment(ts1);
                            var deliveryDate1 = new Date(m1);
                            var milliseconds1 = parseInt(deliveryDate1.getTime() / 1000.0);
                            var CurrentDateOnlyTicks = "/Date(" + milliseconds1 + "000+0530)/";
                            var CurrentDateOnly = moment(new Date(parseInt(CurrentDateOnlyTicks.substr(6))));
                            $scope.currentSystemDateOnly = CurrentDateOnly;

                            var ts = moment(CurrentDateToLocal, "DD-MM-YYYY HH:mm").valueOf();
                            var m = moment(ts);
                            var deliveryDate = new Date(m);
                            var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                            var CurrentDateToTicks = "/Date(" + milliseconds + "000+0530)/";

                            var CurrentDate = moment(new Date(parseInt(CurrentDateToTicks.substr(6))));
                            //console.log("------------start-----------");
                            //console.log("quotationFreezTime>>>>>>>>>>" + quotationFreezTime);
                            //console.log("------------end-------------");
                            //console.log("CurrentDate>>>>>>>>>>>>>>>>>" + CurrentDate);
                            if (quotationFreezTime < CurrentDate) {
                                $scope.quotationFreezTimeValidation = true;
                                $scope.postRequestLoding = false;
                                return false;
                            }
                            else
                                if (quotationFreezTime >= expStartTime) {
                                    $scope.expNegotiationTimeValidation = true;
                                    $scope.postRequestLoding = false;
                                    return false;
                                }

                            if ($scope.selectedCurrency) {
                                let selecCurrencyTemp = _.filter($scope.companyCurrencies, function (currItem) { return currItem.currency === $scope.selectedCurrency.value; });
                                if (selecCurrencyTemp && selecCurrencyTemp.length > 0) {
                                    if (quotationFreezTime > selecCurrencyTemp[0].StartTimeLocal && expStartTime > selecCurrencyTemp[0].StartTimeLocal &&
                                        expStartTime <= selecCurrencyTemp[0].EndTimeLocal && expStartTime <= selecCurrencyTemp[0].EndTimeLocal) {

                                    } else {

                                        swal({
                                            title: "Error!",
                                            title: "Selected currency conversion rate has expired, please validate.",
                                            type: "error",
                                            showCancelButton: false,
                                            confirmButtonColor: "#DD6B55",
                                            confirmButtonText: "Ok",
                                            closeOnConfirm: true
                                        },
                                            function () {
                                                return false;
                                            });

                                        return false;
                                    }
                                }
                            }

                            $scope.pageNo = $scope.pageNo + 1;
                            //  console.log("page NO:::::::::::" + $scope.pageNo);

                        })

                }

                if (pageNo == 4) {
                    $scope.formRequest.subcategoriesView = '';
                    for (i = 0; i < $scope.sub.selectedSubcategories.length; i++) {
                        $scope.formRequest.subcategoriesView += $scope.sub.selectedSubcategories[i].subcategory + ",";
                    }
                    $scope.pageNo = $scope.pageNo + 1;
                }

                if (pageNo != 3) {
                    $scope.pageNo = $scope.pageNo + 1;
                }

                // return $scope.pageNo;
                // location.reload();
            }

            $scope.previouspage = function () {
                $scope.pageNo = $scope.pageNo - 1;
                // location.reload();
                //return $scope.pageNo;
            }

            $scope.gotopage = function (pageNo) {
                $scope.pageNo = pageNo;


                return $scope.pageNo;
            }


            $scope.reqDepartments = [];
            $scope.userDepartments = [];

            $scope.GetReqDepartments = function () {
                $scope.reqDepartments = [];
                auctionsService.GetReqDepartments(userService.getUserId(), $scope.stateParamsReqID, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.reqDepartments = response;

                            $scope.noDepartments = true;

                            $scope.reqDepartments.forEach(function (item, index) {
                                if (item.isValid == true) {
                                    $scope.noDepartments = false;
                                }
                            });

                        }
                    });
            }

            // $scope.GetReqDepartments();


            $scope.SaveReqDepartments = function (reqID) {

                $scope.reqDepartments.forEach(function (item, index) {
                    item.reqID = reqID;
                    item.userID = userService.getUserId();
                })

                var params = {
                    "listReqDepartments": $scope.reqDepartments,
                    sessionID: userService.getUserToken()
                };
                if ($scope.formRequest.isForwardBidding) {
                    fwdauctionsService.SaveReqDepartments(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {

                            }
                        });
                } else {
                    auctionsService.SaveReqDepartments(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {

                            }
                        });
                }


            };

            $scope.noDepartments = false;


            $scope.noDepartmentsFunction = function (value) {

                $scope.departmentsValidation = false;

                if (value == 'NA') {
                    $scope.reqDepartments.forEach(function (item, index) {
                        item.isValid = false;
                    });
                }
                else {
                    $scope.reqDepartments.forEach(function (item, index) {
                        if (item.isValid == true)
                            $scope.noDepartments = false;
                    });
                }
            };


            $scope.reqDeptDesig = [];

            $scope.GetReqDeptDesig = function () {
                $scope.reqDeptDesig = [];
                auctionsService.GetReqDeptDesig(userService.getUserId(), $scope.stateParamsReqID, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.reqDeptDesig = response;
                            $scope.noDepartments = true;
                            $scope.reqDeptDesig.forEach(function (item, index) {
                                if (item.isValid == true) {
                                    $scope.noDepartments = false;
                                }
                            });
                        }
                    });
            };

            // $scope.GetReqDeptDesig();


            $scope.SaveReqDeptDesig = function (reqID) {

                $scope.reqDeptDesig.forEach(function (item, index) {
                    item.reqID = reqID;
                    item.userID = userService.getUserId();
                })

                var params = {
                    "listReqDepartments": $scope.reqDeptDesig,
                    sessionID: userService.getUserToken()
                };

                if ($scope.formRequest.isForwardBidding) {
                    fwdauctionsService.SaveReqDeptDesig(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {

                            }
                        });
                } else {
                    auctionsService.SaveReqDeptDesig(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                //growlService.growl(".", "success");
                                //$scope.GetCompanyDepartments();
                                //$scope.addnewdeptView = false;
                                //$scope.department = {
                                //    userID: $scope.userID,
                                //    deptID: 0,
                                //    sessionID: $scope.sessionID
                                //};
                                ////$window.history.back();
                            }
                        });
                }


            };

            $scope.AssignVendorToCompany = function (vendorPhone, vendorEmail) {

                if ($scope.newVendor.altPhoneNum == undefined) {
                    $scope.newVendor.altPhoneNum = '';
                }
                if ($scope.newVendor.altEmail == undefined) {
                    $scope.newVendor.altEmail = '';
                }

                var params = {
                    userID: userService.getUserId(),
                    vendorPhone: vendorPhone,
                    vendorEmail: vendorEmail,
                    sessionID: userService.getUserToken(),
                    category: $scope.formRequest.category,
                    subCategory: $scope.selectedSubCategoriesList,
                    altPhoneNum: $scope.newVendor.altPhoneNum,
                    altEmail: $scope.newVendor.altEmail
                };

                auctionsService.AssignVendorToCompany(params)
                    .then(function (response) {

                        $scope.vendor = response;

                        if ($scope.vendor.errorMessage == '' || $scope.vendor.errorMessage == 'VENDOR ADDED SUCCESSFULLY') {
                            $scope.formRequest.auctionVendors.push({ vendorName: $scope.vendor.firstName + ' ' + $scope.vendor.lastName, companyName: $scope.vendor.email, vendorID: $scope.vendor.userID });
                            $scope.newVendor = {};
                            $scope.checkVendorPhoneUniqueResult = false;
                            $scope.checkVendorEmailUniqueResult = false;
                            $scope.checkVendorPanUniqueResult = false;
                            $scope.checkVendorTinUniqueResult = false;
                            $scope.checkVendorStnUniqueResult = false;
                            $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
                            growlService.growl('Vendor Added Successfully', "success");
                        }
                        else {
                            $scope.newVendor = {};
                            $scope.checkVendorPhoneUniqueResult = false;
                            $scope.checkVendorEmailUniqueResult = false;
                            $scope.checkVendorPanUniqueResult = false;
                            $scope.checkVendorTinUniqueResult = false;
                            $scope.checkVendorStnUniqueResult = false;
                            $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
                            growlService.growl($scope.vendor.errorMessage, "inverse");
                        }
                    });
            };



            $scope.searchvendorstring = '';

            $scope.searchVendors = function (value) {
                $scope.Vendors = _.filter($scope.VendorsTemp, function (item) { return item.companyName.toUpperCase().indexOf(value.toUpperCase()) > -1; });
                // $scope.vendorsLoaded = true;
                // $scope.Vendors = _.filter($scope.allCompanyVendors, function (item) { return item.companyName.toUpperCase().indexOf(value.toUpperCase()) > -1; });
                // var temp = $scope.formRequest.auctionVendors;
                // $scope.formRequest.auctionVendors.forEach(function (item, index) {
                // $scope.Vendors = _.filter($scope.Vendors, function (vendor) { return vendor.vendorID !== item.vendorID; });
                // });
            }


            $scope.exportItemsToExcel = function () {
                var mystyle = {
                    sheetid: 'RequirementDetails',
                    headers: true,
                    column: {
                        style: 'font-size:15px;background:#233646;color:#FFF;'
                    }
                };

                alasql('SELECT itemID as [ItemID], productIDorName as [ArticleName], productNo as [ArticleID], productDescription as [Description], productQuantity as [Quantity], productQuantityIn as [Units], productBrand as PreferredBrand, othersBrands as OtherBrands INTO XLSX(?,{headers:true,sheetid: "RequirementDetails", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["RequirementDetails.xlsx", $scope.formRequest.listRequirementItems]);
            }

            $scope.exportRequirementItemsToExcel = function () {
                var name = 'REQUIREMENT_SAVE';
                reportingService.downloadTemplate(name, userService.getUserId(),'','', $scope.stateParamsReqID);
            };

            $scope.cancelClick = function () {
                $window.history.back();
            }




            $scope.paymentRadio = false;
            $scope.paymentlist = [];

            $scope.addpaymentvalue = function () {
                var listpaymet =
                {
                    reqTermsID: 0,
                    reqID: $scope.stateParamsReqID,
                    userID: userService.getUserId(),
                    reqTermsDays: 0,
                    reqTermsPercent: 0,
                    reqTermsType: 'PAYMENT',
                    paymentType: '+',
                    isRevised: 0
                };
                $scope.paymentlist.push(listpaymet);
            };

            $scope.delpaymentvalue = function (val, id) {

                if (id > 0) {
                    $scope.listTerms.push(id);
                }

                $scope.paymentlist.splice(val, 1);
            };

            $scope.resetpayment = function () {
                $scope.paymentlist = [];
            };




            $scope.deliveryRadio = false;
            $scope.deliveryList = [];

            $scope.adddeliveryvalue = function () {
                var deliveryObj =
                {
                    reqTermsID: 0,
                    reqID: $scope.stateParamsReqID,
                    userID: userService.getUserId(),
                    reqTermsDays: 0,
                    reqTermsPercent: 0,
                    reqTermsType: 'DELIVERY',
                    isRevised: 0
                };
                $scope.deliveryList.push(deliveryObj);
            };

            $scope.deldeliveryvalue = function (val, id) {

                if (id > 0) {
                    $scope.listTerms.push(id);
                }
                $scope.deliveryList.splice(val, 1);
            };

            $scope.resetdelivery = function () {
                $scope.deliveryList = [];
            };










            $scope.listRequirementTerms = [];

            $scope.SaveRequirementTerms = function (reqID) {

                $scope.listRequirementTerms = [];

                $scope.deliveryList.forEach(function (item, index) {
                    item.reqID = reqID;
                    $scope.listRequirementTerms.push(item);
                });

                $scope.paymentlist.forEach(function (item, index) {

                    item.reqID = reqID;

                    if (item.paymentType == '-') {
                        item.reqTermsDays = -(item.reqTermsDays);
                    }

                    $scope.listRequirementTerms.push(item);
                });

                var params = {
                    "listRequirementTerms": $scope.listRequirementTerms,
                    sessionID: userService.getUserToken()
                };
                if ($scope.formRequest.isForwardBidding) {
                    fwdauctionsService.SaveReqDepartments(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                //growlService.growl(".", "success");
                                //$scope.GetCompanyDepartments();
                                //$scope.addnewdeptView = false;
                                //$scope.department = {
                                //    userID: $scope.userID,
                                //    deptID: 0,
                                //    sessionID: $scope.sessionID
                                //};
                                ////$window.history.back();
                            }
                        });
                } else {
                    auctionsService.SaveRequirementTerms(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                //growlService.growl(".", "success");
                                //$scope.GetCompanyDepartments();
                                //$scope.addnewdeptView = false;
                                //$scope.department = {
                                //    userID: $scope.userID,
                                //    deptID: 0,
                                //    sessionID: $scope.sessionID
                                //};
                                ////$window.history.back();
                            }
                        });
                }


            };




            $scope.GetRequirementTerms = function () {
                auctionsService.GetRequirementTerms(userService.getUserId(), $scope.stateParamsReqID, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.listRequirementTerms = response;

                            $scope.listRequirementTerms.forEach(function (item, index) {
                                if (item.reqTermsType == 'DELIVERY') {
                                    $scope.deliveryList.push(item);
                                    $scope.deliveryRadio = true;
                                }
                                else if (item.reqTermsType == 'PAYMENT') {

                                    if (item.reqTermsDays > 0) {
                                        item.paymentType = '+';
                                    }
                                    else if (item.reqTermsDays < 0) {
                                        item.paymentType = '-';
                                        item.reqTermsDays = -(item.reqTermsDays);
                                    }

                                    $scope.paymentlist.push(item);
                                    $scope.paymentRadio = true;
                                }
                            });



                        }
                    });
            }

            // $scope.GetRequirementTerms();




            $scope.listTerms = [];

            $scope.DeleteRequirementTerms = function () {

                var params = {
                    "listTerms": $scope.listTerms,
                    sessionID: userService.getUserToken()
                };

                if ($scope.formRequest.isForwardBidding) {
                    fwdauctionsService.SaveReqDepartments(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {

                            }
                        });
                } else {
                    auctionsService.DeleteRequirementTerms(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {

                            }
                        });
                }
            };




            $scope.paymentTypeFunction = function (type) {
                if (type == 'ADVANCE') {

                }
                if (type == 'POST') {

                }
            }







            $scope.GetCIJList = function () {
                auctionsService.GetCIJList($scope.compID, $scope.sessionid)
                    .then(function (response) {
                        $scope.cijList = response;

                        $scope.cijList.forEach(function (item, index) {
                            var cijObj = $.parseJSON(item.cij);
                            item.cij = cijObj;
                        });


                    })
            }


            // $scope.GetCIJList();



            $scope.GetIndentList = function () {
                auctionsService.GetIndentList(userService.getUserCompanyId(), userService.getUserToken())
                    .then(function (response) {
                        $scope.indentList = response;
                    })
            }

            // $scope.GetIndentList();


            $scope.changeIndent = function (val) {
            };

            $scope.formRequest.category = '';

            $scope.goToStage = function (stage, currentStage) {
                $scope.postRequestLoding = true;
                $scope.tableValidation = $scope.validationFailed = false;
                if (stage == 2 && currentStage == 1) {

                }
                $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                    if (item.productQuantity <= 0 || item.productQuantity == undefined || item.productQuantity == '') {
                        $scope.tableValidation = true;
                        $scope.validationFailed = true;
                    }
                });
                if ($scope.tableValidation) {
                    $scope.postRequestLoding = false;
                    return false;
                }

                if ($scope.formRequest.title == null || $scope.formRequest.title == '') {
                    $scope.titleValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }

                if (currentStage == 2 && stage > currentStage) {
                    if ($scope.formRequest.deliveryLocation == null || $scope.formRequest.deliveryLocation == '') {
                        $scope.deliveryLocationValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.paymentTerms == null || $scope.formRequest.paymentTerms == '') {
                        $scope.paymentTermsValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.deliveryTime == null || $scope.formRequest.deliveryTime == '') {
                        $scope.deliveryTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                }
                if (currentStage == 3 && stage > currentStage) {
                    if ($scope.formRequest.urgency == null || $scope.formRequest.urgency == '') {
                        $scope.urgencyValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }

                    if (($scope.formRequest.quotationPriceLimit == null || $scope.formRequest.quotationPriceLimit == '' || $scope.formRequest.quotationPriceLimit <= '') && $scope.formRequest.isQuotationPriceLimit == true) {
                        $scope.quotationPriceLimitValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.isQuotationPriceLimit == false) {
                        $scope.formRequest.quotationPriceLimit = 0;
                    }

                    if ($scope.formRequest.noOfQuotationReminders > 0) {
                        $scope.formRequest.noOfQuotationReminders = parseInt($scope.formRequest.noOfQuotationReminders);
                    }
                    if ($scope.formRequest.remindersTimeInterval > 0) {
                        $scope.formRequest.remindersTimeInterval = parseInt($scope.formRequest.remindersTimeInterval);
                    }


                    if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                        $scope.noOfQuotationRemindersValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                        $scope.remindersTimeIntervalValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.isTechEval == true && (!$scope.selectedQuestionnaire || $scope.selectedQuestionnaire.evalID <= 0)) {
                        $scope.questionnaireValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                        $scope.quotationFreezTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                }
                if (!$scope.postRequestLoding) {
                    return false;
                } else {
                    $scope.postRequestLoding = true;
                    var ts = userService.toUTCTicks($scope.formRequest.quotationFreezTime);
                    var m = moment(ts);
                    var quotationDate = new Date(m);
                    var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                    $scope.formRequest.quotationFreezTime = "/Date(" + milliseconds + "000+0530)/";
                    var ts = userService.toUTCTicks($scope.formRequest.expStartTime);
                    var m = moment(ts);
                    var quotationDate = new Date(m);
                    var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                    $scope.formRequest.expStartTime = "/Date(" + milliseconds + "000+0530)/";

                    $scope.formRequest.requirementID = $stateParams.Id ? $stateParams.Id : -1;
                    $scope.formRequest.customerID = userService.getUserId();
                    $scope.formRequest.customerFirstname = userService.getFirstname();
                    $scope.formRequest.customerLastname = userService.getLastname();
                    $scope.formRequest.isClosed = "NOTSTARTED";
                    $scope.formRequest.endTime = "";
                    $scope.formRequest.sessionID = userService.getUserToken();
                    $scope.formRequest.subcategories = "";
                    $scope.formRequest.budget = 100000;
                    $scope.formRequest.custCompID = userService.getUserCompanyId();
                    $scope.formRequest.customerCompanyName = userService.getUserCompanyId();

                    for (i = 0; i < $scope.sub.selectedSubcategories.length; i++) {
                        $scope.formRequest.subcategories += $scope.sub.selectedSubcategories[i].subcategory + ",";
                    }

                    var category = [];
                    if ($scope.formRequest.category) {
                        category.push($scope.formRequest.category);
                    }
                    $scope.formRequest.category = category;
                    if (!$scope.formRequest.isForwardBidding) {
                        $scope.formRequest.cloneID = 0;
                        if ($stateParams.reqObj && $stateParams.reqObj.cloneId && $stateParams.reqObj.cloneId !== "") {
                            $scope.formRequest.cloneID = $stateParams.reqObj.cloneId;
                        }

                        $scope.formRequest.listRequirementItems.forEach(function (product, index) {
                            if (product.productQuotationTemplate && product.productQuotationTemplate.length > 0) {
                                product.productQuotationTemplate.forEach(function (subItem, index) {
                                    if (subItem) {
                                        $scope.saveSubItemOptions(subItem);
                                    }
                                });
                            }
                        });

                        auctionsService.postrequirementdata($scope.formRequest)
                            .then(function (response) {
                                if (response.objectID != 0) {
                                    if ($scope.selectedProject) {
                                        $scope.selectedProject.sessionID = userService.getUserToken();
                                        $scope.selectedProject.requirement = {};
                                        $scope.selectedProject.requirement.requirementID = response.objectID;
                                        var param = { item: $scope.selectedProject };
                                        //prmCompanyService.SaveProjectRequirement(param).then(function (projecgtMappingResponse) {
                                        //    console.log("saving project mapping");
                                        //    console.log(projecgtMappingResponse);
                                        //    console.log("END saving project mapping");
                                        //});
                                    }
                                    $scope.SaveReqDepartments(response.objectID);
                                    //$scope.SaveReqDeptDesig(response.objectID);

                                    //$scope.DeleteRequirementTerms();
                                    // $scope.SaveRequirementTerms(response.objectID);

                                    if ($scope.selectedQuestionnaire != null && $scope.selectedQuestionnaire != undefined && $scope.selectedQuestionnaire.evalID != undefined) {
                                        techevalService.getquestionnaire($scope.selectedQuestionnaire.evalID, 1)
                                            .then(function (response1) {
                                                $scope.selectedQuestionnaire = response1;

                                                if ($scope.isTechEval) {
                                                    $scope.selectedQuestionnaire.reqID = response.objectID;
                                                }
                                                else {
                                                    $scope.selectedQuestionnaire.reqID = 0;
                                                }
                                                $scope.selectedQuestionnaire.sessionID = userService.getUserToken();
                                                var params = {
                                                    questionnaire: $scope.selectedQuestionnaire
                                                }
                                                techevalService.assignquestionnaire(params)
                                                    .then(function (response) {
                                                    })
                                            })
                                    }
                                    if (stage) {
                                        $state.go('save-requirement', { Id: response.objectID });
                                    }
                                }
                            });
                    } else {
                        fwdauctionsService.postrequirementdata($scope.formRequest)
                            .then(function (response) {
                                if (response.objectID != 0) {

                                    //$scope.SaveReqDepartments(response.objectID);
                                    $scope.SaveReqDeptDesig(response.objectID);

                                    swal({
                                        title: "Done!",
                                        text: "Requirement Created Successfully",
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    },
                                        function () {
                                            $scope.postRequestLoding = false;
                                            //$state.go('view-requirement');
                                            $state.go('fwd-view-req', { 'Id': response.objectID });
                                        });
                                }
                            });
                    }
                }
                //$scope.postRequest(0, stage - 1, false, stage);                
            }

            $scope.goBack = function (stage, reqID) {
                $state.go('save-requirement', { Id: reqID });
            }

            $scope.removeAttach = function (index) {
                $scope.formRequest.multipleAttachments.splice(index, 1);
            }

            //$scope.productsList = [];
            //$scope.nonCoreproductsList = [];
            //catalogService.getUserProducts().then(function (response) {
            //    $scope.productsList = response;
            //    $scope.nonCoreproductsList = $scope.productsList.filter(function (product) {
            //        return product.isCoreProductCategory == 0;
            //    });

            //    // re-set values to back;
            //    if ($scope.nonCoreproductsList && $scope.nonCoreproductsList.length > 0) {
            //        $scope.nonCoreproductsList.forEach(function (product, index) {
            //            if (!$scope.isEdit) {
            //                product.doHide = true;
            //                //  $scope.AddOtherRequirementItem(product);
            //            }
            //        });
            //    }
            //});

            // Pagination For User Products//
            $scope.userProductsPage = 0;
            $scope.userProductsPageSize = 200;
            $scope.userProductsfetchRecordsFrom = $scope.userProductsPage * $scope.userProductsPageSize;
            $scope.userProductstotalCount = 0;
            // Pagination For User Products//

            $scope.productsList = [];
            $scope.nonCoreproductsList = [];
            $scope.getUserProducts = function (IsPaging, searchString, index, fieldType) {

                if ($scope.productsList.length <= 0 && !searchString) {
                    if ($scope.nonCoreproductsList && $scope.nonCoreproductsList.length > 0) {
                        $scope.nonCoreproductsList.forEach(function (product, index) {
                            if (!$scope.isEdit) {
                                product.doHide = true;
                            }

                        });
                    }

                } else {
                    catalogService.getUserProducts($scope.userProductsfetchRecordsFrom, $scope.userProductsPageSize, searchString ? searchString : "").then(function (response) {
                        $scope.productsList = response;
                        $scope.userproductsLoading = false;

                        $scope.fillingProduct($scope.productsList, index, fieldType, searchString);

                        //$scope.nonCoreproductsList = $scope.productsList.filter(function (product) {
                        //    return product.isCoreProductCategory == 0;
                        //});

                        //re-set values to back;

                    });
                }
            };

            $scope.fillingProduct = function (output, index, fieldType, searchString) {

                output = $scope.productsList.filter(function (product) {
                    //if(product.isCoreProductCategory === 1) {
                    //    if (fieldType == 'NAME') {
                    //        return (String(product.prodName).toUpperCase().includes(searchString) == true);
                    //    }
                    //    else if (fieldType == 'CODE') {
                    //        return (String(product.prodNo).toUpperCase().includes(searchString) == true);
                    //    } 
                    //    else if (fieldType == 'CODEMAIN') {
                    //        return (String(product.prodCode).toUpperCase().includes(searchString) == true);
                    //    }
                    //}
                    return product.isCoreProductCategory === 1;
                });


                if (fieldType == 'NAME') {
                    $scope["filterProducts_" + index] = output;
                    // $scope.formRequest.listRequirementItems[index].productIDorName = searchString;
                }
                else if (fieldType == 'CODE') {
                    $scope["filterProducts_Code_" + index] = output;
                    // $scope.formRequest.listRequirementItems[index].productNo = searchString;
                }
                else if (fieldType == 'CODEMAIN') {
                    $scope["filterProducts_Code1_" + index] = output;
                    // $scope.formRequest.listRequirementItems[index].productCode = searchString;
                }
            }

            //$scope.productsList = catalogService.getUserProducts();
            //$scope.nonCoreproductsList = $scope.productsList.filter(function (product) {
            //    return product.isCoreProductCategory == 0;
            //});
            //$scope.getProducts = function () {
            //    catalogService.getUserProducts($scope.compID, userService.getUserId())
            //        .then(function (response) {
            //            $scope.productsList = response;
            //            $scope.nonCoreproductsList = $scope.productsList.filter(function (product) {
            //                return product.isCoreProductCategory == 0;
            //            });
            //            console.log($scope.nonCoreproductsList);
            //        });
            //};

            //$scope.getProducts();

            // in form catalogue ctrl
            $scope.fillTextbox = function (selProd, index) {
                $scope['ItemSelected_' + index] = true;
                $scope.formRequest.listRequirementItems[index].productIDorName = selProd.prodName;
                $scope.formRequest.listRequirementItems[index].catalogueItemID = selProd.prodId;
                $scope.formRequest.listRequirementItems[index].productNo = selProd.articleType;
                // $scope.formRequest.listRequirementItems[index].ARTICLE_TYPE = selProd.ARTICLE_TYPE;
                $scope.formRequest.listRequirementItems[index].hsnCode = selProd.prodHSNCode;
                $scope.formRequest.listRequirementItems[index].productDescription = selProd.prodDesc;
                $scope.formRequest.listRequirementItems[index].productCode = selProd.prodCode;
                //$scope.formRequest.listRequirementItems[index].productQuantityInList = []; //;
                //$scope.formRequest.listRequirementItems[index].productQuantityInList.push(selProd.prodQty);
                //  $scope.formRequest.listRequirementItems[index].productQuantityIn = selProd.prodQty;
                $scope.formRequest.listRequirementItems[index].productQuantityIn = "NUMBER";
                if (selProd.isNonCoreProductCategory) {
                    $scope.formRequest.listRequirementItems[index].productQuantity = 1;
                }
                $scope.formRequest.listRequirementItems[index].isNonCoreProductCategory = selProd.isNonCoreProductCategory;
                $scope['filterProducts_' + index] = null;
                $scope["filterProducts_Code_" + index] = null;
                $scope["filterProducts_Code1_" + index] = null;

                if (!selProd.isNonCoreProductCategory || selProd.isCoreProductCategory) {
                    $scope.GetProductQuotationTemplate(selProd.prodId, index, true);
                } else {
                    $scope.formRequest.listRequirementItems = _.sortBy($scope.formRequest.listRequirementItems, function (o) {
                        if (o.isNonCoreProductCategory) { return o.isNonCoreProductCategory; } else { return 0; }
                    });
                }
                $scope.postRequestLoding = false;
                $scope.AddProductRespId = 0;
                $scope.formRequest.listRequirementItems[index].responseError = '';
            }

            $scope.autofillProduct = function (prodName, index, fieldType) {
                $scope['ItemSelected_' + index] = false;
                var output = [];
                if (prodName && prodName != '' && prodName.length > 0) {
                    $scope.searchingUserProducts = angular.lowercase(prodName);
                    $scope.userproductsLoading = true;
                    $scope.getUserProducts(0, $scope.searchingUserProducts, index, fieldType);
                    //prodName = String(prodName).toUpperCase();
                    //output = $scope.productsList.filter(function (product) {
                    //    if (product.isCoreProductCategory === 1) {
                    //        if (fieldType == 'NAME') {
                    //            return (String(product.prodName).toUpperCase().includes(prodName) == true);
                    //        }
                    //        else if (fieldType == 'CODE') {
                    //            return (String(product.prodNo).toUpperCase().includes(prodName) == true);
                    //        }
                    //        else if (fieldType == 'CODEMAIN') {
                    //            return (String(product.prodCode).toUpperCase().includes(prodName) == true);
                    //        }
                    //    }
                    //});
                } 

                if (output.length == 0) {
                    if (fieldType == 'NAME') {
                        //$scope.formRequest.listRequirementItems[index].productCode = "";
                    }
                    if (fieldType == 'CODEMAIN') {
                       // $scope.formRequest.listRequirementItems[index].productIDorName = "";
                    }
                   // $scope.formRequest.listRequirementItems[index].productIDorName = "";
                    $scope.formRequest.listRequirementItems[index].productNo = "";
                    
                    $scope.formRequest.listRequirementItems[index].catalogueItemID = 0;
                    $scope.formRequest.listRequirementItems[index].hsnCode = "";
                    $scope.formRequest.listRequirementItems[index].productDescription = "";
                    $scope.formRequest.listRequirementItems[index].processDescription = "";
                    $scope.formRequest.listRequirementItems[index].fabricDescription = "";
                    $scope.showAddProduct = true;
                }
                if (fieldType == 'NAME') {
                    $scope["filterProducts_" + index] = output;
                }
                else if (fieldType == 'CODE') {
                    $scope["filterProducts_Code_" + index] = output;
                }
                else if (fieldType == 'CODEMAIN') {
                    $scope["filterProducts_Code1_" + index] = output;
                }
                 $scope.postRequestLoding = false;
                $scope.AddProductRespId = 0;
                $scope.formRequest.listRequirementItems[index].responseError = '';
            }

            $scope.onBlurProduct = function (index, type, text) {
                if ($scope['ItemSelected_' + index] == false) {
                    if (type != 'NAME' && type != 'CODEMAIN') // !$scope.formRequest.isRFP &&
                    {
                        $scope.formRequest.listRequirementItems[index].productIDorName = "";
                        $scope.formRequest.listRequirementItems[index].productNo = "";
                        $scope.formRequest.listRequirementItems[index].productCode = "";
                    }

                }

                if (text == "" || text == null || text == undefined) {
                    $scope.formRequest.listRequirementItems[index].productIDorName = "";
                    $scope.formRequest.listRequirementItems[index].productNo = "";
                    $scope.formRequest.listRequirementItems[index].productCode = "";
                    $scope.formRequest.listRequirementItems[index].catalogueItemID = 0;
                    $scope.formRequest.listRequirementItems[index].hsnCode = "";
                    $scope.formRequest.listRequirementItems[index].productDescription = "";
                    $scope.formRequest.listRequirementItems[index].processDescription = "";
                    $scope.formRequest.listRequirementItems[index].fabricDescription = "";
                    $scope.showAddProduct = true;
                }
                //$scope['filterProducts_' + index] = null;
            }




            $scope.formSplitChange = function () {
                var splitenabled = $scope.formRequest.isSplitEnabled;
                $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                    item.splitenabled = splitenabled;
                    if (splitenabled) {
                        item.productQuantity = 1;
                        $scope.formRequest.isDiscountQuotation = 0;
                    }
                });
            }

            $scope.onSplitChange = function (index) {


                if ($scope.formRequest.listRequirementItems[index].splitenabled == true) {
                    $scope.formRequest.listRequirementItems[index].productQuantity = 1;
                }
            }

            $scope.getCatalogCategories = function () {
                auctionsService.getcatalogCategories(userService.getUserId())
                    .then(function (response) {
                        $scope.companyCatalogueList = response;

                        $scope.catName = $scope.companyCatalogueList[0];
                        $scope.searchCategoryVendors($scope.catName);

                    });
            };

            $scope.searchCategoryVendors = function (str) {
                $scope.getvendors(str);
            };

            $scope.getproductvendors = function () {
                $scope.vendorsLoaded = false;
                $scope.Vendors = [];

                //products.push($scope.formRequest.category);
                //$scope.formRequest.category = category;
                //if ($scope.selectedProducts.length > 0) {
                var params = { 'products': $scope.selectedProducts, 'sessionID': userService.getUserToken(), 'uID': userService.getUserId(), evalID: $scope.isTechEval ? $scope.selectedQuestionnaire.evalID : 0 };
                $http({
                    method: 'POST',
                    url: domain + 'getvendorsbyproducts',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                }).then(function (response) {
                    if (response && response.data) {
                        if (response.data.length > 0) {
                            $scope.Vendors = response.data;
                            // if (allVendors === 1) {
                            // $scope.allCompanyVendors = response.data;
                            // allVendors = 0;
                            // }  
                            $scope.vendorsLoaded = true;
                            for (var j in $scope.formRequest.auctionVendors) {
                                for (var i in $scope.Vendors) {
                                    if ($scope.Vendors[i].vendorName == $scope.formRequest.auctionVendors[j].vendorName) {
                                        $scope.Vendors.splice(i, 1);
                                    }
                                }
                            }

                            $scope.VendorsTemp = $scope.Vendors;
                            if ($scope.searchvendorstring != '') {
                                $scope.searchVendors($scope.searchvendorstring);
                            } else {
                                $scope.searchVendors('');
                            }


                        }
                        $scope.getCatalogCategories();
                        //$scope.formRequest.auctionVendors =[];
                    } else {
                    }
                }, function (result) {
                });
                //}

                //$scope.getCatalogCategories();

            };

            $scope.getCurrencies();



            /*region start WORKFLOW*/

            $scope.getWorkflows = function () {
                workflowService.getWorkflowList()
                    .then(function (response) {
                        $scope.workflowList = [];
                        $scope.workflowListTemp = response;
                        $scope.workflowListTemp.forEach(function (item, index) {
                            if (item.WorkflowModule == $scope.WorkflowModule) {
                                $scope.workflowList.push(item);
                            }
                        });

                        //if (userService.getUserObj().isSuperUser) {
                        //    $scope.workflowList = $scope.workflowList;
                        //}
                        //else {
                        //    $scope.workflowList = $scope.workflowList.filter(function (item) {
                        //        return item.deptID == userService.getSelectedUserDeptID();

                        //    });
                        //}



                    });
            };
            //form Ctrl
            // $scope.getWorkflows();

            $scope.getItemWorkflow = function () {
                workflowService.getItemWorkflow(0, $stateParams.Id, $scope.WorkflowModule)
                    .then(function (response) {
                        $scope.itemWorkflow = response;
                        if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                            $scope.currentStep = 0;

                            var count = 0;

                            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {

                                if (track.status == 'APPROVED' || track.status == 'HOLD') {
                                    $scope.isFormdisabled = true;
                                }

                                if (track.status == 'APPROVED') {
                                    $scope.isWorkflowCompleted = true;
                                    $scope.orderInfo = track.order;
                                    $scope.assignToShow = track.status;

                                }
                                else {
                                    $scope.isWorkflowCompleted = false;
                                }



                                if (track.status == 'REJECTED' && count == 0) {
                                    count = count + 1;
                                }

                                if ((track.status == 'PENDING' || track.status == 'HOLD') && count == 0) {
                                    count = count + 1;
                                    $scope.IsUserApproverForStage(track.approverID);
                                    $scope.currentAccess = track.order;
                                }

                                if ((track.status == 'PENDING' || track.status == 'HOLD' || track.status == 'REJECTED') && $scope.currentStep == 0) {
                                    $scope.currentStep = track.order;
                                    return false;
                                }
                            });
                        }
                    });

            };

            $scope.updateTrack = function (step, status) {

                $scope.commentsError = '';

                if ($scope.isReject) {
                    $scope.commentsError = 'Please Save Rejected Items/Qty';
                    return false;
                }

                if (status == 'REJECTED' && (step.comments == null || step.comments == "")) {
                    $scope.commentsError = 'Please enter comments';
                    return false;
                }

                step.status = status;
                step.sessionID = $scope.sessionID;
                step.modifiedBy = userService.getUserId();

                step.moduleName = $scope.WorkflowModule;

                workflowService.SaveWorkflowTrack(step)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            $scope.getItemWorkflow();
                            //location.reload();
                            //     $state.go('list-pr');
                        }
                    })
            };

            $scope.assignWorkflow = function (moduleID) {
                workflowService.assignWorkflow(({ wID: $scope.formRequest.workflowID, moduleID: moduleID, user: userService.getUserId(), sessionID: $scope.sessionID }))
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                            $scope.isSaveDisable = false;
                        }
                        else {
                            //  $state.go('list-pr');
                        }
                    })
            };

            $scope.IsUserApprover = false;

            $scope.functionResponse = false;

            $scope.IsUserApproverForStage = function (approverID) {
                workflowService.IsUserApproverForStage(approverID, userService.getUserId())
                    .then(function (response) {
                        $scope.IsUserApprover = response;
                    });
            };

            $scope.isApproverDisable = function (index) {

                var disable = true;

                var previousStep = {};

                $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {

                    if (index == stepIndex) {
                        if (stepIndex == 0) {
                            if (userService.getLocalDeptDesigt().deptId == step.department.deptID && userService.getLocalDeptDesigt().desigId == step.approver.desigID &&
                                (step.status == 'PENDING' || step.status == 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                        else if (stepIndex > 0) {
                            if (previousStep.status == 'PENDING' || previousStep.status == 'HOLD' || previousStep.status == 'REJECTED') {
                                disable = true;
                            }
                            else if (userService.getLocalDeptDesigt().deptId == step.department.deptID && userService.getLocalDeptDesigt().desigId == step.approver.desigID &&
                                (step.status == 'PENDING' || step.status == 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                    }
                    previousStep = step;
                })

                return disable;
            };

            /*region end WORKFLOW*/

            $scope.PRList1 = [];

            $scope.GetReqPRList = function () {
                var params = {
                    "userid": userService.getUserId(),
                    "deptid": $scope.deptID,
                    "desigid": $scope.desigID,
                    "depttypeid": $scope.deptTypeID,
                    "onlyopen": 1,
                    "sessionid": userService.getUserToken(),
                }
                PRMPRServices.getreqprlist(params)
                    .then(function (response) {
                        $scope.PRList = response;
                        $scope.PRList1 = response;
                        $scope.selectRequirementPRS();
                        $scope.PRList.forEach(function (item, index) {
                            item.isSelected = false;
                        })

                        // console.log("main array count>>>" + $scope.PRList.length);

                        $scope.PRList = $scope.PRList.filter(function (prObj) {
                            prObj.CREATED_DATE = userService.toLocalDate(prObj.CREATED_DATE);
                            prObj.DELIVERY_DATE = userService.toLocalDate(prObj.DELIVERY_DATE);
                            return prObj.PR_STATUS !== 'COMPLETED';
                        });
                        //  console.log("filtered array count>>>" + $scope.PRList.length);
                        if ($stateParams.prDetail && $stateParams.prDetail.PR_ID) {
                            $scope.formRequest.PR_ID = $stateParams.prDetail.PR_ID;
                            $scope.formRequest.PR_NUMBER = $stateParams.prDetail.PR_NUMBER;
                            $stateParams.prDetail.isSelected = true;
                            $scope.fillReqItems($stateParams.prDetail);
                            //$scope.GetPRItemsList({ PR_ID: $stateParams.prDetail.PR_ID });
                        }
                    })
            };

           // $scope.GetReqPRList();

            $scope.getPRNumber = function (pr_ID) {
                var params = {
                    "prid": pr_ID,
                    "sessionid": userService.getUserToken()
                };
                PRMPRServices.getprdetails(params)
                    .then(function (response) {
                        $scope.PRDetails = response;
                        if ($scope.PRDetails.PR_ID == pr_ID) {
                            //$scope.formRequest.PR_ID = $scope.PRDetails.PR_ID;
                            $scope.formRequest.PR_NUMBER = $scope.PRDetails.PR_NUMBER;
                        }
                    })
            }

            $scope.fillReqItems = function (pr) {
                $scope.formRequest.PR_ID = pr.PR_ID;
                if (pr.isSelected) {
                    $scope.GetPRItemsList(pr);
                } else if (!pr.isSelected) {
                    $scope.GetPRItemsList(pr);


                }

                $scope.PRList = _.orderBy($scope.PRList, ['isSelected'], ['desc']);
            }


            $scope.prDeleteItemsArr = [];

            $scope.GetPRItemsList = function (pr) {
                if (pr && pr.PR_ID > 0) {
                    var params = {
                        "prid": pr.PR_ID,
                        "sessionid": userService.getUserToken()
                    }
                    PRMPRServices.getpritemslist(params)
                        .then(function (response) {
                            $scope.PRItemsList = response;


                            // First check whether this PR exists in requirement items or not if exists then reduce the quantity and return the item//

                            $scope.prDeleteItemsArr = [];

                            if (!pr.isSelected) {
                                if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                    //var success = $scope.PRItemsList.every(function (val) {//pritemlist array
                                    //    if ($scope.formRequest.listRequirementItems.some(productIds => productIds.productId === val.PRODUCT_ID)) {// requirement items array 
                                    //        var index = $scope.formRequest.listRequirementItems.findIndex(c => c.productId === val.PRODUCT_ID);
                                    //        var deductQuantity = $scope.formRequest.listRequirementItems[index].productQuantity - val.REQUIRED_QUANTITY;
                                    //        $scope.formRequest.listRequirementItems[index].productQuantity = deductQuantity;
                                    //    }
                                    //});

                                    //$scope.deleteItem(pr);
                                    //return;
                                    $scope.PRItemsList.forEach(function (prItem, prIndex) {
                                        $scope.formRequest.listRequirementItems.forEach(function (rfqItem, rfqIndex) {
                                            if (prItem.PRODUCT_ID == rfqItem.productId) {
                                                var deductQuantity = rfqItem.productQuantity - prItem.REQUIRED_QUANTITY;
                                                rfqItem.productQuantity = deductQuantity;
                                                if (rfqItem.productQuantity <= 0) {
                                                    $scope.prDeleteItemsArr.push(rfqItem);
                                                }
                                            } else {
                                                if (prItem.PR_ID == rfqItem.PR_ID) {
                                                    $scope.prDeleteItemsArr.push(rfqItem);
                                                }
                                            }
                                        });
                                    });

                                    if ($scope.prDeleteItemsArr && $scope.prDeleteItemsArr.length > 0) {
                                        $scope.prDeleteItemsArr.forEach(function (item, index) {
                                            $scope.deleteItem(item);
                                        });
                                    }
                                    return;
                                }
                            }

                            // First check whether this PR exists in requirement items or not if exists then reduce the quantity and return the item//


                            var selectedPrs = $scope.PRList.filter(function (pr) {
                                return pr.isSelected;
                            });

                            var validPRItems = $scope.PRItemsList.filter(function (prItem) {
                                return (!prItem.REQ_ID || prItem.REQ_ID <= 0);
                            });

                            //To check it is first PR selected in List
                            if (selectedPrs && selectedPrs.length <= 1 && validPRItems && validPRItems.length > 0) {
                                $scope.formRequest.listRequirementItems = [];
                            }

                            var currentPR = $scope.PRList.filter(function (prDetails) {
                                return prDetails.PR_ID === +pr.PR_ID;
                            });

                            if (currentPR && currentPR.length > 0) {
                                currentPR[0].isSelected = true;
                            }

                            if (!validPRItems || validPRItems.length <= 0) {
                                swal("Warning!", "All items in PR already linked to existing Requirement.", "error");
                                if (currentPR && currentPR.length > 0) {
                                    currentPR[0].isSelected = false;
                                }
                            }

                            $scope.PRItemsList.forEach(function (item, index) {
                                if (!item.REQ_ID || item.REQ_ID <= 0) {
                                    $scope.requirementItems =
                                        {
                                            productSNo: index + 1,
                                            ItemID: 0,
                                            productIDorName: item.ITEM_NAME,
                                            productNo: item.ITEM_NUM,
                                            hsnCode: item.HSN_CODE,
                                            productDescription: item.ITEM_DESCRIPTION,
                                            productQuantity: item.REQUIRED_QUANTITY,
                                            productQuantityIn: item.UNITS,
                                            productBrand: item.BRAND,
                                            othersBrands: '',
                                            isDeleted: 0,
                                            productImageID: item.ATTACHMENTS,
                                            attachmentName: '',
                                            //budgetID: 0,
                                            //bcInfo: '',
                                            productCode: item.ITEM_CODE,
                                            splitenabled: 0,
                                            fromrange: 0,
                                            torange: 0,
                                            requiredQuantity: 0,
                                            I_LLP_DETAILS: "",
                                            itemAttachment: "",
                                            itemMinReduction: 0,
                                            productId: item.PRODUCT_ID,
                                            PR_ID: item.PR_ID
                                        };

                                    if ($scope.requirementItems.productImageID == null || $scope.requirementItems.productImageID == '') {
                                        $scope.requirementItems.productImageID = 0;
                                    } else {
                                        $scope.requirementItems.productImageID == item.ATTACHMENTS;
                                    }



                                    if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                        var existingItem = $scope.formRequest.listRequirementItems.filter(function (currentItem) {
                                            return currentItem.productId == $scope.requirementItems.productId;
                                        });

                                        if (existingItem && existingItem.length > 0) {
                                            existingItem[0].productQuantity += $scope.requirementItems.productQuantity;
                                        } else {
                                            $scope.formRequest.listRequirementItems.push($scope.requirementItems);
                                        }
                                    } else {
                                        $scope.formRequest.listRequirementItems.push($scope.requirementItems);
                                    }

                                    //Correcting SNO (because Each and Every Time It is Filling with PR Items List)//

                                    if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                        $scope.formRequest.listRequirementItems.forEach(function (snoItem, snoIndex) {
                                            snoItem.productSNo = snoIndex + 1;
                                        })
                                    }

                                    //Correcting SNO (because Each and Every Time It is Filling with PR Items List)//

                                    $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                                        var selProd = $scope.productsList.filter(function (prod) {
                                            //return prod.prodCode == item.productIDorName;//prodName
                                            return prod.prodCode == item.productCode;
                                        });

                                        if (selProd && selProd != null && selProd.length > 0) {
                                            $scope.autofillProduct(item.productIDorName, index)
                                            $scope.fillTextbox(selProd[0], index);
                                        }
                                    });
                                }
                            });
                        });
                }
            };





            $scope.GetProductQuotationTemplate = function (productId, index, productChange) {

                var params = {
                    "catitemid": productId,
                    "sessionid": userService.getUserToken()
                };

                catalogService.GetProductQuotationTemplate(params)
                    .then(function (response) {
                        if (response) {
                            var currentItem = $scope.formRequest.listRequirementItems[index];
                            currentItem.productQuotationTemplateMaster = response;
                            //if (currentItem.isCoreProductCategory == 0 || currentItem.isNonCoreProductCategory == 1) {
                            //    currentItem.AddFabricArray = [];
                            //    currentItem.AddProcessArray = [];
                            //    currentItem.AddAccessoriesArray = [];
                            //}

                            if (!currentItem.productQuotationTemplate || currentItem.productQuotationTemplate.length <= 0 ||
                                !$scope.isEdit) {
                                currentItem.productQuotationTemplate = response;
                                if ($scope.isEdit) {
                                    currentItem.productQuotationTemplate.forEach(function (templateItem, itemIndexs) {
                                        templateItem.IS_VALID = 0;
                                    });
                                }
                            }
                            else if (currentItem.productQuotationTemplateMaster && currentItem.productQuotationTemplateMaster.length > 0) {
                                if (productChange) {
                                    currentItem.productQuotationTemplate = currentItem.productQuotationTemplateMaster;
                                } else {
                                    currentItem.productQuotationTemplateMaster.forEach(function (templateItem, itemIndexs) {
                                        if (templateItem && templateItem.NAME) {
                                            var filterResult = currentItem.productQuotationTemplateArray.filter(function (template) {
                                                return (template.NAME === templateItem.NAME);
                                            });

                                            if (!filterResult || filterResult.length <= 0) {
                                                templateItem.IS_VALID = 0;
                                                currentItem.productQuotationTemplate.unshift(templateItem);
                                            }
                                        }
                                    });
                                }
                            }

                            $scope.formRequest.listRequirementItems = _.sortBy($scope.formRequest.listRequirementItems, function (o) {
                                if (o.isNonCoreProductCategory) { return o.isNonCoreProductCategory; } else { return 0; }
                            });
                        }

                        currentItem.productQuotationTemplate = _.sortBy(currentItem.productQuotationTemplate, function (o) {
                            if (o.SORT_ORDER) { return o.SORT_ORDER } else { return o.T_ID };
                        });
                    });
            };

            $scope.GetProductQuotationTemplateBulk = function (catalogItemIds) {

                var params = {
                    "ids": catalogItemIds,
                    "sessionid": userService.getUserToken()
                };

                catalogService.GetProductQuotationTemplateBulk(params)
                    .then(function (response) {
                        if (response) {
                            console.log(response);
                        }
                    });
            };

            $scope.saveSubItemOptions = function (templateObj) {
                if (templateObj && templateObj.NAME && templateObj.DESCRIPTION) {
                    if (templateObj.NAME && templateObj.DESCRIPTION && templateObj.DESCRIPTION1) {
                        let optionVal = templateObj.NAME.toUpperCase() + '$$$$' + templateObj.DESCRIPTION.toUpperCase();
                        var tempval = _.filter($scope.subItemOptions, function (field) {
                            return field.configValue.toUpperCase() === optionVal;
                        });

                        let params = {
                            listCompanyConfiguration: [],
                            sessionID: userService.getUserToken()
                        };
                        if (!tempval || tempval.length <= 0) {
                            let listCompanyConfigurationObj = {
                                CompConfigID: 0,
                                compID: userService.getUserCompanyId(),
                                configKey: 'SUB_ITEM_OPT',
                                isValid: 1,
                                configValue: optionVal,
                                configText: optionVal,
                            };

                            $scope.subItemOptions.push(listCompanyConfigurationObj);
                            params.listCompanyConfiguration.push(listCompanyConfigurationObj);
                        }


                        let optionVal1 = templateObj.NAME.toUpperCase() + '$$$$' + templateObj.DESCRIPTION.toUpperCase() + '$$$$' + templateObj.DESCRIPTION1.toUpperCase();;
                        var tempval1 = _.filter($scope.subItemOptions, function (field) {
                            return field.configValue.toUpperCase() === optionVal1;
                        });
                        if (!tempval1 || tempval1.length <= 0) {
                            let listCompanyConfigurationObj1 = {
                                CompConfigID: 0,
                                compID: userService.getUserCompanyId(),
                                configKey: 'SUB_ITEM_OPT',
                                isValid: 1,
                                configValue: optionVal1,
                                configText: optionVal1,
                            };

                            $scope.subItemOptions.push(listCompanyConfigurationObj1);
                            params.listCompanyConfiguration.push(listCompanyConfigurationObj1);
                        }

                        if (params.listCompanyConfiguration && params.listCompanyConfiguration.length > 0) {
                            auctionsService.SaveCompanyConfiguration(params)
                                .then(function (response) {
                                    console.log(response);
                                });
                        }
                    }
                }
            };

            //Below product sub item
            $scope.SaveProductQuotationTemplate = function (obj) {
                $scope.saveSubItemOptions(obj);
                var sameNameError = false;
                obj.currentProductQuotationTemplate.forEach(function (item, itemIndex) {
                    if (obj.NAME.toUpperCase() == item.NAME.toUpperCase() && obj.T_ID == 0) {
                        sameNameError = true;
                    }
                });


                if (sameNameError) { growlService.growl("Same name Error.", "inverse"); return false; }


                if (obj.SORT_ORDER_VAL) {
                    let tempSortField = _.filter(obj.currentProductQuotationTemplate, function (field) {
                        return obj.SORT_ORDER_VAL === field.NAME;
                    });

                    if (tempSortField && tempSortField.length > 0) {
                        obj.SORT_ORDER = tempSortField[0].SORT_ORDER + 0.1;
                    } else {
                        if (obj.currentProductQuotationTemplate.length > 0) {
                            obj.SORT_ORDER = obj.currentProductQuotationTemplate[obj.currentProductQuotationTemplate.length - 1].SORT_ORDER + 1;
                        } else {
                            obj.SORT_ORDER = 0;
                        }
                    }
                }

                var params = {
                    "productquotationtemplate": obj,
                    "sessionid": userService.getUserToken()
                };

                catalogService.SaveProductQuotationTemplate(params)
                    .then(function (response) {
                        if (response) {
                            growlService.growl("Saved Successfully.", "success");
                            $scope.resetTemplateObj();
                            if (obj.PRODUCT_ID > 0 && obj.index >= 0) {
                                $scope.GetProductQuotationTemplate(obj.PRODUCT_ID, obj.index, true);
                            }
                        }
                    });
            };

            $scope.resetTemplateObj = function (product, index) {
                if (!product) {
                    product = { catalogueItemID: 0, productQuotationTemplate: [] };
                }
                if (!index) {
                    index = 0;
                }

                $scope.QuotationTemplateObj = {
                    currentProductQuotationTemplate: product.productQuotationTemplate,
                    index: index,
                    T_ID: 0,
                    TEMP_ID: '',
                    TEMP_TYPE: '',
                    PRODUCT_ID: product.catalogueItemID,
                    NAME: '',
                    DESCRIPTION: '',
                    DESCRIPTION1: '',
                    // HAS_SPECIFICATION: 0,
                    HAS_PRICE: 1,
                    HAS_QUANTITY: 1,
                    CONSUMPTION: 0,
                    UOM: '',
                    HAS_TAX: 0,
                    IS_VALID: 1,
                    SORT_ORDER: 0,
                    SORT_ORDER_VAL: '',
                    U_ID: userService.getUserId()
                };
            };

            $scope.resetTemplateObj();
            //^Above product sub item

            $scope.loadCustomFields = function () {
                var params = {
                    "compid": userService.getUserCompanyId(),
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.getCustomFieldList(params)
                    .then(function (response) {
                        $scope.customFieldList = response;
                        if ($scope.isEdit) {
                            params = {
                                "compid": userService.getUserCompanyId(),
                                "fieldmodule": 'REQUIREMENT',
                                "moduleid": $stateParams.Id,
                                "sessionid": userService.getUserToken()
                            };

                            PRMCustomFieldService.GetCustomFieldsByModuleId(params)
                                .then(function (response) {
                                    $scope.selectedcustomFieldList = response;
                                    $scope.selectedcustomFieldList.forEach(function (cfItem, index) {
                                        cfItem.IS_SELECTED == 1;

                                        var cfTempField = _.filter($scope.customFieldList, function (field) {
                                            return field.CUST_FIELD_ID == cfItem.CUST_FIELD_ID;
                                        });
                                        if (cfTempField && cfTempField.length > 0) {
                                            cfTempField[0].IS_SELECTED = 1;
                                            cfTempField[0].FIELD_VALUE = cfItem.FIELD_VALUE;
                                            cfTempField[0].MODULE_ID = cfItem.MODULE_ID;
                                        }
                                    });
                                });
                        }
                    });
            };

            $scope.loadCustomFields();

            $scope.addCustomFieldToFrom = function (field) {
                $scope.selectedcustomFieldList = _.filter($scope.customFieldList, function (field) {
                    return field.IS_SELECTED == 1;
                });
            };

            $scope.validateSubItemQuantity = function (product, productQuotationTemplate) {
                if (!productQuotationTemplate.CONSUMPTION) {
                    productQuotationTemplate.CONSUMPTION = 1;
                } else {
                    product.productQuantity = 1;
                }
            };


            $scope.saveRequirementCustomFields = function (requirementId) {
                if (requirementId) {
                    $scope.selectedcustomFieldList.forEach(function (item, index) {
                        item.MODULE_ID = requirementId;
                        item.ModifiedBy = $scope.userID;
                    });

                    var params = {
                        "details": $scope.selectedcustomFieldList,
                        "sessionid": userService.getUserToken()
                    };

                    PRMCustomFieldService.saveCustomFieldValue(params)
                        .then(function (response) {

                        });
                }
            };

            $scope.GetPRMFieldMappingDetails = function () {
                $scope.prmFieldMappingDetails = {};
                var template = $scope.selectedTemplate ? $scope.selectedTemplate : 'DEFAULT';
                userService.GetPRMFieldMappingDetails(template).then(function (mappingDetails) {
                    mappingDetails.forEach(function (item, index) {
                        $scope.prmFieldMappingDetails[item.FIELD_NAME] = item.FIELD_ALIAS_NAME;
                    });
                });
            };

            $scope.GetPRMFieldMappingDetails();

            $scope.GetPRMFieldMappingTemplates = function () {
                PRMCustomFieldService.GetPRMFieldMappingTemplates().then(function (response) {
                    $scope.prmFieldMappingTemplates = response;
                });
            };

            $scope.GetPRMFieldMappingTemplates();


            $scope.searchingCategoryVendors = function (value) {
                if (value) {
                    $scope.VendorsList = _.filter($scope.VendorsTempList1, function (item) { return item.companyName.toUpperCase().indexOf(value.toUpperCase()) > -1; });
                }
            };

            $scope.selectFormCategorySearch = function (vendor) {
                var error = false;

                $scope.formRequest.auctionVendors.forEach(function (SV, SVIndex) {
                    if (SV.vendorID == vendor.vendorID) {
                        error = true;
                        growlService.growl("Vendor already selected", "inverse");

                    }
                })

                if (error == true) {
                    return;
                }

                $scope.Vendors.forEach(function (UV, UVIndex) {
                    if (UV.vendorID == vendor.vendorID) {
                        error = true;
                    }
                })

                if (error == false) {
                    vendor.isFromGlobalSearch = 1;
                    $scope.formRequest.auctionVendors.push(vendor);
                    $scope.VendorsList = $scope.VendorsList.filter(function (obj) {
                        return obj != vendor;
                    });
                    $scope.ShowDuplicateVendorsNames.push(vendor);
                }
                else {
                    $scope.selectForA(vendor);
                    vendor.isChecked = false;
                }
            };

            // Category Based Vendors //
            $scope.VendorsList = [];
            $scope.VendorsTempList1 = [];

            $scope.getCategoryVendors = function (str) {
                $scope.ShowDuplicateVendorsNames = [];
                $scope.VendorsList = [];
                $scope.VendorsTempList1 = [];
                $scope.vendorsLoaded = false;
                var category = '';

                category = catObj.catCode;

                var params = { 'Categories': category, 'sessionID': userService.getUserToken(), 'uID': userService.getUserId(), evalID: $scope.isTechEval ? $scope.selectedQuestionnaire.evalID : 0 };
                $http({
                    method: 'POST',
                    url: domain + 'getVendorsbyCategories',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                }).then(function (response) {
                    if (response && response.data) {
                        if (response.data.length > 0) {
                            $scope.VendorsList = response.data;
                            $scope.VendorsTempList = $scope.VendorsList;



                            $scope.Vendors.forEach(function (item, index) {
                                $scope.VendorsTempList.forEach(function (item1, index1) {
                                    if (item.vendorID == item1.vendorID) {
                                        $scope.ShowDuplicateVendorsNames.push(item1);
                                        $scope.VendorsList.splice(index1, 1);
                                    }
                                })
                            });

                            if ($scope.formRequest.auctionVendors.length > 0) {
                                $scope.formRequest.auctionVendors.forEach(function (item1, index1) {
                                    $scope.VendorsTempList.forEach(function (item2, index2) {
                                        if (item1.vendorID == item2.vendorID) {
                                            $scope.ShowDuplicateVendorsNames.push(item2);
                                            $scope.VendorsList.splice(index2, 1);
                                        }
                                    });
                                });
                            }


                            $scope.VendorsTempList1 = $scope.VendorsList;

                            if ($scope.ShowDuplicateVendorsNames.length > 0) {
                                $scope.totalItems = $scope.ShowDuplicateVendorsNames.length;
                            } else {
                                $scope.totalItems = $scope.ShowDuplicateVendorsNames.length;
                            }

                        } else {
                            $scope.VendorsList = [];
                        }

                        if ($scope.searchCategoryVendorstring != '') {
                            $scope.searchingCategoryVendors($scope.searchCategoryVendorstring);
                        } else {
                            $scope.searchingCategoryVendors('');
                        }


                    } else {

                    }
                }, function (result) {
                });

            }
            // Category Based Vendors //

            $scope.validateLineItemsList = function () {
                if ($scope.nonCoreproductsList && $scope.nonCoreproductsList.length > 0 && $scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                    $scope.nonCoreproductsList.forEach(function (nonCoreProduct, index) {
                        var filterItems = $scope.formRequest.listRequirementItems.filter(function (product) {
                            return product.catalogueItemID === nonCoreProduct.prodId;
                        });

                        if (filterItems && filterItems.length > 0) {
                            nonCoreProduct.doHide = true;
                        }
                    });
                }
            };

            $scope.selectRequirementPRS = function () {
                if ($stateParams.Id) {
                    if ($scope.PRList && $scope.PRList.length > 0 && $scope.formRequest.PR_ID && $scope.formRequest.PR_ID !== '') {
                        var requirementPRs = $scope.formRequest.PR_ID.split(',');
                        requirementPRs.forEach(function (requirementPRs, index) {
                            var filterPR = $scope.PRList.filter(function (pr) {
                                return pr.PR_ID === +requirementPRs;
                            });

                            if (filterPR && filterPR.length > 0) {
                                filterPR[0].isSelected = true;
                            }
                        });

                        $scope.PRList = _.orderBy($scope.PRList, ['isSelected'], ['desc']);
                    }
                }
            };


            $('.selected-items-box').bind('click', function (e) {
                $('.multiple-selection-dropdown .list').slideToggle('fast');
            });




            $scope.totalPRItems = 0;
            $scope.currentPRPage = 1;
            $scope.currentPRPage2 = 1;
            $scope.itemsPerPRPage = 10;
            $scope.itemsPerPRPage2 = 10;
            $scope.maxPRSize = 10;

            $scope.setPRPage = function (pageNo) {
                $scope.currentPRPage = pageNo;
            };

            $scope.pagePRChanged = function () {
            };


            $scope.searchTable = function (str) {
                var filterText = angular.lowercase(str);
                if (!str || str == '' || str == undefined || str == null) {
                    $scope.PRList = $scope.PRList1;
                }
                else {
                    $scope.PRList = $scope.PRList1.filter(function (products) {
                        return (String(angular.lowercase(products.PR_NUMBER)).includes(filterText) == true);
                    });
                }


                $scope.totalPRItems = $scope.PRList.length;
            }



            $scope.createProduct = function (product, index) {

                $scope.itemProductNameErrorMessage = '';
                $scope.itemProductUnitsErrorMessage = '';

                if (!product.productCode) {

                    if (!product.productCode) {
                        $scope.itemProductNameErrorMessage = 'Please Enter Article number.';
                    }

                    //else if (!product.productQuantityIn) {
                    //    $scope.itemProductUnitsErrorMessage = 'Please Enter Units.';
                    //} else if (!product.productCode) {
                    //    $scope.itemProductUnitsErrorMessage = 'Please Article number.';
                    //}
                    return
                }


                $scope.productObj = {
                    prodId: 0,
                    prodCode: product.productCode,
                    compId: userService.getUserCompanyId(),
                    prodName: product.productIDorName,
                    prodNo: product.productNo,
                    prodQty: product.productQuantityIn,
                    isValid: 1,
                    ModifiedBy: userService.getUserId()
                };

                var params = {
                    reqProduct: $scope.productObj,
                    sessionID: userService.getUserToken()
                }

                catalogService.addproduct(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                            $scope.postRequestLoding = true;
                            $scope.formRequest.listRequirementItems[index].responseID = response.repsonseId;
                            $scope.AddProductRespId = response.repsonseId;
                            $scope.formRequest.listRequirementItems[index].responseError = response.errorMessage;
                        }
                        else {
                            growlService.growl("Product added successfully.", "success");
                            $scope.itemProductNameErrorMessage = '';
                            $scope.itemProductUnitsErrorMessage = '';
                            var productID = response.repsonseId;
                            $scope.formRequest.listRequirementItems[index].catalogueItemID = productID;
                            $scope.formRequest.listRequirementItems[index].isNewItem = true;
                            $scope.GetProductQuotationTemplate(productID, index, true);
                            $scope.showAddProduct = false;
                            $scope.AddProductRespId = 0;
                            $scope.formRequest.listRequirementItems[index].responseError = '';
                            $scope.postRequestLoding = false;
                        }
                    });
            }

            $scope.getDDLOptions = function (templateObj) {
                let optionsArray = [];
                if ($scope.subItemOptions && $scope.subItemOptions.length > 0 && templateObj && templateObj.NAME) {
                    let name = templateObj.NAME.toUpperCase();
                    $scope.subItemOptions.forEach(function (option, index) {
                        if (option.configValue.indexOf(name + '$$$$') > -1) {
                            optionsArray.push(option.configValue.split('$$$$')[1]);
                        }
                    });

                    optionsArray = optionsArray.filter((v, i, a) => a.indexOf(v) === i);
                    if (optionsArray.length <= 0) {
                        templateObj.DESCRIPTION = '';
                    }

                    templateObj.DESCRIPTION_OPT = optionsArray;
                }
            };

            $scope.getDDLOptions1 = function (templateObj) {
                let optionsArray = [];
                if ($scope.subItemOptions && $scope.subItemOptions.length > 0 && templateObj && templateObj.NAME && templateObj.DESCRIPTION) {
                    let name = templateObj.NAME.toUpperCase() + '$$$$' + templateObj.DESCRIPTION + '$$$$';
                    $scope.subItemOptions.forEach(function (option, index) {
                        if (option.configValue.indexOf(name) > -1) {
                            optionsArray.push(option.configValue.split('$$$$')[2]);
                        }
                    });

                    optionsArray = optionsArray.filter((v, i, a) => a.indexOf(v) === i);
                    if (optionsArray.length <= 0) {
                        templateObj.DESCRIPTION1 = '';
                    }
                    templateObj.DESCRIPTION_OPT1 = optionsArray;
                }
            };

            $scope.getSortOrderList = function (templateObj) {
                let optionsArray = [];
                templateObj.currentProductQuotationTemplate.forEach(function (option, index) {
                    optionsArray.push(option.NAME);
                });

                templateObj.SORT_ORDER_OPT = optionsArray;
            };

            $scope.subItemPrice = [];
            $scope.subPriceObj = {
                price: 0,
                index: -1
            }

            $scope.calculateTotalPrice = function (fabricArray, processArray, sundriesArray, productIdx, type, cutMake) {
                $scope.additionalPrice = 0;
                $scope.cutmakePrice = 0;
                $scope.subItemPrice.push($scope.subPriceObj);
                $scope.subItemPrice.forEach(function (obj) {
                    if (obj.index == productIdx) {

                    } else {
                        obj.index = $scope.subPriceObj.index + 1;
                        $scope.TotalPrice = 0;
                    }
                })

                if (fabricArray && fabricArray.length > 0) {
                    $scope.TotalPrice = _.sumBy(fabricArray, function (fabricUnitPrice) { return fabricUnitPrice.TOTAL_SUB_ITEM_PRICE; });
                    // $scope.TotalPrice = _.sumBy(fabricArray, function (fabricUnitPrice) { return fabricUnitPrice.FINAL_SS20_PRICE; });
                }
                if (processArray && processArray.length > 0) {
                    $scope.TotalPrice += _.sumBy(processArray, function (processUnitPrice) { return processUnitPrice.TOTAL_SUB_ITEM_PRICE; });
                    //  $scope.TotalPrice += _.sumBy(processArray, function (processUnitPrice) { return processUnitPrice.COST_PER_GARMENT; });
                }
                if (sundriesArray && sundriesArray.length > 0) {
                    $scope.TotalPrice += _.sumBy(sundriesArray, function (sundriesUnitPrice) { return sundriesUnitPrice.TOTAL_SUB_ITEM_PRICE; });
                }
                if (cutMake >= 0) {
                    $scope.cutmakePrice += parseFloat(cutMake);
                    // $scope.TotalPrice += $scope.cutmakePrice;
                }


                $scope.formRequest.listRequirementItems[productIdx].subItemsTotalPrice = $scope.TotalPrice + $scope.cutmakePrice;



                $scope.formRequest.listRequirementItems.forEach(function (item, itemIDX) {
                    if (itemIDX == productIdx) {

                        if (parseFloat(item.rejectionPercentage) > 100) {
                            growlService.growl("Please enter Rejection Percentage less than 100", "inverse");
                            item.rejectionPercentage = 0;
                            item.rejectionPerAmount = 0;
                            return false;
                        }

                        if (parseFloat(item.marginPercentage) > 100) {
                            growlService.growl("Please enter Margin Percentage less than 100", "inverse");
                            item.marginPercentage = 0;
                            item.marginPerAmount = 0;
                            return false;
                        }

                        if (parseFloat(item.rejectionPercentage) >= 0) {
                            $scope.additionalPrice += (item.subItemsTotalPrice * (parseFloat(item.rejectionPercentage) / 100));
                            item.rejectionPerAmount = (item.subItemsTotalPrice * (parseFloat(item.rejectionPercentage) / 100));
                        }
                        if (parseFloat(item.testing) >= 0) {
                            $scope.additionalPrice += parseFloat(item.testing);
                        }
                        if (parseFloat(item.marginPercentage) >= 0) {
                            $scope.additionalPrice += ((item.subItemsTotalPrice + item.rejectionPerAmount) * (parseFloat(item.marginPercentage) / 100));
                            item.marginPerAmount = ((item.subItemsTotalPrice + item.rejectionPerAmount) * (parseFloat(item.marginPercentage) / 100));
                        }
                        if (parseFloat(item.financeCharges) >= 0) {
                            $scope.additionalPrice += parseFloat(item.financeCharges);
                        }
                        if (parseFloat(item.freightCharges) >= 0) {
                            $scope.additionalPrice += parseFloat(item.freightCharges);
                        }
                        if (parseFloat(item.chaCharges) >= 0) {
                            $scope.additionalPrice += parseFloat(item.chaCharges);
                        }
                    }
                })

                $scope.formRequest.listRequirementItems[productIdx].subItemsTotalPrice += $scope.additionalPrice;


            }

            $scope.calculateSubItemPrice = function (fabricArray, processArray, sundriesArray, subIdx, productIdx, type) {

                if (fabricArray && type == 'FABRIC') {

                    if (fabricArray.FINAL_SS20_PRICE && fabricArray.CONSUMPTION) {
                        fabricArray.TOTAL_SUB_ITEM_PRICE = (parseFloat(fabricArray.FINAL_SS20_PRICE) * parseFloat(fabricArray.CONSUMPTION));
                        $scope.formRequest.listRequirementItems[productIdx].AddFabricArray[subIdx].TOTAL_SUB_ITEM_PRICE = fabricArray.TOTAL_SUB_ITEM_PRICE;
                        $scope.formRequest.listRequirementItems[productIdx].AddFabricArray[subIdx] = fabricArray;
                        $scope.calculateTotalPrice($scope.formRequest.listRequirementItems[productIdx].AddFabricArray, $scope.formRequest.listRequirementItems[productIdx].AddProcessArray, $scope.AddAccessoriesArray, productIdx, 'FABRIC', $scope.formRequest.listRequirementItems[productIdx].cutMake);
                    }
                    $scope.formRequest.listRequirementItems[productIdx].FabricSubTotalPrice = _.sumBy($scope.formRequest.listRequirementItems[productIdx].AddFabricArray, function (SUN) { return SUN.TOTAL_SUB_ITEM_PRICE; });
                }
                if (processArray) {
                    if (type == 'Wash') {

                        if (processArray.COST_PER_GARMENT && processArray.CONSUMPTION) {
                            processArray.TOTAL_SUB_ITEM_PRICE = (parseFloat(processArray.COST_PER_GARMENT) * parseFloat(processArray.CONSUMPTION));
                            $scope.formRequest.listRequirementItems[productIdx].AddProcessArray[subIdx].TOTAL_SUB_ITEM_PRICE = processArray.TOTAL_SUB_ITEM_PRICE;
                            $scope.formRequest.listRequirementItems[productIdx].AddProcessArray[subIdx] = processArray;
                            $scope.calculateTotalPrice($scope.formRequest.listRequirementItems[productIdx].AddFabricArray, $scope.formRequest.listRequirementItems[productIdx].AddProcessArray, $scope.formRequest.listRequirementItems[productIdx].AddAccessoriesArray, productIdx, 'WASH', $scope.formRequest.listRequirementItems[productIdx].cutMake);
                        }

                    }

                    if (type == 'Print') {
                        if (processArray.COST_PER_GARMENT && processArray.CONSUMPTION) {
                            processArray.TOTAL_SUB_ITEM_PRICE = (parseFloat(processArray.COST_PER_GARMENT) * parseFloat(processArray.CONSUMPTION));
                            $scope.formRequest.listRequirementItems[productIdx].AddProcessArray[subIdx].TOTAL_SUB_ITEM_PRICE = processArray.TOTAL_SUB_ITEM_PRICE;
                            $scope.formRequest.listRequirementItems[productIdx].AddProcessArray[subIdx] = processArray;
                            $scope.calculateTotalPrice($scope.formRequest.listRequirementItems[productIdx].AddFabricArray, $scope.formRequest.listRequirementItems[productIdx].AddProcessArray, $scope.formRequest.listRequirementItems[productIdx].AddAccessoriesArray, productIdx, 'PRINT', $scope.formRequest.listRequirementItems[productIdx].cutMake);
                        }
                    }

                    if (type == 'Embroidery') {
                        if (processArray.COST_PER_GARMENT && processArray.CONSUMPTION) {
                            processArray.TOTAL_SUB_ITEM_PRICE = (parseFloat(processArray.COST_PER_GARMENT) * parseFloat(processArray.CONSUMPTION));
                            $scope.formRequest.listRequirementItems[productIdx].AddProcessArray[subIdx].TOTAL_SUB_ITEM_PRICE = processArray.TOTAL_SUB_ITEM_PRICE;
                            $scope.formRequest.listRequirementItems[productIdx].AddProcessArray[subIdx] = processArray;
                            $scope.calculateTotalPrice($scope.formRequest.listRequirementItems[productIdx].AddFabricArray, $scope.formRequest.listRequirementItems[productIdx].AddProcessArray, $scope.formRequest.listRequirementItems[productIdx].AddAccessoriesArray, productIdx, 'EMBROIDERY', $scope.formRequest.listRequirementItems[productIdx].cutMake);
                        }
                    }

                    if (type == 'Applique') {
                        if (processArray.COST_PER_GARMENT && processArray.CONSUMPTION) {
                            processArray.TOTAL_SUB_ITEM_PRICE = (parseFloat(processArray.COST_PER_GARMENT) * parseFloat(processArray.CONSUMPTION));
                            $scope.formRequest.listRequirementItems[productIdx].AddProcessArray[subIdx].TOTAL_SUB_ITEM_PRICE = processArray.TOTAL_SUB_ITEM_PRICE;
                            $scope.formRequest.listRequirementItems[productIdx].AddProcessArray[subIdx] = processArray;
                            $scope.calculateTotalPrice($scope.formRequest.listRequirementItems[productIdx].AddFabricArray, $scope.formRequest.listRequirementItems[productIdx].AddProcessArray, $scope.formRequest.listRequirementItems[productIdx].AddAccessoriesArray, productIdx, 'APPLIQUE', $scope.formRequest.listRequirementItems[productIdx].cutMake);
                        }
                    }

                    if (type == 'Embellishment') {
                        if (processArray.COST_PER_GARMENT && processArray.CONSUMPTION) {
                            processArray.TOTAL_SUB_ITEM_PRICE = (parseFloat(processArray.COST_PER_GARMENT) * parseFloat(processArray.CONSUMPTION));
                            $scope.formRequest.listRequirementItems[productIdx].AddProcessArray[subIdx].TOTAL_SUB_ITEM_PRICE = processArray.TOTAL_SUB_ITEM_PRICE;
                            $scope.formRequest.listRequirementItems[productIdx].AddProcessArray[subIdx] = processArray;
                            $scope.calculateTotalPrice($scope.formRequest.listRequirementItems[productIdx].AddFabricArray, $scope.formRequest.listRequirementItems[productIdx].AddProcessArray, $scope.formRequest.listRequirementItems[productIdx].AddAccessoriesArray, productIdx, 'EMBELLISHMENT', $scope.formRequest.listRequirementItems[productIdx].cutMake);
                        }
                    }

                    $scope.formRequest.listRequirementItems[productIdx].ProcessSubTotalPrice = _.sumBy($scope.formRequest.listRequirementItems[productIdx].AddProcessArray, function (SUN) { return SUN.TOTAL_SUB_ITEM_PRICE; });
                }

                if (sundriesArray) {

                    if (type == 'SEWING') {
                        if (sundriesArray.SEWING_COST && sundriesArray.SEWING_CONSUMPTION) {
                            sundriesArray.TOTAL_SUB_ITEM_PRICE = (parseFloat(sundriesArray.SEWING_COST) * parseFloat(sundriesArray.SEWING_CONSUMPTION));
                            $scope.formRequest.listRequirementItems[productIdx].AddAccessoriesArray[subIdx].TOTAL_SUB_ITEM_PRICE = sundriesArray.TOTAL_SUB_ITEM_PRICE;

                            $scope.formRequest.listRequirementItems[productIdx].AddAccessoriesArray[subIdx] = sundriesArray;
                            $scope.calculateTotalPrice($scope.formRequest.listRequirementItems[productIdx].AddFabricArray, $scope.formRequest.listRequirementItems[productIdx].AddProcessArray, $scope.formRequest.listRequirementItems[productIdx].AddAccessoriesArray, productIdx, 'SEWING', $scope.formRequest.listRequirementItems[productIdx].cutMake);
                        }
                    }
                    if (type == 'PACKAGING') {
                        if (sundriesArray.PACKAGING_COST && sundriesArray.PACKAGING_CONSUMPTION) {
                            sundriesArray.TOTAL_SUB_ITEM_PRICE = (parseFloat(sundriesArray.PACKAGING_COST) * parseFloat(sundriesArray.PACKAGING_CONSUMPTION));
                            $scope.formRequest.listRequirementItems[productIdx].AddAccessoriesArray[subIdx].TOTAL_SUB_ITEM_PRICE = sundriesArray.TOTAL_SUB_ITEM_PRICE;

                            $scope.formRequest.listRequirementItems[productIdx].AddAccessoriesArray[subIdx] = sundriesArray;
                            $scope.calculateTotalPrice($scope.formRequest.listRequirementItems[productIdx].AddFabricArray, $scope.formRequest.listRequirementItems[productIdx].AddProcessArray, $scope.formRequest.listRequirementItems[productIdx].AddAccessoriesArray, productIdx, 'PACKAGING', $scope.formRequest.listRequirementItems[productIdx].cutMake);
                        }
                    }
                    $scope.formRequest.listRequirementItems[productIdx].SundriesSubTotalPrice = _.sumBy($scope.formRequest.listRequirementItems[productIdx].AddAccessoriesArray, function (SUN) { return SUN.TOTAL_SUB_ITEM_PRICE; });
                }

            }

            $scope.DeleteSubItem = function (type, subIdx, Mainidx, cutmake) {

                $scope.formRequest.listRequirementItems.forEach(function (item, itemIdx) {

                    if (Mainidx == itemIdx) {
                        if (type == "FABRIC") {
                            if (item.AddFabricArray && item.AddFabricArray.length > 1) {
                                item.AddFabricArray.forEach(function (subItem, subItemIdx) {
                                    if (subItemIdx == subIdx) {
                                        item.AddFabricArray = _.filter(item.AddFabricArray, function (x, xIdx) { return xIdx !== subIdx; });
                                        $scope.formRequest.listRequirementItems[Mainidx].FabricSubTotalPrice = _.subtract($scope.formRequest.listRequirementItems[Mainidx].FabricSubTotalPrice, subItem.TOTAL_SUB_ITEM_PRICE);
                                    }

                                })

                                $scope.calculateTotalPrice(item.AddFabricArray, item.AddProcessArray, item.AddAccessoriesArray, Mainidx, '', cutmake);
                            }


                        }

                        if (type == "PROCESS") {
                            if (item.AddProcessArray && item.AddProcessArray.length > 1) {
                                item.AddProcessArray.forEach(function (subItem, subItemIdx) {
                                    if (subItemIdx == subIdx) {
                                        item.AddProcessArray = _.filter(item.AddProcessArray, function (x, xIdx) { return xIdx !== subIdx; });
                                        $scope.formRequest.listRequirementItems[Mainidx].ProcessSubTotalPrice = _.subtract($scope.formRequest.listRequirementItems[Mainidx].ProcessSubTotalPrice, subItem.TOTAL_SUB_ITEM_PRICE);
                                    }
                                })

                                $scope.calculateTotalPrice(item.AddFabricArray, item.AddProcessArray, item.AddAccessoriesArray, Mainidx, '', cutmake);
                            }

                        }

                        if (type == "SUNDRIES") {
                            if (item.AddAccessoriesArray && item.AddAccessoriesArray.length > 1) {
                                item.AddAccessoriesArray.forEach(function (subItem, subItemIdx) {
                                    if (subItemIdx == subIdx) {
                                        item.AddAccessoriesArray = _.filter(item.AddAccessoriesArray, function (x, xIdx) { return xIdx !== subIdx; });
                                        $scope.formRequest.listRequirementItems[Mainidx].SundriesSubTotalPrice = _.subtract($scope.formRequest.listRequirementItems[Mainidx].SundriesSubTotalPrice, subItem.TOTAL_SUB_ITEM_PRICE);
                                    }
                                })

                                $scope.calculateTotalPrice(item.AddFabricArray, item.AddProcessArray, item.AddAccessoriesArray, Mainidx, '', cutmake);
                            }

                        }
                    }

                })
            }

            $scope.clearFeildavalues = function (obj, idx) {
                $scope.formRequest.listRequirementItems.forEach(function (item, itemIDX) {

                    if (idx == itemIDX) {
                        for (var key in obj) {
                            if (obj.hasOwnProperty(key)) {
                                if (typeof obj[key] === 'string') {
                                    obj[key] = '';
                                } else if (typeof obj[key] === 'number') {
                                    obj[key] = 0;
                                }
                                else if (typeof obj[key] === 'boolean') {
                                    obj[key] = false;
                                }
                                else if (obj[key] instanceof Array) {
                                    obj[key] = [];
                                }
                            }
                        }
                    }
                })

            }

            $scope.clearFeildsByNotNominated = function (obj, subIdx, MainIdx, type, cutmake, nominated,isTrue) {

                if (type == 'FABRIC') {
                    $scope.GetItemsDataFromExcel();
                    $scope.formRequest.listRequirementItems[MainIdx].FabricSubTotalPrice = _.subtract($scope.formRequest.listRequirementItems[MainIdx].FabricSubTotalPrice, obj.TOTAL_SUB_ITEM_PRICE);
                    for (var key in obj) {
                        if (key != "NAME") {
                            if (obj.hasOwnProperty(key)) {
                                if (typeof obj[key] === 'string') {
                                    obj[key] = '';
                                } else if (typeof obj[key] === 'number') {
                                    obj[key] = 0;
                                }
                                else if (typeof obj[key] === 'boolean') {
                                    obj[key] = false;
                                }
                                else if (obj[key] instanceof Array) {
                                    obj[key] = [];
                                }
                            }
                        }
                        
                    }
                    
                    $scope.formRequest.listRequirementItems[MainIdx].AddFabricArray[subIdx] = obj;
                    if (nominated == 1) {
                        $scope.formRequest.listRequirementItems[MainIdx].AddFabricArray[subIdx].IS_FABRIC_NOMINATED = 1;
                    }
                    $scope.formRequest.listRequirementItems[MainIdx].AddFabricArray[subIdx].CONSUMPTION = 1;
                    $scope.calculateTotalPrice($scope.formRequest.listRequirementItems[MainIdx].AddFabricArray, $scope.formRequest.listRequirementItems[MainIdx].AddProcessArray, $scope.formRequest.listRequirementItems[MainIdx].AddAccessoriesArray, MainIdx, '', cutmake);

                }

                if (type == 'PROCESS') {
                    $scope.GetPrintDataFromExcel();
                    $scope.GetWashDataFromExcel();
                    $scope.formRequest.listRequirementItems[MainIdx].ProcessSubTotalPrice = _.subtract($scope.formRequest.listRequirementItems[MainIdx].ProcessSubTotalPrice, obj.TOTAL_SUB_ITEM_PRICE);
                    for (var key in obj) {
                        if (key != "PROCESS_TYPE") {
                            if (obj.hasOwnProperty(key)) {
                                if (typeof obj[key] === 'string') {
                                    obj[key] = '';
                                } else if (typeof obj[key] === 'number') {
                                    obj[key] = 0;
                                }
                                else if (typeof obj[key] === 'boolean') {
                                    obj[key] = false;
                                }
                                else if (obj[key] instanceof Array) {
                                    obj[key] = [];
                                }
                            }
                        }
                    }
                    $scope.formRequest.listRequirementItems[MainIdx].AddProcessArray[subIdx] = obj;
                    $scope.formRequest.listRequirementItems[MainIdx].AddProcessArray[subIdx].COST_PER_GARMENT = 0;
                    $scope.formRequest.listRequirementItems[MainIdx].AddProcessArray[subIdx].CONSUMPTION = 1;
                    if (nominated == 1) {
                        $scope.formRequest.listRequirementItems[MainIdx].AddProcessArray[subIdx].IS_PROCESS_NOMINATED = 1;
                    }
                    $scope.calculateTotalPrice($scope.formRequest.listRequirementItems[MainIdx].AddFabricArray, $scope.formRequest.listRequirementItems[MainIdx].AddProcessArray, $scope.formRequest.listRequirementItems[MainIdx].AddAccessoriesArray, MainIdx, '', cutmake);
                }

                if (type == 'SUNDRIES') {
                    // $scope.GetSundriesFromExcel();
                    $scope.formRequest.listRequirementItems[MainIdx].SundriesSubTotalPrice = _.subtract($scope.formRequest.listRequirementItems[MainIdx].SundriesSubTotalPrice, obj.TOTAL_SUB_ITEM_PRICE);
                    for (var key in obj) {
                        if (key != "TYPE") {
                            if (obj.hasOwnProperty(key)) {

                                if (typeof obj[key] === 'string') {
                                    obj[key] = '';
                                } else if (typeof obj[key] === 'number') {
                                    obj[key] = 0;
                                }
                                else if (typeof obj[key] === 'boolean') {
                                    obj[key] = false;
                                }
                                else if (obj[key] instanceof Array) {
                                    obj[key] = [];
                                }
                            }
                        }
                    }
                    obj.BRANDS = $scope.formRequest.listRequirementItems[MainIdx].BRANDS;
                    obj.GENDER = $scope.formRequest.listRequirementItems[MainIdx].GENDER;
                    obj.isDisabled = isTrue;
                    $scope.formRequest.listRequirementItems[MainIdx].AddAccessoriesArray[subIdx] = obj;
                   
                    if (nominated == 1) {
                        $scope.formRequest.listRequirementItems[MainIdx].AddAccessoriesArray[subIdx].IS_SUNDRIES_NOMINATED = 1;
                    }

                    $scope.calculateTotalPrice($scope.formRequest.listRequirementItems[MainIdx].AddFabricArray, $scope.formRequest.listRequirementItems[MainIdx].AddProcessArray, $scope.formRequest.listRequirementItems[MainIdx].AddAccessoriesArray, MainIdx, '', cutmake);

                }
            }


            function createTempObject(itemidx) {

                $scope.formRequest.listRequirementItems.forEach(function (item, idx) {
                    if (idx == itemidx) {
                        $scope.AddAccessoriesObj2 = {
                            // processSNo: $scope.processSNo++,
                            SEWING_ID: '',
                            PACK_ID: '',
                            SEWING_NAME: '',
                            PACKAGING_NAME: '',
                            CATEGORY: '',
                            POLYBAG: '',
                            CARTON: 0,
                            IS_SUNDRIES_NOMINATED: 0,
                            UOM: '',
                            UNIT_PRICE: 0,
                            CONSUMPTION: 1,
                            FILE_ID: '',
                            NOMINATED_SUPPLIER: '',
                            PHILOSPHY_LABLE: '',
                            TYPE_OF_SUNDRIES: '',
                            STYLING: '',
                            ARTICLE_TYPE: '',
                            BRANDS: '',
                            GENDER: '',
                            SEWING_COST: 0,
                            PACKAGING_COST: 0,
                            NOMINATED_SUPPLIER: '',
                            TOTAL_SUB_ITEM_PRICE: 0,
                            TYPE: 'SEWING',
                            SEWING_CONSUMPTION: 1,
                            PACKAGING_CONSUMPTION: 1,
                            IS_NEW_OBJ: 0

                        };

                        $scope.AddAccessoriesObj = {
                            // processSNo: $scope.processSNo++,
                            SEWING_ID: '',
                            PACK_ID: '',
                            SEWING_NAME: '',
                            PACKAGING_NAME: '',
                            CATEGORY: '',
                            POLYBAG: '',
                            CARTON: 0,
                            IS_SUNDRIES_NOMINATED: 0,
                            UOM: '',
                            UNIT_PRICE: 0,
                            CONSUMPTION: 1,
                            FILE_ID: '',
                            FILE_NAME: '',
                            FILE_IMAGE_ID: 0,
                            NOMINATED_SUPPLIER: '',
                            PHILOSPHY_LABLE: '',
                            TYPE_OF_SUNDRIES: '',
                            STYLING: '',
                            ARTICLE_TYPE: '',
                            BRANDS: '',
                            GENDER: '',
                            SEWING_COST: 0,
                            PACKAGING_COST: 0,
                            NOMINATED_SUPPLIER: '',
                            TOTAL_SUB_ITEM_PRICE: 0,
                            TYPE: 'SEWING',
                            SEWING_CONSUMPTION: 1,
                            PACKAGING_CONSUMPTION: 1,
                            IS_NEW_OBJ: 0

                        };
                    }
                })

            }

            function scrollToElement(elementId, reqItem) {
                if (reqItem && !reqItem.widgetState) {
                    reqItem.widgetState = 1;
                    $timeout(function () {
                        $location.hash(elementId);
                        $anchorScroll.yOffset = 0;
                        $anchorScroll();
                    }, 2000);
                }
                else {
                    $location.hash(elementId);
                    $anchorScroll.yOffset = 0;
                    $anchorScroll();
                }
            }


            $scope.showScoreCardPopUp = false;

            $scope.showVendorScoreCard = function (vendorID) {
                $scope.scoreCardValues = [];
                $scope.showScoreCardPopUp = false;
                auctionsService.GetVendorCapacityAndScoreCard(vendorID, userService.getUserToken())
                    .then(function (response) {

                        if (response && response.length > 0) {
                            $scope.scoreCardValues1 = response;
                            if ($scope.scoreCardValues1 && $scope.scoreCardValues1.length > 0) {
                                var obj = {
                                    "VENDOR_CAPACITY_JSON": JSON.parse($scope.scoreCardValues1[0].value)
                                };
                                var obj1 = {
                                    "SCORE_CARD_JSON": JSON.parse($scope.scoreCardValues1[1].value)
                                };
                                $scope.scoreCardValues.push(obj);
                                $scope.scoreCardValues.push(obj1);
                            }

                            $scope.showScoreCardPopUp = true;
                        }

                    });
            };

        }]);