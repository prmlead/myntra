﻿prmApp
    .controller('viewapmcCtrl', ["$scope", "$stateParams", "$state", "$window", "$log", "$filter", "$http", "domain", "userService", "growlService", "apmcService",
        function ($scope, $stateParams, $state, $window, $log, $filter, $http, domain, userService, growlService, apmcService) {
            $scope.companyID = userService.getUserCompanyId();
            $scope.sessionID = userService.getUserToken();
            $scope.apmcID = $stateParams.apmcID == "" || $stateParams.apmcID == null ? 0 : $stateParams.apmcID;
            $scope.Vendors = [];
            $scope.selectedA = [];
            $scope.selectedB = [];
            $scope.categories = [];
            $scope.categoriesdata = [];
            $scope.selectedSubcategories = [];
            $scope.showCategoryDropdown = true;
            $scope.subcategories = [];
            $scope.apmc = {
                apmcID: 0,
                title: '',
                description: '',
                apmcVendors: [],
                company: {
                    companyID: $scope.companyID
                },
                customer: {
                    userID: userService.getUserId()
                }
            };

            $scope.sub = {
                selectedSubcategories: []
            };

            $scope.changeCategory = function () {
                $scope.apmc.apmcVendors = [];
                $scope.loadSubCategories();
                //$scope.GetVendorsForTechEval();
                $scope.GetCompanyVendors();
            }


            $scope.selectSubcat = function (subcat) {
                if ($scope.apmcID > 0) {
                    $scope.apmc.apmcVendors = [];
                }
                $scope.vendorsLoaded = false;
                var category = [];
                var count = 0;
                var succategory = "";
                $scope.sub.selectedSubcategories = $filter('filter')($scope.subcategories, { ticked: true });
                selectedcount = $scope.sub.selectedSubcategories.length;
                if (selectedcount > 0) {
                    succategory = _.map($scope.sub.selectedSubcategories, 'id');
                    category.push(succategory);
                    if ($scope.apmc.category != undefined) {
                        var params = { 'Categories': succategory, 'sessionID': userService.getUserToken(), 'count': selectedcount, 'uID': userService.getUserId() };
                        $http({
                            method: 'POST',
                            url: domain + 'getvendorsbycatnsubcat',
                            encodeURI: true,
                            headers: { 'Content-Type': 'application/json' },
                            data: params
                        }).then(function (response) {
                            if (response && response.data) {
                                if (response.data.length > 0) {
                                    $scope.Vendors = response.data;
                                    $scope.vendorsLoaded = true;
                                    for (var j in $scope.apmc.apmcVendors) {
                                        for (var i in $scope.Vendors) {
                                            if ($scope.Vendors[i].vendorName == $scope.apmc.apmcVendors[j].firstName + ' ' + $scope.apmc.apmcVendors[j].lastName) {
                                                $scope.Vendors.splice(i, 1);
                                            }
                                        }
                                    }
                                }
                            } else {
                                //console.log(response.data[0].errorMessage);
                            }
                        }, function (result) {
                            //console.log("there is no current auctions");
                        });
                    }
                } else {
                    $scope.GetCompanyVendors();
                }

            }

            $scope.loadSubCategories = function () {
                $scope.subcategories = _.filter($scope.categoriesdata, { category: $scope.apmc.category });
                /*$scope.subcategories = _.map($scope.subcategories, 'subcategory');*/
                //console.log($scope.subcategories);
            }

            if ($scope.apmcID > 0) {
                apmcService.getApmc($scope.apmcID, 1)
                    .then(function (response) {
                        if (response) {
                            $scope.apmc = response;
                            $scope.loadSubCategories();
                            $scope.GetCompanyVendors();
                        }
                    })
            }

            $scope.getCategories = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getcategories?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken(),
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data) {

                        if (response.data.length > 0) {
                            $scope.categories = _.uniq(_.map(response.data, 'category'));
                            $scope.categoriesdata = response.data;
                            $scope.showCategoryDropdown = true;
                        }
                    } else {
                        //console.log(response.data[0].errorMessage);
                    }
                }, function (result) {
                    //console.log("there is no current auctions");
                });
            }
            $scope.getCategories();

            $scope.disableSaveAPMC = false;

            $scope.saveApmc = function () {
                $scope.disableSaveAPMC = true;

                $scope.apmc.sessionID = $scope.sessionID;
                $scope.apmc.company = {
                    companyID: userService.getUserCompanyId()
                }
                $scope.apmc.customer = {
                    userID: userService.getUserId()
                }
                $scope.apmc.apmcSettings.customer = {
                    userID: userService.getUserId()
                }
                if (!$scope.apmc.apmcVendors) {
                    $scope.apmc.apmcVendors = [];
                }
                var params = {
                    apmc: $scope.apmc
                };
                apmcService.saveApmc(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            $scope.disableSaveAPMC = false;
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            $scope.disableSaveAPMC = false;
                            growlService.growl("APMC Saved Successfully.", "success");
                            $state.go("manageapmc");
                        }
                    })
            };

            $scope.selectForA = function (item) {
                var index = $scope.selectedA.indexOf(item);
                if (index > -1) {
                    $scope.selectedA.splice(index, 1);
                } else {
                    $scope.selectedA.splice($scope.selectedA.length, 0, item);
                }
                for (i = 0; i < $scope.selectedA.length; i++) {
                    $scope.apmc.apmcVendors.push($scope.selectedA[i]);
                    $scope.Vendors.splice($scope.Vendors.indexOf($scope.selectedA[i]), 1);
                }
                $scope.reset();
            }

            $scope.selectForB = function (item) {
                var index = $scope.selectedB.indexOf(item);
                if (index > -1) {
                    $scope.selectedB.splice(index, 1);
                } else {
                    $scope.selectedB.splice($scope.selectedA.length, 0, item);
                }
                for (i = 0; i < $scope.selectedB.length; i++) {
                    $scope.Vendors.push($scope.selectedB[i]);
                    $scope.apmc.apmcVendors.splice($scope.apmc.apmcVendors.indexOf($scope.selectedB[i]), 1);
                }
                $scope.reset();
            }

            $scope.AtoB = function () {

            }

            $scope.BtoA = function () {

            }

            $scope.reset = function () {
                $scope.selectedA = [];
                $scope.selectedB = [];
            }

            $scope.GetCompanyVendors = function () {
                $scope.vendorsLoaded = false;
                var category = [];

                category.push($scope.apmc.category);
                if ($scope.apmc.category != undefined) {
                    var params = { 'Categories': category, 'sessionID': userService.getUserToken(), 'uID': userService.getUserId() };
                    $http({
                        method: 'POST',
                        url: domain + 'getvendors',
                        encodeURI: true,
                        headers: { 'Content-Type': 'application/json' },
                        data: params
                    }).then(function (response) {
                        if (response && response.data) {
                            if (response.data.length > 0) {
                                $scope.Vendors = response.data;
                                $scope.vendorsLoaded = true;
                                for (var j in $scope.apmc.apmcVendors) {
                                    for (var i in $scope.Vendors) {
                                        if ($scope.Vendors[i].vendorName == $scope.apmc.apmcVendors[j].vendor.firstName + ' ' + $scope.apmc.apmcVendors[j].vendor.lastName) {
                                            $scope.Vendors.splice(i, 1);
                                        }
                                    }
                                }
                            }
                            //$scope.questionnaire.vendors =[];
                        } else {
                            //console.log(response.data[0].errorMessage);
                        }
                    }, function (result) {
                        //console.log("there is no current auctions");
                    });
                }
            }


            $scope.saveApmcVendors = function () {
                var params = {
                    apmcID: $scope.apmcID,
                    vendors: $scope.apmc.vendors,
                    sessionID: userService.getUserToken()
                };

                apmcService.saveVendorsForQuestionnaire(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Questionnaire will be sent to the selected Vendors.", "success");
                        }
                    })
            }



            $scope.isExists = false;

            $scope.CheckUniqueIfExists = function (param, idtype) {

                $scope.isExists = false;

                $scope.params = {
                    param: param,
                    idtype: idtype,
                    sessionID: userService.getUserToken()
                }

                apmcService.CheckUniqueIfExists($scope.params)
                    .then(function (response) {
                        $scope.isExists = response;
                    });
            }



        }]);