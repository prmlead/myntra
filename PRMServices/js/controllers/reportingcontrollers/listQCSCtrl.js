﻿prmApp
.controller('listQCSCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService",
    "PRMPRServices", "reportingService","workflowService",
    function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService,
        PRMPRServices, reportingService, workflowService) {

        $scope.userID = userService.getUserId();
        $scope.sessionID = userService.getUserToken();
        $scope.reqID = $stateParams.reqID;

        $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
        $scope.desigID = userService.getSelectedUserDesigID();

        $scope.QCSList = [];

        $scope.GetQCSList = function () {
            var params = {
                "uid": $scope.userID,
                "reqid": $scope.reqID,
                "sessionid": userService.getUserToken()
            };
            reportingService.GetQCSList(params)
                .then(function (response) {
                    $scope.QCSList = response;
                    $scope.QCSList.forEach(function (qcs, qcsIndex) {
                        qcs.CREATED_DATE = userService.toLocalDate(qcs.CREATED_DATE);
                    });
                });
        };

        $scope.GetQCSList();

        $scope.goToSaveDomesticQCS = function (reqID, qcsID) {
            var url = $state.href("cost-comparisions-qcs", { "reqID": reqID, "qcsID": qcsID });
            window.open(url, '_self');
        };

        $scope.goToSaveImportQCS = function (reqID, qcsID) {
            var url = $state.href("import-qcs", { "reqID": reqID, "qcsID": qcsID });
            window.open(url, '_self');
        };


        $scope.qcsApprovalList = [];

        $scope.goToApprovalQCS = function () {

            var url = $state.href("approval-qcs-list");
            window.open(url, '_blank');
           
        };


}]);