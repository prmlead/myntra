﻿prmApp
    .controller('importQcsCtrl', ["$scope", "$state", "$log", "$stateParams", "userService", "auctionsService", "$window",
        "$timeout", "reportingService", "growlService", "workflowService",
        function ($scope, $state, $log, $stateParams, userService, auctionsService, $window,
            $timeout, reportingService, growlService, workflowService) {
            $scope.vendorWidth = 12;
            $scope.reqId = $stateParams.reqID;
            $scope.qcsID = +$stateParams.qcsID;
            $scope.requirementDetails = {};
            $scope.qcsRequirementDetails;
            $scope.isTechSpecExport = false;
            $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;
           // $scope.isSuperUser = userService.getUserObj().isSuperUser;
            if (!$scope.isCustomer) {
                $state.go('home');
            }

            //$scope.includeGstInCal = true;
            $scope.userId = userService.getUserId();
            $scope.sessionid = userService.getUserToken();
            $scope.editForm = false;

            $scope.deptIDs = [];
            $scope.desigIDs = [];
            
            $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
            if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
                $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                    $scope.deptIDs.push(item.deptID);
                    item.listDesignation.forEach(function (item1, index1) {
                        if (item1.isAssignedToUser && item1.isValid) {
                            $scope.desigIDs.push(item1.desigID);
                        }
                    });
                });
            }

            /*region start WORKFLOW*/
            $scope.workflowList = [];
            $scope.itemWorkflow = [];
            $scope.qcsVendors = [];
            $scope.qcsItems = [];
            $scope.obj = {
                auctionVendors: []
            };
            $scope.objNew = {
                auctionVendors: []
            };
            $scope.workflowObj = {};
            $scope.workflowObj.workflowID = 0;
            $scope.currentStep = 0;
            $scope.orderInfo = 0;
            $scope.assignToShow = '';
            $scope.isWorkflowCompleted = false;
            $scope.WorkflowModule = 'QCS';
            $scope.disableWFSelection = false;
            /*region end WORKFLOW*/

            $scope.QCSDetails = {
                QCS_ID: 0,
                REQ_ID: $scope.reqId,
                U_ID: userService.getUserId(),
                QCS_CODE: '',
                PO_CODE: '',
                RECOMMENDATIONS: '',
                UNIT_CODE: ''
            };

            $scope.doPrint = false;

            $scope.printReport = function () {
                $scope.doPrint = true;
                $timeout(function () {
                    $window.print();
                    $scope.doPrint = false
                }, 1000);
            };

            $scope.htmlToCanvasSaveLoading = false;

            $scope.htmlToCanvasSave = function (format) {
                $scope.htmlToCanvasSaveLoading = true;
                setTimeout(function () {
                    try {
                        var name = "Comparisions-ReqID-" + $scope.reqId + "." + format;
                        var canvas = document.createElement("canvas");
                        if (format == 'pdf') {
                            document.getElementById("widget").style["display"] = "";
                        }
                        else {
                            document.getElementById("widget").style["display"] = "inline-block";
                        }

                        html2canvas($("#widget"), {
                            onrendered: function (canvas) {
                                theCanvas = canvas;

                                const a = document.createElement("a");
                                a.style = "display: none";
                                a.href = canvas.toDataURL();

                                // Add Image to HTML
                                //document.body.appendChild(canvas);

                                /* Save As PDF */
                                if (format == 'pdf') {
                                    var imgData = canvas.toDataURL();
                                    var pdf = new jsPDF();
                                    pdf.addImage(imgData, 'JPEG', 0, 0);
                                    pdf.save(name);
                                }
                                else {
                                    a.download = name;
                                    a.click();
                                }

                                // Clean up 
                                //document.body.removeChild(canvas);
                                document.getElementById("widget").style["display"] = "";
                                $scope.htmlToCanvasSaveLoading = false;
                            }
                        });
                    }
                    catch (err) {
                        document.getElementById("widget").style["display"] = "";
                        $scope.htmlToCanvasSaveLoading = false;
                    }

                }, 500);

            };

            $scope.SaveQCSDetails = function (val) {
                $scope.QCSDetails.QCS_TYPE = 'IMPORT';
                if (!$scope.QCSDetails.QCS_CODE) {
                    $scope.QCSDetails.QCS_CODE = new Date().getUTCMilliseconds();
                }

                $scope.QCSDetails.REQ_JSON = JSON.stringify($scope.requirementDetails);

                if ($scope.QCSDetails.CREATED_USER == null) {
                    $scope.QCSDetails.CREATED_USER = $scope.userId;
                }
                if ($scope.QCSDetails.U_ID == 0 || $scope.QCSDetails.U_ID == null) {
                    $scope.QCSDetails.U_ID = $scope.userId;
                }

                var params = {
                    "qcsdetails": $scope.QCSDetails,
                    "sessionid": userService.getUserToken()
                };

                if ($scope.workflowObj.workflowID) {
                    params.qcsdetails.WF_ID = $scope.workflowObj.workflowID;
                }

                //if (!params.qcsdetails.WF_ID > 0) {
                //    growlService.growl('Please select Workflow', "inverse");
                //    return;
                //}
                reportingService.SaveQCSDetails(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            if (val == 1) {
                                $scope.goToQCSList($scope.reqId);
                                growlService.growl("Details saved successfully.", "success");
                            } else {
                                var url = $state.href("import-qcs", { "reqID": $scope.reqId, "qcsID": response.objectID });
                                window.open(url, '_self');
                            }
                        }
                    });
            };

            $scope.isQCSImportFormdisabled = true;

            $scope.checkIsFormDisable = function () {
                $scope.isQCSImportFormdisabled = false;
                if ($scope.itemWorkflow.length == 0) {
                    $scope.isQCSImportFormdisabled = true;
                } else {
                    if (($scope.QCSDetails.CREATED_BY == +userService.getUserId() || $scope.QCSDetails.MODIFIED_BY == +userService.getUserId()) && $scope.itemWorkflow[0].WorkflowTracks.length > 0 &&
                        $scope.itemWorkflow[0].WorkflowTracks[0].status !== "APPROVED" && $scope.itemWorkflow[0].WorkflowTracks[0].order == 1 && $scope.itemWorkflow[0].workflowID > 0) {
                        $scope.isQCSImportFormdisabled = true;
                    }
                }
            };

            $scope.GetQCSDetails = function () {
                var params = {
                    "qcsid": $scope.qcsID,
                    "sessionid": userService.getUserToken()
                };
                reportingService.GetQCSDetails(params)
                    .then(function (response) {
                        $scope.QCSDetails = response;

                        if ($scope.QCSDetails.WF_ID > 0) {
                            $scope.workflowObj.workflowID = $scope.QCSDetails.WF_ID;
                        }

                        if ($scope.QCSDetails.REQ_JSON) {
                            $scope.qcsRequirementDetails = JSON.parse($scope.QCSDetails.REQ_JSON);
                        }

                        $scope.getRequirementData();
                        $scope.getItemWorkflow();
                    });
            };

            if ($scope.qcsID > 0) {
                $scope.GetQCSDetails();
            }

            $scope.goToQCSList = function (reqID) {
                var url = $state.href("list-qcs", { "reqID": $scope.reqId });
                window.open(url, '_self');
            };

            /*region start WORKFLOW*/

            $scope.getWorkflows = function () {
                workflowService.getWorkflowList()
                    .then(function (response) {
                        $scope.workflowList = [];
                        $scope.workflowListTemp = response;
                        $scope.workflowListTemp.forEach(function (item, index) {
                            if (item.WorkflowModule == $scope.WorkflowModule) {
                                $scope.workflowList.push(item);
                            }
                        });

                        if (userService.getUserObj().isSuperUser) {
                            $scope.workflowList = $scope.workflowList;
                        }
                        else {
                            $scope.workflowList = $scope.workflowList.filter(function (item) {
                                return item.deptID == userService.getSelectedUserDepartmentDesignation().deptID;

                            });
                        }
                    });
            };

            $scope.getWorkflows();

            $scope.getItemWorkflow = function () {
                workflowService.getItemWorkflow(0, $scope.qcsID, $scope.WorkflowModule)
                    .then(function (response) {
                        $scope.itemWorkflow = response;
                        $scope.checkIsFormDisable();
                        if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                            $scope.currentStep = 0;

                            var count = 0;

                            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {

                                if (track.status == 'APPROVED' || track.status == 'HOLD') {
                                    $scope.isFormdisabled = true;
                                }

                                if (track.status == 'APPROVED') {
                                    $scope.isWorkflowCompleted = true;
                                    $scope.orderInfo = track.order;
                                    $scope.assignToShow = track.status;

                                }
                                else {
                                    $scope.isWorkflowCompleted = false;
                                }



                                if (track.status == 'REJECTED' && count == 0) {
                                    count = count + 1;
                                }

                                if ((track.status == 'PENDING' || track.status == 'HOLD') && count == 0) {
                                    count = count + 1;
                                    $scope.IsUserApproverForStage(track.approverID);
                                    $scope.currentAccess = track.order;
                                }

                                if ((track.status == 'PENDING' || track.status == 'HOLD' || track.status == 'REJECTED') && $scope.currentStep == 0) {
                                    $scope.currentStep = track.order;
                                    return false;
                                }
                            });
                        }
                    });

            };

            $scope.updateTrack = function (step, status) {
                $scope.disableAssignPR = true;
                $scope.commentsError = '';

                var tempArray = $scope.itemWorkflow[0].WorkflowTracks[$scope.itemWorkflow[0].WorkflowTracks.length - 1];
                if (step.order == tempArray.order && status == 'APPROVED') {
                    $scope.disableAssignPR = false;
                } else {
                    $scope.disableAssignPR = true;
                }

                if ($scope.isReject) {
                    $scope.commentsError = 'Please Save Rejected Items/Qty';
                    return false;
                }

                if (status == 'REJECTED' && (step.comments == null || step.comments == "")) {
                    $scope.commentsError = 'Please enter comments';
                    return false;
                }

                step.status = status;
                step.sessionID = $scope.sessionid;
                step.modifiedBy = userService.getUserId();

                step.moduleName = $scope.WorkflowModule;

                workflowService.SaveWorkflowTrack(step)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            $scope.getItemWorkflow();
                            //location.reload();
                            //  $state.go('list-pr');
                        }
                    });
            };

            $scope.assignWorkflow = function (moduleID) {
                workflowService.assignWorkflow(({ wID: $scope.workflowObj.workflowID, moduleID: moduleID, user: userService.getUserId(), sessionID: $scope.sessionid }))
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                            $scope.isSaveDisable = false;
                        }
                        else {
                            //  $state.go('list-pr');
                        }
                    });
            };

            $scope.IsUserApprover = false;

            $scope.functionResponse = false;

            $scope.IsUserApproverForStage = function (approverID) {
                workflowService.IsUserApproverForStage(approverID, userService.getUserId())
                    .then(function (response) {
                        $scope.IsUserApprover = response;
                    });
            };

            $scope.isApproverDisable = function (index) {

                var disable = true;

                var previousStep = {};

                $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {

                    if (index == stepIndex) {
                        if (stepIndex == 0) {
                            if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                (step.status == 'PENDING' || step.status == 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                        else if (stepIndex > 0) {
                            if (previousStep.status == 'PENDING' || previousStep.status == 'HOLD' || previousStep.status == 'REJECTED') {
                                disable = true;
                            }
                            else if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                (step.status == 'PENDING' || step.status == 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                    }
                    previousStep = step;
                })

                return disable;
            };

            /*region end WORKFLOW*/


            $scope.editFormPage = function () {
                $scope.editForm = !$scope.editForm;
                $scope.requirementDetails.auctionVendors.forEach(function (vendor) {
                    $scope.fieldValidation(vendor);
                });
            };

            $scope.fieldValidation = function (vendor) {
                if ($scope.editForm && vendor.INCO_TERMS) {
                    auctionsService.getIncotermProductConfig(vendor.INCO_TERMS, userService.getUserToken())
                        .then(function (response) {
                            vendor.INCO_TERMS_CONFIG = response;

                        });
                } else {
                    vendor.isEdit = false;
                }
            };

            $scope.fieldValidationList = function (vendor) {
                if (vendor.INCO_TERMS) {
                    auctionsService.getIncotermProductConfig(vendor.INCO_TERMS, userService.getUserToken())
                        .then(function (response) {
                            vendor.INCO_TERMS_CONFIG = response;
                        });
                }
            };

            $scope.unitPriceCalculation = function (item) {
                item.revUnitPrice = item.unitPrice;
            };

            $scope.getRequirementData = function () {
                auctionsService.getrequirementdata({ "reqid": $scope.reqId, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                    .then(function (response) {
                        $scope.qcsItems = [];
                        if (response.CB_TIME_LEFT > 0) {
                            response.status = "STARTED";

                        }

                        response.listRequirementItems.forEach(function (reqItem, index) {
                            reqItem.isVisible = true;
                            var qcsItem = {
                                itemID: reqItem.itemID,
                                itemName: reqItem.productIDorName,
                                isSelected: true
                            };

                            $scope.qcsItems.push(qcsItem);
                        });

                        if (response) {
                            $scope.qcsVendors = [];
                            $scope.requirementDetails = response;
                            $scope.vendorWidth = Math.floor(12 / ($scope.requirementDetails.auctionVendors.length + 1));
                            $scope.requirementDetails.auctionVendors = _.filter($scope.requirementDetails.auctionVendors, function (vendor) { return vendor.companyName != 'PRICE_CAP'; });
                            if ($scope.qcsID > 0 && $scope.qcsRequirementDetails) {
                                $scope.requirementDetails.customerComment = $scope.qcsRequirementDetails.customerComment;
                                $scope.requirementDetails.includeGstInCal = $scope.qcsRequirementDetails.includeGstInCal;
                                $scope.requirementDetails.listRequirementItems.forEach(function (item, index) {
                                    item.maxHeight = '';
                                    var tempItem = _.filter($scope.qcsRequirementDetails.listRequirementItems, function (qcsReqItem) { return qcsReqItem.itemID === item.itemID; });
                                    if (tempItem && tempItem.length > 0) {
                                        item.qtyDistributed = tempItem[0].qtyDistributed;
                                    }
                                });

                                

                                $scope.requirementDetails.auctionVendors.forEach(function (vendor, index) {
                                    vendor.isVisible = true;
                                    var qcsVendor = {
                                        vendorID: vendor.vendorID,
                                        vendorCompany: vendor.companyName,
                                        isSelected: true
                                    };

                                    $scope.qcsVendors.push(qcsVendor);

                                    var tempQCSVendor = _.filter($scope.qcsRequirementDetails.auctionVendors, function (qcsVendor) { return qcsVendor.vendorID === vendor.vendorID; });
                                    if (tempQCSVendor && tempQCSVendor.length > 0) {
                                        vendor.currencyRate = tempQCSVendor[0].currencyRate;
                                        vendor.customerComment = tempQCSVendor[0].customerComment;
                                        vendor.revChargeAny = tempQCSVendor[0].revChargeAny;
                                        vendor.seaFreight = tempQCSVendor[0].seaFreight;
                                        vendor.insurance = tempQCSVendor[0].insurance;
                                        vendor.basicCustomDuty = tempQCSVendor[0].basicCustomDuty;
                                        vendor.antiDumping = tempQCSVendor[0].antiDumping;
                                        vendor.socialWelfareCharge = tempQCSVendor[0].socialWelfareCharge;
                                        vendor.clearingCharges = tempQCSVendor[0].clearingCharges;
                                        vendor.socialWelfareCharge = tempQCSVendor[0].socialWelfareCharge;
                                        vendor.localTransportCharges = tempQCSVendor[0].localTransportCharges;
                                        vendor.iGst = tempQCSVendor[0].iGst;

                                        auctionsService.getIncotermProductConfig(vendor.INCO_TERMS, userService.getUserToken())
                                            .then(function (response) {

                                                vendor.listRequirementItems.forEach(function (vendorItem, index) {

                                                    //console.log(JSON.parse(vendorItem.productQuotationTemplateJson));
                                                    if (vendorItem.productQuotationTemplateJson && vendorItem.productQuotationTemplateJson != '' && vendorItem.productQuotationTemplateJson != null && vendorItem.productQuotationTemplateJson != undefined) {
                                                        vendorItem.productQuotationTemplateJson = JSON.parse(vendorItem.productQuotationTemplateJson);

                                                    }
                                                    //console.log(vendorItem.productQuotationTemplateJson);
                                                    var newArray = response.filter(function (res) { return res.ProductId == vendorItem.catalogueItemID; });
                                                    var tempQCSVendorItem = _.filter(tempQCSVendor[0].listRequirementItems, function (qcsVendorItem) { return qcsVendorItem.itemID === vendorItem.itemID; });

                                                    if (newArray && newArray.length > 0) {
                                                        if (newArray[0].IS_CUSTOMER_EDIT == 1) {

                                                            if (tempQCSVendorItem && tempQCSVendorItem.length > 0) {
                                                                vendorItem.unitPrice = tempQCSVendorItem[0].unitPrice;
                                                                vendorItem.revUnitPrice = tempQCSVendorItem[0].revUnitPrice;
                                                            }
                                                        }
                                                    }
                                                    vendorItem.qtyDistributed = tempQCSVendorItem[0].qtyDistributed;
                                                });
                                            });

                                    }
                                });
                            } else {
                                $scope.requirementDetails.includeGstInCal = true;
                                $scope.requirementDetails.auctionVendors.forEach(function (vendor, index) {
                                    vendor.currencyRate = vendor.vendorCurrencyFactor;
                                    vendor.isVisible = true;
                                    var qcsVendor = {
                                        vendorID: vendor.vendorID,
                                        vendorCompany: vendor.companyName,
                                        isSelected: true
                                    };

                                    $scope.qcsVendors.push(qcsVendor);
                                    vendor.listRequirementItems.forEach(function (vendorItem, index) {
                                        //$scope.displayLeastItemPriceColor();
                                        if (vendorItem.productQuotationTemplateJson && vendorItem.productQuotationTemplateJson != '' && vendorItem.productQuotationTemplateJson != null && vendorItem.productQuotationTemplateJson != undefined) {
                                            vendorItem.productQuotationTemplateJson = JSON.parse(vendorItem.productQuotationTemplateJson);
                                        }
                                    });
                                });
                            }
                        }
                    });
            };

            if (!$scope.qcsID) {
                $scope.getRequirementData();
            }

            $scope.getVendorItemPrices = function (item, vendor) {
                var emptyObj = {
                    unitPrice: 0,
                    revUnitPrice: 0
                };
                var vendorItemPrices = _.filter(vendor.listRequirementItems, function (vendorItem) { return vendorItem.itemID === item.itemID; });
                if (vendorItemPrices && vendorItemPrices.length > 0) {
                    if (vendorItemPrices[0] && vendorItemPrices[0].productQuotationTemplateJson.length > 0) {
                        item.maxHeight = '180px';
                    }
                    return vendorItemPrices[0];
                } else {
                    return emptyObj;
                }
            };

            $scope.getVendorTotalPriceWithoutTax = function (vendor) {
                var price = 0;
                vendor.listRequirementItems.forEach(function (item, index) {
                    price += item.revUnitPrice * item.productQuantity;
                });

                return +price;
            };

            $scope.getVendorTotalLandingPrice = function (vendor) {
                var price = 0;
                price += $scope.getVendorTotalPriceWithoutTax(vendor);

                if (vendor.revChargeAny) {
                    price -= (+vendor.revChargeAny);
                }

                return price;
            };

            $scope.getVendorTotalUnitItemPrices = function (vendor) {
                var price = 0;
                vendor.listRequirementItems.forEach(function (item, index) {
                    price += (+item.unitPrice);
                });

                return price;
            };

            $scope.getVendorTotalRevUnitItemPrices = function (vendor) {
                var price = 0;
                vendor.listRequirementItems.forEach(function (item, index) {
                    price += (+item.revUnitPrice);
                });

                return price;
            };

            $scope.getVendorTotalTax = function (vendor, taxType) {
                var totalTax = 0;
                vendor.listRequirementItems.forEach(function (item, index) {
                    if (taxType === 'CGST') {
                        totalTax += (item.revUnitPrice * item.productQuantity) * (item.cGst / 100);
                    }

                    if (taxType === 'IGST') {
                        totalTax += (item.revUnitPrice * item.productQuantity) * (item.iGst / 100);
                    }

                    if (taxType === 'SGST') {
                        totalTax += (item.revUnitPrice * item.productQuantity) * (item.sGst / 100);
                    }
                });

                return totalTax;
            };

            $scope.isNonCoreItemEditable = function (item, vendor) {
                var isEditable = false;

                if (item && !item.isCoreProductCategory && vendor && vendor.INCO_TERMS && vendor.INCO_TERMS_CONFIG) {
                    vendor.INCO_TERMS_CONFIG.forEach(function (incoItem) {
                        vendor.listRequirementItems.forEach(function (vendorItem, itemIndex) {
                            if (item.catalogueItemID === incoItem.ProductId && item.catalogueItemID === vendorItem.catalogueItemID) {
                                if (incoItem.IS_CUSTOMER_EDIT) {
                                    isEditable = true;
                                }
                            }
                        });
                    });
                }

                return isEditable;
            };

            $scope.incoTermsPrice = function (vendor, type) {
                if (vendor.INCO_TERMS === type) {
                    return $scope.getVendorTotalPriceWithoutTax(vendor);
                } else {
                    return '';
                }
            };

            $scope.cifTotal = function (vendor) {
                var insurance = !vendor.insurance ? 0 : +vendor.insurance;
                var seaFreight = !vendor.seaFreight ? 0 : +vendor.seaFreight;
                var totalPrice = !$scope.getVendorTotalPriceWithoutTax(vendor) ? 0 : $scope.getVendorTotalPriceWithoutTax(vendor);
                return (insurance + totalPrice + seaFreight);
            };

            $scope.totalValueInINR = function (vendor) {
                var cifTotal = $scope.cifTotal(vendor);
                const currencyConversion = vendor.currencyRate ? +vendor.currencyRate : 1;
                return cifTotal * currencyConversion;
            };

            $scope.basicCustomDuty = function (vendor) {
                var tax = vendor.basicCustomDuty ? +vendor.basicCustomDuty : 0;
                return $scope.totalValueInINR(vendor) * (tax / 100);
            };

            $scope.socialWelfareTotal = function (vendor) {
                var tax = vendor.socialWelfareCharge ? +vendor.socialWelfareCharge : 0;
                return $scope.basicCustomDuty(vendor) * (tax / 100);
            };

            $scope.calculateImportIGST = function (vendor) {
                var tax = vendor.iGst ? +vendor.iGst : 0;
                var dumping = vendor.antiDumping ? +vendor.antiDumping : 0;
                var socialWelfareCharge = vendor.socialWelfareCharge ? +vendor.socialWelfareCharge : 0;
                const currencyConversionRate = vendor.currencyRate ? +vendor.currencyRate : 1;
                var totalINRValue = $scope.totalValueInINR(vendor) ? $scope.totalValueInINR(vendor) : 0;
                var basicCustomDuty = $scope.basicCustomDuty(vendor) ? $scope.basicCustomDuty(vendor) : 0;
                return (tax / 100) * (totalINRValue + basicCustomDuty + (dumping * currencyConversionRate) + ((socialWelfareCharge / 100) * basicCustomDuty));
            };

            $scope.calculateTotal = function (vendor, includeGstInCal) {
                var totalPrice = 0 , tax = 0;
                
                var locaTransportCharges = vendor.localTransportCharges ? +vendor.localTransportCharges : 0;
                var clearingCharges = vendor.clearingCharges ? +vendor.clearingCharges : 0;
                var dumping = vendor.antiDumping ? +vendor.antiDumping : 0;
                var socialWelfareCharge = vendor.socialWelfareCharge ? +vendor.socialWelfareCharge : 0;
                const currencyConversionRate = vendor.currencyRate ? +vendor.currencyRate : 1;
                var totalINRValue = $scope.totalValueInINR(vendor) ? $scope.totalValueInINR(vendor) : 0;
                var basicCustomDuty = $scope.basicCustomDuty(vendor) ? $scope.basicCustomDuty(vendor) : 0;

                totalPrice += totalINRValue + basicCustomDuty + (dumping * currencyConversionRate) + ((socialWelfareCharge / 100) * basicCustomDuty) + $scope.calculateImportIGST(vendor);

                if (!includeGstInCal)
                {
                    tax = $scope.calculateImportIGST(vendor);
                    totalPrice -= tax;
                }

                //vendor.landedCost = totalPrice + locaTransportCharges + clearingCharges;

                return totalPrice;
            };


            $scope.calculateLandedCost = function (vendor, includeGstInCal) {
                var landedCost = 0, tax = 0, totalPrice = 0;

                var locaTransportCharges = vendor.localTransportCharges ? +vendor.localTransportCharges : 0;
                var clearingCharges = vendor.clearingCharges ? +vendor.clearingCharges : 0;
                var dumping = vendor.antiDumping ? +vendor.antiDumping : 0;
                var socialWelfareCharge = vendor.socialWelfareCharge ? +vendor.socialWelfareCharge : 0;
                const currencyConversionRate = vendor.currencyRate ? +vendor.currencyRate : 1;
                var totalINRValue = $scope.totalValueInINR(vendor) ? $scope.totalValueInINR(vendor) : 0;
                var basicCustomDuty = $scope.basicCustomDuty(vendor) ? $scope.basicCustomDuty(vendor) : 0;

                totalPrice += totalINRValue + basicCustomDuty + (dumping * currencyConversionRate) + ((socialWelfareCharge / 100) * basicCustomDuty) + $scope.calculateImportIGST(vendor)
                              + locaTransportCharges + clearingCharges;

                if (!includeGstInCal) {
                    tax = $scope.calculateImportIGST(vendor);
                    totalPrice -= tax;
                }

                vendor.landedCost = totalPrice;
                landedCost = totalPrice;

                return landedCost;
            }; 

        $scope.getItemRank = function (item, vendor) {
            var vendorItemPrices = _.filter(vendor.listRequirementItems, function (vendorItem) { return vendorItem.itemID === item.itemID; });
            if (vendorItemPrices && vendorItemPrices.length > 0) {
                return vendorItemPrices[0].itemRank;
            }
        };

        $scope.exportTechSpec = function () {
            setTimeout(function () {
                tableToExcel('testTable', 'Comparitives');
                setTimeout(function () {
                    location.reload();
                }, 1000);
            }, 3000);

            
        };

        $scope.isVendorVisible = function (qcsVendor) {
            qcsVendor.isSelected = !qcsVendor.isSelected;
            var vendorTemp = _.filter($scope.requirementDetails.auctionVendors, function (auctionVendor) { return auctionVendor.vendorID === qcsVendor.vendorID; });
            if (vendorTemp && vendorTemp.length > 0) {
                vendorTemp[0].isVisible = qcsVendor.isSelected;
            }
        };

        $scope.isReqItemVisible = function (qcsItem) {
            qcsItem.isSelected = !qcsItem.isSelected;
            var itemTemp = _.filter($scope.requirementDetails.listRequirementItems, function (reqItem) { return reqItem.itemID === qcsItem.itemID; });
            if (itemTemp && itemTemp.length > 0) {
                itemTemp[0].isVisible = qcsItem.isSelected;
            }
        };


        $scope.CalculateRankBasedOnLandingPrice = function (vendor) {
            vendor.landingPriceRank = 'NA';
            var validVendorsForRanking = _.filter($scope.requirementDetails.auctionVendors, function (auctionVendor) { return auctionVendor.landedCost > 0 && auctionVendor.isQuotationRejected === 0; });
            if (validVendorsForRanking && validVendorsForRanking.length > 0) {
                var sortedVendors = _.orderBy(validVendorsForRanking, ['landedCost'], ['asc']);
                var rank = _.findIndex(sortedVendors, function (vendor1) { return vendor1.vendorID === vendor.vendorID; });
                if (rank >= 0) {
                    vendor.landingPriceRank = rank + 1;
                }
            }

            return vendor.landingPriceRank;
        };





        $scope.UpdateOtherCharges = function () {
            $scope.SaveQCSDetails(0);
            $scope.getCalculatedOtherCharges();

        };

        $scope.saveVendorOtherChargesObject = {
            vendorID: 0,
            DIFFERENTIAL_FACTOR: 0,
            requirementID: 0
        };

        $scope.getCalculatedOtherCharges = function () {

            $scope.saveVendorOtherCharges = [];

            $scope.requirementDetails.auctionVendors.forEach(function (item, index) {


                    item.DIFFERENTIAL_FACTOR = 0;
                    item.DIFFERENTIAL_FACTOR = $scope.calculateLandedCost(item, $scope.requirementDetails.includeGstInCal) - item.revVendorTotalPrice;

                    $scope.saveVendorOtherChargesObject = {
                        vendorID: 0,
                        DIFFERENTIAL_FACTOR: 0,
                        requirementID: 0
                    };

                    $scope.saveVendorOtherChargesObject.vendorID = item.vendorID;
                    $scope.saveVendorOtherChargesObject.DIFFERENTIAL_FACTOR = item.DIFFERENTIAL_FACTOR;
                    $scope.saveVendorOtherChargesObject.requirementID = $scope.requirementDetails.requirementID;
                $scope.saveVendorOtherCharges.push($scope.saveVendorOtherChargesObject);


            });

            var params =
            {
                userID: userService.getUserId(),
                vendorOtherChargesArr: $scope.saveVendorOtherCharges,
                sessionID: userService.getUserToken()
            }


            auctionsService.saveVendorOtherCharges(params)
                .then(function (response) {

                    if (response.errorMessage === "") {
                        growlService.growl("Other Charges Updated Successfully", "success");
                    } else {
                        growlService.growl("Other Charges Updation Failed", "inverse");
                    }

                });



            };


            $scope.isUserBelongsToDeptandDesig = function (deptID, desigID) {
                var isEligible = true;

                if ($scope.deptIDs.indexOf(deptID) != -1 && $scope.desigIDs.indexOf(desigID) != -1) {
                    isEligible = true;
                } else {
                    isEligible = false;
                }

                return isEligible;
            };

}]);