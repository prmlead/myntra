﻿prmApp
    .controller('importQcsCtrlV2', ["$scope", "$state", "$log", "$stateParams", "userService", "auctionsService", "$window",
        "$timeout", "reportingService",
        function ($scope, $state, $log, $stateParams, userService, auctionsService, $window, $timeout, reportingService) {
            $scope.reqId = $stateParams.reqID;
            $scope.isUOMDifferent = false;
            $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;
            if (!$scope.isCustomer) {
                $state.go('home');
            };

            $scope.userId = userService.getUserId();
            $scope.sessionid = userService.getUserToken();
            $scope.listRequirementTaxes = [];
            $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;
            $scope.uomDetails = [];
            $scope.subIemsColumnsVisibility = {
                hideSPECIFICATION: true,
                hideQUANTITY: true,
                hideTAX: true,
                hidePRICE: true
            };

            $scope.itemRowStart = 9;
            $scope.numberOfVendors = 0;
            $scope.vendorStartCell = 'J';
            $scope.vendorEndCell = 'J';

            $scope.handleUOMItems = function (item) {
                if (item) {
                    item.reqVendors.forEach(function (vendor, index) {
                        var uomDetailsObj = {
                            itemId: item.itemID,
                            vendorId: vendor.vendorID,
                            containsUOMItem: false
                        };

                        if (vendor.quotationPrices) {
                            uomDetailsObj.containsUOMItem = item.productQuantityIn.toUpperCase() != vendor.quotationPrices.vendorUnits.toUpperCase();
                        }

                        $scope.uomDetails.push(uomDetailsObj);
                    });
                }
            };

            $scope.containsUOMITems = function (vendorId) {
                var filteredUOMitems = _.filter($scope.uomDetails, function (uomItem) { return uomItem.vendorId == vendorId && uomItem.containsUOMItem == true; });
                if (filteredUOMitems && filteredUOMitems.length > 0) {
                    return true;
                }

                return false;
            };

            $scope.ReqReportForExcel = [];
            $scope.GetReqReportForExcel = function () {
                reportingService.GetReqReportForExcel($scope.reqId, userService.getUserToken())
                    .then(function (response) {
                        $scope.ReqReportForExcel = response;

                        if ($scope.ReqReportForExcel && $scope.ReqReportForExcel.reqItems && $scope.ReqReportForExcel.reqItems.length > 0) {
                            if ($scope.ReqReportForExcel.reqItems[0].reqVendors) {
                                $scope.numberOfVendors = $scope.ReqReportForExcel.reqItems[0].reqVendors.length;
                            }                            
                        }

                        let tempIntFromChar = $scope.vendorStartCell.charCodeAt(0);
                        $scope.vendorEndCell = String.fromCharCode(tempIntFromChar + ($scope.numberOfVendors - 1));

                        //$scope.ReqReportForExcel.currentDate = new moment($scope.ReqReportForExcel.currentDate).format("DD-MM-YYYY HH:mm");
                        //$scope.ReqReportForExcel.postedOn = new moment($scope.ReqReportForExcel.postedOn).format("DD-MM-YYYY HH:mm");
                        //$scope.ReqReportForExcel.startTime = new moment($scope.ReqReportForExcel.startTime).format("DD-MM-YYYY HH:mm");
                        // // // #INTERNATIONALIZATION-0-2019-02-12-0-2019-02-12
                        $scope.ReqReportForExcel.currentDate = userService.toLocalDate($scope.ReqReportForExcel.currentDate);
                        $scope.ReqReportForExcel.postedOn = userService.toLocalDate($scope.ReqReportForExcel.postedOn);
                        $scope.ReqReportForExcel.startTime = userService.toLocalDate($scope.ReqReportForExcel.startTime);

                        $scope.ReqReportForExcel.reqItems.forEach(function (item, index) {
                            $scope.handleUOMItems(item);
                            if (item.reqVendors && item.reqVendors.length > 0) {
                                item.reqVendors.forEach(function (vendor, index) {
                                    if (vendor.quotationPrices.productQuotationTemplateJson !== '') {
                                        vendor.quotationPrices.productQuotationTemplateArray1 = JSON.parse(vendor.quotationPrices.productQuotationTemplateJson);

                                        vendor.quotationPrices.productQuotationTemplateArray1.forEach(function (obj, objIdx) {
                                            if (obj.IS_CALCULATED == 6 || obj.IS_CALCULATED == 7 || obj.IS_CALCULATED == 8 || obj.IS_CALCULATED == 9 || obj.IS_CALCULATED == 2 || obj.IS_CALCULATED == 3) {
                                                obj.BULK_PRICE = '-';
                                                obj.CONSUMPTION = '-';
                                                obj.REV_BULK_PRICE = '-';
                                                obj.REV_CONSUMPTION = '-';
                                            }

                                        })

                                    }
                                });
                            }
                        });
                    });
            };

            $scope.GetReqReportForExcel();


            //$scope.getCustomerData = function (userId) {
            //    auctionsService.getrequirementdata({ "reqid": $scope.reqId, "sessionid": userService.getUserToken(), 'userid': userId })
            //        .then(function (response) {
            //            $scope.auctionItem = response;
            //            $scope.auctionItem.auctionVendors = _.filter($scope.auctionItem.auctionVendors, function (x) { return x.companyName !== 'PRICE_CAP'; });
            //            if ($scope.auctionItem.auctionVendors.length > 0) {
            //                $scope.getRequirementData();
            //            };

            //        });
            //}

            //$scope.getVendorData = function (item) {
            //    auctionsService.getrequirementdata({ "reqid": $scope.reqId, "sessionid": userService.getUserToken(), 'userid': item.vendorID })
            //        .then(function (response) {
            //            $scope.auctionItemVendor = response;
            //            item.auctionItemVendor = $scope.auctionItemVendor;
            //        });
            //}

            //$scope.getRequirementData = function () {            
            //    $scope.auctionItem.auctionVendors.forEach(function (item, index) {
            //        if (item.vendorID > 0) {
            //            item.auctionItemVendor = [];
            //            $scope.getVendorData(item);
            //        }
            //    });
            //}

            //$scope.getCustomerData($scope.userId);

            //$scope.auctionItem.auctionVendors.auctionItemVendor.auctionVendors[0].listRequirementItems.productIDorName;
            $scope.ExportToExcel = function (tableid) {
                var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
                var textRange; var j = 0;
                tab = document.getElementById(tableid);//.getElementsByTagName('table'); // id of table
                if (tab == null) {
                    return false;
                }
                if (tab.rows.length == 0) {
                    return false;
                }

                for (j = 0; j < tab.rows.length; j++) {
                    tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
                    //tab_text=tab_text+"</tr>";
                }

                tab_text = tab_text + "</table>";
                tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
                tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
                tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

                var ua = window.navigator.userAgent;
                var msie = ua.indexOf("MSIE ");

                if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
                {
                    txtArea1.document.open("txt/html", "replace");
                    txtArea1.document.write(tab_text);
                    txtArea1.document.close();
                    txtArea1.focus();
                    sa = txtArea1.document.execCommand("SaveAs", true, "download.xls");
                }
                else                 //other browser not tested on IE 11
                    //sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));
                    try {
                        var blob = new Blob([tab_text], { type: "application/vnd.ms-excel" });
                        window.URL = window.URL || window.webkitURL;
                        link = window.URL.createObjectURL(blob);
                        a = document.createElement("a");
                        if (document.getElementById("caption") != null) {
                            a.download = document.getElementById("caption").innerText;
                        }
                        else {
                            a.download = 'download';
                        }

                        a.href = link;

                        document.body.appendChild(a);

                        a.click();

                        document.body.removeChild(a);
                    } catch (e) {
                    }


                return false;
            }


            $scope.doPrint = false;

            $scope.printReport = function () {
                $scope.doPrint = true;
                $timeout(function () {
                    $window.print();
                    $scope.doPrint = false
                }, 1000);
            }


            $scope.htmlToCanvasSaveLoading = false;

            $scope.htmlToCanvasSave = function (format) {
                $scope.htmlToCanvasSaveLoading = true;
                setTimeout(function () {
                    try {
                        var name = "Comparisions-ReqID-" + $scope.reqId + "." + format;
                        var canvas = document.createElement("canvas");
                        if (format == 'pdf') {
                            document.getElementById("widget").style["display"] = "";
                        }
                        else {
                            document.getElementById("widget").style["display"] = "inline-block";
                        }

                        html2canvas($("#widget"), {
                            onrendered: function (canvas) {
                                theCanvas = canvas;

                                const a = document.createElement("a");
                                a.style = "display: none";
                                a.href = canvas.toDataURL();

                                // Add Image to HTML
                                //document.body.appendChild(canvas);

                                /* Save As PDF */
                                if (format == 'pdf') {
                                    var imgData = canvas.toDataURL();
                                    var pdf = new jsPDF();
                                    pdf.addImage(imgData, 'JPEG', 0, 0);
                                    pdf.save(name);
                                }
                                else {
                                    a.download = name;
                                    a.click();
                                }

                                // Clean up 
                                //document.body.removeChild(canvas);
                                document.getElementById("widget").style["display"] = "";
                                $scope.htmlToCanvasSaveLoading = false;
                            }
                        });
                    }
                    catch (err) {
                        document.getElementById("widget").style["display"] = "";
                        $scope.htmlToCanvasSaveLoading = false;
                    }
                    finally {

                    }

                }, 500);

            };

            $scope.getTotalTax = function (quotation) {
                if (quotation) {
                    if (!quotation.cGst || quotation.cGst == undefined) {
                        quotation.cGst = 0;
                    }

                    if (!quotation.sGst || quotation.sGst == undefined) {
                        quotation.sGst = 0;
                    }

                    if (!quotation.iGst || quotation.iGst == undefined) {
                        quotation.iGst = 0;
                    }

                    return quotation.cGst + quotation.sGst + quotation.iGst;

                } else {
                    return 0;
                }
            };

            $scope.isOnlySpecificationTemplate = function (productQuotationTemplateArray) {
                var isOnlySpecificatoin = true;
                if (productQuotationTemplateArray.length > 0) {
                    var items = _.filter(productQuotationTemplateArray, function (item) { return item.HAS_PRICE; });
                    if (items && items.length > 0) {
                        isOnlySpecificatoin = false;
                    }
                }

                return isOnlySpecificatoin;
            };

            $scope.handleSubItemsVisibility = function (productQuotationTemplateArray) {
                if (productQuotationTemplateArray && productQuotationTemplateArray.length > 0) {
                    var items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_SPECIFICATION === 1);
                    });

                    if (items && items.length > 0) {
                        $scope.subIemsColumnsVisibility.hideSPECIFICATION = false;
                    }

                    items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_QUANTITY === 1);
                    });

                    if (items && items.length > 0) {
                        $scope.subIemsColumnsVisibility.hideQUANTITY = false;
                    }

                    items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_TAX === 1);
                    });

                    if (items && items.length > 0) {
                        $scope.subIemsColumnsVisibility.hideTAX = false;
                    }

                    items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_PRICE === 1);
                    });

                    if (items && items.length > 0) {
                        $scope.subIemsColumnsVisibility.hidePRICE = false;
                    }
                }
            };
        }]);