﻿prmApp
    .controller('approvalQCSListCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService",
    "PRMPRServices", "reportingService","workflowService",
    function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService,
        PRMPRServices, reportingService, workflowService) {

        $scope.userID = userService.getUserId();
        $scope.sessionID = userService.getUserToken();

        $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
        $scope.desigID = userService.getSelectedUserDesigID();

        $scope.qcsApprovalList = [];
        $scope.qcsApprovalList1 = [];
        $scope.qcsApprovalListTemporary = [];


        /*PAGINATION CODE*/
        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 8;

        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.pageChanged = function () {
            ////console.log('Page changed to: ' + $scope.currentPage);
        };
            /*PAGINATION CODE*/


        $scope.getQCSApprovalList = function () {

            var params = {
                "userid": $scope.userID,
                "deptid": $scope.deptID,
                "desigid": $scope.desigID,
                "type": "QCS"
            };
            workflowService.GetApprovalList(params)
                .then(function (response) {
                    $scope.qcsApprovalList = response;
                    $scope.qcsApprovalList1 = response;
                    $scope.qcsApprovalListTemporary = response;
                    
                    $scope.qcsApprovalList.forEach(function (qcs, qcsIndex) {
                        qcs.APPROVAL_DATE = userService.toLocalDate(qcs.APPROVAL_DATE);
                    });

                    $scope.totalItems = $scope.qcsApprovalList.length;

                });
        };


        $scope.getQCSApprovalList();



        $scope.setFilters = function (search) {

            if (search) {
                $scope.qcsApprovalList = $scope.qcsApprovalList1.filter(function (qcs) {
                    return (String(qcs.MODULE_ID).indexOf(search) >= 0 || qcs.MODULE_TYPE.indexOf(search) >= 0 || String(qcs.REFER_MODULE_LINK_ID).indexOf(search) >= 0 || qcs.REFER_MODULE_LINK_NAME.indexOf(search) >= 0);
                });
                $scope.totalItems = $scope.qcsApprovalList.length;
            } else {
                $scope.qcsApprovalList = [];
                $scope.qcsApprovalList = $scope.qcsApprovalListTemporary;
                $scope.totalItems = $scope.qcsApprovalListTemporary.length;
            }


        };

}]);