﻿prmApp
.controller('costComparisionsQcsCtrl', ["$scope", "$state", "$log", "$stateParams", "userService", "auctionsService", "$window",
    "$timeout", "reportingService", "growlService", "workflowService",
    function ($scope, $state, $log, $stateParams, userService, auctionsService, $window,
        $timeout, reportingService, growlService, workflowService) {

        $scope.reqId = $stateParams.reqID;
        $scope.qcsID = +$stateParams.qcsID;
        $scope.requirementDetails = {};
        $scope.qcsRequirementDetails;
        $scope.isUOMDifferent = false;
        $scope.isTechSpecExport = false;
        $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;
       // $scope.isSuperUser = userService.getUserObj().isSuperUser;
        if (!$scope.isCustomer) {
            $state.go('home');
        }
        //$scope.includeGstInCal = true;
        $scope.userId = userService.getUserId();
        $scope.sessionID = userService.getUserToken();
        $scope.qcsVendors = [];
        $scope.qcsItems = [];
        $scope.editForm = false;
        $scope.deptIDs = [];
        $scope.desigIDs = [];
        $scope.vendorWidth = 12;


        $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
        if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
            $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                $scope.deptIDs.push(item.deptID);
                item.listDesignation.forEach(function (item1, index1) {
                    if (item1.isAssignedToUser && item1.isValid) {
                        $scope.desigIDs.push(item1.desigID);
                    }
                });
            });
        }

        $scope.listRequirementTaxes = [];
        $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;
        $scope.uomDetails = [];

        /*region start WORKFLOW*/
        $scope.workflowList = [];
        $scope.itemWorkflow = [];
        $scope.obj = {
            auctionVendors : []
        };
        $scope.objNew = {
            auctionVendors: []
        };
        $scope.workflowObj = {};
        $scope.workflowObj.workflowID = 0;
        $scope.currentStep = 0;
        $scope.orderInfo = 0;
        $scope.assignToShow = '';
        $scope.isWorkflowCompleted = false;
        $scope.WorkflowModule = 'QCS';
        $scope.disableWFSelection = false;
            /*region end WORKFLOW*/

        $scope.vendorPrices = [];
        

        $scope.QCSDetails = {
            QCS_ID: 0,
            REQ_ID: $scope.reqId,
            U_ID: userService.getUserId(),
            QCS_CODE: '',
            PO_CODE: '',
            RECOMMENDATIONS: '',
            UNIT_CODE: ''
        };

        $scope.doPrint = false;
        $scope.printReport = function () {
            $scope.doPrint = true;
            $timeout(function () {
                $window.print();
                $scope.doPrint = false;
            }, 1000);
        };

        $scope.htmlToCanvasSaveLoading = false;
        $scope.htmlToCanvasSave = function (format) {
            $scope.htmlToCanvasSaveLoading = true;
            setTimeout(function () {
                try {
                    var name = "Comparisions-ReqID-" + $scope.reqId + "." + format;
                    var canvas = document.createElement("canvas");
                    if (format == 'pdf') {
                        document.getElementById("widget").style["display"] = "";
                    }
                    else {
                        document.getElementById("widget").style["display"] = "inline-block";
                    }

                    html2canvas($("#widget"), {
                        onrendered: function (canvas) {
                            theCanvas = canvas;

                            const a = document.createElement("a");
                            a.style = "display: none";
                            a.href = canvas.toDataURL();

                            // Add Image to HTML
                            //document.body.appendChild(canvas);

                            /* Save As PDF */
                            if (format == 'pdf') {
                                var imgData = canvas.toDataURL();
                                var pdf = new jsPDF();
                                pdf.addImage(imgData, 'JPEG', 0, 0);
                                pdf.save(name);
                            }
                            else {
                                a.download = name;
                                a.click();
                            }

                            // Clean up 
                            //document.body.removeChild(canvas);
                            document.getElementById("widget").style["display"] = "";
                            $scope.htmlToCanvasSaveLoading = false;
                        }
                    });
                }
                catch (err) {
                    document.getElementById("widget").style["display"] = "";
                    $scope.htmlToCanvasSaveLoading = false;
                }

            }, 500);

        };

        $scope.getTotalTax = function (quotation) {
            if (quotation) {
                if (!quotation.cGst || quotation.cGst == undefined) {
                    quotation.cGst = 0;
                }

                if (!quotation.sGst || quotation.sGst == undefined) {
                    quotation.sGst = 0;
                }

                if (!quotation.iGst || quotation.iGst == undefined) {
                    quotation.iGst = 0;
                }

                return quotation.cGst + quotation.sGst + quotation.iGst;

            } else {
                return 0;
            }
        };

        $scope.SaveQCSDetails = function (val) {
            $scope.QCSDetails.QCS_TYPE = 'DOMESTIC';
            if (!$scope.QCSDetails.QCS_CODE) {
                $scope.QCSDetails.QCS_CODE = new Date().getUTCMilliseconds();
            }

            $scope.QCSDetails.REQ_JSON = JSON.stringify($scope.requirementDetails);
            if ($scope.QCSDetails.CREATED_USER == null) {
                $scope.QCSDetails.CREATED_USER = $scope.userId;
            }
            if ($scope.QCSDetails.U_ID == 0 || $scope.QCSDetails.U_ID == null) {
                $scope.QCSDetails.U_ID = $scope.userId;
            }
            var params = {
                "qcsdetails": $scope.QCSDetails,
                "sessionid": userService.getUserToken()
            };

            if ($scope.workflowObj.workflowID) {
                params.qcsdetails.WF_ID = $scope.workflowObj.workflowID;
            }

            //if (!params.qcsdetails.WF_ID > 0) {
            //    growlService.growl('Please select Workflow', "inverse");
            //    return;
            //}
            reportingService.SaveQCSDetails(params)
                .then(function (response) {
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        
                        if (val == 1) {
                            $scope.goToQCSList($scope.reqId);
                            growlService.growl("Details saved successfully.", "success");
                        } else {
                            var url = $state.href("cost-comparisions-qcs", { "reqID": $scope.reqId, "qcsID": response.objectID });
                            window.open(url, '_self');
                        }
                    }
                });
        };

        $scope.isQCSFormdisabled = true;

        $scope.checkIsFormDisable = function () {
            $scope.isQCSFormdisabled = false;
            if ($scope.itemWorkflow.length == 0) {
                $scope.isQCSFormdisabled = true; 
            } else {
                if (($scope.QCSDetails.CREATED_BY == +userService.getUserId() || $scope.QCSDetails.MODIFIED_BY == +userService.getUserId()) && $scope.itemWorkflow[0].WorkflowTracks.length > 0 &&
                    $scope.itemWorkflow[0].WorkflowTracks[0].status !== "APPROVED" && $scope.itemWorkflow[0].WorkflowTracks[0].order == 1 && $scope.itemWorkflow[0].workflowID > 0) {
                    $scope.isQCSFormdisabled = true;
                }
            }
            
        };
        
        $scope.GetQCSDetails = function () {
            var params = {
                "qcsid": $scope.qcsID,
                "sessionid": userService.getUserToken()
            };
            reportingService.GetQCSDetails(params)
                .then(function (response) {
                    $scope.QCSDetails = response;

                    if ($scope.QCSDetails.WF_ID > 0) {
                        $scope.workflowObj.workflowID = $scope.QCSDetails.WF_ID;
                    }

                    if ($scope.QCSDetails.REQ_JSON) {
                        $scope.qcsRequirementDetails = JSON.parse($scope.QCSDetails.REQ_JSON);
                    }

                    $scope.getRequirementData();
                    $scope.getItemWorkflow();
                });
        };

        if ($scope.qcsID > 0) {
            $scope.GetQCSDetails();
            //$scope.checkIsFormDisable();
        }

        $scope.goToQCSList = function (reqID) {
            var url = $state.href("list-qcs", { "reqID": $scope.reqId });
            window.open(url, '_self');
        };
        
        /*region start WORKFLOW*/

        $scope.getWorkflows = function () {
            workflowService.getWorkflowList()
                .then(function (response) {
                    $scope.workflowList = [];
                    $scope.workflowListTemp = response;
                    $scope.workflowListTemp.forEach(function (item, index) {
                        if (item.WorkflowModule == $scope.WorkflowModule) {
                            $scope.workflowList.push(item);
                        }
                    });

                    if (userService.getUserObj().isSuperUser) {
                        $scope.workflowList = $scope.workflowList;
                    }
                    else {
                        $scope.workflowList = $scope.workflowList.filter(function (item) {
                            return item.deptID == userService.getSelectedUserDepartmentDesignation().deptID;

                        });
                    }
                });
        };

        $scope.getWorkflows();

        $scope.getItemWorkflow = function () {
            workflowService.getItemWorkflow(0, $scope.qcsID, $scope.WorkflowModule)
                .then(function (response) {
                    $scope.itemWorkflow = response;
                    $scope.checkIsFormDisable();
                    if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                        $scope.currentStep = 0;

                        var count = 0;

                        $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {

                            if (track.status == 'APPROVED' || track.status == 'HOLD') {
                                $scope.isFormdisabled = true;
                            }

                            if (track.status == 'APPROVED') {
                                $scope.isWorkflowCompleted = true;
                                $scope.orderInfo = track.order;
                                $scope.assignToShow = track.status;

                            }
                            else {
                                $scope.isWorkflowCompleted = false;
                            }



                            if (track.status == 'REJECTED' && count == 0) {
                                count = count + 1;
                            }

                            if ((track.status == 'PENDING' || track.status == 'HOLD') && count == 0) {
                                count = count + 1;
                                $scope.IsUserApproverForStage(track.approverID);
                                $scope.currentAccess = track.order;
                            }

                            if ((track.status == 'PENDING' || track.status == 'HOLD' || track.status == 'REJECTED') && $scope.currentStep == 0) {
                                $scope.currentStep = track.order;
                                return false;
                            }
                        });
                    }
                });
        };

        $scope.updateTrack = function (step, status) {
            $scope.disableAssignPR = true;
            $scope.commentsError = '';

            var tempArray = $scope.itemWorkflow[0].WorkflowTracks[$scope.itemWorkflow[0].WorkflowTracks.length - 1];
            if (step.order == tempArray.order && status == 'APPROVED') {
                $scope.disableAssignPR = false;
            } else {
                $scope.disableAssignPR = true;
            }

            if ($scope.isReject) {
                $scope.commentsError = 'Please Save Rejected Items/Qty';
                return false;
            }

            if (status == 'REJECTED' && (step.comments == null || step.comments == "")) {
                $scope.commentsError = 'Please enter comments';
                return false;
            }

            step.status = status;
            step.sessionID = $scope.sessionID;
            step.modifiedBy = userService.getUserId();

            step.moduleName = $scope.WorkflowModule;

            workflowService.SaveWorkflowTrack(step)
                .then(function (response) {
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        $scope.getItemWorkflow();
                        //location.reload();
                      //  $state.go('list-pr');
                    }
                })
        };

        $scope.assignWorkflow = function (moduleID) {
            workflowService.assignWorkflow(({ wID: $scope.workflowObj.workflowID, moduleID: moduleID, user: userService.getUserId(), sessionID: $scope.sessionID }))
                .then(function (response) {
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                        $scope.isSaveDisable = false;
                    }
                    else {
                        //  $state.go('list-pr');
                    }
                });
        };

        $scope.IsUserApprover = false;

        $scope.functionResponse = false;

        $scope.IsUserApproverForStage = function (approverID) {
            workflowService.IsUserApproverForStage(approverID, userService.getUserId())
                .then(function (response) {
                    $scope.IsUserApprover = response;
                });
        };

        $scope.isApproverDisable = function (index) {

            var disable = true;

            var previousStep = {};

            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {

                if (index == stepIndex) {
                    if (stepIndex == 0) {
                        if ($scope.isUserBelongsToDeptandDesig(step.department.deptID,step.approver.desigID) &&
                            (step.status == 'PENDING' || step.status == 'HOLD')) {
                            disable = false;
                        }
                        else {
                            disable = true;
                        }
                    }
                    else if (stepIndex > 0) {
                        if (previousStep.status == 'PENDING' || previousStep.status == 'HOLD' || previousStep.status == 'REJECTED') {
                            disable = true;
                        }
                        else if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                            (step.status == 'PENDING' || step.status == 'HOLD')) {
                            disable = false;
                        }
                        else {
                            disable = true;
                        }
                    }
                }
                previousStep = step;
            })

            return disable;
        };

    /*region end WORKFLOW*/


        $scope.editFormPage = function () {
            $scope.editForm = !$scope.editForm;
            $scope.requirementDetails.auctionVendors.forEach(function (vendor) {
                $scope.fieldValidation(vendor);
            });
        };

        $scope.fieldValidation = function (vendor) {
            if ($scope.editForm && vendor.INCO_TERMS) {
                auctionsService.getIncotermProductConfig(vendor.INCO_TERMS, userService.getUserToken())
                    .then(function (response) {
                        vendor.INCO_TERMS_CONFIG = response;
                       
                    });
            } else {
                vendor.isEdit = false;
            }
        };

        $scope.fieldValidationList = function (vendor) {
            if (vendor.INCO_TERMS) {
                auctionsService.getIncotermProductConfig(vendor.INCO_TERMS, userService.getUserToken())
                    .then(function (response) {
                        vendor.INCO_TERMS_CONFIG = response;
                    });
            }
        };

        $scope.unitPriceCalculation = function (item) {
            //if (parseInt(item.revUnitPrice) > parseInt(item.unitPrice)) {
                item.revUnitPrice = item.unitPrice;
            //}
        };

        $scope.getRequirementData = function () {
            auctionsService.getrequirementdata({ "reqid": $scope.reqId, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                .then(function (response) {
                    $scope.qcsItems = [];
                    if (response.CB_TIME_LEFT > 0) {
                        response.status = "STARTED";
                    }

                    response.listRequirementItems.forEach(function (reqItem, index) {
                        reqItem.isVisible = true;
                        var qcsItem = {
                            itemID: reqItem.itemID,
                            itemName: reqItem.productIDorName,
                            isSelected: true
                        };

                        $scope.qcsItems.push(qcsItem);
                    });

                    if (response) {
                        $scope.qcsVendors = [];
                        $scope.requirementDetails = response;
                        $scope.vendorWidth = Math.floor(12 / ($scope.requirementDetails.auctionVendors.length + 1));
                        $scope.requirementDetails.auctionVendors = _.filter($scope.requirementDetails.auctionVendors, function (vendor) { return vendor.companyName != 'PRICE_CAP'; });
                        if ($scope.qcsID > 0 && $scope.qcsRequirementDetails) {
                            $scope.requirementDetails.customerComment = $scope.qcsRequirementDetails.customerComment;
                            $scope.requirementDetails.includeGstInCal = $scope.qcsRequirementDetails.includeGstInCal;
                            $scope.requirementDetails.listRequirementItems.forEach(function (item, index) {
                                console.log(item.productQuotationTemplateJson);
                                if (item.productQuotationTemplateJson) {
                                    item.productQuotationTemplateJsonObj = JSON.parse(item.productQuotationTemplateJson);
                                }

                                var tempItem = _.filter($scope.qcsRequirementDetails.listRequirementItems, function (qcsReqItem) { return qcsReqItem.itemID === item.itemID; });
                                if (tempItem && tempItem.length > 0) {
                                    item.qtyDistributed = tempItem[0].qtyDistributed;
                                }
                            });

                            $scope.requirementDetails.auctionVendors.forEach(function (vendor, index) {
                                    vendor.isVisible = true;
                                    var qcsVendor = {
                                        vendorID: vendor.vendorID,
                                        vendorCompany: vendor.companyName,
                                        isSelected: true
                                    };

                                    $scope.qcsVendors.push(qcsVendor);
                                    var tempQCSVendor = _.filter($scope.qcsRequirementDetails.auctionVendors, function (qcsVendor) { return qcsVendor.vendorID === vendor.vendorID; });
                                    if (tempQCSVendor && tempQCSVendor.length > 0) {
                                        vendor.payLoadFactor = tempQCSVendor[0].payLoadFactor;
                                        vendor.revPayLoadFactor = tempQCSVendor[0].revPayLoadFactor;
                                        vendor.revChargeAny = tempQCSVendor[0].revChargeAny;
                                        vendor.customerComment = tempQCSVendor[0].customerComment;

                                        auctionsService.getIncotermProductConfig(vendor.INCO_TERMS, userService.getUserToken())
                                            .then(function (response) {

                                                vendor.listRequirementItems.forEach(function (vendorItem, index) {
                                                
                                                    if (vendorItem.productQuotationTemplateJson) {
                                                        vendorItem.productQuotationTemplateJson = JSON.parse(vendorItem.productQuotationTemplateJson);

                                                    }

                                                    var newArray = response.filter(function (res) { return res.ProductId == vendorItem.catalogueItemID; });
                                                    var tempQCSVendorItem = _.filter(tempQCSVendor[0].listRequirementItems, function (qcsVendorItem) { return qcsVendorItem.itemID === vendorItem.itemID; });
                                                    if (newArray && newArray.length > 0) {
                                                        if (newArray[0].IS_CUSTOMER_EDIT == 1) {
                                                            if (tempQCSVendorItem && tempQCSVendorItem.length > 0) {
                                                                vendorItem.unitPrice = tempQCSVendorItem[0].unitPrice;
                                                                vendorItem.revUnitPrice = tempQCSVendorItem[0].revUnitPrice;
                                                            }
                                                        }
                                                    }

                                                    vendorItem.qtyDistributed = tempQCSVendorItem[0].qtyDistributed;

                                                });
                                            });
                                    }
                            });
                        } else {
                            $scope.requirementDetails.includeGstInCal = true;
                            $scope.requirementDetails.listRequirementItems.forEach(function (item, index) {
                                item.maxHeight = '';
                                if (item.productQuotationTemplateJson) {
                                    item.productQuotationTemplateJsonObj = JSON.parse(item.productQuotationTemplateJson);
                                }
                            });

                            $scope.requirementDetails.auctionVendors.forEach(function (vendor, index) {
                                vendor.isVisible = true;
                                var qcsVendor = {
                                    vendorID: vendor.vendorID,
                                    vendorCompany: vendor.companyName,
                                    isSelected: true
                                };

                                $scope.qcsVendors.push(qcsVendor);
                                vendor.listRequirementItems.forEach(function (vendorItem, index) {
                                    //$scope.displayLeastItemPriceColor();
                                    if (vendorItem.productQuotationTemplateJson) {
                                        vendorItem.productQuotationTemplateJson = JSON.parse(vendorItem.productQuotationTemplateJson);
                                    }
                                });
                            });
                        }
                    }
                });
        };

        $scope.getItemRank = function (item, vendor) {
            var vendorItemPrices = _.filter(vendor.listRequirementItems, function (vendorItem) { return vendorItem.itemID === item.itemID; });
            if (vendorItemPrices && vendorItemPrices.length > 0) {
                return vendorItemPrices[0].itemRank;
            }
        };

        if (!$scope.qcsID) {
            $scope.getRequirementData();
        }

        $scope.getVendorItemPrices = function (item, vendor) {
            
            var emptyObj = {
                unitPrice: 0,
                revUnitPrice: 0
            };
            if (vendor) {
                var vendorItemPrices = _.filter(vendor.listRequirementItems, function (vendorItem) { return vendorItem.itemID === item.itemID; });
                if (vendorItemPrices && vendorItemPrices.length > 0) {
                    if (vendorItemPrices[0].revUnitPrice > vendorItemPrices[0].unitPrice) {
                        vendorItemPrices[0].revUnitPrice = 0;
                        growlService.growl("Revised Price cannot be greater than initial Price", "inverse");
                    }

                    if (vendorItemPrices[0] && vendorItemPrices[0].productQuotationTemplateJson.length > 0) {
                        item.maxHeight = '182px';
                    }

                    return vendorItemPrices[0];
                } else {
                    return emptyObj;
                }
            }
            else {
                return emptyObj;
            }
        };

        $scope.getVendorTotalPriceWithoutTax = function (vendor) {
            var price = 0;
            vendor.listRequirementItems.forEach(function (item, index) {
                price += item.revUnitPrice * item.productQuantity;
            });

            return price;
        };

        $scope.getVendorTotalInitPriceWithoutTax = function (vendor) {
            var price = 0;
            vendor.listRequirementItems.forEach(function (item, index) {
                price += item.unitPrice * item.productQuantity;
            });

            return price;
        };

        $scope.changePayload = function (vendor) {
            vendor.revPayLoadFactor = vendor.payLoadFactor;
        };

        $scope.getVendorTotalLandingPrice = function (vendor, includeGstInCal) {
            var price = 0, cGSTax = 0, sGSTax = 0, iGSTax = 0;

            price += $scope.getVendorTotalPriceWithoutTax(vendor);

            if (includeGstInCal) {
                cGSTax = $scope.getVendorTotalTax(vendor, 'CGST');
                iGSTax = $scope.getVendorTotalTax(vendor, 'IGST');
                sGSTax = $scope.getVendorTotalTax(vendor, 'SGST');

                price += cGSTax + iGSTax + sGSTax;
            }

            if (vendor.revPayLoadFactor) {
                price += (+vendor.revPayLoadFactor);
            }
            if (vendor.revChargeAny) {
                price -= (+vendor.revChargeAny);
            }
            vendor.landingPrice = price;
            return price;
        };

        $scope.getVendorTotalInitLandingPrice = function (vendor, includeGstInCal) {
            var price = 0, cGSTax = 0, sGSTax = 0, iGSTax = 0;

            price += $scope.getVendorTotalInitPriceWithoutTax(vendor);

            if (includeGstInCal) {
                cGSTax = $scope.getVendorInitTotalTax(vendor, 'CGST');
                iGSTax = $scope.getVendorInitTotalTax(vendor, 'IGST');
                sGSTax = $scope.getVendorInitTotalTax(vendor, 'SGST');

                price += cGSTax + iGSTax + sGSTax;
            }

            if (vendor.payLoadFactor) {
                price += (+vendor.payLoadFactor);
            }
            if (vendor.revChargeAny) {
                price -= (+vendor.revChargeAny);
            }
            vendor.initLandingPrice = price;
            return price;
        };

        $scope.getVendorTotalUnitItemPrices = function (vendor) {
            var price = 0;
            vendor.listRequirementItems.forEach(function (item, index) {
                price += (+item.unitPrice);
            });

            if (vendor.payLoadFactor) {
                price += (+vendor.payLoadFactor);
            }

            return price;
        };

        $scope.getVendorTotalRevUnitItemPrices = function (vendor) {
            var price = 0;
            vendor.listRequirementItems.forEach(function (item, index) {
                price += (+item.revUnitPrice);
            });

            if (vendor.revPayLoadFactor) {
                price += (+vendor.revPayLoadFactor);
            }

            return price;
        };

        $scope.getVendorTotalTax = function (vendor, taxType) {
            var totalTax = 0;
            vendor.listRequirementItems.forEach(function (item, index) {
                if (taxType === 'CGST') {
                    totalTax += (item.revUnitPrice * item.productQuantity) * (item.cGst/100);
                }

                if (taxType === 'IGST') {
                    totalTax += (item.revUnitPrice * item.productQuantity) * (item.iGst / 100);
                }

                if (taxType === 'SGST') {
                    totalTax += (item.revUnitPrice * item.productQuantity) * (item.sGst / 100);
                }
            });

            return totalTax;
        };

        $scope.getVendorInitTotalTax = function (vendor, taxType) {
            var totalTax = 0;
            vendor.listRequirementItems.forEach(function (item, index) {
                if (taxType === 'CGST') {
                    totalTax += (item.unitPrice * item.productQuantity) * (item.cGst / 100);
                }

                if (taxType === 'IGST') {
                    totalTax += (item.unitPrice * item.productQuantity) * (item.iGst / 100);
                }

                if (taxType === 'SGST') {
                    totalTax += (item.unitPrice * item.productQuantity) * (item.sGst / 100);
                }
            });

            return totalTax;
        };

        $scope.isNonCoreItemEditable = function (item, vendor) {
            var isEditable = false;

            if (item && !item.isCoreProductCategory && vendor && vendor.INCO_TERMS && vendor.INCO_TERMS_CONFIG) {
                vendor.INCO_TERMS_CONFIG.forEach(function (incoItem) {
                    vendor.listRequirementItems.forEach(function (vendorItem, itemIndex) {
                        if (item.catalogueItemID === incoItem.ProductId && item.catalogueItemID === vendorItem.catalogueItemID) {
                            if (incoItem.IS_CUSTOMER_EDIT) {
                                isEditable = true;
                            }
                        }
                    });
                });
            }

            return isEditable;
        };

        $scope.exportTechSpec = function () {
            setTimeout(function () {
                tableToExcel('testTable', 'Comparitives');
                setTimeout(function () {
                    location.reload();
                }, 1000);
            }, 3000);

            
        };

        $scope.isVendorVisible = function (qcsVendor) {
            qcsVendor.isSelected = !qcsVendor.isSelected;
            var vendorTemp = _.filter($scope.requirementDetails.auctionVendors, function (auctionVendor) { return auctionVendor.vendorID === qcsVendor.vendorID; });
            if (vendorTemp && vendorTemp.length > 0) {
                vendorTemp[0].isVisible = qcsVendor.isSelected;
            }
        };

        $scope.isReqItemVisible = function (qcsItem) {
            qcsItem.isSelected = !qcsItem.isSelected;
            var itemTemp = _.filter($scope.requirementDetails.listRequirementItems, function (reqItem) { return reqItem.itemID === qcsItem.itemID; });
            if (itemTemp && itemTemp.length > 0) {
                itemTemp[0].isVisible = qcsItem.isSelected;
            }
        };

        $scope.CalculateRankBasedOnLandingPrice = function (vendor) {
            vendor.landingPriceRank = 'NA';
            var validVendorsForRanking = _.filter($scope.requirementDetails.auctionVendors, function (auctionVendor) { return auctionVendor.landingPrice > 0 && auctionVendor.isQuotationRejected === 0; });

            if (validVendorsForRanking && validVendorsForRanking.length > 0) {
                var sortedVendors = _.orderBy(validVendorsForRanking, ['landingPrice'], ['asc']);
                var rank = _.findIndex(sortedVendors, function (vendor1) { return vendor1.vendorID === vendor.vendorID; });
                if (rank >= 0) {
                    vendor.landingPriceRank = rank + 1;
                }                
            }
            
            return vendor.landingPriceRank;
        };

        $scope.CalculateSavingsBasedOnLandingPrice = function (vendor) {
            vendor.savings = 0;
            var validVendorsForSavings = _.filter($scope.requirementDetails.auctionVendors, function (auctionVendor) { return auctionVendor.initLandingPrice > 0; });
            if (validVendorsForSavings && validVendorsForSavings.length > 0) {
                var sortedVendors = _.orderBy(validVendorsForSavings, ['initLandingPrice'], ['asc']);
                var rank = _.findIndex(sortedVendors, function (vendor1) { return vendor1.vendorID === vendor.vendorID; });
                if (rank >= 0) {
                    vendor.savings = sortedVendors[0].initLandingPrice - vendor.landingPrice;
                }
            }

            return vendor.savings;
        };



        $scope.UpdateOtherCharges = function () {
            $scope.SaveQCSDetails(0);
            $scope.getCalculatedOtherCharges();

        };

        $scope.saveVendorOtherChargesObject = {
            vendorID: 0,
            DIFFERENTIAL_FACTOR: 0,
            requirementID: 0
        };

        $scope.getCalculatedOtherCharges = function () {

            $scope.saveVendorOtherCharges = [];

            $scope.requirementDetails.auctionVendors.forEach(function (item, index) {
                    item.DIFFERENTIAL_FACTOR = 0;
                    item.DIFFERENTIAL_FACTOR = $scope.getVendorTotalLandingPrice(item, $scope.requirementDetails.includeGstInCal) - item.revVendorTotalPrice;

                    $scope.saveVendorOtherChargesObject = {
                        vendorID: 0,
                        DIFFERENTIAL_FACTOR: 0,
                        requirementID: 0
                    };

                    $scope.saveVendorOtherChargesObject.vendorID = item.vendorID;
                    $scope.saveVendorOtherChargesObject.DIFFERENTIAL_FACTOR = item.DIFFERENTIAL_FACTOR;
                    $scope.saveVendorOtherChargesObject.requirementID = $scope.requirementDetails.requirementID;
                    $scope.saveVendorOtherCharges.push($scope.saveVendorOtherChargesObject);
            });

            var params =
            {
                userID: userService.getUserId(),
                vendorOtherChargesArr: $scope.saveVendorOtherCharges,
                sessionID: userService.getUserToken()
            }


            auctionsService.saveVendorOtherCharges(params)
                .then(function (response) {

                    if (response.errorMessage === "") {
                        growlService.growl("Other Charges Updated Successfully", "success");
                    } else {
                        growlService.growl("Other Charges Updation Failed", "inverse");
                    }

                });



        };



        $scope.isUserBelongsToDeptandDesig = function (deptID, desigID) {
            var isEligible = true;

            if ($scope.deptIDs.indexOf(deptID) != -1 && $scope.desigIDs.indexOf(desigID) != -1) {
                isEligible = true;
            } else {
                isEligible = false;
            }

            return isEligible;
        };

}]);