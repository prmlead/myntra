﻿
prmApp
    .controller('consalidatedReportCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "userService", "auctionsService", "fwdauctionsService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log", "reportingService", "$stateParams",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService, fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService, $stateParams) {
            $scope.formRequest = {};
            $scope.formRequest.isForwardBidding = false;
            $scope.consalidatedReport = [];
            
            /*pagination code*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 8;
            $scope.revisedUserL1 = [];
            $scope.reqStatus = 'ALL';


            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
            };

            /* CLIENT STATUS MAPPING TO PRM STATUS */
            $scope.isCustomer = userService.getUserType();
            $scope.prmStatus = function (type, status) {
                return userService.NegotiationStatus(type, status);
            };
            /* CLIENT STATUS MAPPING TO PRM STATUS */

            //$scope.reportFromDate = '';
            //$scope.reportToDate = '';
            console.log($stateParams)
            if (!_.isEmpty($stateParams)) {
                $scope.reportFromDate = $stateParams.fromDate;
                $scope.reportToDate = $stateParams.toDate;
            } else{
                $scope.reportToDate = moment().format('YYYY-MM-DD');
                $scope.reportFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");
            }
            $scope.consalidatedReport1 = [];

            $scope.getConsalidatedReport = function () {
                $scope.errMessage = '';

                //var ts = userService.toUTCTicks($scope.reportFromDate);
                //var m = moment(ts);
                //var quotationDate = new Date(m);
                //var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                //var reportFromDate = "/Date(" + milliseconds + "000+0530)/";

                //var ts = userService.toUTCTicks($scope.reportToDate);
                //var m = moment(ts);
                //var quotationDate = new Date(m);
                //var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                //var reportToDate = "/Date(" + milliseconds + "000+0530)/";

                reportingService.getConsolidatedReport($scope.reportFromDate, $scope.reportToDate)
                    .then(function (response) {
                        $scope.consalidatedReport = response;
                        $scope.consalidatedReport1 = response;
                        $scope.totalItems = $scope.consalidatedReport.length;
                        $scope.consalidatedReport.forEach(function (item, index) {
                            item.closed = $scope.prmStatus($scope.isCustomer, item.closed);
                            item.quotationFreezTime = $scope.GetDateconverted(item.quotationFreezTime);
                            item.expectedStartTime = $scope.GetDateconverted(item.expectedStartTime);
                            item.reqPostedOn = $scope.GetDateconverted(item.reqPostedOn);
                            item.startTime = $scope.GetDateconverted(item.startTime);

                            if (String(item.startTime).includes('9999')) {
                                item.startTime = '';
                            }

                            if (item.status == 'UNCONFIRMED') {
                                item.statusColor = 'text-warning';
                            }
                            else if (item.status == 'NOT STARTED') {
                                item.statusColor = 'text-warning';
                            }
                            else if (item.status == 'STARTED') {
                                item.statusColor = 'text-danger';
                            }
                            else if (item.status == 'Negotiation Ended') {
                                item.statusColor = 'text-success';
                            }
                            else if (item.status == 'Negotiation Ended') {
                                item.statusColor = 'text-success';
                            }
                            else {
                                item.statusColor = '';
                            }

                            if (item.noOfVendorsInvited > 0 && item.noOfVendorsParticipated > 0 && item.noOfVendorsInvited == item.noOfVendorsParticipated && item.status != 'Negotiation Ended' && item.status != 'VENDOR_FINALISED' &&
                                item.status != 'STARTED' && item.status != 'NOTSTARTED') {
                                item.status = 'IN_REVIEW_WITH_MYNTRA';

                            }

                            if (item.status == "UNCONFIRMED" && (item.status != "NOTSTARTED" || item.status != "STARTED" || item.status != "Negotiation Ended")) {
                                if (item.noOfVendorsInvited === 0) {
                                    item.status = "Saved_As_Draft";
                                }
                            }
                            item.status = $scope.prmStatus($scope.isCustomer, item.status);
                            item.ScheduledTime = "";
                            if (item.status.toUpperCase() == "SAVED_AS_DRAFT") {
                                item.ScheduledTime = '-';
                            } else if (item.status.toUpperCase() == "PENDING_VENDOR_QUOTATION" || item.status.toUpperCase() == "NEGOTIATION_CANCELLED") {
                                item.ScheduledTime = item.expectedStartTime;
                            } else {
                                item.ScheduledTime = item.startTime;
                            }

                        });

                        //if ($scope.consalidatedReport && $scope.consalidatedReport.length > 0) {
                        //    $scope.getStatusFilter($scope.reqStatus);
                        //}

                    });
            };
            $scope.filterArray = [];

            $scope.getStatusFilter = function (filterVal) {
                $scope.filterArray = [];
                if (filterVal == 'ALL') {
                    $scope.consalidatedReport = $scope.consalidatedReport1;

                } else {
                    $scope.filterArray = $scope.consalidatedReport1.filter(function (item) {
                        return $scope.prmStatus($scope.isCustomer, item.status) === $scope.prmStatus($scope.isCustomer, filterVal);
                    });
                    $scope.consalidatedReport = $scope.filterArray;
                }

                $scope.totalItems = $scope.consalidatedReport.length;

            };

            $scope.getConsalidatedReport();

            $scope.GetReport = function () {
                
                

                alasql('SELECT requirementID as [Requirement ID],title as [Requirement Title], ' +
                    'status as Status, ' +
                    'reqPostedOn as [Posted On],POSTED_BY_USER as [Posted By], quotationFreezTime as [Freez Time], expectedStartTime as [Exp.Negotiation Start Time], ScheduledTime as [Negotiation scheduled time],IL1_vendTotalPrice as [Initial Least Price], ' +
                    'RL1_companyName as [L1 Company Name],  RL1_revVendTotalPrice as [L1 Rev Price], ' + 
                    'RL2_companyName as [L2 Company Name],  RL2_revVendTotalPrice as [L2 Rev Price],  ' +
                    'basePriceSavings as [Savings],savingsPercentage as [Savings %],QUANTITY as [QTY],ARTICLE_TYPE as [Article Type], BRAND as [Brand Name], SEASON as [Season] '+
                    'INTO XLSX(?, { headers: true, sheetid: "ConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ',
                    ["ConsolidatedReport.xlsx", $scope.consalidatedReport]);


            }
            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return userService.toLocalDate(dateBefore);
                }
            };


        }]);