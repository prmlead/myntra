﻿
prmApp
    .controller('consalidatedReportCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "userService", "auctionsService", "fwdauctionsService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log", "reportingService",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService, fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService) {
            $scope.formRequest = {};
            $scope.formRequest.isForwardBidding = false;
            $scope.consalidatedReport = [];
            /*pagination code*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 8;

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
            };
           
           
            $scope.getConsalidatedReport = function () {
                $scope.errMessage = '';
               
                
                fromDate = $scope.reportFromDate;
                toDate = $scope.reportToDate;
                if (fromDate > toDate) {
                    $scope.errMessage = 'To Date should be greater than From Date';
                    return false;
                }
                reportingService.getConsolidatedReport({ "from": fromDate, "to": toDate, "userid": userService.getUserId(),"sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.consalidatedReport = response;
                       
                        $scope.totalItems = $scope.consalidatedReport.length;
                        $scope.consalidatedReport.forEach(function (item, index) {
                            item.quotation_FREEZ_TIME = $scope.GetDateconverted(item.quotation_FREEZ_TIME);
                            item.req_POSTED_ON = $scope.GetDateconverted(item.req_POSTED_ON);
                        })

                    })
            }

            $scope.GetReport = function () {
                //alasql.fn.handleDate = function (date) {
                //    return new moment(date).format("MM/DD/YYYY");
                //}
                
                alasql('SELECT requirementID as RequirementID, REQ_TITLE as RequirementTitle,noOfvendorsParticipated as [vendorsParticipated],noOfVendorsInvited as [VendorsInvited], req_CATEGORY as [Category], status as Status,quotation_FREEZ_TIME  as [Freez Time],req_POSTED_ON as [Posted On],L1Name as [L1Name],l1RevPrice as [l1RevPrice],L2Name as [L2Name],l2RevPrice as [l2RevPrice] INTO XLSX(?,{headers:true,sheetid: "ConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["ConsolidatedReport.xlsx", $scope.consalidatedReport]);

            }
            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return new moment(dateBefore).format("DD-MM-YYYY HH:mm");
                }
            };


        }]);