﻿prmApp
    .controller('listprCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMPRServices",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPRServices) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
            $scope.sessionID = userService.getUserToken();
            $scope.myAuctions1 = [];
            $scope.selectedRFP;
            $scope.selectedPR;
            $scope.filters = {
                status: '',
                plant: '',
                material: '',
                docType: '',
                purchaseGroup: '',
                searchKeyword: ''
            };

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;

            $scope.PlantsList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];
            $scope.materialGroupList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];
            $scope.purchaseGroupList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];
            $scope.docTypeList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];
            $scope.prStatusList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };
            /*PAGINATION CODE*/


            $scope.PRList = [];
            $scope.filteredPRList = [];
            $scope.PRItems = [];

            $scope.setFilters = function () {
                $scope.filteredPRList = $scope.PRList;
                if ($scope.filters.status) {
                    $scope.filteredPRList = $scope.PRList.filter(function (pr) {
                        return pr.PR_STATUS === $scope.filters.status;
                    });
                }

                if ($scope.filters.plant) {
                    $scope.filteredPRList = $scope.filteredPRList.filter(function (pr) {
                        return pr.PLANT === $scope.filters.plant;
                    });
                }

                //if ($scope.filters.material) {
                //    $scope.filteredPRList = $scope.filteredPRList.filter(function (pr) {
                //        return pr.MATERIAL_GROUP_LIST.indexOf($scope.filters.material) >= 0;
                //    });
                //}

                if ($scope.filters.docType) {
                    $scope.filteredPRList = $scope.filteredPRList.filter(function (pr) {
                        return pr.DOC_TYPE === $scope.filters.docType;
                    });
                }

                //if ($scope.filters.purchaseGroup) {
                //    $scope.filteredPRList = $scope.filteredPRList.filter(function (pr) {
                //        return pr.DOC_TYPE === $scope.filters.purchaseGroup;
                //    });
                //}

                if ($scope.filters.searchKeyword) {
                    $scope.filteredPRList = $scope.filteredPRList.filter(function (pr) {
                        return (pr.PR_NUMBER.indexOf($scope.filters.searchKeyword) >= 0 ||
                            (pr.CREATED_BY + '').indexOf($scope.filters.searchKeyword) >= 0 ||
                            pr.CREATED_DATE.indexOf($scope.filters.searchKeyword) >= 0) ||
                            pr.MATERIAL_GROUP_LIST.indexOf($scope.filters.searchKeyword.toUpperCase()) >= 0;
                    });
                    $scope.totalItems = $scope.filteredPRList.count;
                } else {
                    $scope.totalItems = $scope.PRList.length;
                }
            };

            $scope.getprlist = function () {
                //$scope.PRDetails = response;
                var params = {
                    "userid": userService.getUserId(),
                    "sessionid": userService.getUserToken(),
                    "deptid": userService.getSelectedUserDepartmentDesignation().deptID,
                    "desigid": userService.getSelectedUserDesigID(),
                    "depttypeid": userService.getSelectedUserDepartmentDesignation().deptTypeID
                };

                PRMPRServices.getprlist(params)
                    .then(function (response) {
                        $scope.PRList = response;
                        //userService.toLocalDate(
                        $scope.PRList.forEach(function (item, index) {
                            item.CREATED_DATE = userService.toLocalDate(item.CREATED_DATE).split(' ')[0];
                            item.DELIVERY_DATE = userService.toLocalDate(item.DELIVERY_DATE).split(' ')[0];
                            item.RELEASE_DATE = userService.toLocalDate(item.RELEASE_DATE).split(' ')[0];
                            //if (item.COMPLETED_ITEMS <= 0) {
                            //    item.PR_STATUS = 'NEW';
                            //}
                            if (item.REQ_IDS.contains("0,")) {
                                item.REQ_IDS = item.REQ_IDS.replace("0,", "");
                            } else if (item.REQ_IDS.contains("0")) {
                                item.REQ_IDS = item.REQ_IDS.replace("0", "");
                            }
                            item.PR_REQ_IDS = [];
                            if (item.REQ_IDS && item.REQ_IDS != null) {
                                if (item.REQ_IDS.contains(",")) {
                                    item.PR_REQ_IDS = item.REQ_IDS.split(",").map(function (el) { return +el; });

                                }
                            }
                        });

                        $scope.filteredPRList = $scope.PRList;

                        /*PAGINATION CODE START*/
                        $scope.totalItems = $scope.PRList.length;
                        /*PAGINATION CODE END*/
                        document.body.scrollTop = 0; // For Chrome, Safari and Opera 
                        document.documentElement.scrollTop = 0; // For IE and Firefox
                    });

            };

            $scope.getprlist();

            $scope.getprfieldmapping = function (type) {
                var params = {
                    //"type": "'" + type + "'"
                    "type": type
                };

                PRMPRServices.getprfieldmapping(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.PlantsList = $scope.PlantsList.concat(response.filter(function (item) { return item.FIELD_TYPE === 'PLANTS'; }));
                            $scope.materialGroupList = $scope.materialGroupList.concat(response.filter(function (item) { return item.FIELD_TYPE === 'MATERIAL_GROUP'; }));
                            $scope.purchaseGroupList = $scope.purchaseGroupList.concat(response.filter(function (item) { return item.FIELD_TYPE === 'PURCHASE_GROUP'; }));
                            $scope.docTypeList = $scope.docTypeList.concat(response.filter(function (item) { return item.FIELD_TYPE === 'DOC_TYPE'; }));
                            $scope.prStatusList = $scope.prStatusList.concat(response.filter(function (item) { return item.FIELD_TYPE === 'STATUS'; }));
                        }
                    });

            };

            $scope.getprfieldmapping("'PLANTS', 'STATUS', 'DOC_TYPE', 'MATERIAL_GROUP', 'PURCHASE_GROUP'");

            $scope.getPrItems = function (prDetails) {
                if (prDetails) {
                    var params = {
                        "prid": prDetails.PR_ID
                    };
                    PRMPRServices.getpritemslist(params)
                        .then(function (response) {
                            console.log(response);
                            $scope.PRItems = response;
                            $scope.PRItems.forEach(function (item, index) {
                                item.CREATED_DATE = userService.toLocalDate(item.CREATED_DATE).split(' ')[0];
                                item.DELIVERY_DATE = userService.toLocalDate(item.DELIVERY_DATE).split(' ')[0];
                            });

                            prDetails.PRItems = $scope.PRItems;
                        });
                }
            };


            $scope.goToPrEdit = function (id) {
                var url = $state.href("save-pr-details", { "Id": id });
                window.open(url, '_self');
            };

            $scope.goToPrAction = function (id) {
                var url = $state.href("pr-actions", { "Id": id });
                window.open(url, '_blank');
            };

            $scope.crateRequirement = function (prDetails) {
                if (prDetails) {
                    $state.go('save-requirementAdv', { 'prDetail': prDetails });
                    //$window.open(url, '_blank');
                }
            };

            $scope.showLinkToRFP = function (prDetails) {
                $scope.selectedPR = prDetails;
                $scope.getAuctions();
            };

            $scope.getAuctions = function () {
                auctionsService.getmyAuctions({ "userid": userService.getUserId(), "reqstatus": $scope.reqStatus, "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.myAuctions1 = [];
                        $scope.myAuctions1 = response;
                        $scope.myAuctions1 = $scope.myAuctions1.filter(function (item) {
                            return item.isRFP === true;
                        });

                        console.log($scope.myAuctions1);
                    });

            };
            $scope.rfqID = 0;

            $scope.LinkToRFP = function () {
                console.log($scope.selectedPR);
                console.log($scope.selectedRFP);
                $scope.rfqID = 0;
                if ($scope.selectedPR && $scope.selectedRFP) {
                    var params = {
                        "userid": userService.getUserId(),
                        "sessionid": userService.getUserToken(),
                        "reqid": $scope.selectedRFP.requirementID,
                        "prid": $scope.selectedPR.PR_ID
                    };
                    PRMPRServices.linkRFPToPR(params)
                        .then(function (response) {
                            if (response && response.errorMessage === '') {
                                $scope.rfqID = response.objectID;
                                growlService.growl("Successfully link to RFP", "success");
                                angular.element('#linkRFP').modal('hide');
                                console.log(response);
                            } else {
                                growlService.growl("Error linking PR to RFP, please contact support team.", "inverse");
                            }
                        });
                }
            };

            $scope.reset = function () {
                $scope.rfqID = 0;
            }


            $scope.isLast = function (last) {
                var a = '';
                if (last) {
                    a = '';
                } else {
                    a = ',';
                }
                return a;
            };

        }]);