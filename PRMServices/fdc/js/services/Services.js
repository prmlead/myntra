prmApp.constant('PRMFDCServiceDomain', 'forward/svc/PRMFDCService.svc/REST/');
prmApp.service('PRMFDCServiceService', ["PRMFDCServiceDomain", "userService", "httpServices",
    function (PRMFwdReqServiceDomain, userService, httpServices) {
        var PRMFDCServiceService = this;

        PRMFDCServiceService.getrequirementdata = function (params) {
            let url = PRMFDCServiceDomain + 'getrequirementdata?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid;
            return httpServices.get(url);
        };


        return PRMFDCServiceService;

}]);