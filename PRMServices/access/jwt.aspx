﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="jwt.aspx.cs" Inherits="PRMServices.access.jwt" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.6.4.min.js"></script>
    <script>
        function getUrlVars() {
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }

        $(document).ready(function () {
            var jwtToken = getUrlVars()['jwt'];
            if (!jwtToken) {
                alert('No TOken found');
                $('#errorInfo').text('Invalid Token Passed. Please contact PRM360 support.');
            } else {
                localStorage.setItem('ssojwttoken', jwtToken);
                localStorage.setItem('authentication', jwtToken);
                setTimeout(function () {
                    //location.href = "https://myntra.prm360.com/wireframe/prm360.html#/login";
                    location.href = "https://myntra.prm360.com/myntrasso/prm360.html#/login";
                    //window.location = ("http://localhost:9401/prm360.html#/login");
                }, 100)
                
            }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
                Mynthra SSO page. Validating user... <span id="errorInfo" style="color:red"></span>
        </div>
    </form>

    
</body>
</html>
