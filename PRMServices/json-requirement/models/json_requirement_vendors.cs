﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class json_requirement_vendors : json_entity
    {

        [DataMember] [DataNames("RV_ID")] public int RV_ID { get; set; }
        [DataMember] [DataNames("REQ_ID")] public int REQ_ID { get; set; }
        [DataMember] [DataNames("VENDOR_NAME")] public string VENDOR_NAME { get; set; }        
        [DataMember] [DataNames("VENDOR_COMPANY_NAME")] public string VENDOR_COMPANY_NAME { get; set; }

    }
}