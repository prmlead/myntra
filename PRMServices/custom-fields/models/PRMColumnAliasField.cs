﻿using PRM.Core.Common;
using PRM.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PRMServices.models
{
    [DataContract]
    public class PRMFieldMappingDetails : Entity
    {
        [DataMember]
        [DataNames("COLUMN_ALIAS_ID")]
        public int COLUMN_ALIAS_ID { get; set; }

        [DataMember]
        [DataNames("COMP_ID")]
        public int COMP_ID { get; set; }

        [DataMember]
        [DataNames("FIELD_NAME")]
        public string FIELD_NAME { get; set; }

        [DataMember]
        [DataNames("FIELD_ALIAS_NAME")]
        public string FIELD_ALIAS_NAME { get; set; }

        [DataMember]
        [DataNames("TABLE_NAME")]
        public string TABLE_NAME { get; set; }

        [DataMember]
        [DataNames("COLUMN_NAME")]
        public string COLUMN_NAME { get; set; }

        [DataMember]
        [DataNames("TEMPLATE_NAME")]
        public string TEMPLATE_NAME { get; set; }
    }
}