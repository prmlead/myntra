﻿using PRM.Core.Common;
using PRM.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PRMServices.models
{
    [DataContract]
    public class PRMCustomField : Entity
    {
        [DataMember]
        [DataNames("CUST_FIELD_ID")]
        public int CUST_FIELD_ID { get; set; }

        [DataMember]
        [DataNames("COMP_ID")]
        public int COMP_ID { get; set; }

        [DataMember]
        [DataNames("FIELD_NAME")]
        public string FIELD_NAME { get; set; }

        [DataMember]
        [DataNames("FIELD_HELP_TEXT")]
        public string FIELD_HELP_TEXT { get; set; }

        [DataMember]
        [DataNames("FIELD_TYPE")]
        public string FIELD_TYPE { get; set; }

        [DataMember]
        [DataNames("FIELD_MODULE")]
        public string FIELD_MODULE { get; set; }

        [DataMember]
        [DataNames("IS_REQUIRED")]
        public int IS_REQUIRED { get; set; }

        [DataMember]
        [DataNames("FIELD_VALUE")]
        public string FIELD_VALUE { get; set; }

        [DataMember]
        [DataNames("FIELD_DEFAULT_VALUE")]
        public string FIELD_DEFAULT_VALUE { get; set; }

        [DataMember]
        [DataNames("IS_READ_ONLY")]
        public int IS_READ_ONLY { get; set; }

        [DataMember]
        [DataNames("VISIBLE_TO_VENDOR")]
        public int VISIBLE_TO_VENDOR { get; set; }

        [DataMember]
        [DataNames("IS_VALID")]
        public int IS_VALID { get; set; }

        [DataMember]
        [DataNames("MODULE_ID")]
        public int MODULE_ID { get; set; }

        [DataMember]
        [DataNames("MODULE_ID")]
        public int IS_SELECTED { get; set; }        
    }
}