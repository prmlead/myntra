﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using PRMServices.Models;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMReportService
    {
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getlivebiddingreport?reqid={reqID}&count={count}&sessionid={sessionID}")]
        LiveBidding[] GetLiveBiddingReport(int reqID, int count, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getitemwisereport?reqid={reqID}&sessionid={sessionID}")]
        ItemWiseReport[] GetItemWiseReport(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "deliverytimelinereport?reqid={reqID}&sessionid={sessionID}")]
        DeliveryTimeLine[] GetDeliveryTimeLineReport(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "paymenttermsreport?reqid={reqID}&sessionid={sessionID}")]
        DeliveryTimeLine[] GetPaymentTermsReport(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getreqdetails?reqid={reqID}&sessionid={sessionID}")]
        ReportsRequirement GetReqDetails(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "gettemplates?template={template}&compid={compID}&userid={userID}&reqid={reqID}&from={from}&to={to}&sessionid={sessionID}&vendorIds={vendorIds}&sendEmail={sendEmail}")]
        string GetTemplates(string template, int compID, int userID, int reqID, string from, string to, string sessionID, string vendorIds = null, bool sendEmail = false);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getreqreportforexcel?reqid={reqID}&sessionid={sessionID}")]
        ExcelRequirement GetReqReportForExcel(int reqID, string sessionID);

        //[WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getReqItemWiseVendors?reqid={reqID}&sessionid={sessionID}")]
        //MACRequirement GetReqItemWiseVendors(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getconsolidatedreports?from={from}&to={to}&userid={userID}&sessionid={sessionID}")]
        List<ConsolidatedReport> GetConsolidatedReports(string sessionID, string from, string to, int userID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getLogisticConsolidatedreports?from={from}&to={to}&userid={userID}&sessionid={sessionID}")]
        List<LogisticConsolidatedReport> GetLogisticConsolidatedReports(string sessionID, string from, string to, int userID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getAccountingConsolidatedreports?from={from}&to={to}&userid={userID}&sessionid={sessionID}")]
        List<AccountingConsolidatedReport> GetAccountingConsolidatedReports(string sessionID, string from, string to, int userID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getconsolidatedtemplates?template={template}&from={from}&to={to}&userid={userID}&sessionid={sessionID}")]
        string GetConsolidatedTemplates(string template, string from, string to, int userID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getconsolidatedbasepricereports?from={from}&to={to}&userid={userID}&sessionid={sessionID}")]
        List<ConsolidatedBasePriceReports> GetConsolidatedBasePriceReports(string sessionID, string from, string to, int userID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetUserLoginTemplates?template={template}&from={from}&to={to}&userid={userID}&sessionid={sessionID}")]
        string GetUserLoginTemplates(string template, string from, string to, int userID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetLogsTemplates?template={template}&from={from}&to={to}&companyID={companyID}&sessionid={sessionID}")]
        string GetLogsTemplates(string template, string from, string to, int companyID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetQCSTemplate?reqId={reqId}&userId={userId}&qcsType={qcsType}&sessionId={sessionId}")]
        string GetQCSTemplate(int reqId, int userId, string qcsType, string sessionId);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetImportQCSTemplate?reqId={reqId}&qcsType={qcsType}&sessionId={sessionId}")]
        string GetImportQCSTemplate(int reqId, string qcsType, string sessionId);

        #region QCS

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getqcslist?uid={uid}&reqid={reqid}&sessionid={sessionid}")]
        List<QCSDetails> GetQCSList(int uid, int reqid, string sessionid);


        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "qcsdetails?qcsid={qcsid}&sessionid={sessionid}")]
        QCSDetails GetQCSDetails(int qcsid, string sessionid);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "saveqcsdetails")]
        Response SaveQCSDetails(QCSDetails qcsdetails, string sessionid);

        #endregion QCS


    }
}
