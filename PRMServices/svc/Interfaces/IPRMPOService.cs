﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using PRMServices.Models;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMPOService
    {
        //[WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getpoinformation?reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        //List<POInformation> GetPOInformation(int reqID, int userID, string sessionID);

        //[OperationContract]
        //[WebInvoke(Method = "POST",
        //RequestFormat = WebMessageFormat.Json,
        //ResponseFormat = WebMessageFormat.Json,
        //BodyStyle = WebMessageBodyStyle.WrappedRequest,
        //UriTemplate = "savepoinfo")]
        //Response SavePOInfo(POInformation[] poList, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getdespoinfo?reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        POVendor GetDesPoInfo(int reqID, int userID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getvendors?reqid={reqID}&sessionid={sessionID}")]
        List<UserDetails> GetVendors(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getdescdispatch?poid={poID}&dtid={dtID}&sessionid={sessionID}")]
        DispatchTrack GetDescDispatch(int poID, int dtID, string sessionID);
        
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getvendorpolist?reqid={reqID}&userid={userID}&poid={poID}&sessionid={sessionID}")]
        VendorPO GetVendorPoList(int reqID, int userID, int poID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getvendorpoinfo?reqid={reqID}&userid={userID}&poid={poID}&sessionid={sessionID}")]
        VendorPO GetVendorPoInfo(int reqID, int userID, int poID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getdispatchtracklist?poid={poID}&sessionid={sessionID}")]
        List<DispatchTrack> GetDispatchTrackList(int poID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getdispatchtrack?poid={poID}&dtid={dispatchTrackID}&sessionid={sessionID}")]
        List<DispatchTrack> GetDispatchTrack(int poID, int dispatchTrackID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getpaymenttrack?vendorid={vendorID}&poid={poID}&sessionid={sessionID}")]
        PaymentTrack GetPaymentTrack(int vendorID, int poID, string sessionID);


        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "checkuniqueifexists")]
        bool CheckUniqueIfExists(string param, string idtype, string sessionID);




        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savedescpoinfo")]
        Response SaveDescPoInfo(POVendor povendor);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updatepostatus")]
        Response UpdatePOStatus(int poID, string status, string sessionID);
        
        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savedesdispatchtrack")]
        Response SaveDesDispatchTrack(DispatchTrack dispatchtrack, POVendor povendor);        

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savedispatchtrack")]
        Response SaveDispatchTrack(DispatchTrack dispatchtrack, string requestType);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savevendorpoinfo")]
        Response SaveVendorPOInfo(VendorPO vendorpo);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savepaymentinfo")]
        Response SavePaymentInfo(PaymentInfo paymentinfo);
       
    }
}
