﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using PRMServices.Models;
using PRMServices.Models.Vendor;
using CATALOG = PRMServices.Models.Catalog;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMServices
    {
        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getrunningauctions?userid={userID}&sessionid={sessionID}&section={section}&limit={limit}")]
        List<RunningAuction> GetAuctions(int userID, string sessionID, string section, int limit);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcurrentauctions?userid={userID}&sessionid={sessionID}&section={section}&limit={limit}")]
        List<Requirement> GetRunningAuctions(int userID, string sessionID, string section, int limit);

        //#CB-0-2018-12-05
        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getrequirementdata?reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        Requirement GetRequirementData(int reqID, int userID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getpodetails?reqid={reqID}&userid={userID}&vendorid={vendorID}&sessionid={sessionID}")]
        RequirementPO GetPODetails(int reqID, int userID, int vendorID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuserinfo?userid={userID}&sessionid={sessionID}")]
        UserInfo GetUser(int userID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getsuperuserdetails?userid={userID}&sessionid={sessionID}")]
        UserInfo GetSuperUser(int userID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getrequirementsreport?userid={userID}&sessionid={sessionID}")]
        List<ReportData> GetRequirementsReport(int userID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getpricecomparison?reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        PriceComparison GetPriceComparison(int reqID, int userID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getpricecomparisonprenegotiation?reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        PriceComparison GetPriceComparisonPreNegotiation(int reqID, int userID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getCatalogCategories?uid={uid}")]
        List<CATALOG.Category> GetCatalogCategories(int uid);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "generatepoforuser")]
        Response UpdatePOData(RequirementPO reqPO);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "selectvendor")]
        Response SelectVendor(int userID, int vendorID, int reqID, string reason, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "stopbids")]
        Response StopBids(int reqID, int userID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "endnegotiation")]
        Response EndNegotiation(int reqID, int userID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getattachment?attachid={attachmentID}&sessionid={sessionID}")]
        Response GetAttachment(string attachmentID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getattachmentbase64?attachid={attachmentID}&sessionid={sessionID}")]
        FileData GetAttachmentBase64(string attachmentID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "materialdispatch")]
        Response MaterialDispatch(ReqMaterialDetails reqMat);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getmyauctions?userid={userID}&sessionid={sessionID}")]
        List<Requirement> GetMyAuctions(int userID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getsubuserdata?userid={userID}&sessionid={sessionID}&IS_TEAM_MEMBER={IS_TEAM_MEMBER}")]
        List<UserInfo> GetSubUserData(int userID, string sessionID, int IS_TEAM_MEMBER = 0);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getactiveleads?userid={userID}&sessionid={sessionID}")]
        List<Requirement> GetActiveLeads(int userID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "paymentdetails")]
        Response PaymentDetails(PaymentDetails paymentDets);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcomments?reqid={reqID}&userID={userID}&sessionid={sessionID}")]
        List<Comment> GetComments(int reqID, int userID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuserdetails?userid={userID}&sessionid={sessionID}")]
        UserDetails GetUserDetails(int userID, string sessionID);

        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "register")]
        Response Register(Register register, List<User> altUsers = null);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcategories?userid={userID}")]
        List<CategoryObj> GetCategories(int userID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getusersubcategories?userid={userID}&sessionid={sessionID}")]
        List<CategoryObj> GetSubCategories(int userID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getusercategories?userid={userID}&sessionid={sessionID}")]
        List<CategoryObj> GetUserCategories(int userID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuserssubcategories?userid={userID}&sessionid={sessionID}")]
        List<CategoryObj> GetUserSubCategories(int userID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanytypes")]
        List<string> GetCompanyTypes();

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getusercredentials?userid={userID}&sessionid={sessionID}")]
        List<Credentials> GetUserCredentials(int userID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getdashboardstats?userid={userID}&sessionid={sessionID}&fromdate={fromDate}&todate={toDate}&depts={depts}")]
        DashboardStats GetDashboardStats(int userID, string sessionID, DateTime fromDate, DateTime toDate, int depts);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getdate")]
        DateTime GetDate();

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getdatelogistics")]
        DateTime GetDateLogistics();

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getserverdatetime")]
        ServerDateTime GetServerDateTime();

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "issessionvalid?sessionid={sessionID}")]
        Response IsSessionValid(string sessionid);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getvendorstatistics?userid={userid}&sessionid={sessionID}")]
        VendorStatistics GetVendorStatistics(string sessionID, int userid);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updatestatus")]
        Response UpdateStatus(int reqid, int userid, string status, string type, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "getrevisedquotations")]
        Response GetRevisedQuotations(int reqID, int userID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "generatepo")]
        Response GeneratePO(int reqid, int userid, byte[] POfile, string POfileName, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Wrapped,
        UriTemplate = "updateauctionstart")]
        Response UpdateAuctionStart(int reqID, int userID, DateTime date, string sessionID, string negotiationDuretion, NegotiationSettings NegotiationSettings);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Wrapped,
        UriTemplate = "checkuserifexists")]
        bool CheckUserIfExists(string phone, string idtype);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Wrapped,
        UriTemplate = "isnegotiationrunning")]
        bool IsNegotationRunning(int userID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "loginsso")]
        Response LoginSSO(string ssojwttoken, string source);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "loginuser")]
        Response LoginUser(string username, string password, string source);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "loginuserbyhash")]
        Response LoginUserByHash(string userhashtoken, string source);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "logoutuser")]
        Response LogoutUser(int userID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updatebidtime")]
        Response UpdateBidTime(int reqID, int userID, long newTicks, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "requirementsave")]
        Response RequirementSave(Requirement requirement, byte[] attachment);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "registeruser")]
        Response RegisterUser(UserInfo userInfo);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "addvendortoauction")]
        Response AddVendorToAuction(VendorDetails vendor, int reqID, string sessionID, int userID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "makeabid")]
        Response MakeABid(int reqID, int userID, double price, byte[] quotation, string quotationName, string sessionID,
            double tax, double freightcharges, string warranty, string payment, string duration, string validity,
            List<RequirementItems> quotationObject, int revised, double priceWithoutTax, string type, double discountAmount,
            List<RequirementTaxes> listRequirementTaxes, string otherProperties);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "verifyotp")]
        Response VerifyOTP(int OTP, int userID, string phone, int isMobile);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "verifyemailotp")]
        Response VerifyEmailOTP(int OTP, int userID, string email);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "addnewvendor")]
        UserInfo AddNewVendor(VendorInfo vendorInfo);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "addnewcustomer")]
        Response AddNewCustomer(VendorInfo customerInfo);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "getvendors")]
        List<VendorDetails> GetVendors(string Categories, string sessionID, int uID, int evalID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getSharedLinkVendors?userid={userid}&vendorids={vendorids}&sessionid={sessionid}")]
        List<VendorDetails> GetSharedLinkVendors(int userid, string vendorids, string sessionid);


        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getUnBlockedVendors?userid={userid}&vendorids={vendorids}&sessionid={sessionid}")]
        List<VendorDetails> GetUnBlockedVendors(int userid, string vendorids, string sessionid);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "getvendorsbycatnsubcat")]
        List<VendorDetails> GetVendorsByCatNSubCat(int[] Categories, string sessionID, int count, int uID, int evalID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "getvendorswithoutcategories")]
        List<VendorDetails> GetVendorsWithOutCategories(int uID, int evalID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savecomment")]
        Response SaveComment(Comment com);
        // TODO: Add your service operations here

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "forgotpassword")]
        Response ForgotPassword(string email, string sessionid);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "changepassword")]
        Response ChangePassword(int userID, string username, string oldPass, string newPass);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "resetpassword")]
        Response resetpassword(string email, string sessionid, string NewPass, string ConfNewPass);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updateuserinfo")]
        Response UpdateUserInfo(UserDetails user);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updateuserbasicinfo")]
        Response UpdateUserBasicInfo(UserDetails user);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updateuserprofileinfo")]
        Response UpdateUserProfileInfo(UserDetails user);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "sendotpforemail")]
        Response SendOTPForEmail(int userID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "sendcontactemail")]
        Response SendContactEmail(string Name, string Email, string Mobile, string Comment, string Industry);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "sendotp")]
        Response SendOTP(int userID, string phone = null);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "sendotpformobile")]
        Response SendOTPforMobile(string phone);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updatecredentials")]
        Response UpdateCredentials(int userID, List<CredentialUpload> files, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "deleterequirement")]
        Response DeleteRequirement(int reqID, int userID, string sessionID, string reason);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "deleteuser")]
        Response DeleteUser(int userID, int referringUserID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "activateuser")]
        Response ActivateUser(int userID, int referringUserID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "userratings")]
        Response UserRatings(int uID, int userID, float rating, string sessionID);

        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "verifydocuments")]
        Response VerifyDocuments(string phone, List<CredentialUpload> files, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DocumentVerification?phone={phone}")]
        Response DocumentVerification(string phone);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DeleteLogin?phone={phone}")]
        Response DeleteLogin(string phone);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "TestGetOtp?phone={phone}")]
        Response TestGetOtp(string phone);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "deleteattachment")]
        Response DeleteAttachment(int userID, int reqID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updateexpdelandpaydate")]
        Response UpadteExpDelAndPayDate(int reqid, DateTime date, string type, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "makeabidformobile")]
        Response MakeABidForMobile(int reqID, int userID, double price, string strQuotation, string quotationName, string sessionID, double tax);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "startNegotiation")]
        Response StartNegotiation(int reqID, int userID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "restartnegotiation")]
        Response RestartNegotiation(int reqID, int userID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "deletevendorfromauction")]
        Response DeleteVendorFromAuction(int userID, int reqID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "testverifyuser?phone={phone}")]
        Response testVerifyUser(string phone);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getkeyvaluepairs?parameter={parameter}")]
        KeyValuePair[] GetKeyValuePairs(string parameter);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getbidhistory?reqid={reqID}&userID={userID}&sessionid={sessionID}")]
        List<Comment> GetBidHistory(int reqID, int userID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "appversion?uid={uID}&appversioncode={appVersionCode}")]
        AppInfo AppVersion(int uID, double appVersionCode);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getIncotermProductConfig?incoTerm={incoTerm}&sessionid={sessionid}")]
        List<ProductConfig> GetIncotermProductConfig(string incoTerm, string sessionid);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "isnegotiationended?reqid={reqID}&sessionid={sessionID}")]
        Response IsNegotiationEnded(int reqID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getRFQAssignUsers?reqid={reqid}&sessionid={sessionid}&assignedby={assignedby}")]
        Response GetRFQAssignUsers(int reqid, string sessionid, int assignedby);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, UriTemplate = "saveuserphone")]
        Response SaveUserPhone(int uid, string model, string os, string token, string sessionid);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, UriTemplate = "uploadquotation")]
        Response UploadQuotation(List<RequirementItems> quotationObject, int userID, int reqID, string sessionID, double price,
            double tax, double freightcharges, double vendorBidPrice, string warranty, string payment, string duration,
            string validity, int revised, string DesfileName, double discountAmount, List<RequirementTaxes> listRequirementTaxes,
            string otherProperties, string gstNumber, double packingCharges,double packingChargesTaxPercentage, double packingChargesWithTax,
            double installationCharges, double installationChargesTaxPercentage, double installationChargesWithTax,double revpackingCharges,double revinstallationCharges, 
            double revpackingChargesWithTax, double revinstallationChargesWithTax, string selectedVendorCurrency, 
            string uploadType, List<FileUpload> multipleAttachments,

            double freightCharges,
            double freightChargesTaxPercentage, 
            double freightChargesWithTax,
            double revfreightCharges,
            double revfreightChargesWithTax,
            string INCO_TERMS, int TaxFiledValidation, string isVendAckChecked, string vendorOrigin);
         
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, UriTemplate = "uploadquotationsfromexcel")]
        Response UploadQuotationsFromExcel(int reqID, int userID, string sessionID, byte[] quotationAttachment);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "sendtelegrammsg")]
        TelegramMsg SendTelegramMsg(TelegramMsg telegrammsg);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "requestSupport")]
        Response RequestSupport(TelegramMsg telegrammsg);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "sendpushnotification")]
        Response SendPushNotification(PushNotifications push);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "revquotationupload")]
        Response RevQuotationUpload(int reqID, int userID, double price, byte[] quotation, string quotationName, string sessionID, double tax);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "revquotationuploadformobile")]
        Response RevQuotationUploadForMobile(int reqID, int userID, double price, string strQuotation, string quotationName, string sessionID, double tax);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getrequirementreminders")]
        List<RequirementReminders> GetRequirementReminders();

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getquotationreminders")]
        List<RequirementReminders> GetQuotationReminders();

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getnegotiationstartalerts")]
        List<RequirementReminders> GetNegotiationStartAlerts();


        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "quatationaprovel")]
        Response QuatationAprovel(int reqID, int customerID, int vendorID, bool value, string reason, string sessionID, 
            string action, decimal technicalScore);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savedifferentialfactor")]
        Response SaveDifferentialFactor(int reqID, int customerID, int vendorID, decimal value, string reason, string sessionID);

        //[OperationContract]
        //[WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "assignvendorstocustomer?phone={phone}")]
        //Response AssignVendorsToCustomer(string phone);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanyvendors?userid={userID}&sessionid={sessionID}")]
        List<UserDetails> GetCompanyVendors(int userID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "itemwiseselectvendor")]
        Response ItemWiseSelectVendor(int userID, int vendorID, int reqID, int itemID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanyleads?userid={userID}&searchstring={searchString}&searchtype={searchType}&sessionid={sessionID}")]
        List<Requirement> GetCompanyLeads(int userID, string searchString, string searchType, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savenegotiationsettings")]
        Response SaveNegotiationSettings(NegotiationSettings NegotiationSettings);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "vendorreminders")]
        Response VendorReminders(int reqID, int userID, string message, int[] vendorIDs, string[] vendorCompanyNames, string sessionID, string requestType);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getvendorreminders?reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        List<Reminders> GetVendorReminders(int reqID, int userID, string sessionID);

        //[OperationContract]
        //[WebInvoke(Method = "POST",
        //BodyStyle = WebMessageBodyStyle.WrappedRequest,
        //RequestFormat = WebMessageFormat.Json,
        //ResponseFormat = WebMessageFormat.Json,
        //UriTemplate = "generatenewquotation")]
        //Response GenerateNewQuotation(int reqID, int customerID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "activatecompanyvendor")]
        Response ActivateCompanyVendor(int customerID, int vendorID, int isValid, string sessionID);


        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.Bare,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "VendorStatusUpdate")]
        Response UpdateCompanyVendorStatus(VendorStatusUpdateModel model);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "saveVendorOtherCharges")]
        Response SaveVendorOtherCharges(int userID, VendorDetails[] vendorOtherChargesArr, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.Bare,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "UpdateVendorScore")]
        Response UpdateVendorScore(VendorScoreUpdateModel model);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getreportsrequirement?reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        Requirement GetReportsRequirement(int reqID, int userID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "importentity")]
        Response ImportEntity(ImportEntity entity);

        //#CB-0-2018-12-05
        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savepricecap")]
        Response SavePriceCap(int reqID, string sessionID, int userID, string reqType, double priceCapValue, int isUnitPriceBidding, bool IS_CB_ENABLED, bool IS_CB_NO_REGRET);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuseraccess?userid={userID}&sessionid={sessionID}")]
        List<UserAccess> GetUserAccess(int userID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "saveuseraccess")]
        Response SaveUserAccess(List<UserAccess> listUserAccess, string sessionID);


        #region Department Designation START

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuserdepartmentdesignations?userid={userID}&sessionid={sessionID}")]
        List<Department> GetUserDepartmentDesignations(int userID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "saveuserdepartmentdesignation")]
        Response SaveUserDepartmentDesignation(List<Department> listUserDeptDesig, string sessionID);

        #endregion Department Designation END



        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanydepartments?userid={userID}&sessionid={sessionID}")]
        List<CompanyDepartments> GetCompanyDepartments(int userID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savecompanydepartment")]
        Response SaveCompanyDepartment(CompanyDepartments companyDepartment);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "deletedepartment")]
        Response DeleteDepartment(int deptID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "saveuserdepartments")]
        Response SaveUserDepartments(List<UserDepartments> listUserDepartments, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       UriTemplate = "saveworkcategory")]
        Response SaveWorkCategory(List<BudgetCodeEntity> listWorkCategories, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savebudgetcode")]
        Response SaveBudgetCode(List<BudgetCodeEntity> listBudgetCodes, string sessionID);


        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getworkcategories?userid={userID}&sessionid={sessionID}")]
        List<BudgetCodeEntity> GetWorkCategories(int userID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getbudgetcodes?userid={userID}&sessionid={sessionID}")]
        List<BudgetCodeEntity> GetBudgetCodes(int userID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuserdepartments?userid={userID}&sessionid={sessionID}")]
        List<UserDepartments> GetUserDepartments(int userID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuserdesignations?userid={userID}&sessionid={sessionID}")]
        List<UserDesignations> GetUserDesignations(int userID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuserdeptdesig?userid={userID}&sessionid={sessionID}")]
        List<UserDepartments> GetUserDeptDesig(int userID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompdeptdesig?compid={compID}&sessionid={sessionID}")]
        List<UserDepartments> GetCompanyDeptDesig(int compID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanydeptdesigtypes?userId={userid}&type={type}&sessionid={sessionID}")]
        List<CompanyDeptDesigTypes> GetCompanyDeptDesigTypes(int userId, string type, string sessionID);


        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "saveuserdeptdesig")]
        Response SaveUserDeptDesig(List<UserDepartments> listUserDeptDesig, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getdepartmentusers?deptid={deptID}&userid={userID}&sessionid={sessionID}")]
        List<UserDepartments> GetDepartmentUsers(int deptID, int userID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanydesignations?userid={userID}&sessionid={sessionID}")]
        List<CompanyDesignations> GetCompanyDesignations(int userID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savecompanydesignations")]
        Response SaveCompanyDesignations(CompanyDesignations companyDesignations);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "deletedesignation")]
        Response DeleteDesignation(int desigID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savereqdepartments")]
        Response SaveReqDepartments(List<ReqDepartments> listReqDepartments, string sessionID);


        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "shareemaillinkstoVendors")]
        Response ShareemaillinkstoVendors(int userID, List<VendorDetails> vendorIDs, string sessionID);


        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savereqdeptdesig")]
        Response SaveReqDeptDesig(List<ReqDepartments> listReqDepartments, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getreqdepartments?userid={userID}&reqid={reqID}&sessionid={sessionID}")]
        List<ReqDepartments> GetReqDepartments(int userID, int reqID, string sessionID);



        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getreqdeptdesig?userid={userID}&reqid={reqID}&sessionid={sessionID}")]
        List<ReqDepartments> GetReqDeptDesig(int userID, int reqID, string sessionID);


        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getisauthorized?userid={userID}&reqid={reqID}&sessionid={sessionID}")]
        Response GetIsAuthorized(int userID, int reqID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savecompanyconfiguration")]
        Response SaveCompanyConfiguration(List<CompanyConfiguration> listCompanyConfiguration, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanyconfiguration?compid={compID}&configkey={configKey}&sessionid={sessionID}")]
        List<CompanyConfiguration> GetCompanyConfiguration(int compID, string configKey, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanyconfigurationforlogin?compid={compID}&configkey={configKey}&sessionid={sessionID}")]
        List<CompanyConfiguration> GetCompanyConfigurationForLogin(int compID, string configKey, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "reqtechsupport")]
        Response ReqTechSupport(ReqShare reqShare);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "saverunningitemprice")]
        Response SaveRunningItemPrice(List<RequirementItems> itemsList, int userID, int reqID, double price, double vendorBidPrice, double freightcharges,
            double revfreightCharges, double revpackingCharges, double revinstallationCharges);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "assignvendortocompany")]
        User AssignVendorToCompany(int userID, string vendorPhone, string vendorEmail, string category, string[] subCategory, string sessionID, string altPhoneNum, string altEmail);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savemulticontacts")]
        Response SaveMultiContacts(int userID, string phone, string email, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getmaterialreceiveddata?reqid={reqID}&sessionid={sessionID}")]
        MaterialReceived GetMaterialReceivedData(int reqID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "materialreceived")]
        Response MaterialReceived(MaterialReceived materialreceived);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savecompanycategories")]
        Response SaveCompanyCategories(int compID, int id, string category, string subcategory, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "updatepricecap")]
        Response UpdatePriceCap(int uID, int reqID, double price, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanycategories?compid={compID}&sessionid={sessionID}")]
        List<CategoryObj> GetCompanyCategories(int compID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savealternatecontacts")]
        Response SaveAlternateContacts(UserDetails user);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getalternatecontacts?userid={userID}&compid={compID}&sessionid={sessionID}")]
        List<UserDetails> GetAlternateContacts(int userID, int compID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "saverequirementterms")]
        Response SaveRequirementTerms(List<RequirementTerms> listRequirementTerms, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getrequirementterms?userid={userID}&reqid={reqID}&sessionid={sessionID}")]
        List<RequirementTerms> GetRequirementTerms(int reqID, int userID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "deleterequirementterms")]
        Response DeleteRequirementTerms(List<int> listTerms, string sessionID);


        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "validatecurrencyrate?reqid={reqid}&sessionid={sessionid}")]
        Response ValidateCurrencyRate(int reqid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "refreshrequirementcurrency?reqid={reqid}&sessionid={sessionid}")]
        Response RefreshRequirementCurrency(int reqid, string sessionid);



        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "companycurrencies?compid={compid}&sessionid={sessionID}")]
        List<Currencies> GetCompanyCurrencies(int compid, string sessionID);



        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "resetpasswordbyotp?phone={phone}")]
        Response ResetPasswordByOTP(string phone);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "opsGetandResetPass?phone={phone}" +
            "&newpass={newpass}&vc={verificationcode}&vt={verificationtype}")]
        Response opsGetandResetPass(string phone, string newpass, string verificationcode, string verificationtype);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "enablemarginfields")]
        Response EnableMarginFields(int isEnabled, int reqID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getvendorrequirements?userid={userID}&negotiationStarted={negotiationStarted}&sessionid={sessionID}")]
        List<Requirement> GetVendorRequirements(int userID, int negotiationStarted, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "itempreviousprice?productname={productname}&productno={productno}&brand={brand}&companyid={companyid}&sessionid={sessionid}")]
        VendorDetails ItemPreviousPrice(string productname, string productno, string brand, int companyid, string sessionid);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getaltusers?clientid={clientID}&vendorid={vendorID}&sessionid={sessionID}")]
        List<User> GetAltUsers(int clientID, int vendorID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "uploadclientsidequotation")]
        List<RequirementItems> UploadClientSideQuotation(int reqID, int userID, string sessionID, byte[] quotationAttachment);

        [OperationContract]
        [WebInvoke(Method = "POST",
     BodyStyle = WebMessageBodyStyle.WrappedRequest,
     RequestFormat = WebMessageFormat.Json,
     ResponseFormat = WebMessageFormat.Json,
     UriTemplate = "uploadSubItemquotation")]
        Requirement UploadSubItemquotation(int reqID, int userID, string sessionID, byte[] quotationAttachment);

        //  let url = domain + 'itempreviousprice?productname=' + itemDetails.productIDorName.trim() + '&productno=' + itemDetails.productIDorName.trim() + '&brand=' + itemDetails.productBrand.trim() + '&sessionid=' + itemDetails.sessionID;

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getotp?phone={phone}")]
        Response GetOTP(string phone);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "updatepassword")]
        Response UpdatePassword(int otp, string passCode, string phone);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuserdetailsbydeptdesigtypeid?compid={compID}" +
            "&depttypeid={deptTypeID}&desigtypeid={desigTypeID}&sessionid={sessionID}")]
        UserDetails GetUserDetailsByDeptDesigTypeID(int compID, int deptTypeID, int desigTypeID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getonlinestatus?userid={userID}&sessionid={sessionID}")]
        Response GetOnlineStatus(string sessionID, int userID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getvendorproducts?userid={userID}&sessionid={sessionID}")]
        List<RequirementItems> GetVendorProducts(string sessionID, int userID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getactiveusers?compid={companyID}&sessionid={sessionID}")]
        List<ActiveUsers> GetActiveUsers(int companyID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getactivebuyers?userid={userID}&sessionid={sessionID}")]
        List<ActiveUsers> GetActiveBuyers(int userID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getusersloginstatus?userid={userID}&from={from}&to={to}&sessionid={sessionID}")]
        List<ActiveUsers> GetUsersLoginStatus(int userID, string from, string to, string sessionID);


        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getactiveuserstemplates?template={template}&userid={userID}&from={from}&to={to}&sessionid={sessionID}")]
        string GetActiveUsersTemplates(string template, int userID, string from, string to, string sessionID);

        #region COUNTER BID

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savecbprices")]
        Response SaveCBPrices(VendorDetails auctionVendor, int userID, int isCustomer, string sessionID, string bidComments, int vendorID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savecbpricescustomer")]
        Response SaveCBPricesCustomer(VendorDetails auctionVendor, int userID, int isCustomer, string sessionID, string bidComments, int vendorID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "freezecounterbid")]
        Response FreezeCounterBid(int reqID, int vendorID, string sessionID, string freezedBy, int freezeValue);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcbpricesaudit?reqid={reqID}&vendorid={vendorID}&sessionid={sessionID}")]
        List<CBPrices> GetCBPricesAudit(int reqID, int vendorID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "updatecbtime")]
        Response UpdateCBTime(DateTime cbEndTime, int userID, int reqID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "cbstopquotations")]
        Response CBStopQuotations(int stopCBQuotations, int userID, int reqID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "cbmarkascomplete")]
        Response CBMarkAsComplete(int isCBCompleted, int userID, int reqID, string sessionID);



        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "markAsCompleteREQ")]
        Response MarkAsCompleteREQ(int isREQCompleted, int userID, int reqID, string sessionID);

        #endregion COUNTER BID

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getemaillogs?from={from}&to={to}&compid={companyID}&sessionid={sessionid}")]
        List<EmailLogs> GetEmailLogs(string from, string to, int companyID, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getsmslogs?from={from}&to={to}&compid={companyID}&sessionid={sessionid}")]
        List<EmailLogs> GetSMSLogs(string from, string to, int companyID, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanyidbyvendorid?id={ID}")]
        EmailLogs GetCompanyidByVendorid(int ID);



        // # Catalogue Start Region

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "getvendorsbyproducts")]
        List<VendorDetails> GetVendorsByProducts(string[] products, string sessionID, int uID, int evalID);


        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "token")]
        string LoginUserToken(string username, string password);


        // # Catalogue End Region


        // Category Based Vendors // 
        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "getVendorsbyCategories")]
        List<VendorDetails> GetVendorsByCategories(string Categories, string sessionID, int uID, int evalID);
        // Category Based Vendors //

        //Approve Vendor Through Workflow//
        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "approveVendorThroughWorkflow")]
        Response ApproveVendorThroughWorkflow(int customerID, int vendorID, int isValid, string sessionID);
        //Approve Vendor Through Workflow//

        // Block Vendor //
        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "blockUnBlockVendor")]
        Response BlockUnBlockVendor(int customerID, int vendorID, int isValid, string type, int unblockWfID, string sessionID);
        // Block Vendor //

        //Vendor Accepted Registartion//
        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "acceptRegistration")]
        Response AcceptRegistration(int vendorID, int wfID, int compID, string sessionID);
        //Vendor Accepted Registartion//


        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getlastprice?companyid={companyid}&productIDorName={productIDorName}&productno={productno}&brand={brand}&sessionid={sessionid}")]
        List<LastPrices> GetLastPrice(int companyid, string productIDorName, string productno, string brand, string sessionid);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getToDoData?myDate={myDate}&userId={userId}&userType={userType}&sessionid={sessionid}")]
        List<Requirement> GetToDoData(string myDate, int userId, string userType, string sessionid);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "isvalidquotationupload?reqid={reqid}&sessionid={sessionid}")]
        bool IsValidQuotationUpload(string reqid, string sessionid);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "assignRFQToUser")]
        Response AssignRFQToUser(int userid, string sessionid, bool type, int reqid, int assignedBy);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getfabricdetails?name={name}&weaveKnits={weaveKnits}&patternType={patternType}&composition={composition}&sessionid={sessionID}&designStyle={designStyle}")]
        ApparelFabric GetFabricDetails(string name, string weaveKnits, string patternType, string composition, string sessionID, string designStyle);


        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getwashprocessdetails?processType={processType}&processCategory={processCategory}&typeOfGarment={typeOfGarment}&sessionid={sessionID}&washType={washType}&articleType={articleType}")]
        List <ApparelProcess> GetWashProcessDetails(string processType, string processCategory, string typeOfGarment, string sessionID, string washType, string articleType);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getprintprocessdetails?printType={printType}&size={size}&color={color}&sessionid={sessionID}")]
        ApparelProcess GetPrintProcessDetails(string printType, string size, string color, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getapparelrequirementdetails?articleType={articleType}&articleID={articleID}&articleNumber={articleNumber}&styleID={styleID}&sessionid={sessionID}")]
        ApparelRequirement GetApparelRequirementDetails(string articleType, string articleID, string articleNumber, string styleID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getfabric?type={type}&sessionid={sessionID}")]
        List<ApparelFabric> GetFabric(string type, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetItemsDataFromExcel?reqID={reqID}&userID={userID}&sessionid={sessionID}")]
        List<ApparelFabric> GetItemsDataFromExcel(int reqID, int userID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetWashDataFromExcel?sessionid={sessionID}")]
        List<ApparelProcess> GetWashDataFromExcel(string sessionID); 

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetPrintDataFromExcel?sessionid={sessionID}")]
        List<ApparelProcess> GetPrintDataFromExcel(string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetEmbroideryDataFromExcel?sessionid={sessionID}")]
        List<ApparelProcess> GetEmbroideryDataFromExcel(string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetAppliqueDataFromExcel?sessionid={sessionID}")]
        List<ApparelProcess> GetAppliqueDataFromExcel(string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetEmbellishmentData?sessionid={sessionID}")]
        List<ApparelProcess> GetEmbellishmentData(string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetSewingSundriesFromExcel?sessionid={sessionID}")]
        List<ApparelSundries> GetSewingSundriesFromExcel(string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetPackagingSundriesFromExcel?sessionid={sessionID}")]
        List<ApparelSundries> GetPackagingSundriesFromExcel(string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetReqItemDataFromExcel?sessionid={sessionID}")]
        List<ApparelRequirement> GetReqItemDataFromExcel(string sessionID);


        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "requirementsettings?reqid={reqid}&sessionid={sessionid}")]
        List<RequirementSetting> GetRequirementSettings(int reqid, string sessionid);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "saverequirementsetting")]
        Response SaveRequirementSetting(RequirementSetting requirementSetting);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetItemBidHistory?reqid={reqID}&userID={userID}&sessionid={sessionID}")]
        List<RequirementItems> GetItemBidHistory(int reqID, int userID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetCurrencies?parameter={parameter}")]
        List<Currencies> GetCurrencies(string parameter);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savecurrencyrates")]
        Response SaveCurrencyRates(string currencyName, decimal currencyRate, int compID, DateTime startTime, DateTime endTime, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetCurrencybyCompany?compid={compid}&sessionID={sessionID}")]
        List<Currencies> GetCurrencybyCompany(int compid, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "uploadrequirementitemssaveexcel")]
        List<RequirementItems> uploadRequirementItemsSaveExcel(int reqID, bool isrfp, int userID, int compId, string sessionID, byte[] requirementItemsAttachment, int templateid);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanyname")]
        string GetCompanyName();

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetLeastItemPrices?dateFrom={dateFrom}&dateTo={dateTo}")]
        List<RevItemLeastPrices> GetLeastItemPrices(string dateFrom, string dateTo);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetVendorCapacityAndScoreCard?U_ID={U_ID}&sessionID={sessionID}")]
        List<KeyValuePair> GetVendorCapacityAndScoreCard(int U_ID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetAllItemPrices?dateFrom={dateFrom}&dateTo={dateTo}")]
        RevAllItemPrices GetAllItemPrices(string dateFrom, string dateTo);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcurrencyfactors?userid={userID}&sessionid={sessionID}&compid={compID}")]
        List<CurrencyFactors> GetCurrencyFactors(int userID, string sessionID, int compID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcurrencyfactoraudit?currencycode={currencycode}&sessionid={sessionid}&compid={compid}")]
        List<CurrencyFactors> GetCurrencyFactorAudit(string currencycode, int compid, string sessionid);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savecurrencyfactor")]
        Response SaveCurrencyFactor(CurrencyFactors currencyFactor);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "uploadQCSQtyAllocation")]
        Response UploadQCSQtyAllocation(int reqId, int userId, string sessionId, byte[] qcsQtyAllocation);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "downloadReport?compId={compId}&userid={userid}&type={type}&fromdate={fromdate}&todate={todate}&sessionId={sessionId}")]
        string DownloadReport(int compId, int userid,int type, string fromdate, string todate,string sessionId);
    }
}
