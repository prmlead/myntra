﻿using System;
using System.IO;
using System.Data;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Collections.Generic;
using System.ServiceModel.Activation;

using OfficeOpenXml;
using PRMServices.Common;
using PRMServices.Models;
using MySql.Data.MySqlClient;
using PRMServices;
using PdfSharp.Pdf;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using PRMServices.SQLHelper;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMPOService : IPRMPOService
    {
        private MySQLBizClass sqlHelper = new MySQLBizClass();

        #region DESC

        public POVendor GetDesPoInfo(int reqID, int userID, string sessionID)
        {
            Utilities.ValidateSession(sessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            POVendor PoObject = new POVendor();
            try
            {
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("po_GetDesPoInfo", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    PoObject = POUtility.GetDescPoObject(row);
                }
            }
            catch (Exception ex)
            {
                PoObject.ErrorMessage = ex.Message;
            }

            return PoObject;
        }

        public List<UserDetails> GetVendors(int reqID, string sessionID)
        {
            Utilities.ValidateSession(sessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<UserDetails> listUser = new List<UserDetails>();
            try
            {
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("po_getVendors", sd);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        UserDetails user = new UserDetails();
                        user.UserID = row["U_ID"] != DBNull.Value ? Convert.ToInt16(row["U_ID"]) : 0;
                        user.FirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                        user.LastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                        user.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                        user.Price = row["REV_VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_TOTAL_PRICE"]) : 0;
                        listUser.Add(user);
                    }
                }
            }
            catch (Exception ex)
            {
                UserDetails user = new UserDetails();
                user.ErrorMessage = ex.Message;
                listUser.Add(user);
            }

            return listUser;
        }

        public DispatchTrack GetDescDispatch(int poID, int dtID, string sessionID)
        {
            Utilities.ValidateSession(sessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            DispatchTrack dispatchtrack = new DispatchTrack();
            try
            {
                sd.Add("P_PO_ID", poID);
                sd.Add("P_DT_ID", dtID);
                DataSet ds = sqlHelper.SelectList("po_GetDesDispatchInfo", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    dispatchtrack = POUtility.GetDescDispatchObject(row);
                }
            }
            catch (Exception ex)
            {
                dispatchtrack.ErrorMessage = ex.Message;
            }

            return dispatchtrack;
        }

        public Response SaveDescPoInfo(POVendor povendor)
        {
            Utilities.ValidateSession(povendor.SessionID, null);
            Response response = new Response();
            try
            {
                string fileName = string.Empty;
                if (povendor.POFile != null)
                {
                    fileName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "req" + povendor.ReqID + "_user" + povendor.VendorID + "_" + povendor.POFile.FileName);
                    SaveFile(fileName, povendor.POFile.FileStream);
                    fileName = "req" + povendor.ReqID + "_user" + povendor.VendorID + "_" + povendor.POFile.FileName;
                    Response res = SaveAttachment(fileName);
                    fileName = res.ObjectID.ToString();
                    povendor.POLink = fileName;
                }
                else
                {
                    PRMServices prm = new PRMServices();
                    Requirement req = prm.GetRequirementData(povendor.ReqID, povendor.CreatedBy, povendor.SessionID);
                    Requirement reqVendor = prm.GetRequirementData(povendor.ReqID, povendor.VendorID, povendor.SessionID);
                    UserDetails Vendor = prm.GetUserDetails(povendor.VendorID, povendor.SessionID);
                    UserDetails Customer = prm.GetUserDetails(povendor.CreatedBy, povendor.SessionID);
                    int margin = 12;
                    long tick = DateTime.Now.Ticks;
                    PdfDocument pdf = PdfGenerator.GeneratePdf(GenerateDesPO(povendor, req, reqVendor, Vendor, Customer), PdfSharp.PageSize.A4, margin);
                    pdf.Save(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "PO_" + povendor.ReqID + "_" + povendor.VendorID + "_" + tick + ".pdf"));
                    fileName = "PO_" + povendor.ReqID + "_" + povendor.VendorID + "_" + tick + ".pdf";
                    Response responce = SaveAttachment(fileName);
                    fileName = responce.ObjectID.ToString();
                    povendor.POLink = fileName;
                }

                DataSet ds = POUtility.SaveDescPoInfoEntity(povendor);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response UpdatePOStatus(int poID, string status, string sessionID)
        {
            Utilities.ValidateSession(sessionID, null);
            Response response = new Response();
            try
            {
                string query = string.Format("UPDATE poinformation SET PO_STATUS = '{0}' WHERE PO_ID = {1};", status, poID);
                DataSet ds = sqlHelper.ExecuteQuery(query);
                response.ObjectID = 0;
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }
        
        public Response SaveDesDispatchTrack(DispatchTrack dispatchtrack, POVendor povendor)
        {
            Utilities.ValidateSession(dispatchtrack.SessionID, null);
            Response response = new Response();
            try
            {
                string fileName = string.Empty;
                if (dispatchtrack.POFile != null)
                {
                    fileName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "req" + povendor.ReqID + "_user" + povendor.VendorID + "_" + dispatchtrack.POFile.FileName);
                    SaveFile(fileName, dispatchtrack.POFile.FileStream);
                    fileName = "req" + povendor.ReqID + "_user" + povendor.VendorID + "_" + dispatchtrack.POFile.FileName;
                    Response res = SaveAttachment(fileName);
                    fileName = res.ObjectID.ToString();
                    dispatchtrack.DispatchLink = fileName;
                }
                else if (1 == 0)
                {
                    PRMServices prm = new PRMServices();
                    Requirement req = prm.GetRequirementData(povendor.ReqID, povendor.CreatedBy, povendor.SessionID);
                    Requirement reqVendor = prm.GetRequirementData(povendor.ReqID, povendor.VendorID, povendor.SessionID);
                    UserDetails Vendor = prm.GetUserDetails(povendor.VendorID, povendor.SessionID);
                    UserDetails Customer = prm.GetUserDetails(povendor.CreatedBy, povendor.SessionID);
                    int margin = 12;
                    long tick = DateTime.Now.Ticks;
                    PdfDocument pdf = PdfGenerator.GeneratePdf(GenerateDesPO(povendor, req, reqVendor, Vendor, Customer), PdfSharp.PageSize.A4, margin);
                    pdf.Save(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "PO_" + povendor.ReqID + "_" + povendor.VendorID + "_" + tick + ".pdf"));
                    fileName = "PO_" + povendor.ReqID + "_" + povendor.VendorID + "_" + tick + ".pdf";
                    Response responce = SaveAttachment(fileName);
                    fileName = responce.ObjectID.ToString();
                    povendor.POLink = fileName;
                }

                DataSet ds = POUtility.SaveDesDispatchTrackObject(dispatchtrack, povendor);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        #endregion DESC

        #region ITEM

        public VendorPO GetVendorPoList(int reqID, int userID, int poID, string sessionID)
        {
            Utilities.ValidateSession(sessionID, null);
            VendorPO vendorpo = new VendorPO();
            vendorpo.Vendor = new UserDetails();
            vendorpo.Req = new Requirement();
            vendorpo.ListPOItems = new List<POItems>();

            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_PO_ID", poID);
                DataSet ds = sqlHelper.SelectList("po_GetPoList", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    vendorpo.Vendor = POUtility.GetVendorPoObject(row);
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                {
                    DataRow row1 = ds.Tables[1].Rows[0];
                    vendorpo.Req = POUtility.GetVendorPoReqObject(row1);
                }

                if (ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow row2 in ds.Tables[2].Rows)
                    {
                        POItems poitems = new POItems();
                        poitems = POUtility.GetVendorPoItemsObject(row2);
                        vendorpo.ListPOItems.Add(poitems);
                    }
                }
            }
            catch (Exception ex)
            {
                vendorpo.ErrorMessage = ex.Message;
            }

            return vendorpo;
        }

        public VendorPO GetVendorPoInfo(int reqID, int userID, int poID, string sessionID)
        {
            Utilities.ValidateSession(sessionID, null);
            VendorPO vendorpo = new VendorPO();
            vendorpo.Vendor = new UserDetails();
            vendorpo.Req = new Requirement();
            vendorpo.ListPOItems = new List<POItems>();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_PO_ID", poID);
                DataSet ds = sqlHelper.SelectList("po_GetVendorPoInfo", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    vendorpo.Vendor = POUtility.GetVendorPoObject(row);
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                {
                    DataRow row1 = ds.Tables[1].Rows[0];
                    vendorpo.Req = POUtility.GetVendorPoReqObject(row1);
                }

                if (ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow row2 in ds.Tables[2].Rows)
                    {
                        POItems poitems = new POItems();
                        poitems = POUtility.GetVendorPoItemsObject(row2);
                        vendorpo.ListPOItems.Add(poitems);
                    }
                }
            }
            catch (Exception ex)
            {
                vendorpo.ErrorMessage = ex.Message;
            }

            return vendorpo;
        }

        public Response SaveVendorPOInfo(VendorPO vendorpo)
        {
            Utilities.ValidateSession(vendorpo.SessionID, null);
            Response response = new Response();
            Requirement req = new Requirement();
            Requirement vendorreq = new Requirement();
            UserDetails customer = new UserDetails();
            UserDetails vendor = new UserDetails();
            PRMPRService prmpr = new PRMPRService();
            try
            {
                PRMServices prm = new PRMServices();
                string folderPath = HttpContext.Current.Server.MapPath(Utilities.FILE_URL);
                req = prm.GetRequirementData(vendorpo.Req.RequirementID, vendorpo.Req.CustomerID, vendorpo.SessionID);
                vendorreq = prm.GetRequirementData(vendorpo.Req.RequirementID, vendorpo.Vendor.UserID, vendorpo.SessionID);
                vendor = prm.GetUserDetails(vendorpo.Vendor.UserID, vendorpo.SessionID);
                customer = prm.GetUserDetails(vendorpo.Req.CustomerID, vendorpo.SessionID);
                string fileName = string.Empty;


                int poSeries = 0;
                if (vendorpo.ListPOItems[0].POID == 0)
                {
                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_MODIFIED_BY", vendorpo.Req.CustomerID);
                    DataSet ds1 = sqlHelper.SelectList("po_genSeries", sd);
                    if (ds1 != null && ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0 && ds1.Tables[0].Rows[0][0] != null)
                    {
                        poSeries = ds1.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds1.Tables[0].Rows[0][0].ToString()) : -1;
                        // response.Message = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                    }
                    vendorpo.PurchaseOrderID = prmpr.generatePRNumber("", "PO", vendorpo.SessionID, vendorpo.ListPOItems[0].COMP_ID, vendorpo.ListPOItems[0].DEPT_ID, vendorpo.ListPOItems[0].PurchaseID);
                }
                else
                {
                    vendorpo.PurchaseOrderID =  vendorpo.ListPOItems[0].PurchaseID;
                }


                if (vendorpo.POFile != null)
                {
                    fileName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "req" + req.RequirementID + "_user" + vendor.UserID + "_" + vendorpo.POFile.FileName);
                    SaveFile(fileName, vendorpo.POFile.FileStream);
                    fileName = "req" + req.RequirementID + "_user" + vendor.UserID + "_" + vendorpo.POFile.FileName;
                    Response res = SaveAttachment(fileName);
                    fileName = res.ObjectID.ToString();
                    vendorpo.POLink = fileName;
                }
                else
                {
                    long nowTicks = DateTime.Now.Ticks;
                    int margin = 16;
                    //PdfDocument pdf = PdfGenerator.GeneratePdf(GenerateItemizedPO(vendorpo, req, vendorreq, customer, vendor), PdfSharp.PageSize.A4, margin);
                    //pdf.Save(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "vendorPO" + req.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf"));
                    //fileName = "vendorPO" + req.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf";
                    List<KeyValuePair<string, DataTable>> poTbl = new List<KeyValuePair<string, DataTable>>();
                    poTbl = PdfUtilities.MakeDataTablePo(vendorpo, req, vendorreq, customer, vendor);
                    PdfUtilities.MakeDataTablePoToPdf(poTbl, @folderPath + "vendorPO" + req.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf", customer, vendor, vendorpo);
                    fileName = "vendorPO" + req.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf";
                    Response res = SaveAttachment(fileName);
                    fileName = res.ObjectID.ToString();
                    vendorpo.POLink = fileName;
                }


                foreach (POItems POItem in vendorpo.ListPOItems)
                {
                    POItem.PurchaseID = vendorpo.PurchaseOrderID;

                    DataSet ds = POUtility.SavePOItemEntity(POItem, vendorpo, req, vendor);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                    {
                        response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }




        //public Response SaveMACVendorPOInfo(MACRequirement vendorpo)
        //{
        //    Utilities.ValidateSession(vendorpo.SessionID, null);
        //    Response response = new Response();
        //    //Requirement req = new Requirement();
        //    //Requirement vendorreq = new Requirement();
        //    //UserDetails customer = new UserDetails();
        //    //UserDetails vendor = new UserDetails();
        //    try
        //    {
        //        foreach (MACRequirementItems macItem in vendorpo.ReqItems)
        //        {
        //            //DataSet ds = POUtility.SaveMACPOItemEntity(macItem);
        //            //if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
        //            //{
        //            //    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
        //            //}
        //            //public static DataSet SaveMACPOItemEntity(MACRequirementItems poitems, int reqID)
        //            //{
        //            //    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
        //            //    sd.Add("P_PO_ID", 0);
        //            //    sd.Add("P_REQ_ID", reqID);
        //            //    sd.Add("P_VENDOR_ID", poitems.VendorID);
        //            //    sd.Add("P_ITEM_ID", poitems.ItemID);
        //            //    sd.Add("P_VENDOR_PO_QUANTITY", 0);
        //            //    sd.Add("P_PO_QUANTITY", poitems.ProductQuantity);
        //            //    sd.Add("P_EXPECTED_DELIVERY_DATE", poitems.ExpectedDeliveryDate);
        //            //    sd.Add("P_PO_PRICE", poitems.RevVendorUnitPrice);
        //            //    sd.Add("P_PO_COMMENTS", "");
        //            //    sd.Add("P_PO_STATUS", "");
        //            //    sd.Add("P_MODIFIED_BY", 0);
        //            //    sd.Add("P_CREATED_BY", 0);
        //            //    sd.Add("P_PURCHASE_ORDER_ID", poitems.PurchaseID);
        //            //    sd.Add("P_PO_LINK", vendorpo.POLink);
        //            //    sd.Add("P_DELIVERY_ADDR", poitems.DeliveryAddress);
        //            //    sd.Add("P_INDENT_ID", '');
        //            //    sd.Add("P_PO_TOTAL_PRICE", poitems.PoTotalPrice);
        //            //    DataSet ds = sqlHelper.SelectList("po_SavePoInfo", sd);
        //            //    return ds;
        //            //}
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        response.ErrorMessage = ex.Message;
        //    }

        //    return response;
        //}


        public List<DispatchTrack> GetDispatchTrackList(int poID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Utilities.ValidateSession(sessionID, null);
            List<DispatchTrack> listDispatchTrack = new List<DispatchTrack>();
            Requirement req = new Requirement();
            UserDetails vendor = new UserDetails();
            try
            {
                sd.Add("P_PO_ID", poID);
                DataSet ds = sqlHelper.SelectList("po_GetDispatchTrackList", sd);
                List<POItems> listDispatcPOItems = new List<POItems>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                {
                    DataRow row1 = ds.Tables[1].Rows[0];
                    req = POUtility.GetRequirementDetails(row1);
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[2].Rows.Count > 0)
                {
                    DataRow row2 = ds.Tables[2].Rows[0];
                    vendor = POUtility.GetVendorDetails(row2);
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DispatchTrack dispatchtrack = new DispatchTrack();
                        dispatchtrack = POUtility.GetDispatchTrackObject(row);
                        dispatchtrack.VendorPOObject = new VendorPO();
                        dispatchtrack.VendorPOObject.Req = new Requirement();
                        dispatchtrack.VendorPOObject.Req = req;
                        dispatchtrack.VendorPOObject.Vendor = vendor;
                        listDispatchTrack.Add(dispatchtrack);
                    }
                }
            }
            catch (Exception ex)
            {
                DispatchTrack dispatchtrack = new DispatchTrack();
                dispatchtrack.ErrorMessage = ex.Message;
                listDispatchTrack.Add(dispatchtrack);
            }

            return listDispatchTrack;
        }

        public List<DispatchTrack> GetDispatchTrack(int poID, int dispatchTrackID, string sessionID)
        {
            Utilities.ValidateSession(sessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<DispatchTrack> listDispatchTrack = new List<DispatchTrack>();
            try
            {
                sd.Add("P_PO_ID", poID);
                sd.Add("P_DT_ID", dispatchTrackID);
                DataSet ds = sqlHelper.SelectList("po_GetDispatchTrack", sd);
                List<POItems> listDispatcPOItems = new List<POItems>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0 && ds.Tables[1].Rows[0][0] != null)
                {
                    foreach (DataRow row in ds.Tables[1].Rows)
                    {
                        POItems dispatchPoItem = new POItems();
                        dispatchPoItem = POUtility.GetDispatchPoItem(row);
                        listDispatcPOItems.Add(dispatchPoItem);
                    }
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DispatchTrack dispatchtrack = new DispatchTrack();
                        dispatchtrack = POUtility.GetDispatchTrackObject(row);
                        dispatchtrack.POItemsEntity = listDispatcPOItems.Where(p => p.PurchaseID == dispatchtrack.PurchaseID).ToList();
                        listDispatchTrack.Add(dispatchtrack);
                    }
                }
            }
            catch (Exception ex)
            {
                DispatchTrack dispatchtrack = new DispatchTrack();
                dispatchtrack.ErrorMessage = ex.Message;
                listDispatchTrack.Add(dispatchtrack);
            }

            return listDispatchTrack;
        }

        public Response SaveDispatchTrack(DispatchTrack dispatchtrack, string requestType)
        {
            Utilities.ValidateSession(dispatchtrack.SessionID, null);
            PRMNotifications notifications = new PRMNotifications();
            Response response = new Response();
            Requirement newreq = new Requirement();
            Requirement vendorreq = new Requirement();
            UserDetails customer = new UserDetails();
            UserDetails vendor = new UserDetails();
            try
            {
                PRMServices prm = new PRMServices();
                string folderPath = HttpContext.Current.Server.MapPath(Utilities.FILE_URL);
                string fileName = string.Empty;
                newreq = prm.GetRequirementData(dispatchtrack.POItemsEntity[0].ReqID, 0, dispatchtrack.SessionID);
                vendorreq = prm.GetRequirementData(dispatchtrack.POItemsEntity[0].ReqID, dispatchtrack.POItemsEntity[0].VendorID, dispatchtrack.SessionID);
                customer = prm.GetUserDetails(newreq.CustomerID, dispatchtrack.SessionID);
                vendor = prm.GetUserDetails(dispatchtrack.POItemsEntity[0].VendorID, dispatchtrack.SessionID);
                if (requestType == "DISPATCH")
                {
                    string mrrTable = string.Empty;
                    foreach (POItems poItem in dispatchtrack.POItemsEntity)
                    {
                        string xml = string.Empty;
                        xml = notifications.GenerateEmailBody("MrrDispatchXML");
                        xml = String.Format(xml, poItem.ProductIDorName, poItem.VendorPOQuantity, poItem.SumDispatchQuantity, poItem.SumRecivedQuantity, poItem.DispatchQuantity);
                        mrrTable += xml;

                    }

                    long nowTicks = DateTime.Now.Ticks;
                    int margin = 16;
                    //PdfDocument pdf = PdfGenerator.GeneratePdf(GenerateMRRPO(dispatchtrack, newreq, mrrTable, customer), PdfSharp.PageSize.A4, margin);
                    //pdf.Save(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "PurchaseID_" + dispatchtrack.PurchaseID + "_DispatchCode_" + dispatchtrack.DispatchCode + ".pdf"));


                    List<KeyValuePair<string, DataTable>> poTbl = new List<KeyValuePair<string, DataTable>>();
                    poTbl = PdfUtilities.MakeDataTableDispatch(dispatchtrack, newreq, vendorreq, customer, vendor);
                    PdfUtilities.MakeDataTableDispatchToPdf(poTbl, @folderPath + "MaterialDispatch" + newreq.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf", customer, vendor, dispatchtrack);
                    fileName = "MaterialDispatch" + newreq.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf";
                    Response responce = SaveAttachment(fileName);
                    fileName = responce.ObjectID.ToString();
                    dispatchtrack.DispatchLink = fileName;
                }

                if (requestType == "RECEIVE")
                {
                    string mrrTable = string.Empty;
                    foreach (POItems poItem in dispatchtrack.POItemsEntity)
                    {
                        string xml = string.Empty;
                        xml = notifications.GenerateEmailBody("MrrReceiveXML");
                        xml = String.Format(xml, poItem.ProductIDorName, poItem.VendorPOQuantity, poItem.SumDispatchQuantity, poItem.SumRecivedQuantity, poItem.SumReturnQuantity, poItem.DispatchQuantity, poItem.RecivedQuantity, poItem.ReturnQuantity);
                        mrrTable += xml;

                    }

                    long nowTicks = DateTime.Now.Ticks;
                    int margin = 16;
                    List<KeyValuePair<string, DataTable>> poTbl = new List<KeyValuePair<string, DataTable>>();
                    poTbl = PdfUtilities.MakeDataTableMRR(dispatchtrack, newreq, vendorreq, customer, vendor);
                    PdfUtilities.MakeDataTableMRRToPdf(poTbl, @folderPath + "MRR" + newreq.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf", customer, vendor, dispatchtrack);
                    fileName = "MRR" + newreq.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf";
                    //PdfDocument pdf = PdfGenerator.GeneratePdf(GenerateMRRPOReportpdf(dispatchtrack, newreq, mrrTable, customer), PdfSharp.PageSize.A4, margin);
                    //pdf.Save(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "PurchaseID_" + dispatchtrack.PurchaseID + "_ReceivedCode_" + dispatchtrack.RecivedCode + ".pdf"));
                    Response responce = SaveAttachment(fileName);
                    fileName = responce.ObjectID.ToString();
                    dispatchtrack.RecivedLink = Convert.ToInt32(fileName);
                }

                Response res = SaveAttachment(fileName);
                fileName = res.ObjectID.ToString();
                foreach (POItems poItem in dispatchtrack.POItemsEntity)
                {
                    DataSet ds = POUtility.SaveDispatchTrackObject(dispatchtrack, poItem, requestType);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                    {
                        response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public PaymentTrack GetPaymentTrack(int vendorID, int poID, string sessionID)
        {
            Utilities.ValidateSession(sessionID, null);
            PaymentTrack paymenttrack = new PaymentTrack();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                sd.Add("P_VENDOR_ID", vendorID);
                sd.Add("P_PO_ID", poID);
                DataSet ds = sqlHelper.SelectList("po_GetPaymentTrack", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    UserDetails Vendor = new UserDetails();
                    DataRow row = ds.Tables[0].Rows[0];
                    Vendor = POUtility.GetVendorPoObject(row);
                    paymenttrack.Vendor = Vendor;
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                {
                    Requirement Req = new Requirement();
                    DataRow row1 = ds.Tables[1].Rows[0];
                    Req = POUtility.GetVendorPoReqObject(row1);
                    paymenttrack.Req = Req;
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[2].Rows.Count > 0)
                {
                    POItems PO = new POItems();
                    DataRow row2 = ds.Tables[2].Rows[0];
                    PO = POUtility.GetPoObject(row2);
                    paymenttrack.PO = PO;
                }

                List<DispatchTrack> ListDispatchObject = new List<DispatchTrack>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[3].Rows.Count > 0 && ds.Tables[3].Rows[0][0] != null)
                {
                    foreach (DataRow row3 in ds.Tables[3].Rows)
                    {
                        DispatchTrack DispatchObject = new DispatchTrack();
                        DispatchObject = POUtility.GetPaymentTrackObject(row3);
                        ListDispatchObject.Add(DispatchObject);
                    }

                    paymenttrack.DispatchObject = ListDispatchObject;
                }

                List<PaymentInfo> ListPaymentInfo = new List<PaymentInfo>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[4].Rows.Count > 0 && ds.Tables[4].Rows[0][0] != null)
                {
                    foreach (DataRow row4 in ds.Tables[4].Rows)
                    {
                        PaymentInfo paymentinfo = new PaymentInfo();
                        paymentinfo = POUtility.GetPaymentInfoObject(row4);
                        ListPaymentInfo.Add(paymentinfo);
                    }

                    paymenttrack.PaymentInfoObject = ListPaymentInfo;
                }
            }
            catch (Exception ex)
            {
                paymenttrack.ErrorMessage = ex.Message;
            }

            return paymenttrack;
        }

        public Response SavePaymentInfo(PaymentInfo paymentinfo)
        {
            Utilities.ValidateSession(paymentinfo.SessionID, null);
            Response response = new Response();
            try
            {
                DataSet ds = POUtility.SavePaymentInfoObject(paymentinfo);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public bool CheckUniqueIfExists(string param, string idtype, string sessionID)
        {
            bool response = false;
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PARAM", param);
                sd.Add("P_ID_TYPE", idtype);
                DataSet ds = sqlHelper.SelectList("cp_CheckUniqueIfExists", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    int result = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    if (result > 0)
                    {
                        response = true;
                    }
                    else
                    {
                        response = false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return response;
        }

        #endregion ITEM

        #region Private
        
        private void SaveFile(string fileName, byte[] fileContent)
        {
            Utilities.SaveFile(fileName, fileContent);
            //PTMGenericServicesClient ptmClient = new PTMGenericServicesClient();
            //ptmClient.SaveFileBytes(fileContent, fileName);
            //File.WriteAllBytes(fileName, attachment);
        }

        private Response SaveAttachment(string path)
        {
            Response response = new Response();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PATH", path);
                DataSet ds = sqlHelper.SelectList("cp_SaveAttachment", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        private string GenerateItemizedPO(Requirement req, List<POVendor> poVendors, VendorDetails vendor, string sessionID)
        {
            PRMServices prm = new PRMServices();
            UserDetails customer = prm.GetUserDetails(req.CustomerID, sessionID);
            UserDetails vendorObj = prm.GetUserDetails(vendor.VendorID, sessionID);
            string itemRows = string.Empty;
            double tax = 0;
            double totalPriceRev = 0;
            int[] itemsArray = poVendors.Select(p => p.ItemID).ToArray();
            List<RequirementItems> items = req.ListRequirementItems.Where(i => itemsArray.Contains(i.ItemID)).ToList();
            string POID = string.Empty;
            string Comments = string.Empty;
            foreach (POVendor item in poVendors)
            {
                Requirement reqForVendor = prm.GetRequirementData(req.RequirementID, vendor.VendorID, sessionID);
                RequirementItems currentItem = reqForVendor.ListRequirementItems.Where(it => it.ItemID == item.ItemID).FirstOrDefault();
                tax = vendor.Taxes;
                RequirementItems selectedItem = items.Where(i => i.ItemID == item.ItemID).FirstOrDefault();
                string tableRows = "<tr>";
                tableRows += "<td>" + item.ProductIDorName + "</td>";
                tableRows += "<td>" + selectedItem.ProductNo + "</td>";
                tableRows += "<td>" + selectedItem.ProductDescription + "</td>";
                tableRows += "<td>" + selectedItem.ProductBrand + "</td>";
                tableRows += "<td>" + (item.Price * item.VendorPOQuantity).ToString() + "</td>";
                tableRows += "</tr>";
                totalPriceRev += Convert.ToDouble(item.Price * item.VendorPOQuantity);
                itemRows += tableRows;
                POID += item.POID;
                Comments += item.Comments + "<br/>";
            }

            Requirement reqVendor = prm.GetRequirementData(req.RequirementID, vendorObj.UserID, sessionID);
            string taxRows = string.Empty;
            double totalPrice = totalPriceRev;
            foreach (RequirementTaxes taxItem in reqVendor.ListRequirementTaxes)
            {
                string tableRows = "<tr>";
                tableRows += "<td colspan=4>" + taxItem.TaxName + "</td>";
                tableRows += "<td>" + taxItem.TaxPercentage + "%</td>";
                tableRows += "</tr>";
                taxRows += tableRows;
                totalPrice += (totalPriceRev * taxItem.TaxPercentage) / 100;
            }
            totalPrice += vendor.RevVendorFreight;

            Credentials tinCred = prm.GetUserCredentials(req.CustomerID, sessionID).FirstOrDefault(v => v.FileType == "TIN");

            if (string.IsNullOrEmpty(customer.LogoURL))
            {
                customer.LogoURL = "/img/logo.png";
            }

            customer.LogoURL = customer.LogoURL.Replace("/Services/auctionFiles//Services/auctionFiles/", "/Services/auctionFiles/");

            //DateTime dateTimeObj = new DateTime();
            //if (req.DeliveryTime != null)
            //{
            //    dateTimeObj = (DateTime)req.DeliveryTime;
            //}

            string html1 = String.Format(System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "ItemizedPOText.html")),
                "http://prm360.com" + customer.LogoURL,
                customer.CompanyName.ToString(),
                customer.Address.ToString(),
                customer.PhoneNum.ToString(),
                customer.Email.ToString(),
                DateTime.Now.ToShortDateString(),
                POID.ToString(),
                vendorObj.CompanyName.ToString(), // 7
                vendorObj.Address.ToString(),
                vendorObj.PhoneNum.ToString(),
                vendorObj.Email.ToString(),
                DateTime.Now.ToShortDateString(),
                req.Title.ToString(),
                itemRows,
                totalPriceRev.ToString(),
                totalPriceRev.ToString(),
                tax,
                totalPrice,
                !string.IsNullOrEmpty(vendorObj.Address) ? customer.FirstName + " " + customer.LastName + " - " + vendorObj.Address.ToString() : customer.FirstName + " " + customer.LastName + " - " + customer.Address.ToString(),
                poVendors[0].DeliveryAddress.ToString(),
                //dateTimeObj != null ? dateTimeObj.ToShortDateString() : "No Delivery Date Specified by the Customer",
                req.DeliveryTime != "" ? req.DeliveryTime.ToString() : "No Delivery Date Specified by the Customer",              
                "",
                tinCred.CredentialID,
                customer.FirstName.ToString() + " " + customer.LastName.ToString(),
                vendorObj.FirstName.ToString() + " " + vendorObj.LastName.ToString(), // 24
                string.IsNullOrEmpty(Comments) ? "" : Comments,
                taxRows,
                vendor.RevVendorFreight
                );
            return html1;
        }

        private string GenerateDesPO(POVendor povendor, Requirement req, Requirement reqVendor, UserDetails vendor, UserDetails customer)
        {
            string taxRows = string.Empty;
            double totalPrice = Convert.ToDouble(povendor.Price);
            foreach (RequirementTaxes tax in reqVendor.ListRequirementTaxes)
            {
                string tableRows = "<tr>";
                tableRows += "<td colspan=1>" + tax.TaxName + "</td>";
                tableRows += "<td>" + tax.TaxPercentage + "%</td>";
                tableRows += "</tr>";
                taxRows += tableRows;                
            }

            string html = string.Empty;
            html = "DescPO.html";

            string filestring = File.ReadAllText(HttpContext.Current.Server.MapPath(Utilities.FILE_URL + html));
            string htmlRows = string.Empty;
            try
            {
                htmlRows = String.Format(filestring,
                "http://prm360.com" + customer.LogoURL,
                customer.CompanyName.ToString(),
                customer.Address.ToString(),
                customer.PhoneNum.ToString(),
                customer.Email.ToString(),
                DateTime.Now.ToShortDateString(),
                povendor.PurchaseID.ToString(),
                vendor.CompanyName.ToString(), // 7
                vendor.Address.ToString(),
                vendor.PhoneNum.ToString(),
                vendor.Email.ToString(),
                DateTime.Now.ToShortDateString(),
                req.Title.ToString(),
                req.Description.ToString(), // 13
                reqVendor.AuctionVendors[0].RevPrice.ToString(),
                taxRows,
                reqVendor.AuctionVendors[0].RevVendorFreight.ToString(),
                totalPrice.ToString(),
                povendor.ExpectedDeliveryDate.ToString(),
                povendor.DeliveryAddress.ToString(),
                povendor.Comments.ToString(),
                customer.FirstName.ToString() + " " + customer.LastName.ToString()
               );
            }
            catch
            {

            }

            return htmlRows;
        }

        private string GenerateItemizedPO(VendorPO vendorpo, Requirement req, Requirement vendorreq, UserDetails customer, UserDetails vendor)
        {

            PRMServices prm = new PRMServices();
            string itemRows = string.Empty;
            double tax = 0;
            double totalPriceRev = 0;

            List<POItems> poItems = vendorpo.ListPOItems.Where(i => i.VendorPOQuantity > 0).ToList();                
            int[] itemsArray = poItems.Select(p => p.ItemID).ToArray();

            List<RequirementItems> items = vendorreq.ListRequirementItems.Where(i => itemsArray.Contains(i.ItemID)).ToList();

            DateTime? commonExpectedDeliveryDate = DateTime.Now;
            string commmonDeliveryAddress = "";

            foreach (RequirementItems ri in items)
            {
                 List<POItems> poitems = poItems.Where(i => i.ItemID == ri.ItemID).ToList();

                 if(poitems.Count > 0){

                     POItems poitem = poitems[0];

                     var GST = Convert.ToDouble(poitem.CGst) + Convert.ToDouble(poitem.SGst) + Convert.ToDouble(poitem.IGst);
                     var priceQuantity = Convert.ToDouble(poitem.POPrice) * Convert.ToDouble(poitem.VendorPOQuantity);

                     string tableRows = "<tr>";
                     tableRows += "<td>" + ri.ProductIDorName + "</td>";
                     tableRows += "<td>" + ri.ProductNo + "</td>";
                     tableRows += "<td>" + ri.ProductDescription + "</td>";
                     tableRows += "<td>" + ri.ProductBrand + "</td>";
                     tableRows += "<td>" + poitem.POPrice + "</td>";
                     tableRows += "<td>" + poitem.VendorPOQuantity + "</td>";
                     tableRows += "<td>" + GST + "</td>";
                     tableRows += "<td>" + Convert.ToDouble(priceQuantity + ((priceQuantity / 100) * (GST))) + "</td>";

                     if (!vendorpo.Common)
                     {
                         tableRows += "<td>" + poitem.ExpectedDeliveryDate + "</td>";
                         tableRows += "<td>" + poitem.DeliveryAddress + "</td>";
                     }
                     else
                     {
                         commonExpectedDeliveryDate = poitem.ExpectedDeliveryDate;
                         commmonDeliveryAddress = poitem.DeliveryAddress;
                     }

                     
                     tableRows += "</tr>";



                     totalPriceRev += Convert.ToDouble(priceQuantity + ((priceQuantity / 100) * (GST)));
                     itemRows += tableRows;
                 }               

            }


            string POID = string.Empty;
            string Comments = string.Empty;            

            //Requirement reqVendor = prm.GetRequirementData(req.RequirementID, vendorObj.UserID, sessionID);
            string taxRows = string.Empty;
            double totalPrice = totalPriceRev;
            foreach (RequirementTaxes taxItem in vendorreq.ListRequirementTaxes)
            {
                string tableRows = "<tr>";
                tableRows += "<td colspan=4>" + taxItem.TaxName + "</td>";
                tableRows += "<td>" + taxItem.TaxPercentage + "%</td>";
                tableRows += "</tr>";
                taxRows += tableRows;
                totalPrice += (totalPriceRev * taxItem.TaxPercentage) / 100;
            }
            totalPrice += vendorreq.AuctionVendors[0].RevVendorFreight;

            Credentials tinCred = prm.GetUserCredentials(req.CustomerID, vendorpo.SessionID).FirstOrDefault(v => v.FileType == "TIN");

            if (string.IsNullOrEmpty(customer.LogoURL))
            {
                customer.LogoURL = "/img/logo.png";
            }

            customer.LogoURL = customer.LogoURL.Replace("/Services/auctionFiles//Services/auctionFiles/", "/Services/auctionFiles/");

            //DateTime dateTimeObj = new DateTime();
            //if (req.DeliveryTime != null)
            //{
            //    dateTimeObj = (DateTime)req.DeliveryTime;
            //}

            string pageName = "ItemizedPOText.html";

            if (!vendorpo.Common)
            {
                pageName = "ItemizedPOText.html";
            }
            else
            {
                pageName = "ItemizedPOTextCommon.html";
            }
            


            string html1 = String.Format(System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + pageName)),
                "http://prm360.com" + customer.LogoURL,
                customer.CompanyName.ToString(),
                customer.Address.ToString(),
                customer.PhoneNum.ToString(),
                customer.Email.ToString(),
                DateTime.Now.ToShortDateString(),
                vendorpo.ListPOItems[0].PurchaseID.ToString(),
                vendor.CompanyName.ToString(), // 7
                vendor.Address.ToString(),
                vendor.PhoneNum.ToString(),
                vendor.Email.ToString(),
                DateTime.Now.ToShortDateString(),
                req.Title.ToString(),


                itemRows,
                totalPriceRev.ToString(),
                totalPriceRev.ToString(),
                tax,
                totalPrice,
                !string.IsNullOrEmpty(vendor.Address) ? customer.FirstName + " " + customer.LastName + " - " + vendor.Address.ToString() : customer.FirstName + " " + customer.LastName + " - " + customer.Address.ToString(),
                vendorpo.ListPOItems[0].DeliveryAddress.ToString(),
                //dateTimeObj != null ? dateTimeObj.ToShortDateString() : "No Delivery Date Specified by the Customer",
                req.DeliveryTime != "" ? req.DeliveryTime.ToString() : "No Delivery Date Specified by the Customer",
                "",
                tinCred.CredentialID,
                customer.FirstName.ToString() + " " + customer.LastName.ToString(),
                vendor.FirstName.ToString() + " " + vendor.LastName.ToString(), // 24
                string.IsNullOrEmpty(Comments) ? "" : Comments,
                taxRows,
                vendorreq.AuctionVendors[0].RevVendorFreight,
                vendorpo.ListPOItems[0].IndentID.ToString(), //28
                commonExpectedDeliveryDate.ToString(),
                commmonDeliveryAddress.ToString()
                );
            return html1;
        }

        private string GenerateMRRPO(DispatchTrack dispatchdetails , Requirement newreq , string mrrTable , UserDetails customer)
        {
            if (string.IsNullOrEmpty(customer.LogoURL))
            {
                customer.LogoURL = "/img/logo.png";
            }

            customer.LogoURL = customer.LogoURL.Replace("/Services/auctionFiles//Services/auctionFiles/", "/Services/auctionFiles/");


            string html1 = String.Format(System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "MRRPDF.html")),
                dispatchdetails.PurchaseID,
                dispatchdetails.IndentID,
                dispatchdetails.DispatchType,
                dispatchdetails.DeliveryTrackID,
                dispatchdetails.DispatchCode,
                dispatchdetails.DispatchMode,
                dispatchdetails.DispatchDate,
                dispatchdetails.DispatchComments,
                newreq.Title,
                mrrTable.ToString(),
                "http://prm360.com" + customer.LogoURL,
                customer.CompanyName.ToString()

                );
            return html1;
        }

        private string GenerateMRRPOReportpdf(DispatchTrack dispatchdetails, Requirement newreq, string mrrTable, UserDetails customer)
        {

            if (string.IsNullOrEmpty(customer.LogoURL))
            {
                customer.LogoURL = "/img/logo.png";
            }

            customer.LogoURL = customer.LogoURL.Replace("/Services/auctionFiles//Services/auctionFiles/", "/Services/auctionFiles/");


            string html1 = String.Format(System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "MaterialReceivedReportpdf.html")),
                dispatchdetails.PurchaseID,
                dispatchdetails.IndentID,
                dispatchdetails.DispatchType,
                dispatchdetails.DeliveryTrackID,
                dispatchdetails.DispatchCode,
                dispatchdetails.DispatchMode,
                dispatchdetails.DispatchDate,
                dispatchdetails.DispatchComments,
                newreq.Title,
                mrrTable.ToString(),
                dispatchdetails.RecivedCode,
                dispatchdetails.RecivedBy,
                dispatchdetails.RecivedComments,
                dispatchdetails.RecivedDate,
                "http://prm360.com" + customer.LogoURL,
                customer.CompanyName.ToString()
                );
            return html1;
        }

        #endregion
    }
    
}