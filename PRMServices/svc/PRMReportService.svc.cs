﻿using System;
using System.Data;
using System.Web;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using PRMServices.Common;
using PRMServices.Models;
using OfficeOpenXml;
using OfficeOpenXml.DataValidation;
using System.IO;
using System.Linq;
using System.Drawing;
using OfficeOpenXml.Style;
using PRMServices.SQLHelper;
//using PRM.Core.Models.Reports;
using CORE = PRM.Core.Common;
using CATALOG = PRMServices.Models.Catalog;
using MySql.Data.MySqlClient;
using PRMServices.Models.Catalog;
using Microsoft.Office.Interop.Excel;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMReportService : IPRMReportService
    {
        private MySQLBizClass sqlHelper = new MySQLBizClass();
        private PRMServices prmservices = new PRMServices();
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        #region Get

        public LiveBidding[] GetLiveBiddingReport(int reqID, int count, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            if (count <= 0)
            {
                count = 10;
            }

            List<LiveBidding> details = new List<LiveBidding>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID, null);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_COUNT", count);
                DataSet ds = sqlHelper.SelectList("rp_GetLiveBiddingReport", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        LiveBidding detail = ReportUtility.GetLiveBiddingReportObject(row, count);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                LiveBidding error = new LiveBidding();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public LiveBidding[] GetLiveBiddingReport2(int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<LiveBidding> details = new List<LiveBidding>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID, null);
                ReportsRequirement reportsrequirement = new ReportsRequirement();
                reportsrequirement = GetReqDetails(reqID, sessionID);
                TimeSpan duration = reportsrequirement.EndTime.Subtract(reportsrequirement.StartTime);
                int interval = 0;
                for (int a = 10; a < duration.Minutes; a = a + 10)
                {
                    interval = duration.Minutes - 10;
                }

                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("rp_GetLiveBiddingReport2", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        LiveBidding detail = ReportUtility.GetLiveBiddingReportObject2(row);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                LiveBidding error = new LiveBidding();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public ItemWiseReport[] GetItemWiseReport(int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<ItemWiseReport> details = new List<ItemWiseReport>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID, null);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("rp_GetItemWiseReport", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ItemWiseReport detail = ReportUtility.GetItemWiseReportObject(row);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                ItemWiseReport error = new ItemWiseReport();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public DeliveryTimeLine[] GetDeliveryTimeLineReport(int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<DeliveryTimeLine> details = new List<DeliveryTimeLine>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID, null);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("rp_GetDeliveryTimeLineReport", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DeliveryTimeLine detail = ReportUtility.GetIDeliveryTimelineObject(row);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                DeliveryTimeLine error = new DeliveryTimeLine();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public DeliveryTimeLine[] GetPaymentTermsReport(int reqID, string sessionID)
        {
            List<DeliveryTimeLine> details = new List<DeliveryTimeLine>();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID, null);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("rp_GetPaymentTermsReport", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DeliveryTimeLine detail = ReportUtility.GetIDeliveryTimelineObject(row);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                DeliveryTimeLine error = new DeliveryTimeLine();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public ReportsRequirement GetReqDetails(int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            ReportsRequirement reportsrequirement = new ReportsRequirement();
            try
            {
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("rp_GetReqDetails", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    reportsrequirement.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                    reportsrequirement.CustomerID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                    reportsrequirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                    reportsrequirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                    reportsrequirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                    reportsrequirement.EndTime = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                    reportsrequirement.NegotiationDuration = row["NEGOTIATION_DURATION"] != DBNull.Value ? Convert.ToString(row["NEGOTIATION_DURATION"]) : string.Empty;
                    reportsrequirement.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                    reportsrequirement.IsTabular = row["REQ_IS_TABULAR"] != DBNull.Value ? (Convert.ToInt32(row["REQ_IS_TABULAR"]) == 1 ? true : false) : false;
                    reportsrequirement.IsUnitPriceBidding = row["IS_UNIT_PRICE_BIDDING"] != DBNull.Value ? Convert.ToInt32(row["IS_UNIT_PRICE_BIDDING"]) : 0;
                    reportsrequirement.NoOfVendorsInvited = row["NO_OF_VENDORS_INVITED"] != DBNull.Value ? Convert.ToInt32(row["NO_OF_VENDORS_INVITED"]) : 0;
                    reportsrequirement.NoOfVendorsParticipated = row["NO_OF_VENDORS_PARTICIPATED"] != DBNull.Value ? Convert.ToInt32(row["NO_OF_VENDORS_PARTICIPATED"]) : 0;
                }
            }
            catch (Exception ex)
            {

            }

            return reportsrequirement;
        }

        public string GetTemplates(string template, int compID, int userID, int reqID, string from, string to, string sessionID, string vendorIds = null, bool sendEmail= false)
        {
            MemoryStream ms = new MemoryStream();
            byte[] contents = null;
            int maxRows = 500;

            #region addvendors
            if (template.ToLower().Contains("addvendors"))
            {
                PRMServices service = new PRMServices();
                List<CategoryObj> categories = service.GetCategories(userID);
                var uniqueCategories = categories.Select(o => o.Category).Distinct().Where(o=>o.Trim()!=string.Empty).ToList();
                List<string> currencies = new List<string>(new string[] { "INR", "USD", "JPY", "GBP", "CAD", "AUD", "HKD", "EUR", "CNY" });
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("AddVendor");
                wsSheet1.Cells["A1"].Value = "FirstName";
                wsSheet1.Cells["B1"].Value = "LasName";
                wsSheet1.Cells["C1"].Value = "Email";
                wsSheet1.Cells["D1"].Value = "PhoneNumber";
                wsSheet1.Cells["E1"].Value = "CompanyName";
                wsSheet1.Cells["F1"].Value = "Category";
                wsSheet1.Cells["G1"].Value = "Currency";
                wsSheet1.Cells["H1"].Value = "KnownSince";
                var validation = wsSheet1.DataValidations.AddListValidation("G2:G100000");
                validation.ShowErrorMessage = true;
                validation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                validation.ErrorTitle = "Invalid Currency";
                validation.Error = "Invalid currency selected";
                foreach(var currency in currencies)
                {
                    validation.Formula.Values.Add(currency);
                }

                var catValidation = wsSheet1.DataValidations.AddListValidation("F2:F100000");
                catValidation.ShowErrorMessage = true;
                catValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                catValidation.ErrorTitle = "Invalid Category";
                catValidation.Error = "Invalid category entered";
                int count = 1;
                foreach (var cat in uniqueCategories)
                {
                    wsSheet1.Cells["AA" + count.ToString()].Value = cat;
                    count++;
                }

                catValidation.Formula.ExcelFormula = "AA1:AA" + (count-1).ToString();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }                    
            }
            #endregion

            #region vendorquotation
            if (template.ToUpper().Contains("MARGIN_QUOTATION"))
            {
                PRMServices service = new PRMServices();
                Requirement requirement = service.GetRequirementData(reqID, userID, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("MARGIN_QUOTATION");
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["C1"].Value = "HSN_CODE";
                wsSheet1.Cells["D1"].Value = "BRAND";
                wsSheet1.Cells["E1"].Value = "QUANTITY";
                wsSheet1.Cells["F1"].Value = "COST_PRICE";
                wsSheet1.Cells["G1"].Value = "GST";
                wsSheet1.Cells["H1"].Value = "MRP";
                wsSheet1.Cells["I1"].Value = "NET_PRICE";
                wsSheet1.Cells["J1"].Value = "MARGIN_AMOUNT";
                wsSheet1.Cells["K1"].Value = "MARGIN_PERC";
                wsSheet1.Cells["L1"].Value = "ITEM_ID";
                wsSheet1.Cells["M1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["N1"].Value = "PRODUCT_NAME";                
                wsSheet1.Cells["O1"].Value = "UNITS";
                wsSheet1.Cells["P1"].Value = "VENDOR_REMARK";
                wsSheet1.Cells["Q1"].Value = "REQ_ID";
                wsSheet1.Cells["R1"].Value = "TITLE";
                wsSheet1.Cells["S1"].Value = "POSTED_DATE";
                wsSheet1.Cells["T1"].Value = "MANUFACTURER_NAME";

                #region excelStyling
                wsSheet1.Cells["K2:K6000"].Style.Numberformat.Format = "0.00000000";
                wsSheet1.Column(1).Hidden = true;
                wsSheet1.Column(12).Hidden = true;
                wsSheet1.Column(13).Hidden = true;                
                wsSheet1.Cells["A:K"].AutoFitColumns();
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#F2DCDB");
                wsSheet1.Cells["I:I"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["I:I"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["J:J"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["J:J"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["K:K"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["K:K"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                var calcColumns = wsSheet1.Cells["I:K"];
                var calcColumnsFont = calcColumns.Style.Font;
                calcColumnsFont.Bold = true;
                calcColumnsFont.Italic = true;
                #endregion excelStyling

                wsSheet1.Cells["I2:I6000"].Formula = "IF(A2 > 0, F2*(1+(G2/100)), \"\")";
                wsSheet1.Cells["J2:J6000"].Formula = "IF(A2 > 0, H2 - I2, \"\")";
                wsSheet1.Cells["K2:K6000"].Formula = "IFERROR(J2/I2*100,\"\")";
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors) {

                    Requirement vendorReq = service.GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);                  
                }

                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    Requirement req = service.GetRequirementData(entry.Value.RequirementID, entry.Key, sessionID);

                    string vendorRemarks = "";

                    foreach (var vendor in entry.Value.AuctionVendors) {
                        if (vendor.VendorID == entry.Key) {
                            vendorRemarks = vendor.OtherProperties;
                        }
                    }

                    foreach (var item in entry.Value.ListRequirementItems) {
                        var gst = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["A" + index].Value = entry.Key;
                        wsSheet1.Cells["B" + index].Value = entry.Value.AuctionVendors[0].CompanyName;
                        wsSheet1.Cells["L" + index].Value = item.ItemID;
                        wsSheet1.Cells["C" + index].Value = item.ProductNo;
                        wsSheet1.Cells["D" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["E" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["H" + index].Value = item.UnitMRP;
                        wsSheet1.Cells["G" + index].Value = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["M" + index].Value = 0;
                        wsSheet1.Cells["N" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["O" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["P" + index].Value = vendorRemarks;
                        wsSheet1.Cells["Q" + index].Value = req.RequirementID;
                        wsSheet1.Cells["R" + index].Value = req.Title;
                        wsSheet1.Cells["S" + index].Value = req.PostedOn.ToString();
                        wsSheet1.Cells["T" + index].Value = item.OthersBrands;
                        var costPrice = (item.UnitMRP * 100 * 100) / ((item.UnitDiscount * 100) + 10000 + (item.UnitDiscount * gst) + (gst * 100));
                        wsSheet1.Cells["F" + index].Value = Math.Round(costPrice, 2);
                        index++;
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion

            #region revvendorquotation
            if (template.ToUpper().Contains("MARGIN_REV_QUOTATION"))
            {
                PRMServices service = new PRMServices();
                Requirement requirement = service.GetRequirementData(reqID, userID, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("MARGIN_REV_QUOTATION");
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["C1"].Value = "HSN_CODE";
                wsSheet1.Cells["D1"].Value = "BRAND";
                wsSheet1.Cells["E1"].Value = "QUANTITY";
                wsSheet1.Cells["F1"].Value = "COST_PRICE";
                wsSheet1.Cells["G1"].Value = "GST";
                wsSheet1.Cells["H1"].Value = "MRP";
                wsSheet1.Cells["I1"].Value = "NET_PRICE";
                wsSheet1.Cells["J1"].Value = "MARGIN_AMOUNT";
                wsSheet1.Cells["K1"].Value = "MARGIN_PERC";
                wsSheet1.Cells["L1"].Value = "ITEM_ID";
                wsSheet1.Cells["M1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["N1"].Value = "PRODUCT_NAME";
                wsSheet1.Cells["O1"].Value = "UNITS";
                wsSheet1.Cells["P1"].Value = "VENDOR_REMARK";
                wsSheet1.Cells["Q1"].Value = "REQ_ID";
                wsSheet1.Cells["R1"].Value = "TITLE";
                wsSheet1.Cells["S1"].Value = "POSTED_DATE";
                wsSheet1.Cells["T1"].Value = "MANUFACTURER_NAME";

                #region excelStyling
                wsSheet1.Cells["K2:K6000"].Style.Numberformat.Format = "0.00000000";
                wsSheet1.Column(1).Hidden = true;
                wsSheet1.Column(12).Hidden = true;
                wsSheet1.Column(13).Hidden = true;                
                wsSheet1.Cells["A:K"].AutoFitColumns();
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#F2DCDB");
                wsSheet1.Cells["I:I"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["I:I"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["J:J"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["J:J"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["K:K"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["K:K"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                var calcColumns = wsSheet1.Cells["I:K"];
                var calcColumnsFont = calcColumns.Style.Font;
                calcColumnsFont.Bold = true;
                calcColumnsFont.Italic = true;
                #endregion excelStyling

                wsSheet1.Cells["I2:I6000"].Formula = "IF(A2 > 0, F2*(1+(G2/100)), \"\")";
                wsSheet1.Cells["J2:J6000"].Formula = "IF(A2 > 0, H2 - I2, \"\")";
                wsSheet1.Cells["K2:K6000"].Formula = "IFERROR(J2/I2*100,\"\")";
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {
                    Requirement vendorReq = service.GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }

                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    Requirement req = service.GetRequirementData(entry.Value.RequirementID, entry.Key, sessionID);
                    string vendorRemarks = string.Empty;
                    foreach (var vendor in entry.Value.AuctionVendors)
                    {
                        if (vendor.VendorID == entry.Key)
                        {
                            vendorRemarks = vendor.OtherProperties;
                        }
                    }

                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        var gst = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["A" + index].Value = entry.Key;
                        wsSheet1.Cells["B" + index].Value = entry.Value.AuctionVendors[0].CompanyName;
                        wsSheet1.Cells["L" + index].Value = item.ItemID;
                        wsSheet1.Cells["C" + index].Value = item.ProductNo;
                        wsSheet1.Cells["D" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["E" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["H" + index].Value = item.UnitMRP;
                        wsSheet1.Cells["G" + index].Value = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["M" + index].Value = 0;
                        wsSheet1.Cells["N" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["O" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["P" + index].Value = vendorRemarks;
                        wsSheet1.Cells["Q" + index].Value = req.RequirementID;
                        wsSheet1.Cells["R" + index].Value = req.Title;
                        wsSheet1.Cells["S" + index].Value = req.PostedOn.ToString(); ;
                        wsSheet1.Cells["T" + index].Value = item.OthersBrands;
                        var costPrice = 100 * item.UnitMRP / (100 + gst) * (1 + (item.UnitDiscount / 100));
                        wsSheet1.Cells["F" + index].Value = Math.Round(costPrice, 2);
                        index++;
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion

            #region unitprice
            #region vendorquotation
            if (template.ToUpper().Contains("UNIT_PRICE_QUOTATION"))
            {
                PRMServices service = new PRMServices();
                Requirement requirement = service.GetRequirementData(reqID, userID, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("UNIT_PRICE_QUOTATION");
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["C1"].Value = "PRICE";
                wsSheet1.Cells["D1"].Value = "GST";
                wsSheet1.Cells["E1"].Value = "FREIGHT";
                wsSheet1.Cells["F1"].Value = "TOTAL_PRICE";
                wsSheet1.Cells["G1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["H1"].Value = "PRODUCT_ID";
                wsSheet1.Cells["I1"].Value = "PRODUCT_NAME";
                wsSheet1.Cells["J1"].Value = "PRODUCT_NO";
                wsSheet1.Cells["K1"].Value = "BRAND";
                wsSheet1.Cells["L1"].Value = "QUANTITY";
                wsSheet1.Cells["M1"].Value = "UNITS";
                wsSheet1.Cells["N1"].Value = "DESCRIPTION";
                wsSheet1.Cells["O1"].Value = "REQUIREMENT_ID";
                wsSheet1.Cells["F2:F6000"].Formula = "IF(A2 > 0, L2*C2*(1+(D2/100)), \"\")";
                wsSheet1.Cells["A:N"].AutoFitColumns();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {
                    Requirement vendorReq = service.GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }

                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    string vendorRemarks = string.Empty;
                    foreach (var vendor in entry.Value.AuctionVendors)
                    {
                        if (vendor.VendorID == entry.Key)
                        {
                            vendorRemarks = vendor.OtherProperties;
                        }
                    }

                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        var gst = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["A" + index].Value = entry.Key;
                        wsSheet1.Cells["B" + index].Value = entry.Value.AuctionVendors[0].CompanyName;
                        wsSheet1.Cells["C" + index].Value = item.RevUnitPrice;
                        wsSheet1.Cells["D" + index].Value = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["E" + index].Value = entry.Value.AuctionVendors[0].RevVendorFreight;
                        //wsSheet1.Cells["F" + index].Value = item.RevItemPrice;
                        wsSheet1.Cells["G" + index].Value = 0;
                        wsSheet1.Cells["H" + index].Value = item.ItemID;
                        wsSheet1.Cells["I" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["J" + index].Value = item.ProductNo;
                        wsSheet1.Cells["K" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["L" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["M" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["N" + index].Value = item.ProductDescription;
                        wsSheet1.Cells["O" + index].Value = item.RequirementID;
                        index++;
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion
            if (template.ToUpper().Contains("UNIT_QUOTATION"))
            {
                PRMServices service = new PRMServices();
                Requirement requirement = service.GetRequirementData(reqID, userID, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("UNIT_QUOTATION");
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["C1"].Value = "PRICE";
                wsSheet1.Cells["D1"].Value = "GST";
                wsSheet1.Cells["E1"].Value = "FREIGHT";
                wsSheet1.Cells["F1"].Value = "TOTAL_PRICE";
                wsSheet1.Cells["G1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["H1"].Value = "PRODUCT_ID";
                wsSheet1.Cells["I1"].Value = "Main Equipment/Vehicle";
                wsSheet1.Cells["J1"].Value = "Part_No";
                wsSheet1.Cells["K1"].Value = "Make";
                wsSheet1.Cells["L1"].Value = "Specification Of Item";
                wsSheet1.Cells["M1"].Value = "QTY/UOM";
                wsSheet1.Cells["N1"].Value = "Units";
                wsSheet1.Cells["O1"].Value = "Description";
                wsSheet1.Cells["P1"].Value = "REQUIREMENT_ID";
                wsSheet1.Cells["F2:F6000"].Formula = "IF(A2 > 0, C2*M2*(1+(D2/100)), \"\")";
                wsSheet1.Cells["A:P"].AutoFitColumns();
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#F2DCDB");
                wsSheet1.Cells["C:E"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["C:E"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {
                    Requirement vendorReq = service.GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }

                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    string vendorRemarks = string.Empty;
                    foreach (var vendor in entry.Value.AuctionVendors)
                    {
                        if (vendor.VendorID == entry.Key)
                        {
                            vendorRemarks = vendor.OtherProperties;
                        }
                    }

                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        var gst = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["A" + index].Value = entry.Key;
                        wsSheet1.Cells["B" + index].Value = entry.Value.AuctionVendors[0].CompanyName;
                        wsSheet1.Cells["C" + index].Value = item.RevUnitPrice;
                        wsSheet1.Cells["D" + index].Value = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["E" + index].Value = entry.Value.AuctionVendors[0].RevVendorFreight;
                        wsSheet1.Cells["G" + index].Value = 0;
                        wsSheet1.Cells["H" + index].Value = item.ItemID;
                        wsSheet1.Cells["I" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["J" + index].Value = item.ProductNo;
                        wsSheet1.Cells["K" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["L" + index].Value = item.OthersBrands;
                        wsSheet1.Cells["M" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["N" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["O" + index].Value = item.ProductDescription;
                        wsSheet1.Cells["P" + index].Value = item.RequirementID;
                        index++;
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }

            if (template.ToUpper().Contains("UNIT_BIDDING"))
            {
                PRMServices service = new PRMServices();
                Requirement requirement = service.GetRequirementData(reqID, userID, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("UNIT_BIDDING");
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "REQUIREMENT_ID";
                wsSheet1.Cells["C1"].Value = "Main Equipment/Vehicle";
                wsSheet1.Cells["D1"].Value = "Part_No";
                wsSheet1.Cells["E1"].Value = "Make";
                wsSheet1.Cells["F1"].Value = "Specification Of Item";
                wsSheet1.Cells["G1"].Value = "Revised Unit Price";
                wsSheet1.Cells["H1"].Value = "QTY/UOM";
                wsSheet1.Cells["I1"].Value = "GST";
                wsSheet1.Cells["J1"].Value = "Revised Item Price";
                wsSheet1.Cells["K1"].Value = "PRODUCT_ID";
                wsSheet1.Cells["L1"].Value = "Unit Price";
                wsSheet1.Cells["J2:J6000"].Formula = "IF(G2 > 0, G2*H2*(1 + I2/100), \"\")";
                wsSheet1.Cells["A:J"].AutoFitColumns();
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#F2DCDB");
                wsSheet1.Cells["G:G"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["G:G"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {
                    Requirement vendorReq = service.GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }

                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    string vendorRemarks = string.Empty;
                    foreach (var vendor in entry.Value.AuctionVendors)
                    {
                        if (vendor.VendorID == entry.Key)
                        {
                            vendorRemarks = vendor.OtherProperties;
                        }
                    }

                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        var gst = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["A" + index].Value = entry.Key;
                        wsSheet1.Cells["B" + index].Value = item.RequirementID;
                        wsSheet1.Cells["C" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["D" + index].Value = item.ProductNo;
                        wsSheet1.Cells["E" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["F" + index].Value = item.OthersBrands;
                        wsSheet1.Cells["G" + index].Value = item.RevUnitPrice;
                        wsSheet1.Cells["H" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["I" + index].Value = (item.SGst + item.CGst + item.IGst);
                        wsSheet1.Cells["K" + index].Value = item.ItemID;
                        wsSheet1.Cells["L" + index].Value = item.UnitPrice;
                        index++;
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #region 
            if (template.ToUpper().Contains("UNIT_PRICE_BIDDING_DATA"))
            {
                PRMServices service = new PRMServices();
                Requirement requirement = service.GetRequirementData(reqID, userID, sessionID);
                requirement.AuctionVendors = requirement.AuctionVendors.Where(v => v.CompanyName != "PRICE_CAP" && v.IsQuotationRejected != 1).ToList();
                if (!string.IsNullOrEmpty(vendorIds))
                {
                    requirement.AuctionVendors = requirement.AuctionVendors.Where(vend => vendorIds.Contains(vend.VendorID.ToString())).ToList();
                }
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("UNIT_PRICE_BIDDING_DATA");

                //wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                //wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                //wsSheet1.Cells["C1"].Value = "PRICE";
                //wsSheet1.Cells["D1"].Value = "REVISED_PRICE";
                //wsSheet1.Cells["E1"].Value = "GST";
                //wsSheet1.Cells["F1"].Value = "FREIGHT";
                //wsSheet1.Cells["G1"].Value = "REVISED_FREIGHT";
                //wsSheet1.Cells["H1"].Value = "TOTAL_PRICE";
                //wsSheet1.Cells["I1"].Value = "REVISED_TOTAL_PRICE";
                //wsSheet1.Cells["J1"].Value = "ITEM_ID";
                //wsSheet1.Cells["K1"].Value = "PRODUCT_ID";
                //wsSheet1.Cells["L1"].Value = "PRODUCT_NO";
                //wsSheet1.Cells["M1"].Value = "BRAND";
                //wsSheet1.Cells["N1"].Value = "QUANTITY";
                //wsSheet1.Cells["O1"].Value = "UNITS";
                //wsSheet1.Cells["P1"].Value = "DESCRIPTION";

                //VENDOR_ID VENDOR_NAME PRICE REVISED_PRICE   GST FREIGHT REVISED_FREIGHT TOTAL_PRICE 
                //REVISED_TOTAL_PRICE ITEM_ID PRODUCT_ID 
                //PRODUCT_NO  BRAND QUANTITY    UNITS DESCRIPTION

                //VENDOR_NAME Vendor Rank, Product Name, UNIT,    QUANTITY, Quoted Unit Price,   
                //Quoted GST,  Final Bid Price, Total Quoted Price(inc GST),    Total Bid Price(inc GST),   
                //Savings %, Saving Value, Vendor wise total saving %, Vendor wise total saving Value

                wsSheet1.Cells["A1"].Value = "VENDOR NAME";
                wsSheet1.Cells["B1"].Value = "Vendor Rank";
                wsSheet1.Cells["C1"].Value = "Style Id";
                wsSheet1.Cells["D1"].Value = "Vendor Article Number";
                wsSheet1.Cells["E1"].Value = "Article Name";
                wsSheet1.Cells["F1"].Value = "UNIT";
                wsSheet1.Cells["G1"].Value = "QUANTITY";
                wsSheet1.Cells["H1"].Value = "Quoted Unit Price";
                // wsSheet1.Cells["G1"].Value = "Quoted GST";
                wsSheet1.Cells["I1"].Value = "Final Bid Price";
                wsSheet1.Cells["J1"].Value = "Total Quoted Price";
                wsSheet1.Cells["K1"].Value = "Total Bid Price";
                wsSheet1.Cells["L1"].Value = "Savings %";
                wsSheet1.Cells["M1"].Value = "Saving Value";
                wsSheet1.Cells["N1"].Value = "Item Rank";
                wsSheet1.Cells["O1"].Value = "Vendor wise total saving %";
                wsSheet1.Cells["P1"].Value = "Vendor wise total saving Value";
                //wsSheet1.Cells["O1"].Value = "IT";
                wsSheet1.Cells["Q1"].Value = "Buyer Target Cost";
                wsSheet1.Cells["R1"].Value = "Lead Time";


                var calcColumns = wsSheet1.Cells["A1:R1"];
                wsSheet1.Cells.AutoFitColumns();
                var calcColumnsFont = calcColumns.Style.Font;
                calcColumns.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                //calcColumns.Style.Border.Bottom.Color.SetColor(Color.Red);
                calcColumns.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                calcColumns.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));
                calcColumnsFont.Bold = true;
                //calcColumnsFont.Size = 16;
                calcColumnsFont.Color.SetColor(Color.White);

                //calcColumnsFont.Italic = true;


                wsSheet1.Cells["A:R"].AutoFitColumns();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {
                    Requirement vendorReq = service.GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }


                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    entry.Value.AuctionVendors[0].savingsPercentage = 0;
                    entry.Value.AuctionVendors[0].savings = 0;

                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        //K
                        item.savingsPercentage = Math.Round(
                            (((item.ItemPrice - item.RevItemPrice)
                            / (item.ItemPrice)) * 100),
                            2);
                        //L
                        item.savings = Math.Round((item.ItemPrice - item.RevItemPrice), 2);

                    }

                    //M
                    entry.Value.AuctionVendors[0].savingsPercentage = Math.Round(
                        (((entry.Value.AuctionVendors[0].TotalInitialPrice - entry.Value.AuctionVendors[0].RevVendorTotalPrice)
                        / (entry.Value.AuctionVendors[0].TotalInitialPrice)) * 100),
                        2);

                    //N
                    entry.Value.AuctionVendors[0].savings = Math.Round((entry.Value.AuctionVendors[0].TotalInitialPrice - entry.Value.AuctionVendors[0].RevVendorTotalPrice), 2);

                }




                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    string vendorRemarks = string.Empty;
                    double freightcharges = 0;
                    double revfreightcharges = 0;
                    foreach (var vendor in entry.Value.AuctionVendors)
                    {
                        if (vendor.VendorID == entry.Key)
                        {
                            vendorRemarks = vendor.OtherProperties;
                            freightcharges = vendor.VendorFreight;
                            revfreightcharges = vendor.RevVendorFreight;
                        }
                    }

                    var tfrom = index;
                    var tto = index;

                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        var gst = item.CGst + item.SGst + item.IGst;

                        wsSheet1.Cells["A" + index].Value = entry.Value.AuctionVendors[0].CompanyName;
                        if (tfrom == tto)
                        {
                            wsSheet1.Cells["B" + index].Value = entry.Value.AuctionVendors[0].Rank;
                        }
                        wsSheet1.Cells["C" + index].Value = item.ProductNo;
                        wsSheet1.Cells["D" + index].Value = item.ProductCode;
                        wsSheet1.Cells["E" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["F" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["G" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["H" + index].Value = item.UnitPrice;
                        // wsSheet1.Cells["G" + index].Value = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["I" + index].Value = item.RevUnitPrice;
                        wsSheet1.Cells["J" + index].Value = item.ItemPrice;
                        wsSheet1.Cells["K" + index].Value = item.RevItemPrice;
                        wsSheet1.Cells["L" + index].Value = item.savingsPercentage;
                        wsSheet1.Cells["M" + index].Value = item.savings;
                        wsSheet1.Cells["N" + index].Value = item.ItemRank;
                        wsSheet1.Cells["O" + index].Value = entry.Value.AuctionVendors[0].savingsPercentage;
                        wsSheet1.Cells["P" + index].Value = entry.Value.AuctionVendors[0].savings;
                        //wsSheet1.Cells["O" + index].Value = entry.Value.AuctionVendors[0].TotalInitialPrice;
                        wsSheet1.Cells["Q" + index].Value = item.BuyerTargetCost;
                        wsSheet1.Cells["R" + index].Value = item.LeadTime;


                        //wsSheet1.Cells["A" + index].Value = entry.Key;
                        //wsSheet1.Cells["B" + index].Value = entry.Value.AuctionVendors[0].CompanyName;
                        //wsSheet1.Cells["C" + index].Value = item.UnitPrice;// entry.Value.AuctionVendors[0].RunningPrice;
                        //wsSheet1.Cells["D" + index].Value = item.RevUnitPrice; // entry.Value.AuctionVendors[0].RevVendorTotalPrice;
                        //wsSheet1.Cells["E" + index].Value = item.CGst + item.SGst + item.IGst;
                        //wsSheet1.Cells["F" + index].Value = entry.Value.AuctionVendors[0].VendorFreight;
                        //wsSheet1.Cells["G" + index].Value = entry.Value.AuctionVendors[0].RevVendorFreight;
                        //wsSheet1.Cells["H" + index].Value = entry.Value.AuctionVendors[0].TotalInitialPrice;
                        //wsSheet1.Cells["I" + index].Value = entry.Value.AuctionVendors[0].RevVendorTotalPrice;
                        //wsSheet1.Cells["J" + index].Value = item.ItemID;
                        //wsSheet1.Cells["K" + index].Value = item.ProductIDorName;
                        //wsSheet1.Cells["L" + index].Value = item.ProductNo;
                        //wsSheet1.Cells["M" + index].Value = item.ProductBrand;
                        //wsSheet1.Cells["N" + index].Value = item.ProductQuantity;
                        //wsSheet1.Cells["O" + index].Value = item.ProductQuantityIn;
                        //wsSheet1.Cells["P" + index].Value = item.ProductDescription;



                        wsSheet1.Cells.AutoFitColumns();

                        index++;
                        tto = index;
                    }
                    wsSheet1.Cells["B" + tfrom + ":B" + (tto - 1)].Merge = true;
                    wsSheet1.Cells["B" + tfrom + ":B" + (tto - 1)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    wsSheet1.Cells["B" + tfrom + ":B" + (tto - 1)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    wsSheet1.Cells["B" + tfrom + ":B" + (tto - 1)].Style.Font.Size = 16;

                    wsSheet1.Cells["O" + tfrom + ":O" + (tto - 1)].Merge = true;
                    wsSheet1.Cells["O" + tfrom + ":O" + (tto - 1)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    wsSheet1.Cells["O" + tfrom + ":O" + (tto - 1)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    wsSheet1.Cells["O" + tfrom + ":O" + (tto - 1)].Style.Font.Size = 16;

                    wsSheet1.Cells["P" + tfrom + ":P" + (tto - 1)].Merge = true;
                    wsSheet1.Cells["P" + tfrom + ":P" + (tto - 1)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    wsSheet1.Cells["P" + tfrom + ":P" + (tto - 1)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    wsSheet1.Cells["P" + tfrom + ":P" + (tto - 1)].Style.Font.Size = 16;


                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion

            #region ALL REQ BIDDING DATA

            if (template.ToUpper().Contains("ALL_REQ_BIDDING_DATA"))
            {
                List<AllReqBiddingDetails> details = GetAllReqBiddingDetails(compID, userID,from,to);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("UNIT_PRICE_BIDDING_DATA");

                wsSheet1.Cells["A1"].Value = "RFQ Number";
                wsSheet1.Cells["B1"].Value = "RFQ Posted By";
                wsSheet1.Cells["C1"].Value = "VENDOR NAME";
                wsSheet1.Cells["D1"].Value = "Vendor Rank";
                wsSheet1.Cells["E1"].Value = "Style Id";
                wsSheet1.Cells["F1"].Value = "Vendor Article Number";
                wsSheet1.Cells["G1"].Value = "Article Name";
                wsSheet1.Cells["H1"].Value = "UNIT";
                wsSheet1.Cells["I1"].Value = "QUANTITY";
                wsSheet1.Cells["J1"].Value = "Quoted Unit Price";
                wsSheet1.Cells["K1"].Value = "Final Bid Price";

                wsSheet1.Cells["L1"].Value = "FABRIC_TYPE_F1";
                wsSheet1.Cells["M1"].Value = "MILL_F1";
                wsSheet1.Cells["N1"].Value = "DESIGN_STYLE_F1";
                wsSheet1.Cells["O1"].Value = "COMPOSITION_F1";
                wsSheet1.Cells["P1"].Value = "COUNT_CONSTRUCTION_F1";
                wsSheet1.Cells["Q1"].Value = "WEIGHT_UOM_F1";
                wsSheet1.Cells["R1"].Value = "UOM_F1";
                wsSheet1.Cells["S1"].Value = "CUTTABLE_WIDTH_F1";
                wsSheet1.Cells["T1"].Value = "SHRINKAGE_F1";
                wsSheet1.Cells["U1"].Value = "UNIT_PRICE_F1";
                wsSheet1.Cells["V1"].Value = "CONSUMPTION_F1";
                wsSheet1.Cells["W1"].Value = "DESCRIPTION_F1";

                wsSheet1.Cells["X1"].Value = "FABRIC_TYPE_F2";
                wsSheet1.Cells["Y1"].Value = "MILL_F2";
                wsSheet1.Cells["Z1"].Value = "DESIGN_STYLE_F2";
                wsSheet1.Cells["AA1"].Value = "COMPOSITION_F2";
                wsSheet1.Cells["AB1"].Value = "COUNT_CONSTRUCTION_F2";
                wsSheet1.Cells["AC1"].Value = "WEIGHT_UOM_F2";
                wsSheet1.Cells["AD1"].Value = "UOM_F2";
                wsSheet1.Cells["AE1"].Value = "CUTTABLE_WIDTH_F2";
                wsSheet1.Cells["AF1"].Value = "SHRINKAGE_F2";
                wsSheet1.Cells["AG1"].Value = "UNIT_PRICE_F2";
                wsSheet1.Cells["AH1"].Value = "CONSUMPTION_F2";
                wsSheet1.Cells["AI1"].Value = "DESCRIPTION_F2";

                wsSheet1.Cells["AJ1"].Value = "FABRIC_TYPE_F3";
                wsSheet1.Cells["AK1"].Value = "MILL_F3";
                wsSheet1.Cells["AL1"].Value = "DESIGN_STYLE_F3";
                wsSheet1.Cells["AM1"].Value = "COMPOSITION_F3";
                wsSheet1.Cells["AN1"].Value = "COUNT_CONSTRUCTION_F3";
                wsSheet1.Cells["AO1"].Value = "WEIGHT_UOM_F3";
                wsSheet1.Cells["AP1"].Value = "UOM_F3";
                wsSheet1.Cells["AQ1"].Value = "CUTTABLE_WIDTH_F3";
                wsSheet1.Cells["AR1"].Value = "SHRINKAGE_F3";
                wsSheet1.Cells["AS1"].Value = "UNIT_PRICE_F3";
                wsSheet1.Cells["AT1"].Value = "CONSUMPTION_F3";
                wsSheet1.Cells["AU1"].Value = "DESCRIPTION_F3";

                wsSheet1.Cells["AV1"].Value = "SUNDRY_TRIMS_NAME_T1";
                wsSheet1.Cells["AW1"].Value = "UNIT_PRICE_T1";
                wsSheet1.Cells["AX1"].Value = "CONSUMPTION_T1";
                wsSheet1.Cells["AY1"].Value = "UOM_T1";

                wsSheet1.Cells["AZ1"].Value = "SUNDRY_TRIMS_NAME_T2";
                wsSheet1.Cells["BA1"].Value = "UNIT_PRICE_T2";
                wsSheet1.Cells["BB1"].Value = "CONSUMPTION_T2";
                wsSheet1.Cells["BC1"].Value = "UOM_T2";

                wsSheet1.Cells["BD1"].Value = "SUNDRY_TRIMS_NAME_T3";
                wsSheet1.Cells["BE1"].Value = "UNIT_PRICE_T3";
                wsSheet1.Cells["BF1"].Value = "CONSUMPTION_T3";
                wsSheet1.Cells["BG1"].Value = "UOM_T3";

                wsSheet1.Cells["BH1"].Value = "Total Quoted Price";
                wsSheet1.Cells["BI1"].Value = "Total Bid Price";
                wsSheet1.Cells["BJ1"].Value = "Savings %";
                wsSheet1.Cells["BK1"].Value = "Saving Value";
                wsSheet1.Cells["BL1"].Value = "Item Rank";
                wsSheet1.Cells["BM1"].Value = "Vendor wise total saving %";
                wsSheet1.Cells["BN1"].Value = "Vendor wise total saving Value";
                wsSheet1.Cells["BO1"].Value = "Buyer Target Cost";
                wsSheet1.Cells["BP1"].Value = "Lead Time";
                wsSheet1.Cells["BQ1"].Value = "Article Type";
                wsSheet1.Cells["BR1"].Value = "Brand Name";
                wsSheet1.Cells["BS1"].Value = "Season";

                wsSheet1.Cells["A:BS"].AutoFitColumns();
                wsSheet1.Cells["A1:BS1"].Style.Font.Bold = true;
                wsSheet1.Cells["A1:BS1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A1:BS1"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(119, 136, 153));
                wsSheet1.Cells["A1:BS1"].Style.Font.Color.SetColor(Color.White);
                wsSheet1.View.FreezePanes(2, 1);

                int lineNumber = 2;
                foreach (AllReqBiddingDetails item in details)
                {
                    wsSheet1.Cells["A" + lineNumber].Value = item.REQ_ID;
                    wsSheet1.Cells["B" + lineNumber].Value = item.REQ_POSTED_BY;
                    wsSheet1.Cells["C" + lineNumber].Value = item.VENDOR_NAME;
                    wsSheet1.Cells["D" + lineNumber].Value = item.VENDOR_RANK;
                    wsSheet1.Cells["E" + lineNumber].Value = item.STYLE_ID;
                    wsSheet1.Cells["F" + lineNumber].Value = item.ARTICLE_NUMBER;
                    wsSheet1.Cells["G" + lineNumber].Value = item.ARTICLE_NAME;
                    wsSheet1.Cells["H" + lineNumber].Value = item.QUANTITY_IN;
                    wsSheet1.Cells["I" + lineNumber].Value = item.QUANTITY;
                    wsSheet1.Cells["J" + lineNumber].Value = item.QUOTED_UNIT_PRICE;
                    wsSheet1.Cells["K" + lineNumber].Value = item.FINAL_BID_PRICE;


                    for (int f = 1; f <= 3; f++)
                    {

                        if (item.ApparelFabric != null && item.ApparelFabric.Count > 0)
                        {
                            ApparelFabric fab = new ApparelFabric();
                            fab = item.ApparelFabric[0];
                            wsSheet1.Cells["L" + lineNumber].Value = fab.NAME;
                            wsSheet1.Cells["M" + lineNumber].Value = fab.MILL;
                            wsSheet1.Cells["N" + lineNumber].Value = fab.DESIGN_STYLE;
                            wsSheet1.Cells["O" + lineNumber].Value = fab.COMPOSITION;
                            wsSheet1.Cells["P" + lineNumber].Value = fab.COUNT + fab.CONSTRUCTION;
                            wsSheet1.Cells["Q" + lineNumber].Value = fab.WEIGHT_UOM;
                            wsSheet1.Cells["R" + lineNumber].Value = fab.UOM;
                            wsSheet1.Cells["S" + lineNumber].Value = fab.CUTTABLE_WIDTH;
                            wsSheet1.Cells["T" + lineNumber].Value = fab.SHRINKAGE;
                            wsSheet1.Cells["U" + lineNumber].Value = fab.REV_UNIT_PRICE;
                            wsSheet1.Cells["V" + lineNumber].Value = fab.REV_CONSUMPTION;
                            wsSheet1.Cells["W" + lineNumber].Value = fab.DESCRIPTION;
                        }
                        if (item.ApparelFabric != null && item.ApparelFabric.Count > 1)
                        {
                            ApparelFabric fab = new ApparelFabric();
                            fab = item.ApparelFabric[1];
                            wsSheet1.Cells["X" + lineNumber].Value  = fab.NAME;
                            wsSheet1.Cells["Y" + lineNumber].Value  = fab.MILL;
                            wsSheet1.Cells["Z" + lineNumber].Value  = fab.DESIGN_STYLE;
                            wsSheet1.Cells["AA" + lineNumber].Value = fab.COMPOSITION;
                            wsSheet1.Cells["AB" + lineNumber].Value = fab.COUNT + fab.CONSTRUCTION;
                            wsSheet1.Cells["AC" + lineNumber].Value = fab.WEIGHT_UOM;
                            wsSheet1.Cells["AD" + lineNumber].Value = fab.UOM;
                            wsSheet1.Cells["AE" + lineNumber].Value = fab.CUTTABLE_WIDTH;
                            wsSheet1.Cells["AF" + lineNumber].Value = fab.SHRINKAGE;
                            wsSheet1.Cells["AG" + lineNumber].Value = fab.REV_UNIT_PRICE;
                            wsSheet1.Cells["AH" + lineNumber].Value = fab.REV_CONSUMPTION;
                            wsSheet1.Cells["AI" + lineNumber].Value = fab.DESCRIPTION;
                        }
                        if (item.ApparelFabric != null && item.ApparelFabric.Count > 2)
                        {
                            ApparelFabric fab = new ApparelFabric();
                            fab = item.ApparelFabric[2];
                            wsSheet1.Cells["AJ" + lineNumber].Value= fab.NAME;
                            wsSheet1.Cells["AK" + lineNumber].Value= fab.MILL;
                            wsSheet1.Cells["AL" + lineNumber].Value= fab.DESIGN_STYLE;
                            wsSheet1.Cells["AM" + lineNumber].Value = fab.COMPOSITION;
                            wsSheet1.Cells["AN" + lineNumber].Value = fab.COUNT + fab.CONSTRUCTION;
                            wsSheet1.Cells["AO" + lineNumber].Value = fab.WEIGHT_UOM;
                            wsSheet1.Cells["AP" + lineNumber].Value = fab.UOM;
                            wsSheet1.Cells["AQ" + lineNumber].Value = fab.CUTTABLE_WIDTH;
                            wsSheet1.Cells["AR" + lineNumber].Value = fab.SHRINKAGE;
                            wsSheet1.Cells["AS" + lineNumber].Value = fab.REV_UNIT_PRICE;
                            wsSheet1.Cells["AT" + lineNumber].Value = fab.REV_CONSUMPTION;
                            wsSheet1.Cells["AU" + lineNumber].Value = fab.DESCRIPTION;
                        }

                    }
                    for (int t = 1; t <= 3; t++)
                    {
                        if (item.ApparelSundries != null && item.ApparelSundries.Count > 0)
                        {
                            ApparelSundries fab = new ApparelSundries();
                            fab = item.ApparelSundries[0];
                            wsSheet1.Cells["AV" + lineNumber].Value = fab.SEWING_NAME;
                            wsSheet1.Cells["AW" + lineNumber].Value = fab.REV_SEWING_COST;
                            wsSheet1.Cells["AX" + lineNumber].Value = fab.REV_SEWING_CONSUMPTION;
                            wsSheet1.Cells["AY" + lineNumber].Value = fab.UOM;
                        }

                        if (item.ApparelSundries != null && item.ApparelSundries.Count > 1)
                        {
                            ApparelSundries fab = new ApparelSundries();
                            fab = item.ApparelSundries[1];
                            wsSheet1.Cells["AZ" + lineNumber].Value = fab.SEWING_NAME;
                            wsSheet1.Cells["BA" + lineNumber].Value = fab.REV_SEWING_COST;
                            wsSheet1.Cells["BB" + lineNumber].Value = fab.REV_SEWING_CONSUMPTION;
                            wsSheet1.Cells["BC" + lineNumber].Value = fab.UOM;
                        }

                        if (item.ApparelSundries != null && item.ApparelSundries.Count > 2)
                        {
                            ApparelSundries fab = new ApparelSundries();
                            fab = item.ApparelSundries[2];
                            wsSheet1.Cells["BD" + lineNumber].Value = fab.SEWING_NAME;
                            wsSheet1.Cells["BE" + lineNumber].Value = fab.REV_SEWING_COST;
                            wsSheet1.Cells["BF" + lineNumber].Value = fab.REV_SEWING_CONSUMPTION;
                            wsSheet1.Cells["BG" + lineNumber].Value = fab.UOM;
                        }
                    }
                    wsSheet1.Cells["BH" + lineNumber].Value = item.TOTAL_QUOTED_PRICE;
                    wsSheet1.Cells["BI" + lineNumber].Value = item.TOTAL_BID_PRICE;
                    wsSheet1.Cells["BJ" + lineNumber].Value = item.SAVINGS_PERCENTAGE;
                    wsSheet1.Cells["BK" + lineNumber].Value = item.ITEM_SAVINGS_VALUE;
                    wsSheet1.Cells["BL" + lineNumber].Value = item.ITEM_RANK;
                    wsSheet1.Cells["BM" + lineNumber].Value = item.VENDOR_SAVINGS_PERCENTAGE;
                    wsSheet1.Cells["BN" + lineNumber].Value = item.VENDOR_SAVINGS_VALUE;
                    wsSheet1.Cells["BO" + lineNumber].Value = item.BUYER_TARGET_COST;
                    wsSheet1.Cells["BP" + lineNumber].Value = item.LEAD_TIME;
                    wsSheet1.Cells["BQ" + lineNumber].Value = item.ARTICLE_TYPE;
                    wsSheet1.Cells["BR" + lineNumber].Value = item.BRAND;
                    wsSheet1.Cells["BS" + lineNumber].Value = item.SEASON;
                    lineNumber++;
                }
                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }

                if (sendEmail)
                {
                    var toEmails = new List<string>();
                    var ccEmails = new List<string>();

                    System.Data.DataTable emailsDT = sqlHelper.SelectQuery("SELECT * FROM parameters WHERE PARAM_CODE = 'ALL_REQ_BIDDING_DATA'");
                    if (emailsDT != null && emailsDT.Rows.Count > 0)
                    {
                        foreach (var row in emailsDT.AsEnumerable())
                        {
                            var email = row["PARAM_VALUE"] != DBNull.Value ? Convert.ToString(row["PARAM_VALUE"]) : "";
                            var type = row["PARAM_COMMENT"] != DBNull.Value ? Convert.ToString(row["PARAM_COMMENT"]) : "";
                            if (!string.IsNullOrEmpty(email))
                            {
                                if (!string.IsNullOrEmpty(type) && type.Equals("CC"))
                                {
                                    ccEmails.Add(email);
                                }
                                else
                                {
                                    toEmails.Add(email);
                                }
                            }
                        }

                        if (toEmails.Count > 0)
                        {
                            string toEmailsStr = string.Join(",", toEmails);
                            string ccEmailsStr = ccEmails.Count > 0 ? string.Join(",", ccEmails) : "";
                            ms.Position = 0;
                            System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(ms, "BiddingReport.xlsx", "application/vnd.ms-excel");
                            prmservices.SendSMTPEmailSimple(toEmailsStr, ccEmailsStr, "Weekly bidding report - " + DateTime.Now.ToString("yyyy-MM-dd"), "Please find attached weekly bidding report", attachment, null);
                        }
                    }                   
                }
            }

            #endregion ALL REQ BIDDING DATA

            #endregion

            #region bulk margin quotations
            if (template.ToUpper().Contains("BULK_MARGIN_VEND_QUOTATION"))
            {
                PRMServices service = new PRMServices();
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("BULK_MARGIN_VEND_QUOTATION");
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["C1"].Value = "HSN_CODE";
                wsSheet1.Cells["D1"].Value = "BRAND";
                wsSheet1.Cells["E1"].Value = "QUANTITY";
                wsSheet1.Cells["F1"].Value = "COST_PRICE";
                wsSheet1.Cells["G1"].Value = "GST";
                wsSheet1.Cells["H1"].Value = "MRP";
                wsSheet1.Cells["I1"].Value = "NET_PRICE";
                wsSheet1.Cells["J1"].Value = "MARGIN_AMOUNT";
                wsSheet1.Cells["K1"].Value = "MARGIN_PERC";
                wsSheet1.Cells["L1"].Value = "ITEM_ID";
                wsSheet1.Cells["M1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["N1"].Value = "PRODUCT_NAME";
                wsSheet1.Cells["O1"].Value = "UNITS";
                wsSheet1.Cells["P1"].Value = "VENDOR_REMARK";
                wsSheet1.Cells["Q1"].Value = "REQ_ID";
                wsSheet1.Cells["R1"].Value = "TITLE";
                wsSheet1.Cells["S1"].Value = "POSTED_DATE";
                wsSheet1.Cells["T1"].Value = "MANUFACTURER_NAME";
                wsSheet1.Cells["U1"].Value = "QUANTITY_ORIG";
                wsSheet1.Cells["V1"].Value = "QUOTATION_STATUS";
                #region excelStyling
                wsSheet1.Cells["K2:K6000"].Style.Numberformat.Format = "0.00000000";
                wsSheet1.Column(1).Hidden = true;
                wsSheet1.Column(12).Hidden = true;
                wsSheet1.Column(13).Hidden = true;
                wsSheet1.Column(21).Hidden = true;
                wsSheet1.Cells["A:K"].AutoFitColumns();
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#F2DCDB");
                wsSheet1.Cells["I:I"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["I:I"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["J:J"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["J:J"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["K:K"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["K:K"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                var calcColumns = wsSheet1.Cells["I:K"];
                var calcColumnsFont = calcColumns.Style.Font;
                calcColumnsFont.Bold = true;
                calcColumnsFont.Italic = true;
                #endregion excelStyling

                wsSheet1.Cells["I2:I6000"].Formula = "IF(A2 > 0, F2*(1+(G2/100)), \"\")";
                wsSheet1.Cells["J2:J6000"].Formula = "IF(A2 > 0, H2 - I2, \"\")";
                wsSheet1.Cells["K2:K6000"].Formula = "IFERROR(J2/I2*100,\"\")";
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                int index = 2;
                List<Requirement> requirementList = service.GetVendorRequirements(userID, 14, sessionID);
                foreach (Requirement req in requirementList)
                {
                    if (req.AuctionVendors.Count == 0)
                    {
                        continue;
                    }
                    if (req.PostedOn >= DateTime.UtcNow.AddMonths(-1) && req.IsDiscountQuotation == 2 && (req.StartTime == null || req.StartTime > DateTime.UtcNow))
                    {
                        if(req.RequirementID > 0 && req.PostedOn >= DateTime.UtcNow.AddMonths(-1))
                        {
                            foreach (var item in req.ListRequirementItems)
                            {
                                var gst = item.CGst + item.SGst + item.IGst;
                                wsSheet1.Cells["A" + index].Value = userID;
                                wsSheet1.Cells["B" + index].Value = req.AuctionVendors[0].CompanyName;
                                wsSheet1.Cells["L" + index].Value = item.ItemID;
                                wsSheet1.Cells["C" + index].Value = item.ProductNo;
                                wsSheet1.Cells["D" + index].Value = item.ProductBrand;
                                wsSheet1.Cells["E" + index].Value = item.ProductQuantity;
                                wsSheet1.Cells["H" + index].Value = item.UnitMRP;
                                wsSheet1.Cells["G" + index].Value = item.CGst + item.SGst + item.IGst;
                                wsSheet1.Cells["M" + index].Value = 0;
                                wsSheet1.Cells["N" + index].Value = item.ProductIDorName;
                                wsSheet1.Cells["O" + index].Value = item.ProductQuantityIn;
                                wsSheet1.Cells["P" + index].Value = req.AuctionVendors[0].OtherProperties;
                                wsSheet1.Cells["Q" + index].Value = req.RequirementID;
                                wsSheet1.Cells["R" + index].Value = req.Title;
                                wsSheet1.Cells["S" + index].Value = req.PostedOn.ToString(); ;
                                wsSheet1.Cells["T" + index].Value = item.OthersBrands;
                                wsSheet1.Cells["U" + index].Value = item.ProductQuantity;
                                wsSheet1.Cells["V" + index].Value = req.AuctionVendors[0].IsQuotationRejected == -1 ? "Pending/Not Uploaded" : (req.AuctionVendors[0].IsQuotationRejected == 0 ? "Approved" : "Rejected");
                                var costPrice = (item.UnitMRP * 100 * 100) / ((item.UnitDiscount * 100) + 10000 + (item.UnitDiscount * gst) + (gst * 100));
                                wsSheet1.Cells["F" + index].Value = Math.Round(costPrice, 2);
                                index++;
                            }
                        }
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion

            #region bulk margin bidding
            if (template.ToUpper().Contains("BULK_MARGIN_VEND_BIDDING"))
            {
                PRMServices service = new PRMServices();
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("BULK_MARGIN_VEND_BIDDING");
                wsSheet1.Cells["A1"].Value = "REQ_ID";
                wsSheet1.Cells["B1"].Value = "REQ_TITLE";
                wsSheet1.Cells["C1"].Value = "POSTED_DATE";
                wsSheet1.Cells["D1"].Value = "NEGOTIATION_DATE";
                wsSheet1.Cells["E1"].Value = "VENDOR_ID";
                wsSheet1.Cells["F1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["G1"].Value = "HSN_CODE";
                wsSheet1.Cells["H1"].Value = "MANUFACTURER_NAME";
                wsSheet1.Cells["I1"].Value = "BRAND";
                wsSheet1.Cells["J1"].Value = "QUANTITY";
                wsSheet1.Cells["K1"].Value = "COST_PRICE";
                wsSheet1.Cells["L1"].Value = "REV_COST_PRICE";
                wsSheet1.Cells["M1"].Value = "DIFFERENCE";
                wsSheet1.Cells["N1"].Value = "GST";
                wsSheet1.Cells["O1"].Value = "MRP";
                wsSheet1.Cells["P1"].Value = "REV_NET_PRICE";
                wsSheet1.Cells["Q1"].Value = "REV_MARGIN_AMOUNT";
                wsSheet1.Cells["R1"].Value = "REV_MARGIN_PERC";
                wsSheet1.Cells["S1"].Value = "ITEM_ID";
                wsSheet1.Cells["T1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["U1"].Value = "TOTAL_INIT_PRICE";
                wsSheet1.Cells["V1"].Value = "REV_TOTAL_PRICE";
                wsSheet1.Cells["W1"].Value = "PRODUCT_NAME";
                wsSheet1.Cells["X1"].Value = "UNITS";
                wsSheet1.Cells["Y1"].Value = "VENDOR_REMARK";
                wsSheet1.Cells["Z1"].Value = "RANK";
                wsSheet1.Cells["AA1"].Value = "REV_COST_PRICE_ORIG";

                #region excelStyling                

                wsSheet1.Cells["R2:R6000"].Style.Numberformat.Format = "0.00000000";
                wsSheet1.Cells["A:AA"].AutoFitColumns();
                wsSheet1.Column(10).Hidden = true;
                wsSheet1.Column(11).Hidden = true;
                wsSheet1.Column(14).Hidden = true;
                wsSheet1.Column(15).Hidden = true;
                wsSheet1.Column(21).Hidden = true;
                wsSheet1.Column(27).Hidden = true;
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#F2DCDB");
                Color colFromHex1 = System.Drawing.ColorTranslator.FromHtml("#DAF7A6");
                wsSheet1.Cells["P:P"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["P:P"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["Q:Q"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["Q:Q"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["R:R"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["R:R"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["L:L"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["L:L"].Style.Fill.BackgroundColor.SetColor(colFromHex1);
                var calcColumns = wsSheet1.Cells["P:R"];
                var calcColumnsFont = calcColumns.Style.Font;
                calcColumnsFont.Bold = true;
                calcColumnsFont.Italic = true;
                #endregion excelStyling
                
                wsSheet1.Cells["M2:M6000"].Formula = "IF(E2 > 0, ABS(K2 - L2), \"\")";
                wsSheet1.Cells["P2:P6000"].Formula = "IF(E2 > 0, L2*(1+(N2/100)), \"\")";
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                List<Requirement> requirementList = service.GetVendorRequirements(userID, 1, sessionID);
                int index = 2;
                foreach (Requirement req in requirementList)
                {
                    if(req.AuctionVendors.Count > 0 && (req.AuctionVendors[0].IsQuotationRejected == 1 || req.AuctionVendors[0].IsRevQuotationRejected == 1))
                    {
                        continue;
                    }
                    if (req.PostedOn >= DateTime.UtcNow.AddMonths(-1) && req.IsDiscountQuotation == 2 && (req.StartTime < DateTime.UtcNow && req.EndTime > DateTime.UtcNow))
                    {
                        if(req.AuctionVendors == null)
                        {
                            continue;
                        }
                        foreach (var item in req.ListRequirementItems)
                        {
                            var gst = item.CGst + item.SGst + item.IGst;
                            var revCostPrice = (item.UnitMRP * 100 * 100) / ((item.RevUnitDiscount * 100) + 10000 + (item.RevUnitDiscount * gst) + (gst * 100));
                            wsSheet1.Cells["A" + index].Value = req.RequirementID; // entry.Key;
                            wsSheet1.Cells["B" + index].Value = req.Title;  // entry.Value.AuctionVendors[0].CompanyName;
                            wsSheet1.Cells["C" + index].Value = req.PostedOn.ToString();  // entry.Value.AuctionVendors[0].CompanyName;
                            wsSheet1.Cells["D" + index].Value = req.StartTime.ToString();  // entry.Value.AuctionVendors[0].CompanyName;
                            wsSheet1.Cells["E" + index].Value = userID;  // entry.Value.AuctionVendors[0].CompanyName;
                            wsSheet1.Cells["F" + index].Value = req.AuctionVendors[0].CompanyName;  // entry.Value.AuctionVendors[0].CompanyName;

                            wsSheet1.Cells["G" + index].Value = item.ProductNo;
                            wsSheet1.Cells["H" + index].Value = item.OthersBrands;
                            wsSheet1.Cells["I" + index].Value = item.ProductBrand;
                            wsSheet1.Cells["J" + index].Value = item.ProductQuantity;
                            wsSheet1.Cells["L" + index].Value = Math.Round(revCostPrice, 2);

                            wsSheet1.Cells["N" + index].Value = item.CGst + item.SGst + item.IGst;
                            wsSheet1.Cells["O" + index].Value = item.UnitMRP;
                            wsSheet1.Cells["S" + index].Value = item.ItemID;
                            wsSheet1.Cells["T" + index].Value = 0;
                            wsSheet1.Cells["U" + index].Value = req.AuctionVendors[0].TotalInitialPrice;
                            wsSheet1.Cells["V" + index].Value = req.AuctionVendors[0].RevVendorTotalPrice;
                            wsSheet1.Cells["W" + index].Value = item.ProductIDorName;
                            wsSheet1.Cells["X" + index].Value = item.ProductQuantityIn;
                            wsSheet1.Cells["Y" + index].Value = req.AuctionVendors[0].OtherProperties;
                            wsSheet1.Cells["Z" + index].Value = req.AuctionVendors[0].Rank > 0 ? req.AuctionVendors[0].Rank.ToString() : "NA";
                            wsSheet1.Cells["AA" + index].Value = Math.Round(revCostPrice, 2);

                            if (req.IsRevUnitDiscountEnable == 0)
                            {
                                wsSheet1.Cells["Q" + index].Value = 0;
                                wsSheet1.Cells["R" + index].Value = 0;
                            }
                            if (req.IsRevUnitDiscountEnable == 1)
                            {
                                wsSheet1.Cells["Q" + index].Formula = "IF(E" + index + " > 0, ABS(O" + index + " - P" + index + "), \"\")";
                                wsSheet1.Cells["R" + index].Formula = "IFERROR(Q" + index + "/P" + index + "*100,\"\")";
                            }

                            var costPrice = (item.UnitMRP * 100 * 100) / ((item.UnitDiscount * 100) + 10000 + (item.UnitDiscount * gst) + (gst * 100));
                            wsSheet1.Cells["K" + index].Value = Math.Round(costPrice, 2);
                            index++;
                        }
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion

            #region REQUIREMENT_SAVE
            if (template.ToUpper().Contains("REQUIREMENT_SAVE"))
            {
                PRMCustomFieldService fieldService = new PRMCustomFieldService();
                Requirement requirement = new Requirement();
                if (reqID > 0)
                {
                    requirement = prmservices.GetRequirementData(reqID, userID, sessionID);
                }

                PRMCatalogService catalogService = new PRMCatalogService();
                var companyUnits = prmservices.GetCompanyConfiguration(compID, "ITEM_UNITS", sessionID);
                List<Product> products = catalogService.GetProducts(compID, sessionID);

                List<string> seasons = new List<string>(new string[] { "SS", "AW" });
                List<string> months = new List<string>(new string[] { "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEPT","OCT","NOV","DEC" });

                products = products.Where(p => (p.IsValid > 0 && !string.IsNullOrEmpty(p.ProductCode))).ToList();
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("REQUIREMENT_SAVE");
                ExcelWorksheet wsSheet2 = ExcelPkg.Workbook.Worksheets.Add("REQUIREMENT_SAVE_CONFIG");
                wsSheet1.Cells["A1"].Value = "BRAND";
                wsSheet1.Cells["B1"].Value = "GENDER";
                wsSheet1.Cells["C1"].Value = "ARTICLE_TYPE";
                wsSheet1.Cells["D1"].Value = "ARTICLE_NUMBER";
                wsSheet1.Cells["E1"].Value = "ARTICLE_NAME";
                wsSheet1.Cells["F1"].Value = "STYLE_ID";
               // wsSheet1.Cells["G1"].Value = "UOM";
                wsSheet1.Cells["G1"].Value = "QUANTITY";
                wsSheet1.Cells["H1"].Value = "PRODUCT_DESCRIPTION";
                wsSheet1.Cells["I1"].Value = "SEASON";
                wsSheet1.Cells["J1"].Value = "MONTH";
                wsSheet1.Cells["K1"].Value = "BUYER_TARGET_COST";
                wsSheet1.Cells["L1"].Value = "HSN_CODE";
                wsSheet1.Cells["M1"].Value = "FABRIC_PRICE";
                wsSheet1.Cells["N1"].Value = "FABRIC_QUALITY";

                wsSheet1.Cells["A:N"].AutoFitColumns();
                //var validation = wsSheet1.DataValidations.AddListValidation($"G2:G{maxRows}");
                //validation.ShowErrorMessage = true;
                //validation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                //validation.ErrorTitle = "Invalid Units";
                //validation.Error = "Invalid Units Selected";

                var quantityPriceValidation = wsSheet1.DataValidations.AddDecimalValidation($"G2:G{maxRows}");
                quantityPriceValidation.ShowErrorMessage = true;
                quantityPriceValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                quantityPriceValidation.ErrorTitle = "Invalid Quantity";
                quantityPriceValidation.Error = "Please enter value minimum 150.";
                quantityPriceValidation.Operator = ExcelDataValidationOperator.greaterThanOrEqual;
                quantityPriceValidation.Formula.Value = 150;

                var targetCostValidation = wsSheet1.DataValidations.AddDecimalValidation($"K2:K{maxRows}");
                targetCostValidation.ShowErrorMessage = true;
                targetCostValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                targetCostValidation.ErrorTitle = "The value you entered is not valid";
                targetCostValidation.Error = "This cell must be a valid positive number.";
                targetCostValidation.Operator = ExcelDataValidationOperator.greaterThanOrEqual;
                targetCostValidation.Formula.Value = 0D;


                var fabricPriceValidation = wsSheet1.DataValidations.AddDecimalValidation($"M2:M{maxRows}");
                fabricPriceValidation.ShowErrorMessage = true;
                fabricPriceValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                fabricPriceValidation.ErrorTitle = "The value you entered is not valid";
                fabricPriceValidation.Error = "This cell must be a valid positive number.";
                fabricPriceValidation.Operator = ExcelDataValidationOperator.greaterThanOrEqual;
                fabricPriceValidation.Formula.Value = 0D;


                wsSheet1.Cells[$"A2:A{maxRows}"].Style.Numberformat.Format = "@";

                //var validation = wsSheet1.DataValidations.AddListValidation($"I2:I{maxRows}");
                //validation.ShowErrorMessage = true;
                //validation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                //validation.ErrorTitle = "Invalid Season";
                //validation.Error = "Invalid season selected";
                //foreach (var s in seasons)
                //{
                //    validation.Formula.Values.Add(s);
                //}

                //var validation1 = wsSheet1.DataValidations.AddListValidation($"J2:J{maxRows}");
                //validation1.ShowErrorMessage = true;
                //validation1.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                //validation1.ErrorTitle = "Invalid Month";
                //validation1.Error = "Invalid month selected";
                //foreach (var m in months)
                //{
                //    validation1.Formula.Values.Add(m);
                //}

                int count = 1;
                foreach (var unit in companyUnits)
                {
                    wsSheet2.Cells["A" + count.ToString()].Value = unit.ConfigValue;
                    count++;
                }

               // validation.Formula.ExcelFormula = "REQUIREMENT_SAVE_CONFIG!$A1:$A" + (count - 1).ToString();

                //for (int i = 2; i <= maxRows; i++)
                //{
                //    wsSheet1.Cells["B" + (i)].Formula = $"IFNA(INDIRECT(\"REQUIREMENT_SAVE_CONFIG!C\" & MATCH(A{i}, REQUIREMENT_SAVE_CONFIG!$B:$B,0 )),\"\")";
                //    wsSheet1.Cells["C" + (i)].Formula = $"IFNA(INDIRECT(\"REQUIREMENT_SAVE_CONFIG!D\" & MATCH(A{i}, REQUIREMENT_SAVE_CONFIG!$B:$B,0 )),\"\")";
                //}

                if (requirement != null && requirement.ListRequirementItems != null && requirement.ListRequirementItems.Count > 0)
                {
                    int index = 2;
                    foreach (var item in requirement.ListRequirementItems)
                    {
                        if (item.IsCoreProductCategory <= 0)
                        {
                            continue;
                        }

                        wsSheet1.Cells["A" + index].Value = item.BRANDS;
                        wsSheet1.Cells["B" + index].Value = item.GENDER;
                        wsSheet1.Cells["C" + index].Value = item.ARTICLE_TYPE;
                        wsSheet1.Cells["D" + index].Value = item.ProductCode;
                        wsSheet1.Cells["E" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["F" + index].Value = item.ProductNo;
                       // wsSheet1.Cells["G" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["G" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["H" + index].Value = item.ProductDescription;
                        wsSheet1.Cells["I" + index].Value = item.SEASON;
                        wsSheet1.Cells["J" + index].Value = item.BUY_PLAN_MONTH;
                        wsSheet1.Cells["K" + index].Value = item.BuyerTargetCost;
                        wsSheet1.Cells["L" + index].Value = item.HsnCode;
                        wsSheet1.Cells["M" + index].Value = item.FabricPrice;
                        wsSheet1.Cells["N" + index].Value = item.Fabricquality;

                        index++;
                    }
                }


                //var productValidation = wsSheet1.DataValidations.AddListValidation($"A2:A{maxRows}");
                //productValidation.ShowErrorMessage = true;
                //productValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                //productValidation.ErrorTitle = "Invalid Product";
                //productValidation.Error = "Invalid Product entered";



                count = 1;
                foreach (var product in products)
                {
                    wsSheet2.Cells["B" + count.ToString()].Value = product.ProductCode;
                    wsSheet2.Cells["C" + count.ToString()].Value = product.ProductName;
                    wsSheet2.Cells["D" + count.ToString()].Value = product.ProductNo;
                    count++;
                }

                // productValidation.Formula.ExcelFormula = "=REQUIREMENT_SAVE_CONFIG!$B1:$B" + (count - 1).ToString();


                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                wsSheet2.Protection.IsProtected = false;
                wsSheet2.Protection.AllowSelectLockedCells = false;
                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion

            #region EXCEL_VENDOR_QUOTATION

            if (template.ToUpper().Contains("VENDOR_QUOTATION"))
            {
                Requirement excelRequirement = prmservices.GetRequirementData(reqID, userID, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("VENDOR_QUOTATION_"+ reqID);
                int itemsLength = excelRequirement.ListRequirementItems.Count;

                List<string> isregretvalues = new List<string>(new string[] { "FALSE", "TRUE" });
                List<string> regretComments = new List<string>(new string[] { "Capacity Constraints", "Quality Constraints", "OTIF ( Ontime inward fulfillment )", "Working Capital", "Phased out Vendors", "Price issues",
                "Lead time"});

                #region AppSetting
                var fabricTypeParamsTemp = ConfigurationManager.AppSettings["FabricTypeParams"].ToString();
                string[] fabricTypeParams = fabricTypeParamsTemp.Split(',');

                var processCategoryTemp = ConfigurationManager.AppSettings["ProcessCategoryParams"].ToString();
                string[] processCategoryParams = processCategoryTemp.Split(',');

                var uomParamsTemp = ConfigurationManager.AppSettings["UOMParams"].ToString();
                string[] UOMParams = uomParamsTemp.Split(',');

                #endregion AppSetting

                //wsSheet1.Cells["A1:L1"].Value = "-";
                // wsSheet1.Cells["A1:L1"].Merge = true; 242, 242, 242
                wsSheet1.Cells["A1:DF1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A1:DF1"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(242, 242, 242));

                wsSheet1.Cells["M1:P1"].Value = "Mandatory";
                wsSheet1.Cells["M1:P1"].Merge = true;
                wsSheet1.Cells["M1:P1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Cells["M1:P1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells["M1:P1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["M1:P1"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 110, 84));
                wsSheet1.Cells["M1:P1"].Style.Font.Color.SetColor(Color.Black);
                wsSheet1.Cells["M1:P1"].Style.Locked = true;

                wsSheet1.Cells["AZ1:BK1"].Value = "Mandatory";
                wsSheet1.Cells["AZ1:BK1"].Merge = true;
                wsSheet1.Cells["AZ1:BK1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Cells["AZ1:BK1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells["AZ1:BK1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["AZ1:BK1"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 110, 84));
                wsSheet1.Cells["AZ1:BK1"].Style.Font.Color.SetColor(Color.Black);
                wsSheet1.Cells["AZ1:BK1"].Style.Locked = true;

                wsSheet1.Cells["CJ1:CM1"].Value = "Mandatory";
                wsSheet1.Cells["CJ1:CM1"].Merge = true;
                wsSheet1.Cells["CJ1:CM1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Cells["CJ1:CM1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells["CJ1:CM1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["CJ1:CM1"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 110, 84));
                wsSheet1.Cells["CJ1:CM1"].Style.Font.Color.SetColor(Color.Black);
                wsSheet1.Cells["CJ1:CM1"].Style.Locked = true;


                wsSheet1.Cells["DA1:DD1"].Value = "If IsRegret is 'TRUE', Regret comments are mandatory";
                wsSheet1.Cells["DA1:DD1"].Merge = true;
                wsSheet1.Cells["DA1:DD1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Cells["DA1:DD1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells["DA1:DD1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["DA1:DD1"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(247, 250, 4));
                wsSheet1.Cells["DA1:DD1"].Style.Font.Color.SetColor(Color.Black);
                //wsSheet1.Cells["D1:CW1"].Value = "-";
                //wsSheet1.Cells["D1:CW1"].Merge = true;
                wsSheet1.View.FreezePanes(3, 8);
               

                int lineNumber = 2;


                wsSheet1.Cells["A" + lineNumber].Value = "BRAND";
                wsSheet1.Cells["B" + lineNumber].Value = "GENDER";
                wsSheet1.Cells["C" + lineNumber].Value = "ARTICLE_TYPE";
                wsSheet1.Cells["D" + lineNumber].Value = "ARTICLE_NUMBER";
                wsSheet1.Cells["E" + lineNumber].Value = "ARTICLE_NAME";
                wsSheet1.Cells["F" + lineNumber].Value = "STYLE_ID";
                wsSheet1.Cells["G" + lineNumber].Value = "QUANTITY_IN_NUMBERS";
                wsSheet1.Cells["H" + lineNumber].Value = "PRODUCT_DESCRIPTION";
                wsSheet1.Cells["I" + lineNumber].Value = "SEASON";
                wsSheet1.Cells["J" + lineNumber].Value = "BUYER_TARGET_COST";
                wsSheet1.Cells["K" + lineNumber].Value = "MONTH";
                wsSheet1.Cells["L" + lineNumber].Value = "HSN_CODE";

                wsSheet1.Cells["M" + lineNumber].Value = "CUT_MAKE";
                wsSheet1.Cells["N" + lineNumber].Value = "REJECTION_PERCENTAGE";
                wsSheet1.Cells["O" + lineNumber].Value = "MARGIN_PERCENTAGE";
                wsSheet1.Cells["P" + lineNumber].Value = "TESTING";
                wsSheet1.Cells["Q" + lineNumber].Value = "FIN_CHARGES";
                wsSheet1.Cells["R" + lineNumber].Value = "FREIGHT";
                wsSheet1.Cells["S" + lineNumber].Value = "CHA";

                wsSheet1.Cells["T" + lineNumber].Value = "PROCESS_DESCRIPTION";
                wsSheet1.Cells["U" + lineNumber].Value = "FABRIC_DESCRIPTION";
                wsSheet1.Cells["V" + lineNumber].Value = "FABRIC_COST";
                wsSheet1.Cells["W" + lineNumber].Value = "PROCESS_COST";
                wsSheet1.Cells["X" + lineNumber].Value = "TRIMS_COST";
                wsSheet1.Cells["Y" + lineNumber].Value = "TOTAL_COST";

                wsSheet1.Cells["Z" + lineNumber].Value = "PROCESS_TYPE_WASH";
                wsSheet1.Cells["AA" + lineNumber].Value = "PROCESS_CATEGORY_WASH";
                wsSheet1.Cells["AB" + lineNumber].Value = "TYPE_OF_GARMENT_WASH";
                wsSheet1.Cells["AC" + lineNumber].Value = "UNIT_PRICE_WASH";
                wsSheet1.Cells["AD" + lineNumber].Value = "PROCESS_DESCRIPTION_WASH";

                wsSheet1.Cells["AE" + lineNumber].Value = "PROCESS_TYPE_PRINT";
                wsSheet1.Cells["AF" + lineNumber].Value = "PRINT_TYPE";
                wsSheet1.Cells["AG" + lineNumber].Value = "UNIT_PRICE_PRINT";
                wsSheet1.Cells["AH" + lineNumber].Value = "CONSUMPTION_PRINT";
                wsSheet1.Cells["AI" + lineNumber].Value = "PROCESS_DESCRIPTION_PRINT";

                wsSheet1.Cells["AJ" + lineNumber].Value = "PROCESS_TYPE_EMBROIDERY";
                wsSheet1.Cells["AK" + lineNumber].Value = "EMBROIDERY_NAME";
                wsSheet1.Cells["AL" + lineNumber].Value = "UNIT_PRICE_EMBROIDERY";
                wsSheet1.Cells["AM" + lineNumber].Value = "CONSUMPTION_EMBROIDERY";
                wsSheet1.Cells["AN" + lineNumber].Value = "PROCESS_DESCRIPTION_EMBROIDERY";

                wsSheet1.Cells["AO" + lineNumber].Value = "PROCESS_TYPE_APPLIQUE";
                wsSheet1.Cells["AP" + lineNumber].Value = "APPLIQUE_NAME";
                wsSheet1.Cells["AQ" + lineNumber].Value = "UNIT_PRICE_APPLIQUE";
                wsSheet1.Cells["AR" + lineNumber].Value = "CONSUMPTION_APPLIQUE";
                wsSheet1.Cells["AS" + lineNumber].Value = "PROCESS_DESCRIPTION_APPLIQUE";

                wsSheet1.Cells["AT" + lineNumber].Value = "PROCESS_TYPE_EMBELLISHMENT";
                wsSheet1.Cells["AU" + lineNumber].Value = "EMBELLISHMENT_NAME";
                wsSheet1.Cells["AV" + lineNumber].Value = "UOM_EMBELLISHMENT";
                wsSheet1.Cells["AW" + lineNumber].Value = "UNIT_PRICE_EMBELLISHMENT";
                wsSheet1.Cells["AX" + lineNumber].Value = "CONSUMPTION_EMBELLISHMENT";
                wsSheet1.Cells["AY" + lineNumber].Value = "PROCESS_DESCRIPTION_EMBELLISHMENT";

                wsSheet1.Cells["AZ" + lineNumber].Value = "FABRIC_TYPE_F1";
                wsSheet1.Cells["BA" + lineNumber].Value = "MILL_F1";
                wsSheet1.Cells["BB" + lineNumber].Value = "DESIGN_STYLE_F1";
                wsSheet1.Cells["BC" + lineNumber].Value = "COMPOSITION_F1";
                wsSheet1.Cells["BD" + lineNumber].Value = "COUNT_CONSTRUCTION_F1";
                wsSheet1.Cells["BE" + lineNumber].Value = "WEIGHT_UOM_F1";
                wsSheet1.Cells["BF" + lineNumber].Value = "UOM_F1";
                wsSheet1.Cells["BG" + lineNumber].Value = "CUTTABLE_WIDTH_F1";
                wsSheet1.Cells["BH" + lineNumber].Value = "SHRINKAGE_F1";
                wsSheet1.Cells["BI" + lineNumber].Value = "UNIT_PRICE_F1";
                wsSheet1.Cells["BJ" + lineNumber].Value = "CONSUMPTION_F1";
                wsSheet1.Cells["BK" + lineNumber].Value = "DESCRIPTION_F1";

                wsSheet1.Cells["BL" + lineNumber].Value = "FABRIC_TYPE_F2";
                wsSheet1.Cells["BM" + lineNumber].Value = "MILL_F2";
                wsSheet1.Cells["BN" + lineNumber].Value = "DESIGN_STYLE_F2";
                wsSheet1.Cells["BO" + lineNumber].Value = "COMPOSITION_F2";
                wsSheet1.Cells["BP" + lineNumber].Value = "COUNT_CONSTRUCTION_F2";
                wsSheet1.Cells["BQ" + lineNumber].Value = "WEIGHT_UOM_F2";
                wsSheet1.Cells["BR" + lineNumber].Value = "UOM_F2";
                wsSheet1.Cells["BS" + lineNumber].Value = "CUTTABLE_WIDTH_F2";
                wsSheet1.Cells["BT" + lineNumber].Value = "SHRINKAGE_F2";
                wsSheet1.Cells["BU" + lineNumber].Value = "UNIT_PRICE_F2";
                wsSheet1.Cells["BV" + lineNumber].Value = "CONSUMPTION_F2";
                wsSheet1.Cells["BW" + lineNumber].Value = "DESCRIPTION_F2";

                wsSheet1.Cells["BX" + lineNumber].Value = "FABRIC_TYPE_F3";
                wsSheet1.Cells["BY" + lineNumber].Value = "MILL_F3";
                wsSheet1.Cells["BZ" + lineNumber].Value = "DESIGN_STYLE_F3";
                wsSheet1.Cells["CA" + lineNumber].Value = "COMPOSITION_F3";
                wsSheet1.Cells["CB" + lineNumber].Value = "COUNT_CONSTRUCTION_F3";
                wsSheet1.Cells["CC" + lineNumber].Value = "WEIGHT_UOM_F3";
                wsSheet1.Cells["CD" + lineNumber].Value = "UOM_F3";
                wsSheet1.Cells["CE" + lineNumber].Value = "CUTTABLE_WIDTH_F3";
                wsSheet1.Cells["CF" + lineNumber].Value = "SHRINKAGE_F3";
                wsSheet1.Cells["CG" + lineNumber].Value = "UNIT_PRICE_F3";
                wsSheet1.Cells["CH" + lineNumber].Value = "CONSUMPTION_F3";
                wsSheet1.Cells["CI" + lineNumber].Value = "DESCRIPTION_F3";

                wsSheet1.Cells["CJ" + lineNumber].Value = "SUNDRY_TRIMS_NAME_T1";
                wsSheet1.Cells["CK" + lineNumber].Value = "UNIT_PRICE_T1";
                wsSheet1.Cells["CL" + lineNumber].Value = "CONSUMPTION_T1";
                wsSheet1.Cells["CM" + lineNumber].Value = "UOM_T1";

                wsSheet1.Cells["CN" + lineNumber].Value = "SUNDRY_TRIMS_NAME_T2";
                wsSheet1.Cells["CO" + lineNumber].Value = "UNIT_PRICE_T2";
                wsSheet1.Cells["CP" + lineNumber].Value = "CONSUMPTION_T2";
                wsSheet1.Cells["CQ" + lineNumber].Value = "UOM_T2";

                wsSheet1.Cells["CR" + lineNumber].Value = "SUNDRY_TRIMS_NAME_T3";
                wsSheet1.Cells["CS" + lineNumber].Value = "UNIT_PRICE_T3";
                wsSheet1.Cells["CT" + lineNumber].Value = "CONSUMPTION_T3";
                wsSheet1.Cells["CU" + lineNumber].Value = "UOM_T3";

                wsSheet1.Cells["CV" + lineNumber].Value = "SUB_ITEM_COST";
                wsSheet1.Cells["CW" + lineNumber].Value = "PRODUCT_ID";
                wsSheet1.Cells["CX" + lineNumber].Value = "ITEM_COST_WITH_REJECTION";
                wsSheet1.Cells["CY" + lineNumber].Value = "ITEM_COST_WITH_MARGIN";
                wsSheet1.Cells["CZ" + lineNumber].Value = "SUB_TOTAL_COST";
                wsSheet1.Cells["DA" + lineNumber].Value = "ITEM_UNIT_PRICE";
                wsSheet1.Cells["DB" + lineNumber].Value = "ITEM_REV_UNIT_PRICE";
                wsSheet1.Cells["DC" + lineNumber].Value = "IS_REGRET";
                wsSheet1.Cells["DD" + lineNumber].Value = "REGRET_COMMENTS";
                wsSheet1.Cells["DE" + lineNumber].Value = "FABRIC_PRICE";
                wsSheet1.Cells["DF" + lineNumber].Value = "FABRIC_QUALITY";


                wsSheet1.Cells["A" + lineNumber + ":DF" + lineNumber].Style.Font.Bold = true;

                lineNumber++;
                wsSheet1.Cells["A2:DF2"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A2:DF2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                

                //253, 184, 171
                wsSheet1.Cells["M2:P2"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["M2:P2"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(253, 184, 171));
                wsSheet1.Cells["M2:P2"].Style.Font.Color.SetColor(Color.Black);
                wsSheet1.Cells["M2:P2"].Style.Locked = true;

                wsSheet1.Cells["AZ2:BK2"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["AZ2:BK2"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(253, 184, 171));
                wsSheet1.Cells["AZ2:BK2"].Style.Font.Color.SetColor(Color.Black);
                wsSheet1.Cells["AZ2:BK2"].Style.Locked = true;

                wsSheet1.Cells["BA2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                wsSheet1.Cells["BH2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                wsSheet1.Cells["BK2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                wsSheet1.Cells["BC2:BE2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                wsSheet1.Cells["CJ2:CM2"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["CJ2:CM2"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(253, 184, 171));
                wsSheet1.Cells["CJ2:CM2"].Style.Font.Color.SetColor(Color.Black);  
                wsSheet1.Cells["CJ2:CM2"].Style.Locked = true;

                wsSheet1.Cells["DD2"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["DD2"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(253, 184, 171));
                wsSheet1.Cells["DD2"].Style.Font.Color.SetColor(Color.Black);
                wsSheet1.Cells["DD2"].Style.Locked = true;
                              
                wsSheet1.Cells["A:DD"].AutoFitColumns();

                wsSheet1.Protection.IsProtected = true;
                wsSheet1.Protection.AllowSelectLockedCells = true;

                #region formulas and validations



                wsSheet1.Cells[$"V3:V500"].Formula = "IF(G3 > 0, ((BI3*BJ3)+(BU3*BV3)+(CG3*CH3)), \"\")";
                wsSheet1.Cells[$"W3:W500"].Formula = "IF(G3 > 0, ((AC3)+(AG3*AH3)+(AL3*AM3)+(AQ3*AR3)+(AW3*AX3)), \"\")";
                wsSheet1.Cells[$"X3:X500"].Formula = "IF(G3 > 0, ((CK3*CL3)+(CO3*CP3)+(CS3*CT3)), \"\")";
                //wsSheet1.Cells[$"Y3:Y500"].Formula = "IF(G3 > 0, (V3+W3+X3)), \"\")";
                wsSheet1.Cells[$"CV3:CV500"].Formula = "IF(G3 > 0, (V3+W3+X3+M3), \"\")";
                wsSheet1.Cells[$"CX3:CX500"].Formula = "IF(G3 > 0, ((CV3+(CV3*N3/100))), \"\")";
                wsSheet1.Cells[$"CY3:CY500"].Formula = "IF(G3 > 0, ((CX3+(CX3*O3/100))), \"\")";

                wsSheet1.Cells[$"CZ3:CZ500"].Formula = "IF(G3 > 0, ROUNDUP(CY3:CY500, 2), \"\")";
                wsSheet1.Cells[$"Y3:Y500"].Formula = "IF(G3 > 0, ((CZ3+P3+Q3+R3+S3)*G3), \"\")";

                if (excelRequirement.Status != "STARTED")
                {
                    wsSheet1.Cells[$"DA3:DA500"].Formula = "IF(G3 > 0, (Y3/G3), \"\")";
                    wsSheet1.Cells[$"DB3:DB500"].Formula = "IF(G3 > 0, (Y3/G3), \"\")";
                }
                else
                {
                    wsSheet1.Cells[$"DB3:DB500"].Formula = "IF(G3 > 0, (Y3/G3), \"\")";
                }

                

                //wsSheet1.Cells[$"BE3:BE500"].Formula = "IF(AZ3=Woven,NA,IF(AZ3=Circular Knit,gsm,IF(AZ3=Denim,oz,)))";


                var regretValidation = wsSheet1.DataValidations.AddListValidation($"DC2:DC{maxRows}");
                regretValidation.ShowErrorMessage = true;
                regretValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                regretValidation.ErrorTitle = "Invalid Regret Value";
                regretValidation.Error = "Invalid regret value selected";
                foreach (var s in isregretvalues)
                {
                    regretValidation.Formula.Values.Add(s);
                }

                var regretCommentsValidation = wsSheet1.DataValidations.AddListValidation($"DD2:DD{maxRows}");
                regretCommentsValidation.ShowErrorMessage = true;
                regretCommentsValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                regretCommentsValidation.ErrorTitle = "Invalid Regret Comments";
                regretCommentsValidation.Error = "Invalid regret comments selected";
                foreach (var s in regretComments)
                {
                    regretCommentsValidation.Formula.Values.Add(s);
                }

                var fabricType1Validation = wsSheet1.DataValidations.AddListValidation("AZ3:AZ500");
                fabricType1Validation.ShowErrorMessage = true;
                fabricType1Validation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                fabricType1Validation.ErrorTitle = "Invalid Fabric Type";
                fabricType1Validation.Error = "Invalid Fabric Type selected";
                foreach (var ft in fabricTypeParams)
                {
                    fabricType1Validation.Formula.Values.Add(ft);
                }

                

                var fabricType2Validation = wsSheet1.DataValidations.AddListValidation("BL3:BL500");
                fabricType2Validation.ShowErrorMessage = true;
                fabricType2Validation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                fabricType2Validation.ErrorTitle = "Invalid Fabric Type";
                fabricType2Validation.Error = "Invalid Fabric Type selected";
                foreach (var ft in fabricTypeParams)
                {
                    fabricType2Validation.Formula.Values.Add(ft);
                }

                var fabricType3Validation = wsSheet1.DataValidations.AddListValidation("BX3:BX500");
                fabricType3Validation.ShowErrorMessage = true;
                fabricType3Validation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                fabricType3Validation.ErrorTitle = "Invalid Fabric Type";
                fabricType3Validation.Error = "Invalid Fabric Type selected";
                foreach (var ft in fabricTypeParams)
                {
                    fabricType3Validation.Formula.Values.Add(ft);
                }

                var uomParamsEM = wsSheet1.DataValidations.AddListValidation("AV3:AV500");
                uomParamsEM.ShowErrorMessage = true;
                uomParamsEM.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                uomParamsEM.ErrorTitle = "Invalid UOM";
                uomParamsEM.Error = "Invalid UOM selected";
                foreach (var ft in UOMParams)
                {
                    uomParamsEM.Formula.Values.Add(ft);
                }

                var uomF1 = wsSheet1.DataValidations.AddListValidation("BF3:BF500");
                uomF1.ShowErrorMessage = true;
                uomF1.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                uomF1.ErrorTitle = "Invalid UOM";
                uomF1.Error = "Invalid UOM selected";
                foreach (var ft in UOMParams)
                {
                    uomF1.Formula.Values.Add(ft);
                }

                var uomF2 = wsSheet1.DataValidations.AddListValidation("BR3:BR500");
                uomF2.ShowErrorMessage = true;
                uomF2.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                uomF2.ErrorTitle = "Invalid UOM";
                uomF2.Error = "Invalid UOM selected";
                foreach (var ft in UOMParams)
                {
                    uomF2.Formula.Values.Add(ft);
                }

                var uomF3 = wsSheet1.DataValidations.AddListValidation("CD3:CD500");
                uomF3.ShowErrorMessage = true;
                uomF3.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                uomF3.ErrorTitle = "Invalid UOM";
                uomF3.Error = "Invalid UOM selected";
                foreach (var ft in UOMParams)
                {
                    uomF3.Formula.Values.Add(ft);
                }

                var uomT1 = wsSheet1.DataValidations.AddListValidation("CM3:CM500");
                uomT1.ShowErrorMessage = true;
                uomT1.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                uomT1.ErrorTitle = "Invalid UOM";
                uomT1.Error = "Invalid UOM selected";
                foreach (var ft in UOMParams)
                {
                    uomT1.Formula.Values.Add(ft);
                }

                var uomT2 = wsSheet1.DataValidations.AddListValidation("CQ3:CQ500");
                uomT2.ShowErrorMessage = true;
                uomT2.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                uomT2.ErrorTitle = "Invalid UOM";
                uomT2.Error = "Invalid UOM selected";
                foreach (var ft in UOMParams)
                {
                    uomT2.Formula.Values.Add(ft);
                }

                var uomT3 = wsSheet1.DataValidations.AddListValidation("CU3:CU500");
                uomT3.ShowErrorMessage = true;
                uomT3.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                uomT3.ErrorTitle = "Invalid UOM";
                uomT3.Error = "Invalid UOM selected";
                foreach (var ft in UOMParams)
                {
                    uomT3.Formula.Values.Add(ft);
                }

                var processCategory = wsSheet1.DataValidations.AddListValidation("AA3:AA500");
                processCategory.ShowErrorMessage = true;
                processCategory.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                processCategory.ErrorTitle = "Invalid Process Category Type";
                processCategory.Error = "Invalid Process Category Type selected";
                foreach (var ft in processCategoryParams)
                {
                    processCategory.Formula.Values.Add(ft);
                }

                



                #endregion formulas and validations
                VendorDetails vendor = excelRequirement.AuctionVendors[0];
                List<RequirementItems> reqItems = vendor.ListRequirementItems;
                foreach (RequirementItems item in reqItems)
                {
                    item.ApparelFabric = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ApparelFabric>>(item.ApparelFabricJson);
                    item.ApparelProcess = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ApparelProcess>>(item.ApparelProcessJson);
                    item.ApparelSundries = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ApparelSundries>>(item.ApparelSundriesJson);

                    wsSheet1.Cells["A" + lineNumber].Value = item.BRANDS;
                    wsSheet1.Cells["B" + lineNumber].Value = item.GENDER;
                    wsSheet1.Cells["C" + lineNumber].Value = item.ARTICLE_TYPE;
                    wsSheet1.Cells["D" + lineNumber].Value = item.ProductCode;
                    wsSheet1.Cells["E" + lineNumber].Value = item.ProductIDorName;
                    wsSheet1.Cells["F" + lineNumber].Value = item.ProductNo;
                    wsSheet1.Cells["G" + lineNumber].Value = item.ProductQuantity;
                    wsSheet1.Cells["H" + lineNumber].Value = item.ProductDescription;
                    wsSheet1.Cells["I" + lineNumber].Value = item.SEASON;
                    wsSheet1.Cells["J" + lineNumber].Value = item.BuyerTargetCost;
                    wsSheet1.Cells["K" + lineNumber].Value = item.BUY_PLAN_MONTH;
                    wsSheet1.Cells["L" + lineNumber].Value = item.HsnCode;

                    if (excelRequirement.Status != "STARTED")
                    {
                        wsSheet1.Cells["M" + lineNumber].Value = item.CutMake > 0 ? item.CutMake : 0;
                        wsSheet1.Cells["N" + lineNumber].Value = item.RejectionPercentage > 0 ? item.RejectionPercentage : 0;
                        wsSheet1.Cells["O" + lineNumber].Value = item.MarginPercentage > 0 ? item.MarginPercentage : 0;
                        wsSheet1.Cells["P" + lineNumber].Value = item.Testing > 0 ? item.Testing : 0;
                        wsSheet1.Cells["Q" + lineNumber].Value = item.FinanceCharges > 0 ? item.FinanceCharges : 0;
                        wsSheet1.Cells["R" + lineNumber].Value = item.FreightCharges > 0 ? item.FreightCharges : 0;
                        wsSheet1.Cells["S" + lineNumber].Value = item.ChaCharges > 0 ? item.ChaCharges : 0;

                        item.RevCutMake = item.CutMake;
                        item.RevRejectionPercentage = item.RejectionPercentage;
                        item.RevMarginPercentage = item.MarginPercentage;
                        item.RevTesting = item.Testing;
                        item.RevFinanceCharges = item.FinanceCharges;
                        item.RevFreightCharges = item.FreightCharges;
                        item.RevChaCharges = item.ChaCharges;
                    }
                    else
                    {
                        wsSheet1.Cells["M" + lineNumber].Value = item.RevCutMake > 0 ? item.RevCutMake : item.CutMake;
                        wsSheet1.Cells["N" + lineNumber].Value = item.RevRejectionPercentage > 0 ? item.RevRejectionPercentage : item.RejectionPercentage;
                        wsSheet1.Cells["O" + lineNumber].Value = item.RevMarginPercentage > 0 ? item.RevMarginPercentage : item.MarginPercentage;
                        wsSheet1.Cells["P" + lineNumber].Value = item.RevTesting > 0 ? item.RevTesting : item.Testing;
                        wsSheet1.Cells["Q" + lineNumber].Value = item.RevFinanceCharges > 0 ? item.RevFinanceCharges : item.FinanceCharges;
                        wsSheet1.Cells["R" + lineNumber].Value = item.RevFreightCharges > 0 ? item.RevFreightCharges : item.FreightCharges;
                        wsSheet1.Cells["S" + lineNumber].Value = item.RevChaCharges > 0 ? item.RevChaCharges : item.ChaCharges;
                        wsSheet1.Cells["DA" + lineNumber].Value = item.UnitPrice;
                        //wsSheet1.Cells["DB" + lineNumber].Value = item.RevUnitPrice;
                    }
                    
                    //wsSheet1.Cells["M" + lineNumber + ":S" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    //wsSheet1.Cells["M" + lineNumber + ":S" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(237, 187, 153));

                    wsSheet1.Cells["T" + lineNumber].Value = item.ProcessDescription;
                    wsSheet1.Cells["U" + lineNumber].Value = item.FabricDescription;

                    #region WashType
                    if (item.ApparelProcess != null && item.ApparelProcess.Count > 0 && item.ApparelProcess[0].PROCESS_TYPE == "Wash")
                    {
                        var washType = item.ApparelProcess[0];
                        wsSheet1.Cells["Z" + lineNumber].Value = "Wash";
                        wsSheet1.Cells["AA" + lineNumber].Value = washType.PROCESS_CATEGORY.Length > 0 ? washType.PROCESS_CATEGORY:"";
                        wsSheet1.Cells["AB" + lineNumber].Value = washType.TYPE_OF_GARMENT.Length > 0 ? washType.TYPE_OF_GARMENT : "";
                        if (excelRequirement.Status != "STARTED")
                        {
                            wsSheet1.Cells["AC" + lineNumber].Value = washType.UNIT_PRICE > 0 ? washType.UNIT_PRICE : 0;
                        }
                        else
                        {
                            wsSheet1.Cells["AC" + lineNumber].Value = washType.REV_UNIT_PRICE > 0 ? washType.REV_UNIT_PRICE : washType.UNIT_PRICE;
                        }
                        
                        wsSheet1.Cells["AD" + lineNumber].Value = washType.PROCESS_DISCRIPTION.Length > 0 ? washType.PROCESS_DISCRIPTION : "";

                    }
                    else
                    {
                        wsSheet1.Cells["Z" + lineNumber].Value = "Wash";
                        wsSheet1.Cells["AA" + lineNumber].Value = "";
                        wsSheet1.Cells["AB" + lineNumber].Value = "";
                        wsSheet1.Cells["AC" + lineNumber].Value = 0;
                        wsSheet1.Cells["AD" + lineNumber].Value = "";
                    }
                    #endregion WashType

                    #region PrintType
                    if (item.ApparelProcess != null && item.ApparelProcess.Count > 1 && item.ApparelProcess[1].PROCESS_TYPE == "Print")
                    {
                        var printType = item.ApparelProcess[1];
                        wsSheet1.Cells["AE" + lineNumber].Value = "Print";
                        wsSheet1.Cells["AF" + lineNumber].Value = printType.PRINT_TYPE.Length > 0 ? printType.PRINT_TYPE : "";
                        if (excelRequirement.Status != "STARTED")
                        {
                            wsSheet1.Cells["AG" + lineNumber].Value = printType.UNIT_PRICE > 0 ? printType.UNIT_PRICE : 0;
                            wsSheet1.Cells["AH" + lineNumber].Value = Convert.ToDouble(printType.CONSUMPTION) > 1  ? Convert.ToDouble(printType.CONSUMPTION) : 1;
                        }
                        else
                        {
                            wsSheet1.Cells["AG" + lineNumber].Value = printType.REV_UNIT_PRICE;
                            wsSheet1.Cells["AH" + lineNumber].Value = Convert.ToDouble(printType.REV_CONSUMPTION);
                        }
                        wsSheet1.Cells["AI" + lineNumber].Value = printType.PROCESS_DISCRIPTION.Length > 0 ? printType.PROCESS_DISCRIPTION : "";
                    }
                    else
                    {
                        wsSheet1.Cells["AE" + lineNumber].Value = "Print";
                        wsSheet1.Cells["AF" + lineNumber].Value = "";
                        wsSheet1.Cells["AG" + lineNumber].Value = 0;
                        wsSheet1.Cells["AH" + lineNumber].Value = 1;
                        wsSheet1.Cells["AI" + lineNumber].Value = "";
                    }
                    #endregion PrintType

                    #region EmbroideryType
                    if (item.ApparelProcess != null && item.ApparelProcess.Count > 2 && item.ApparelProcess[2].PROCESS_TYPE == "Embroidery")
                    {
                        var Embroidery = item.ApparelProcess[2];
                        wsSheet1.Cells["AJ" + lineNumber].Value = "Embroidery";
                        wsSheet1.Cells["AK" + lineNumber].Value = Embroidery.EMBROIDERY_NAME.Length > 0 ? Embroidery.EMBROIDERY_NAME : "";
                        if (excelRequirement.Status != "STARTED")
                        {
                            wsSheet1.Cells["AL" + lineNumber].Value = Embroidery.UNIT_PRICE > 0 ? Embroidery.UNIT_PRICE : 0;
                            wsSheet1.Cells["AM" + lineNumber].Value = Convert.ToDouble(Embroidery.CONSUMPTION) > 1 ? Convert.ToDouble(Embroidery.CONSUMPTION) : 1;
                        }
                        else
                        {
                            wsSheet1.Cells["AL" + lineNumber].Value = Embroidery.REV_UNIT_PRICE;
                            wsSheet1.Cells["AM" + lineNumber].Value = Convert.ToDouble(Embroidery.REV_CONSUMPTION);
                        }
                        wsSheet1.Cells["AN" + lineNumber].Value = Embroidery.PROCESS_DISCRIPTION.Length > 0 ? Embroidery.PROCESS_DISCRIPTION : "";
                    }
                    else
                    {
                        wsSheet1.Cells["AJ" + lineNumber].Value = "Embroidery";
                        wsSheet1.Cells["AK" + lineNumber].Value = "";
                        wsSheet1.Cells["AL" + lineNumber].Value = 0;
                        wsSheet1.Cells["AM" + lineNumber].Value = 1;
                        wsSheet1.Cells["AN" + lineNumber].Value = "";
                    }
                    #endregion EmbroideryType

                    #region AppliqueType
                    if (item.ApparelProcess != null && item.ApparelProcess.Count > 3 && item.ApparelProcess[3].PROCESS_TYPE == "Applique")
                    {
                        var Applique = item.ApparelProcess[3];
                        wsSheet1.Cells["AO" + lineNumber].Value = "Applique";
                        wsSheet1.Cells["AP" + lineNumber].Value = Applique.APPLIQUE_NAME.Length > 0 ? Applique.APPLIQUE_NAME : "";
                        if (excelRequirement.Status != "STARTED")
                        {
                            wsSheet1.Cells["AQ" + lineNumber].Value = Applique.UNIT_PRICE > 0 ? Applique.UNIT_PRICE : 0;
                            wsSheet1.Cells["AR" + lineNumber].Value = Convert.ToDouble(Applique.CONSUMPTION) > 1 ? Convert.ToDouble(Applique.CONSUMPTION) : 1;
                        }
                        else
                        {
                            wsSheet1.Cells["AQ" + lineNumber].Value = Applique.REV_UNIT_PRICE;
                            wsSheet1.Cells["AR" + lineNumber].Value = Convert.ToDouble(Applique.REV_CONSUMPTION);
                        }
                        wsSheet1.Cells["AS" + lineNumber].Value = Applique.PROCESS_DISCRIPTION.Length > 0 ? Applique.PROCESS_DISCRIPTION : "";
                    }
                    else
                    {
                        wsSheet1.Cells["AO" + lineNumber].Value = "Applique";
                        wsSheet1.Cells["AP" + lineNumber].Value = "";
                        wsSheet1.Cells["AQ" + lineNumber].Value = 0;
                        wsSheet1.Cells["AR" + lineNumber].Value = 1;
                        wsSheet1.Cells["AS" + lineNumber].Value = "";
                    }
                    #endregion AppliqueType

                    #region EmbellishmentType
                    if (item.ApparelProcess != null && item.ApparelProcess.Count > 4 && item.ApparelProcess[4].PROCESS_TYPE == "Embellishment")
                    {
                        var Embellishment = item.ApparelProcess[4];
                        wsSheet1.Cells["AT" + lineNumber].Value = "Embellishment";
                        wsSheet1.Cells["AU" + lineNumber].Value = Embellishment.EMBELLISHMENT_NAME.Length > 0 ? Embellishment.EMBELLISHMENT_NAME : "";
                        if (excelRequirement.Status != "STARTED")
                        {
                            wsSheet1.Cells["AW" + lineNumber].Value = Embellishment.UNIT_PRICE > 0 ? Embellishment.UNIT_PRICE : 0;
                            wsSheet1.Cells["AX" + lineNumber].Value = Convert.ToDouble(Embellishment.CONSUMPTION) > 1 ? Convert.ToDouble(Embellishment.CONSUMPTION) : 1;
                        }
                        else
                        {
                            wsSheet1.Cells["AW" + lineNumber].Value = Embellishment.REV_UNIT_PRICE;
                            wsSheet1.Cells["AX" + lineNumber].Value = Convert.ToDouble(Embellishment.REV_CONSUMPTION);
                        }
                        
                        wsSheet1.Cells["AY" + lineNumber].Value = Embellishment.PROCESS_DISCRIPTION.Length > 0 ? Embellishment.PROCESS_DISCRIPTION : "";
                    }
                    else
                    {
                        wsSheet1.Cells["AT" + lineNumber].Value = "Embellishment";
                        wsSheet1.Cells["AU" + lineNumber].Value = "";
                        wsSheet1.Cells["AW" + lineNumber].Value = 0;
                        wsSheet1.Cells["AX" + lineNumber].Value = 1;
                        wsSheet1.Cells["AY" + lineNumber].Value = "";
                    }
                    #endregion EmbellishmentType

                    if (item.ApparelFabric != null && item.ApparelFabric.Count > 0)
                    {
                        if (item.ApparelFabric.Count > 0)
                        {
                            wsSheet1.Cells["AZ" + lineNumber].Value = item.ApparelFabric[0].NAME;
                            wsSheet1.Cells["BA" + lineNumber].Value = item.ApparelFabric[0].MILL;
                            wsSheet1.Cells["BB" + lineNumber].Value = item.ApparelFabric[0].DESIGN_STYLE;
                            wsSheet1.Cells["BD" + lineNumber].Value = item.ApparelFabric[0].COUNT + " " + item.ApparelFabric[0].CONSTRUCTION;
                            wsSheet1.Cells["BC" + lineNumber].Value = item.ApparelFabric[0].COMPOSITION;
                            wsSheet1.Cells["BE" + lineNumber].Value = item.ApparelFabric[0].WEIGHT_UOM;
                            wsSheet1.Cells["BF" + lineNumber].Value = item.ApparelFabric[0].UOM;
                            wsSheet1.Cells["BG" + lineNumber].Value = item.ApparelFabric[0].CUTTABLE_WIDTH;
                            wsSheet1.Cells["BH" + lineNumber].Value = item.ApparelFabric[0].SHRINKAGE;
                            if (excelRequirement.Status != "STARTED")
                            {
                                wsSheet1.Cells["BI" + lineNumber].Value = item.ApparelFabric[0].UNIT_PRICE;
                                wsSheet1.Cells["BJ" + lineNumber].Value = item.ApparelFabric[0].CONSUMPTION;
                            }
                            else
                            {
                                wsSheet1.Cells["BI" + lineNumber].Value = item.ApparelFabric[0].REV_UNIT_PRICE;
                                wsSheet1.Cells["BJ" + lineNumber].Value = item.ApparelFabric[0].REV_CONSUMPTION;
                            }
                            
                            wsSheet1.Cells["BK" + lineNumber].Value = item.ApparelFabric[0].DESCRIPTION;
                        }
                        if (item.ApparelFabric.Count > 1)
                        {
                            wsSheet1.Cells["BL" + lineNumber].Value = item.ApparelFabric[1].NAME;
                            wsSheet1.Cells["BM" + lineNumber].Value = item.ApparelFabric[1].MILL;
                            wsSheet1.Cells["BN" + lineNumber].Value = item.ApparelFabric[1].DESIGN_STYLE;
                            wsSheet1.Cells["BO" + lineNumber].Value = item.ApparelFabric[1].COUNT + " " + item.ApparelFabric[0].CONSTRUCTION;
                            wsSheet1.Cells["BP" + lineNumber].Value = item.ApparelFabric[1].COMPOSITION;
                            wsSheet1.Cells["BQ" + lineNumber].Value = item.ApparelFabric[1].WEIGHT_UOM;
                            wsSheet1.Cells["BR" + lineNumber].Value = item.ApparelFabric[1].UOM;
                            wsSheet1.Cells["BS" + lineNumber].Value = item.ApparelFabric[1].CUTTABLE_WIDTH;
                            wsSheet1.Cells["BT" + lineNumber].Value = item.ApparelFabric[1].SHRINKAGE;
                            if (excelRequirement.Status != "STARTED")
                            {
                                wsSheet1.Cells["BU" + lineNumber].Value = item.ApparelFabric[1].UNIT_PRICE;
                                wsSheet1.Cells["BV" + lineNumber].Value = item.ApparelFabric[1].CONSUMPTION;
                            }                                                                
                            else                                                             
                            {                                                                
                                wsSheet1.Cells["BU" + lineNumber].Value = item.ApparelFabric[1].REV_UNIT_PRICE;
                                wsSheet1.Cells["BV" + lineNumber].Value = item.ApparelFabric[1].REV_CONSUMPTION;
                            }
                            wsSheet1.Cells["BW" + lineNumber].Value = item.ApparelFabric[1].DESCRIPTION;
                        }
                        if (item.ApparelFabric.Count > 2)
                        {
                            wsSheet1.Cells["BX" + lineNumber].Value = item.ApparelFabric[2].NAME;
                            wsSheet1.Cells["BY" + lineNumber].Value = item.ApparelFabric[2].MILL;
                            wsSheet1.Cells["BZ" + lineNumber].Value = item.ApparelFabric[2].DESIGN_STYLE;
                            wsSheet1.Cells["CA" + lineNumber].Value = item.ApparelFabric[2].COUNT + " " + item.ApparelFabric[0].CONSTRUCTION;
                            wsSheet1.Cells["CB" + lineNumber].Value = item.ApparelFabric[2].COMPOSITION;
                            wsSheet1.Cells["CC" + lineNumber].Value = item.ApparelFabric[2].WEIGHT_UOM;
                            wsSheet1.Cells["CD" + lineNumber].Value = item.ApparelFabric[2].UOM;
                            wsSheet1.Cells["CE" + lineNumber].Value = item.ApparelFabric[2].CUTTABLE_WIDTH;
                            wsSheet1.Cells["CF" + lineNumber].Value = item.ApparelFabric[2].SHRINKAGE;
                            if (excelRequirement.Status != "STARTED")
                            {
                                wsSheet1.Cells["CG" + lineNumber].Value = item.ApparelFabric[2].UNIT_PRICE;
                                wsSheet1.Cells["CH" + lineNumber].Value = item.ApparelFabric[2].CONSUMPTION;
                            }
                            else
                            {
                                wsSheet1.Cells["CG" + lineNumber].Value = item.ApparelFabric[2].REV_UNIT_PRICE;
                                wsSheet1.Cells["CH" + lineNumber].Value = item.ApparelFabric[2].REV_CONSUMPTION;
                            }
                            wsSheet1.Cells["CI" + lineNumber].Value = item.ApparelFabric[2].DESCRIPTION;
                        }

                    }
                    else
                    {
                        wsSheet1.Cells["BI" + lineNumber].Value = 0;
                        wsSheet1.Cells["BJ" + lineNumber].Value = 1; // mandatory
                                                                     //wsSheet1.Cells["BL" + lineNumber].Value = "FABRIC";
                        wsSheet1.Cells["BU" + lineNumber].Value = 0;
                        wsSheet1.Cells["BV" + lineNumber].Value = 1;// mandatory
                                                                    //wsSheet1.Cells["BX" + lineNumber].Value = "FABRIC TYPE_F3";
                        wsSheet1.Cells["CG" + lineNumber].Value = 0;
                        wsSheet1.Cells["CH" + lineNumber].Value = 1;// mandatory
                    }

                    if (item.ApparelSundries != null && item.ApparelSundries.Count > 0)
                    {
                        if (item.ApparelSundries.Count > 0)
                        {
                            wsSheet1.Cells["CJ" + lineNumber].Value = item.ApparelSundries[0].SEWING_NAME;
                            if (excelRequirement.Status != "STARTED")
                            {
                                wsSheet1.Cells["CK" + lineNumber].Value = item.ApparelSundries[0].SEWING_COST > 0 ? item.ApparelSundries[0].SEWING_COST : 0;
                                wsSheet1.Cells["CL" + lineNumber].Value = Convert.ToDouble(item.ApparelSundries[0].SEWING_CONSUMPTION) > 1 ? Convert.ToDouble(item.ApparelSundries[0].SEWING_CONSUMPTION) : 1;
                            }
                            else
                            {
                                wsSheet1.Cells["CK" + lineNumber].Value = item.ApparelSundries[0].REV_SEWING_COST;
                                wsSheet1.Cells["CL" + lineNumber].Value = Convert.ToDouble(item.ApparelSundries[0].REV_SEWING_CONSUMPTION);
                            }
                            wsSheet1.Cells["CM" + lineNumber].Value = item.ApparelSundries[0].UOM;
                        }

                        if (item.ApparelSundries.Count > 1)
                        {
                            wsSheet1.Cells["CN" + lineNumber].Value = item.ApparelSundries[1].SEWING_NAME;
                            if (excelRequirement.Status != "STARTED")
                            {
                                wsSheet1.Cells["CO" + lineNumber].Value = item.ApparelSundries[1].SEWING_COST > 0 ? item.ApparelSundries[1].SEWING_COST : 0;
                                wsSheet1.Cells["CP" + lineNumber].Value = Convert.ToDouble(item.ApparelSundries[1].SEWING_CONSUMPTION) > 1 ? Convert.ToDouble(item.ApparelSundries[1].SEWING_CONSUMPTION) : 1;
                            }
                            else
                            {
                                wsSheet1.Cells["CO" + lineNumber].Value = item.ApparelSundries[1].REV_SEWING_COST;
                                wsSheet1.Cells["CP" + lineNumber].Value = Convert.ToDouble(item.ApparelSundries[1].REV_SEWING_CONSUMPTION);
                            }
                            wsSheet1.Cells["CQ" + lineNumber].Value = item.ApparelSundries[1].UOM;
                        }

                        if (item.ApparelSundries.Count > 2)
                        {
                            wsSheet1.Cells["CR" + lineNumber].Value = item.ApparelSundries[2].SEWING_NAME;
                            if (excelRequirement.Status != "STARTED")
                            {
                                wsSheet1.Cells["CS" + lineNumber].Value = item.ApparelSundries[2].SEWING_COST > 0 ? item.ApparelSundries[2].SEWING_COST : 0;
                                wsSheet1.Cells["CT" + lineNumber].Value = Convert.ToDouble(item.ApparelSundries[2].SEWING_CONSUMPTION) > 1 ? Convert.ToDouble(item.ApparelSundries[2].SEWING_CONSUMPTION) : 1;
                            }
                            else
                            {
                                wsSheet1.Cells["CS" + lineNumber].Value = item.ApparelSundries[2].REV_SEWING_COST;
                                wsSheet1.Cells["CT" + lineNumber].Value = Convert.ToDouble(item.ApparelSundries[2].REV_SEWING_CONSUMPTION);
                            }
                            wsSheet1.Cells["CU" + lineNumber].Value = item.ApparelSundries[2].UOM;
                        }
                    }
                    else
                    {
                        wsSheet1.Cells["CK" + lineNumber].Value = 0;
                        wsSheet1.Cells["CL" + lineNumber].Value = 1;
                        wsSheet1.Cells["CO" + lineNumber].Value = 0;
                        wsSheet1.Cells["CP" + lineNumber].Value = 1;
                        wsSheet1.Cells["CS" + lineNumber].Value = 0;
                        wsSheet1.Cells["CT" + lineNumber].Value = 1;
                    }
                    wsSheet1.Cells["CW" + lineNumber].Value = item.CatalogueItemID;


                    wsSheet1.Cells["DC" + lineNumber].Value = item.IsRegret == false ? "FALSE" : "TRUE";
                    wsSheet1.Cells["DD" + lineNumber].Value = !string.IsNullOrEmpty(item.RegretComments) ? item.RegretComments : "";
                    wsSheet1.Cells["DE" + lineNumber].Value = item.FabricPrice;
                    wsSheet1.Cells["DF" + lineNumber].Value = item.Fabricquality;

                    #region Decimal Validation
                    //if (excelRequirement.Status == "STARTED")
                    //{
                    //    var fabricCost = wsSheet1.DataValidations.AddDecimalValidation($"V{lineNumber}");
                    //    fabricCost.ShowErrorMessage = true;
                    //    fabricCost.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                    //    fabricCost.ErrorTitle = "The value you entered is not valid";
                    //    fabricCost.Error = "This cell must be a lesser than" + " " + item.FabricCost;
                    //    fabricCost.Operator = ExcelDataValidationOperator.greaterThanOrEqual;
                    //    fabricCost.Formula.Value = item.FabricCost;
                    //}

                    var decimalValidation = wsSheet1.DataValidations.AddDecimalValidation($"M{lineNumber}:S{lineNumber}");
                    decimalValidation.ShowErrorMessage = true;
                    decimalValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                    decimalValidation.ErrorTitle = "The value you entered is not valid";
                    decimalValidation.Error = "This cell must be a valid positive number.";
                    decimalValidation.Operator = ExcelDataValidationOperator.greaterThanOrEqual;
                    decimalValidation.Formula.Value = 0D;

                    var decimalValidation1 = wsSheet1.DataValidations.AddDecimalValidation($"AC{lineNumber}");
                    decimalValidation1.ShowErrorMessage = true;
                    decimalValidation1.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                    decimalValidation1.ErrorTitle = "The value you entered is not valid";
                    decimalValidation1.Error = "This cell must be a valid positive number.";
                    decimalValidation1.Operator = ExcelDataValidationOperator.greaterThanOrEqual;
                    decimalValidation1.Formula.Value = 0D;

                    var decimalValidation2 = wsSheet1.DataValidations.AddDecimalValidation($"AG{lineNumber}:AH{lineNumber}");
                    decimalValidation2.ShowErrorMessage = true;
                    decimalValidation2.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                    decimalValidation2.ErrorTitle = "The value you entered is not valid";
                    decimalValidation2.Error = "This cell must be a valid positive number.";
                    decimalValidation2.Operator = ExcelDataValidationOperator.greaterThanOrEqual;
                    decimalValidation2.Formula.Value = 0D;

                    var decimalValidation3 = wsSheet1.DataValidations.AddDecimalValidation($"AL{lineNumber}:AM{lineNumber}");
                    decimalValidation3.ShowErrorMessage = true;
                    decimalValidation3.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                    decimalValidation3.ErrorTitle = "The value you entered is not valid";
                    decimalValidation3.Error = "This cell must be a valid positive number.";
                    decimalValidation3.Operator = ExcelDataValidationOperator.greaterThanOrEqual;
                    decimalValidation3.Formula.Value = 0D;

                    var decimalValidation4 = wsSheet1.DataValidations.AddDecimalValidation($"AQ{lineNumber}:AR{lineNumber}");
                    decimalValidation4.ShowErrorMessage = true;
                    decimalValidation4.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                    decimalValidation4.ErrorTitle = "The value you entered is not valid";
                    decimalValidation4.Error = "This cell must be a valid positive number.";
                    decimalValidation4.Operator = ExcelDataValidationOperator.greaterThanOrEqual;
                    decimalValidation4.Formula.Value = 0D;

                    var decimalValidation5 = wsSheet1.DataValidations.AddDecimalValidation($"AW{lineNumber}:AX{lineNumber}");
                    decimalValidation5.ShowErrorMessage = true;
                    decimalValidation5.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                    decimalValidation5.ErrorTitle = "The value you entered is not valid";
                    decimalValidation5.Error = "This cell must be a valid positive number.";
                    decimalValidation5.Operator = ExcelDataValidationOperator.greaterThanOrEqual;
                    decimalValidation5.Formula.Value = 0D;

                    var decimalValidation6 = wsSheet1.DataValidations.AddDecimalValidation($"BI{lineNumber}:BJ{lineNumber}");
                    decimalValidation6.ShowErrorMessage = true;
                    decimalValidation6.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                    decimalValidation6.ErrorTitle = "The value you entered is not valid";
                    decimalValidation6.Error = "This cell must be a valid positive number.";
                    decimalValidation6.Operator = ExcelDataValidationOperator.greaterThanOrEqual;
                    decimalValidation6.Formula.Value = 0D;

                    var decimalValidation7 = wsSheet1.DataValidations.AddDecimalValidation($"BU{lineNumber}:BV{lineNumber}");
                    decimalValidation7.ShowErrorMessage = true;
                    decimalValidation7.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                    decimalValidation7.ErrorTitle = "The value you entered is not valid";
                    decimalValidation7.Error = "This cell must be a valid positive number.";
                    decimalValidation7.Operator = ExcelDataValidationOperator.greaterThanOrEqual;
                    decimalValidation7.Formula.Value = 0D;

                    var decimalValidation8 = wsSheet1.DataValidations.AddDecimalValidation($"CG{lineNumber}:CH{lineNumber}");
                    decimalValidation8.ShowErrorMessage = true;
                    decimalValidation8.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                    decimalValidation8.ErrorTitle = "The value you entered is not valid";
                    decimalValidation8.Error = "This cell must be a valid positive number.";
                    decimalValidation8.Operator = ExcelDataValidationOperator.greaterThanOrEqual;
                    decimalValidation8.Formula.Value = 0D;

                    var decimalValidation9 = wsSheet1.DataValidations.AddDecimalValidation($"CK{lineNumber}:CL{lineNumber}");
                    decimalValidation9.ShowErrorMessage = true;
                    decimalValidation9.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                    decimalValidation9.ErrorTitle = "The value you entered is not valid";
                    decimalValidation9.Error = "This cell must be a valid positive number.";
                    decimalValidation9.Operator = ExcelDataValidationOperator.greaterThanOrEqual;
                    decimalValidation9.Formula.Value = 0D;

                    var decimalValidation10 = wsSheet1.DataValidations.AddDecimalValidation($"CO{lineNumber}:CP{lineNumber}");
                    decimalValidation10.ShowErrorMessage = true;
                    decimalValidation10.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                    decimalValidation10.ErrorTitle = "The value you entered is not valid";
                    decimalValidation10.Error = "This cell must be a valid positive number.";
                    decimalValidation10.Operator = ExcelDataValidationOperator.greaterThanOrEqual;
                    decimalValidation10.Formula.Value = 0D;

                    var decimalValidation11 = wsSheet1.DataValidations.AddDecimalValidation($"CS{lineNumber}:CT{lineNumber}");
                    decimalValidation11.ShowErrorMessage = true;
                    decimalValidation11.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                    decimalValidation11.ErrorTitle = "The value you entered is not valid";
                    decimalValidation11.Error = "This cell must be a valid positive number.";
                    decimalValidation11.Operator = ExcelDataValidationOperator.greaterThanOrEqual;
                    decimalValidation11.Formula.Value = 0D;

                    #endregion Decimal Validation

                    #region Cells Unlock

                    wsSheet1.Cells[$"A{lineNumber}:L{lineNumber}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    wsSheet1.Cells[$"A{lineNumber}:L{lineNumber}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F1F1F1"));
                    wsSheet1.Cells[$"M{lineNumber}:S{lineNumber}"].Style.Locked = false;

                    wsSheet1.Cells[$"T{lineNumber}:Z{lineNumber}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    wsSheet1.Cells[$"T{lineNumber}:Z{lineNumber}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F1F1F1"));
                    wsSheet1.Cells[$"AA{lineNumber}:AD{lineNumber}"].Style.Locked = false;

                    wsSheet1.Cells[$"AE{lineNumber}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    wsSheet1.Cells[$"AE{lineNumber}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F1F1F1"));

                    wsSheet1.Cells[$"AF{lineNumber}:AI{lineNumber}"].Style.Locked = false;
                    wsSheet1.Cells[$"AJ{lineNumber}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    wsSheet1.Cells[$"AJ{lineNumber}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F1F1F1"));

                    wsSheet1.Cells[$"AK{lineNumber}:AN{lineNumber}"].Style.Locked = false;
                    wsSheet1.Cells[$"AO{lineNumber}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    wsSheet1.Cells[$"AO{lineNumber}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F1F1F1"));

                    wsSheet1.Cells[$"AP{lineNumber}:AS{lineNumber}"].Style.Locked = false;
                    wsSheet1.Cells[$"AT{lineNumber}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    wsSheet1.Cells[$"AT{lineNumber}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F1F1F1"));
                    wsSheet1.Cells[$"AU{lineNumber}:CU{lineNumber}"].Style.Locked = false;

                    wsSheet1.Cells[$"CV{lineNumber}:DB{lineNumber}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    wsSheet1.Cells[$"CV{lineNumber}:DB{lineNumber}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F1F1F1"));
                    if (excelRequirement.Status != "STARTED")
                    {
                        wsSheet1.Cells[$"DC{lineNumber}:DD{lineNumber}"].Style.Locked = false;
                    }
                    else
                    {
                        wsSheet1.Cells[$"DC{lineNumber}:DD{lineNumber}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        wsSheet1.Cells[$"DC{lineNumber}:DD{lineNumber}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F1F1F1"));
                    }
        
                    #endregion Cells Unlock

                    lineNumber++;
                }


                const double minWidth = 25.00;
                const double maxWidth = 40.00;

                wsSheet1.Cells["D:E"].AutoFitColumns(minWidth, maxWidth);               
                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }

            #endregion EXCEL_VENDOR_QUOTATION

            return Convert.ToBase64String(ms.ToArray());
        }

        public List<AllReqBiddingDetails> GetAllReqBiddingDetails(int compID,int userID, string from, string to)
        {
            List<AllReqBiddingDetails> details = new List<AllReqBiddingDetails>();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_FROM_DATE", from);
                sd.Add("P_TO_DATE", to);
                DataSet ds = sqlHelper.SelectList("rp_getBiddingReportBtwDates", sd);
                CORE.DataNamesMapper<AllReqBiddingDetails> mapper = new CORE.DataNamesMapper<AllReqBiddingDetails>();
                details = mapper.Map(ds.Tables[0]).ToList();

                foreach (AllReqBiddingDetails item in details)
                {
                    item.ApparelFabric = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ApparelFabric>>(!string.IsNullOrEmpty(item.FABRIC_JSON) ? item.FABRIC_JSON : string.Empty);
                    item.ApparelProcess = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ApparelProcess>>(!string.IsNullOrEmpty(item.PROCESS_JSON) ? item.PROCESS_JSON : string.Empty);
                    item.ApparelSundries = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ApparelSundries>>(!string.IsNullOrEmpty(item.ACCESSORIES_JSON) ? item.ACCESSORIES_JSON : string.Empty);
                }
            }
            catch (Exception ex)
            {
                //AllReqBiddingDetails error = new AllReqBiddingDetails();
                // details[0].ErrorMessage = ex.Message;
            }

            return details;
        }

        public ExcelRequirement GetReqReportForExcel(int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            ExcelRequirement excelrequirement = new ExcelRequirement();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID, null);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("rp_GetReqReportForExcel", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[3].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[3].Rows[0];
                    excelrequirement.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                    excelrequirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                    excelrequirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.UtcNow;
                    excelrequirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.UtcNow;
                    excelrequirement.CurrentDate = prmservices.GetDate();
                    excelrequirement.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                    excelrequirement.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                    excelrequirement.IS_CB_ENABLED = row["IS_CB_ENABLED"] != DBNull.Value ? Convert.ToInt32(row["IS_CB_ENABLED"]) : 0;
                    excelrequirement.IS_CB_COMPLETED = row["IS_CB_COMPLETED"] != DBNull.Value ? Convert.ToInt32(row["IS_CB_COMPLETED"]) : 0;
                    excelrequirement.PostedByFirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                    excelrequirement.PostedByLastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                    excelrequirement.Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                    excelrequirement.IsTechScoreReq = row["TECH_SCORE_REQ"] != DBNull.Value ? Convert.ToInt16(row["TECH_SCORE_REQ"]) : 0;
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[4].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[4].Rows[0];
                    excelrequirement.PreSavings = row["PRE_SAVING"] != DBNull.Value ? Convert.ToDouble(row["PRE_SAVING"]) : 0;
                    excelrequirement.PostSavings = row["REV_SAVING"] != DBNull.Value ? Convert.ToDouble(row["REV_SAVING"]) : 0;
                }

                List<ExcelVendorDetails> ListRV = new List<ExcelVendorDetails>();
                int rank = 1;
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ExcelVendorDetails RV = new ExcelVendorDetails();

                        #region VENDOR DETAILS
                        RV.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                        RV.TechnicalScore = row["TECHNICAL_SCORE"] != DBNull.Value ? Convert.ToDecimal(row["TECHNICAL_SCORE"]) : 0;
                        RV.VendorName = row["VENDOR_NAME"] != DBNull.Value ? Convert.ToString(row["VENDOR_NAME"]) : string.Empty;
                        RV.VendorID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        RV.VendorFreight = row["VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row["VEND_FREIGHT"]) : 0;
                        RV.RevVendorFreight = row["REV_VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_FREIGHT"]) : 0;
                        RV.TotalInitialPrice = row["VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row["VEND_TOTAL_PRICE"]) : 0;
                        RV.RevVendorTotalPrice = row["REV_VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_TOTAL_PRICE"]) : 0;
                        RV.Warranty = row["WARRANTY"] != DBNull.Value ? Convert.ToString(row["WARRANTY"]) : string.Empty;
                        RV.Validity = row["VALIDITY"] != DBNull.Value ? Convert.ToString(row["VALIDITY"]) : string.Empty;
                        RV.OtherProperties = row["OTHER_PROPERTIES"] != DBNull.Value ? Convert.ToString(row["OTHER_PROPERTIES"]) : string.Empty;
                        RV.Duration = row["DURATION"] != DBNull.Value ? Convert.ToString(row["DURATION"]) : string.Empty;
                        RV.Payment = row["PAYMENT"] != DBNull.Value ? Convert.ToString(row["PAYMENT"]) : string.Empty;
                        RV.GSTNumber = row["GST_NUMBER"] != DBNull.Value ? Convert.ToString(row["GST_NUMBER"]) : string.Empty;
                        RV.SelectedVendorCurrency = row["SELECTED_VENDOR_CURRENCY"] != DBNull.Value ? Convert.ToString(row["SELECTED_VENDOR_CURRENCY"]) : string.Empty;

                        
                        RV.FREIGHT_CHARGES = row["FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["FREIGHT_CHARGES"]) : 0; //*
                        RV.FREIGHT_CHARGES_TAX_PERCENTAGE = row["FREIGHT_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row["FREIGHT_CHARGES_TAX_PERCENTAGE"]) : 0;
                        RV.FREIGHT_CHARGES_WITH_TAX = row["FREIGHT_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["FREIGHT_CHARGES_WITH_TAX"]) : 0;
                        RV.REV_FREIGHT_CHARGES = row["REV_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["REV_FREIGHT_CHARGES"]) : 0;
                        RV.REV_FREIGHT_CHARGES_WITH_TAX = row["REV_FREIGHT_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["REV_FREIGHT_CHARGES_WITH_TAX"]) : 0; //*
                        RV.RevVendorFreightCB = row["REV_FREIGHT_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_FREIGHT_CHARGES_CB"]) : 0;

                        RV.InstallationCharges = row["INSTALLATION_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["INSTALLATION_CHARGES"]) : 0; //*
                        RV.InstallationChargesTaxPercentage = row["INSTALLATION_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row["INSTALLATION_CHARGES_TAX_PERCENTAGE"]) : 0;
                        RV.InstallationChargesWithTax = row["INSTALLATION_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["INSTALLATION_CHARGES_WITH_TAX"]) : 0;
                        RV.RevinstallationCharges = row["REV_INSTALLATION_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["REV_INSTALLATION_CHARGES"]) : 0; //*
                        RV.RevinstallationChargesWithTax = row["REV_INSTALLATION_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["REV_INSTALLATION_CHARGES_WITH_TAX"]) : 0;
                        RV.RevinstallationChargesCB = row["REV_INSTALLATION_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_INSTALLATION_CHARGES_CB"]) : 0;
                        RV.RevinstallationChargesWithTaxCB = RV.RevinstallationChargesCB + ((RV.RevinstallationChargesCB / 100) * (RV.InstallationChargesTaxPercentage));
                        
                        RV.PackingCharges = row["PACKING_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["PACKING_CHARGES"]) : 0; //*
                        RV.PackingChargesTaxPercentage = row["PACKING_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row["PACKING_CHARGES_TAX_PERCENTAGE"]) : 0;
                        RV.PackingChargesWithTax = row["PACKING_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["PACKING_CHARGES_WITH_TAX"]) : 0;
                        RV.RevpackingCharges = row["REV_PACKING_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["REV_PACKING_CHARGES"]) : 0; //*
                        RV.RevpackingChargesWithTax = row["REV_PACKING_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["REV_PACKING_CHARGES_WITH_TAX"]) : 0;
                        RV.RevpackingChargesCB = row["REV_PACKING_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_PACKING_CHARGES_CB"]) : 0;
                        RV.RevpackingChargesWithTaxCB = RV.RevpackingChargesCB + ((RV.RevpackingChargesCB / 100) * (RV.PackingChargesTaxPercentage));

                        RV.RevVendorTotalPriceCB = row["REV_VEND_TOTAL_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_TOTAL_PRICE_CB"]) : 0;

                        RV.INCO_TERMS = row["INCO_TERMS"] != DBNull.Value ? Convert.ToString(row["INCO_TERMS"]) : string.Empty;
                        RV.PayLoadFactor = 0;
                        RV.RevPayLoadFactor = 0;
                        RV.RevChargeAny = 0;
                        RV.IsQuotationRejected = row["IS_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row["IS_QUOTATION_REJECTED"]) : 0;
                        RV.IsRevQuotationRejected = row["IS_REV_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row["IS_REV_QUOTATION_REJECTED"]) : 0;
                        RV.VendorCurrencyFactor = row["CURRENCY_FACTOR"] != DBNull.Value ? Convert.ToDecimal(row["CURRENCY_FACTOR"]) : 0;

                        #endregion VENDOR DETAILS

                        ListRV.Add(RV);
                    }

                    foreach (ExcelVendorDetails vendor in ListRV.Where(v => v.IsQuotationRejected == 0).OrderBy(v => v.TotalInitialPrice).ToList())
                    {
                        vendor.Rank = ListRV.IndexOf(vendor) + 1;
                    }
                }

                List<ExcelRequirementItems> ListRITemp = new List<ExcelRequirementItems>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[1].Rows)
                    {
                        ExcelRequirementItems RI = new ExcelRequirementItems();
                        #region ITEM DETAILS
                        RI.ArticleType = row["ARTICLE_TYPE"] != DBNull.Value ? Convert.ToString(row["ARTICLE_TYPE"]) : string.Empty;
                        RI.ItemID = row["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row["ITEM_ID"]) : 0;
                        RI.ProductIDorName = row["PROD_ID"] != DBNull.Value ? Convert.ToString(row["PROD_ID"]) : string.Empty;
                        RI.ProductNo = row["PROD_NO"] != DBNull.Value ? Convert.ToString(row["PROD_NO"]) : string.Empty;
                        RI.ProductCode = row["PROD_CODE"] != DBNull.Value ? Convert.ToString(row["PROD_CODE"]) : string.Empty;
                        RI.HsnCode = row["HSN_CODE"] != DBNull.Value ? Convert.ToString(row["HSN_CODE"]) : string.Empty;
                        RI.ProductDescription = row["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row["DESCRIPTION"]) : string.Empty;
                        RI.ProductQuantity = row["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row["QUANTITY"]) : 0;
                        RI.ProductQuantityIn = row["QUANTITY_IN"] != DBNull.Value ? Convert.ToString(row["QUANTITY_IN"]) : string.Empty;
                        RI.ProductBrand = row["BRAND"] != DBNull.Value ? Convert.ToString(row["BRAND"]) : string.Empty;
                        RI.OthersBrands = row["OTHER_BRAND"] != DBNull.Value ? Convert.ToString(row["OTHER_BRAND"]) : string.Empty;
                        RI.ItemLastPrice = row["LAST_ITEM_PRICE"] != DBNull.Value ? Convert.ToDouble(row["LAST_ITEM_PRICE"]) : 0;
                        RI.I_LLP_DETAILS = row["I_LLP_DETAILS"] != DBNull.Value ? Convert.ToString(row["I_LLP_DETAILS"]) : string.Empty;
                        RI.ITEM_L1_PRICE = row["ITEM_L1_PRICE"] != DBNull.Value ? Convert.ToDouble(row["ITEM_L1_PRICE"]) : 0;
                        RI.ITEM_L1_COMPANY_NAME = row["ITEM_L1_COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["ITEM_L1_COMPANY_NAME"]) : string.Empty;
                        RI.ITEM_L2_PRICE = row["ITEM_L2_PRICE"] != DBNull.Value ? Convert.ToDouble(row["ITEM_L2_PRICE"]) : 0;
                        RI.ITEM_L2_COMPANY_NAME = row["ITEM_L2_COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["ITEM_L2_COMPANY_NAME"]) : string.Empty;
                        RI.ProductQuotationTemplateJson = row["PRODUCT_QUOTATION_JSON"] != DBNull.Value ? Convert.ToString(row["PRODUCT_QUOTATION_JSON"]) : string.Empty;
                        RI.SEASON = row["SEASON"] != DBNull.Value ? Convert.ToString(row["SEASON"]) : string.Empty;
                        RI.LeadTime = row["LEAD_TIME"] != DBNull.Value ? Convert.ToString(row["LEAD_TIME"]) : string.Empty;
                        RI.IsCore = row["IS_CORE"] != DBNull.Value ? Convert.ToInt32(row["IS_CORE"]) : 0;
                        RI.QtyDistributed = 0;
                        RI.CatalogueItemID = row["CATALOGUE_ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row["CATALOGUE_ITEM_ID"]) : 0;
                        RI.QCS_REMARKS = row["QCS_REMARKS"] != DBNull.Value ? Convert.ToString(row["QCS_REMARKS"]) : string.Empty;
                        RI.QCS_APPROVE_STATUS = row["QCS_APPROVE_STATUS"] != DBNull.Value ? Convert.ToString(row["QCS_APPROVE_STATUS"]) : string.Empty;
                        RI.GENDER = row["GENDER"] != DBNull.Value ? Convert.ToString(row["GENDER"]) : string.Empty;
                        RI.BuyerTargetCost = row["BUYER_TARGET_COST"] != DBNull.Value ? Convert.ToDouble(row["BUYER_TARGET_COST"]) : 0;
                        if (!string.IsNullOrEmpty(RI.ProductQuotationTemplateJson))
                        {
                            try
                            {
                                RI.ProductQuotationTemplateArray = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CATALOG.ProductQuotationTemplate>>(RI.ProductQuotationTemplateJson);
                            }
                            catch
                            {

                            }
                        }

                        RI.ReqVendors = ListRV.Select(item => (ExcelVendorDetails)item.Clone()).ToList().ToArray();
                        #endregion ITEM DETAILS

                        ListRITemp.Add(RI);
                    }
                }

                List<ExcelQuotationPrices> ListQ = new List<ExcelQuotationPrices>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[2].Rows)
                    {
                        ExcelQuotationPrices Q = new ExcelQuotationPrices();

                        #region QUOTATION DETAILS
                        Q.VendorID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        Q.ItemID = row["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row["ITEM_ID"]) : 0;
                        Q.UnitPrice = row["UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row["UNIT_PRICE"]) : 0;
                        Q.RevUnitPrice = row["REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_UNIT_PRICE"]) : 0;
                        Q.CGst = row["C_GST"] != DBNull.Value ? Convert.ToDouble(row["C_GST"]) : 0;
                        Q.SGst = row["S_GST"] != DBNull.Value ? Convert.ToDouble(row["S_GST"]) : 0;
                        Q.IGst = row["I_GST"] != DBNull.Value ? Convert.ToDouble(row["I_GST"]) : 0;
                        Q.ItemPrice = row["PRICE"] != DBNull.Value ? Convert.ToDouble(row["PRICE"]) : 0;
                        Q.RevItemPrice = row["REVICED_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REVICED_PRICE"]) : 0;
                        Q.QCSAllocatedQuantity = row["QCS_ALLOCATED_QTY"] != DBNull.Value ? Convert.ToDouble(row["QCS_ALLOCATED_QTY"]) : 0;
                        Q.IsRegret = row["IS_REGRET"] != DBNull.Value ? (Convert.ToInt32(row["IS_REGRET"]) > 0 ? true : false) : false;
                        Q.RegretComments = row["REGRET_COMMENTS"] != DBNull.Value ? Convert.ToString(row["REGRET_COMMENTS"]) : string.Empty;
                        Q.ItemLevelInitialComments = row["ITEM_LEVEL_INITIALCOMMENT"] != DBNull.Value ? Convert.ToString(row["ITEM_LEVEL_INITIALCOMMENT"]) : string.Empty;
                        Q.ItemLevelRevComments = row["ITEM_LEVEL_REVCOMMENT"] != DBNull.Value ? Convert.ToString(row["ITEM_LEVEL_REVCOMMENT"]) : string.Empty;
                        Q.VendorUnits = row["VENDOR_UNITS"] != DBNull.Value ? Convert.ToString(row["VENDOR_UNITS"]) : string.Empty;
                        Q.VendorBrand = row["BRAND"] != DBNull.Value ? Convert.ToString(row["BRAND"]) : string.Empty;
                        Q.ItemFreightCharges = row["ITEM_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["ITEM_FREIGHT_CHARGES"]) : 0;
                        Q.ItemRevFreightCharges = row["ITEM_REV_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["ITEM_REV_FREIGHT_CHARGES"]) : 0;
                        Q.ItemFreightTAX = row["ITEM_FREIGHT_TAX"] != DBNull.Value ? Convert.ToDouble(row["ITEM_FREIGHT_TAX"]) : 0;
                        Q.RevItemPriceCB = row["REVICED_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row["REVICED_PRICE_CB"]) : 0;
                        Q.RevUnitPriceCB = row["REV_UNIT_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_UNIT_PRICE_CB"]) : 0;
                        Q.ProductQuotationTemplateJson = row["ProductQuotationTemplateJson"] != DBNull.Value ? Convert.ToString(row["ProductQuotationTemplateJson"]) : string.Empty;
                        Q.ApparelFabricJson = row["FABRIC_JSON"] != DBNull.Value ? Convert.ToString(row["FABRIC_JSON"]) : string.Empty;
                        Q.ApparelProcessJson = row["PROCESS_JSON"] != DBNull.Value ? Convert.ToString(row["PROCESS_JSON"]) : string.Empty;
                        Q.ApparelSundriesJson = row["ACCESSORIES_JSON"] != DBNull.Value ? Convert.ToString(row["ACCESSORIES_JSON"]) : string.Empty;

                        Q.CutMake = row["CUT_MAKE"] != DBNull.Value ? Convert.ToDouble(row["CUT_MAKE"]) : 0;
                        Q.RejectionPercentage = row["REJECTION_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row["REJECTION_PERCENTAGE"]) : 0;
                        Q.Testing = row["TESTING"] != DBNull.Value ? Convert.ToDouble(row["TESTING"]) : 0;
                        Q.MarginPercentage = row["MARGIN_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row["MARGIN_PERCENTAGE"]) : 0;
                        Q.FinanceCharges = row["FINANCE_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["FINANCE_CHARGES"]) : 0;
                        Q.FreightCharges = row["FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["FREIGHT_CHARGES"]) : 0;
                        Q.ChaCharges = row["CHA_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["CHA_CHARGES"]) : 0;
                        
                        Q.RevCutMake = row["REV_CUT_MAKE"] != DBNull.Value ? Convert.ToDouble(row["REV_CUT_MAKE"]) : 0;
                        Q.RevRejectionPercentage = row["REV_REJECTION_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row["REV_REJECTION_PERCENTAGE"]) : 0;
                        Q.RevTesting = row["REV_TESTING"] != DBNull.Value ? Convert.ToDouble(row["REV_TESTING"]) : 0;
                        Q.RevMarginPercentage = row["REV_MARGIN_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row["REV_MARGIN_PERCENTAGE"]) : 0;
                        Q.RevFinanceCharges = row["REV_FINANCE_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["REV_FINANCE_CHARGES"]) : 0;
                        Q.RevFreightCharges = row["REV_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["REV_FREIGHT_CHARGES"]) : 0;
                        Q.RevChaCharges = row["REV_CHA_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["REV_CHA_CHARGES"]) : 0;

                        if (!string.IsNullOrEmpty(Q.ProductQuotationTemplateJson))
                        {
                            try
                            {
                                Q.ProductQuotationTemplateArray = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CATALOG.ProductQuotationTemplate>>(Q.ProductQuotationTemplateJson);
                            }
                            catch
                            {

                            }
                            
                        }
                        if (!string.IsNullOrEmpty(Q.ApparelFabricJson))
                        {
                            try
                            {
                                Q.ApparelFabric = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ApparelFabric>>(Q.ApparelFabricJson);
                            }
                            catch{}
                        }
                        if (!string.IsNullOrEmpty(Q.ApparelProcessJson))
                        {
                            try
                            {
                                Q.ApparelProcess = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ApparelProcess>>(Q.ApparelProcessJson);
                            }
                            catch { }
                        }
                        if (!string.IsNullOrEmpty(Q.ApparelSundriesJson))
                        {
                            try
                            {
                                Q.ApparelSundries = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ApparelSundries>>(Q.ApparelSundriesJson);
                            }
                            catch { }
                        }


                        // ITEM LEVEL RANKING //
                        int is_quotation_rejected = row["IS_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row["IS_QUOTATION_REJECTED"]) : 0;
                        Q.ItemRank = row["ITEM_RANK"] != DBNull.Value ? Convert.ToInt32(row["ITEM_RANK"]) : 0;
                        //if (is_quotation_rejected <= 0 && Q.RevUnitPrice > 0)
                        //{
                        //    Q.ItemRank = 1;// row2["ITEM_RANK"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_RANK"]) : 0;
                        //}
                        // ITEM LEVEL RANKING //

                        #endregion QUOTATION DETAILS

                        ListQ.Add(Q);

                    }
                }

                // ITEM LEVEL RANKING //
                //ListQ = ListQ.OrderBy(q => q.ItemID).ThenByDescending(q => q.ItemRank).ThenBy(q => q.RevUnitPrice).ToList();
                //int rank1 = 1;
                //int prevItemId = 0;
                //double prevRevUnitPrice = 0;
                //foreach (var temp in ListQ)
                //{
                //    if (temp.ItemRank > 0)
                //    {
                //        if (prevItemId == temp.ItemID && temp.RevUnitPrice > 0 && prevRevUnitPrice == temp.RevUnitPrice)
                //        {
                //            temp.ItemRank = rank1;
                //        }

                //        if (prevItemId == temp.ItemID && temp.RevUnitPrice > 0 && temp.RevUnitPrice > prevRevUnitPrice)
                //        {
                //            temp.ItemRank = rank1 + 1;
                //            rank1 = rank1 + 1;
                //        }

                //        if (prevItemId != temp.ItemID)
                //        {
                //            temp.ItemRank = 1;
                //            rank1 = 1;
                //        }
                //    }

                //    prevItemId = temp.ItemID;
                //    prevRevUnitPrice = temp.RevUnitPrice;
                //}
                // ITEM LEVEL RANKING //

                for (int i = 0; i < ListRITemp.Count; i++)
                {
                    for (int v = 0; v < ListRITemp[i].ReqVendors.Length; v++)
                    {
                        ListRITemp[i].ReqVendors[v].QuotationPrices = new ExcelQuotationPrices();
                        try
                        {
                            ListRITemp[i].ReqVendors[v].QuotationPrices = ListQ.Where(quotation => quotation.VendorID == ListRITemp[i].ReqVendors[v].VendorID && quotation.ItemID == ListRITemp[i].ItemID).FirstOrDefault();
                        }
                        catch
                        {
                            ListRITemp[i].ReqVendors[v].QuotationPrices = new ExcelQuotationPrices();
                        }
                    }
                }

                excelrequirement.ReqItems = ListRITemp.ToArray();

                foreach (ExcelRequirementItems item in excelrequirement.ReqItems)
                {
                    if (item.ReqVendors != null)
                    {
                        List<ExcelVendorDetails> vendors = new List<ExcelVendorDetails>();
                        vendors = item.ReqVendors.Where(v => v.RevVendorTotalPrice > 0).OrderBy(v => v.RevVendorTotalPrice).ToList();
                        item.ReqVendors = vendors.ToArray();
                    }
                }

            }
            catch (Exception ex)
            {
                excelrequirement.ErrorMessage = ex.Message;
            }

            return excelrequirement;
        }

        //public MACRequirement GetReqItemWiseVendors(int reqID, string sessionID)
        //{
        //    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
        //    MACRequirement excelrequirement = new MACRequirement();
        //    try
        //    {
        //        int isValidSession = Utilities.ValidateSession(sessionID, null);
        //        sd.Add("P_REQ_ID", reqID);
        //        DataSet ds = sqlHelper.SelectList("rp_GetReqReportForExcel", sd);
        //        if (ds != null && ds.Tables.Count > 0 && ds.Tables[3].Rows.Count > 0)
        //        {
        //            DataRow row = ds.Tables[3].Rows[0];
        //            excelrequirement.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
        //            excelrequirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
        //            excelrequirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.UtcNow;
        //            excelrequirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.UtcNow;
        //            excelrequirement.CurrentDate = prmservices.GetDate();
        //            excelrequirement.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
        //            excelrequirement.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
        //            excelrequirement.IS_CB_ENABLED = row["IS_CB_ENABLED"] != DBNull.Value ? Convert.ToInt32(row["IS_CB_ENABLED"]) : 0;
        //            excelrequirement.IS_CB_COMPLETED = row["IS_CB_COMPLETED"] != DBNull.Value ? Convert.ToInt32(row["IS_CB_COMPLETED"]) : 0;
        //            excelrequirement.PostedByFirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
        //            excelrequirement.PostedByLastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
        //        }

        //        if (ds != null && ds.Tables.Count > 0 && ds.Tables[4].Rows.Count > 0)
        //        {
        //            DataRow row = ds.Tables[4].Rows[0];
        //            excelrequirement.PreSavings = row["PRE_SAVING"] != DBNull.Value ? Convert.ToDouble(row["PRE_SAVING"]) : 0;
        //            excelrequirement.PostSavings = row["REV_SAVING"] != DBNull.Value ? Convert.ToDouble(row["REV_SAVING"]) : 0;
        //        }

        //        List<MACVendorDetails> ListRV = new List<MACVendorDetails>();
        //        int rank = 1;
        //        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        //        {
        //            foreach (DataRow row in ds.Tables[0].Rows)
        //            {
        //                MACVendorDetails RV = new MACVendorDetails();

        //                #region VENDOR DETAILS
        //                RV.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
        //                RV.VendorName = row["VENDOR_NAME"] != DBNull.Value ? Convert.ToString(row["VENDOR_NAME"]) : string.Empty;
        //                RV.VendorID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
        //                RV.VendorFreight = row["VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row["VEND_FREIGHT"]) : 0;
        //                RV.RevVendorFreight = row["REV_VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_FREIGHT"]) : 0;
        //                RV.TotalInitialPrice = row["VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row["VEND_TOTAL_PRICE"]) : 0;
        //                RV.RevVendorTotalPrice = row["REV_VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_TOTAL_PRICE"]) : 0;
        //                RV.Warranty = row["WARRANTY"] != DBNull.Value ? Convert.ToString(row["WARRANTY"]) : string.Empty;
        //                RV.Validity = row["VALIDITY"] != DBNull.Value ? Convert.ToString(row["VALIDITY"]) : string.Empty;
        //                RV.OtherProperties = row["OTHER_PROPERTIES"] != DBNull.Value ? Convert.ToString(row["OTHER_PROPERTIES"]) : string.Empty;
        //                RV.Duration = row["DURATION"] != DBNull.Value ? Convert.ToString(row["DURATION"]) : string.Empty;
        //                RV.Payment = row["PAYMENT"] != DBNull.Value ? Convert.ToString(row["PAYMENT"]) : string.Empty;
        //                RV.GSTNumber = row["GST_NUMBER"] != DBNull.Value ? Convert.ToString(row["GST_NUMBER"]) : string.Empty;
        //                RV.SelectedVendorCurrency = row["SELECTED_VENDOR_CURRENCY"] != DBNull.Value ? Convert.ToString(row["SELECTED_VENDOR_CURRENCY"]) : string.Empty;

        //                RV.PackingChargesTaxPercentage = row["PACKING_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row["PACKING_CHARGES_TAX_PERCENTAGE"]) : 0;
        //                RV.PackingChargesWithTax = row["PACKING_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["PACKING_CHARGES_WITH_TAX"]) : 0;
        //                RV.RevpackingChargesWithTax = row["REV_PACKING_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["REV_PACKING_CHARGES_WITH_TAX"]) : 0;
        //                RV.RevpackingChargesCB = row["REV_PACKING_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_PACKING_CHARGES_CB"]) : 0;
        //                RV.RevpackingChargesWithTaxCB = RV.RevpackingChargesCB + ((RV.RevpackingChargesCB / 100) * (RV.PackingChargesTaxPercentage));

        //                RV.InstallationChargesTaxPercentage = row["INSTALLATION_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row["INSTALLATION_CHARGES_TAX_PERCENTAGE"]) : 0;
        //                RV.InstallationChargesWithTax = row["INSTALLATION_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["INSTALLATION_CHARGES_WITH_TAX"]) : 0;
        //                RV.RevinstallationChargesWithTax = row["REV_INSTALLATION_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["REV_INSTALLATION_CHARGES_WITH_TAX"]) : 0;
        //                RV.RevinstallationChargesCB = row["REV_INSTALLATION_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_INSTALLATION_CHARGES_CB"]) : 0;
        //                RV.RevinstallationChargesWithTaxCB = RV.RevinstallationChargesCB + ((RV.RevinstallationChargesCB / 100) * (RV.InstallationChargesTaxPercentage));

        //                RV.FREIGHT_CHARGES_TAX_PERCENTAGE = row["FREIGHT_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row["FREIGHT_CHARGES_TAX_PERCENTAGE"]) : 0;
        //                RV.FREIGHT_CHARGES = row["FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["FREIGHT_CHARGES"]) : 0;
        //                RV.FREIGHT_CHARGES_WITH_TAX = row["FREIGHT_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["FREIGHT_CHARGES_WITH_TAX"]) : 0;
        //                RV.REV_FREIGHT_CHARGES = row["REV_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["REV_FREIGHT_CHARGES"]) : 0;
        //                RV.REV_FREIGHT_CHARGES_WITH_TAX = row["REV_FREIGHT_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["REV_FREIGHT_CHARGES_WITH_TAX"]) : 0;
        //                RV.REV_FREIGHT_CHARGES_WITH_TAX = row["REV_FREIGHT_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["REV_FREIGHT_CHARGES_WITH_TAX"]) : 0;
        //                RV.RevVendorFreightCB = row["REV_FREIGHT_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_FREIGHT_CHARGES_CB"]) : 0;

        //                RV.RevVendorTotalPriceCB = row["REV_VEND_TOTAL_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_TOTAL_PRICE_CB"]) : 0;

        //                #endregion VENDOR DETAILS

        //                ListRV.Add(RV);
        //            }

        //            foreach (MACVendorDetails vendor in ListRV.Where(v => v.IsQuotationRejected == 0).OrderBy(v => v.TotalInitialPrice).ToList())
        //            {
        //                vendor.Rank = ListRV.IndexOf(vendor) + 1;
        //            }
        //        }

        //        List<MACRequirementItems> ListRITemp = new List<MACRequirementItems>();
        //        if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
        //        {
        //            foreach (DataRow row in ds.Tables[1].Rows)
        //            {
        //                MACRequirementItems RI = new MACRequirementItems();
        //                #region ITEM DETAILS
        //                RI.ItemID = row["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row["ITEM_ID"]) : 0;
        //                RI.ProductIDorName = row["PROD_ID"] != DBNull.Value ? Convert.ToString(row["PROD_ID"]) : string.Empty;
        //                RI.ProductNo = row["PROD_NO"] != DBNull.Value ? Convert.ToString(row["PROD_NO"]) : string.Empty;
        //                RI.ProductDescription = row["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row["DESCRIPTION"]) : string.Empty;
        //                RI.ProductQuantity = row["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row["QUANTITY"]) : 0;
        //                RI.ProductQuantityIn = row["QUANTITY_IN"] != DBNull.Value ? Convert.ToString(row["QUANTITY_IN"]) : string.Empty;
        //                RI.ProductBrand = row["BRAND"] != DBNull.Value ? Convert.ToString(row["BRAND"]) : string.Empty;
        //                RI.OthersBrands = row["OTHER_BRAND"] != DBNull.Value ? Convert.ToString(row["OTHER_BRAND"]) : string.Empty;
        //                RI.ItemLastPrice = row["LAST_ITEM_PRICE"] != DBNull.Value ? Convert.ToDouble(row["LAST_ITEM_PRICE"]) : 0;

        //                RI.ReqVendors = ListRV.Select(item => (MACVendorDetails)item.Clone()).ToList().ToArray();
        //                #endregion ITEM DETAILS

        //                ListRITemp.Add(RI);
        //            }
        //        }

        //        List<MACQuotationPrices> ListQ = new List<MACQuotationPrices>();
        //        if (ds != null && ds.Tables.Count > 0 && ds.Tables[2].Rows.Count > 0)
        //        {
        //            foreach (DataRow row in ds.Tables[2].Rows)
        //            {
        //                MACQuotationPrices Q = new MACQuotationPrices();

        //                #region QUOTATION DETAILS
        //                Q.VendorID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
        //                Q.ItemID = row["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row["ITEM_ID"]) : 0;
        //                Q.UnitPrice = row["UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row["UNIT_PRICE"]) : 0;
        //                Q.RevUnitPrice = row["REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_UNIT_PRICE"]) : 0;
        //                Q.CGst = row["C_GST"] != DBNull.Value ? Convert.ToDouble(row["C_GST"]) : 0;
        //                Q.SGst = row["S_GST"] != DBNull.Value ? Convert.ToDouble(row["S_GST"]) : 0;
        //                Q.IGst = row["I_GST"] != DBNull.Value ? Convert.ToDouble(row["I_GST"]) : 0;
        //                Q.ItemPrice = row["PRICE"] != DBNull.Value ? Convert.ToDouble(row["PRICE"]) : 0;
        //                Q.RevItemPrice = row["REVICED_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REVICED_PRICE"]) : 0;
        //                Q.IsRegret = row["IS_REGRET"] != DBNull.Value ? (Convert.ToInt32(row["IS_REGRET"]) > 0 ? true : false) : false;
        //                Q.RegretComments = row["REGRET_COMMENTS"] != DBNull.Value ? Convert.ToString(row["REGRET_COMMENTS"]) : string.Empty;
        //                Q.ItemLevelInitialComments = row["ITEM_LEVEL_INITIALCOMMENT"] != DBNull.Value ? Convert.ToString(row["ITEM_LEVEL_INITIALCOMMENT"]) : string.Empty;
        //                Q.ItemLevelRevComments = row["ITEM_LEVEL_REVCOMMENT"] != DBNull.Value ? Convert.ToString(row["ITEM_LEVEL_REVCOMMENT"]) : string.Empty;
        //                Q.VendorUnits = row["VENDOR_UNITS"] != DBNull.Value ? Convert.ToString(row["VENDOR_UNITS"]) : string.Empty;
        //                Q.VendorBrand = row["BRAND"] != DBNull.Value ? Convert.ToString(row["BRAND"]) : string.Empty;
        //                Q.ItemFreightCharges = row["ITEM_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["ITEM_FREIGHT_CHARGES"]) : 0;
        //                Q.ItemRevFreightCharges = row["ITEM_REV_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["ITEM_REV_FREIGHT_CHARGES"]) : 0;
        //                Q.ItemFreightTAX = row["ITEM_FREIGHT_TAX"] != DBNull.Value ? Convert.ToDouble(row["ITEM_FREIGHT_TAX"]) : 0;
        //                Q.RevItemPriceCB = row["REVICED_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row["REVICED_PRICE_CB"]) : 0;
        //                Q.RevUnitPriceCB = row["REV_UNIT_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_UNIT_PRICE_CB"]) : 0;
        //                #endregion QUOTATION DETAILS

        //                ListQ.Add(Q);

        //            }
        //        }

        //        for (int i = 0; i < ListRITemp.Count; i++)
        //        {
        //            for (int v = 0; v < ListRITemp[i].ReqVendors.Length; v++)
        //            {
        //                ListRITemp[i].ReqVendors[v].QuotationPrices = new MACQuotationPrices();
        //                try
        //                {
        //                    ListRITemp[i].ReqVendors[v].QuotationPrices = ListQ.Where(quotation => quotation.VendorID == ListRITemp[i].ReqVendors[v].VendorID && quotation.ItemID == ListRITemp[i].ItemID).FirstOrDefault();
        //                }
        //                catch
        //                {
        //                    ListRITemp[i].ReqVendors[v].QuotationPrices = new MACQuotationPrices();
        //                }
        //            }
        //        }

        //        excelrequirement.ReqItems = ListRITemp.ToArray();
        //        excelrequirement.ReqVendors = ListRV.ToArray();


        //        //foreach (ExcelRequirementItems item in excelrequirement.ReqItems) {
        //        //    if (item.ReqVendors != null)
        //        //    {
        //        //        List<ExcelVendorDetails> vendors = new List<ExcelVendorDetails>();
        //        //        vendors = item.ReqVendors.OrderBy(v => v.RevVendorTotalPrice).ToList();
        //        //        item.ReqVendors = vendors.ToArray();
        //        //    }
        //        //}

        //    }
        //    catch (Exception ex)
        //    {
        //        excelrequirement.ErrorMessage = ex.Message;
        //    }

        //    return excelrequirement;
        //}

        public List<ConsolidatedReport> GetConsolidatedReports(string sessionID, string from, string to, int userID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<ConsolidatedReport> consolidatedReports = new List<ConsolidatedReport>();
            List<RequirementUsersByRank> revisedReport = new List<RequirementUsersByRank>();
            List<RequirementUsersByRank> initialReport = new List<RequirementUsersByRank>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID, null);
                if (string.IsNullOrEmpty(from))
                {
                    //from = DateTime.MinValue.ToString();
                    from = DateTime.UtcNow.AddDays(-30).ToString();
                }

                if (string.IsNullOrEmpty(to))
                {
                    //to = DateTime.MaxValue.ToString();
                    to = DateTime.UtcNow.ToString();
                }

                //DateTime fromDate = Convert.ToDateTime(from);
                //DateTime toDate = Convert.ToDateTime(to);

                //sd.Add("P_FROM_DATE", Convert.ToDateTime(from).ToUniversalTime());
                //sd.Add("P_TO_DATE", Convert.ToDateTime(to).ToUniversalTime());

                sd.Add("P_FROM_DATE", from);
                sd.Add("P_TO_DATE", to);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("rp_GetConsolidatedReports", sd);


                #region Revised Details (Table 1)

                //List<RevisedDetails> revisedDetails = new List<RevisedDetails>();

                if (ds.Tables[1].Rows.Count > 0)
                {
                    foreach (DataRow row1 in ds.Tables[1].Rows)
                    {
                        RequirementUsersByRank revised = new RequirementUsersByRank();
                        revised.UID = row1["U_ID"] != DBNull.Value ? Convert.ToInt32(row1["U_ID"]) : 0;
                        revised.RequirementID = row1["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row1["REQ_ID"]) : 0;
                        revised.RevBasePrice = row1["REV_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["REV_BASE_PRICE"]) : 0;
                        revised.InitBasePrice = row1["INIT_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["INIT_BASE_PRICE"]) : 0;
                        revised.RevVendTotalPrice = row1["REV_VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["REV_VEND_TOTAL_PRICE"]) : 0;
                        revised.Rank = row1["RN"] != DBNull.Value ? Convert.ToInt32(row1["RN"]) : 0;
                        revised.Quantity = row1["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row1["QUANTITY"]) : 0;
                        revised.CompanyName = row1["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row1["COMPANY_NAME"]) : string.Empty;
                        revised.SelectedVendorCurrency = row1["SELECTED_VENDOR_CURRENCY"] != DBNull.Value ? Convert.ToString(row1["SELECTED_VENDOR_CURRENCY"]) : string.Empty;

                        revisedReport.Add(revised);
                    }
                }

                #endregion Revised Details

                #region initial details (table 2)

                // list<initialdetails> initialdetails = new list<initialdetails>();

                if (ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow row2 in ds.Tables[2].Rows)
                    {
                        RequirementUsersByRank initial = new RequirementUsersByRank();

                        //U_ID, REQ_ID, REV_BASE_PRICE, INIT_BASE_PRICE, RN, QUANTITY, COMPANY_NAME
                        initial.UID = row2["U_ID"] != DBNull.Value ? Convert.ToInt32(row2["U_ID"]) : 0;
                        initial.RequirementID = row2["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row2["REQ_ID"]) : 0;
                        initial.RevBasePrice = row2["REV_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["REV_BASE_PRICE"]) : 0;
                        initial.InitBasePrice = row2["INIT_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["INIT_BASE_PRICE"]) : 0;
                        initial.VendTotalPrice = row2["VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["VEND_TOTAL_PRICE"]) : 0;
                        initial.Rank = row2["RN"] != DBNull.Value ? Convert.ToInt32(row2["RN"]) : 0;
                        initial.Quantity = row2["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row2["QUANTITY"]) : 0;
                       // initial.CompanyName = row2["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row2["COMPANY_NAME"]) : string.Empty;

                        initialReport.Add(initial);
                    }
                }

                #endregion requirement items

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {

                        ConsolidatedReport report = new ConsolidatedReport();
                        report.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                        report.UID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        report.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        report.ReqPostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                        report.QuotationFreezTime = row["QUOTATION_FREEZ_TIME"] != DBNull.Value ? Convert.ToDateTime(row["QUOTATION_FREEZ_TIME"]) : DateTime.MaxValue;
                        report.ExpectedStartTime = row["EXP_START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["EXP_START_TIME"]) : DateTime.MaxValue;
                        report.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                        report.IsNegotiationEnded = row["IS_NEGOTIATION_ENDED"] != DBNull.Value ? Convert.ToInt32(row["IS_NEGOTIATION_ENDED"]) : 0;
                       // report.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                        report.Closed = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                        report.NoOfVendorsInvited = row["NoOfVendorsInvited"] != DBNull.Value ? Convert.ToInt32(row["NoOfVendorsInvited"]) : 0;
                        report.NoOfvendorsParticipated = row["NoOfvendorsParticipated"] != DBNull.Value ? Convert.ToInt32(row["NoOfvendorsParticipated"]) : 0;
                        report.ReqCategory = row["REQ_CATEGORY"] != DBNull.Value ? Convert.ToString(row["REQ_CATEGORY"]) : string.Empty;
                        report.EndTime = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                        report.POSTED_BY_USER = row["USER_NAME"] != DBNull.Value ? Convert.ToString(row["USER_NAME"]) : string.Empty;
                        string Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                        report.QUANTITY = row["QUANTITY"] != DBNull.Value ? Convert.ToString(row["QUANTITY"]) : string.Empty;
                        report.ARTICLE_TYPE = row["ARTICLE_TYPE"] != DBNull.Value ? Convert.ToString(row["ARTICLE_TYPE"]) : string.Empty;
                        report.BRAND = row["BRAND"] != DBNull.Value ? Convert.ToString(row["BRAND"]) : string.Empty;
                        report.SEASON = row["SEASON"] != DBNull.Value ? Convert.ToString(row["SEASON"]) : string.Empty;
                        if (report.EndTime == null)
                        {
                            report.EndTime = DateTime.MaxValue;
                        }
                        report.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                        if (report.StartTime == null)
                        {
                            report.StartTime = DateTime.MaxValue;
                        }
                        
                        DateTime now = DateTime.UtcNow;
                        if (report.EndTime != DateTime.MaxValue && report.EndTime > now && report.StartTime < now)
                        {
                            DateTime date = Convert.ToDateTime(report.EndTime);
                            long diff = Convert.ToInt64((date - now).TotalSeconds);
                            report.Status = prmservices.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString());

                        }
                        else if (report.StartTime != DateTime.MaxValue && report.StartTime > now)
                        {
                            DateTime date = Convert.ToDateTime(report.StartTime);
                            long diff = Convert.ToInt64((date - now).TotalSeconds);
                            report.Status = prmservices.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString());
                        }
                        else if (report.EndTime < now)
                        {
                            if ((Status == prmservices.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString()) || Status == prmservices.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString())) && report.EndTime < now)
                            {

                                report.Status = prmservices.GetEnumDesc<PRMStatus>(PRMStatus.NegotiationEnded.ToString());
                            }
                            else
                            {
                                report.Status = Status;
                            }
                        }
                        else if (report.StartTime == DateTime.MaxValue)
                        {
                            report.Status = prmservices.GetEnumDesc<PRMStatus>(PRMStatus.UNCONFIRMED.ToString());
                        }
                        if (Status == prmservices.GetEnumDesc<PRMStatus>(PRMStatus.DELETED.ToString()))
                        {
                            report.Status = Status;
                        }

                        RequirementUsersByRank InitialUser = initialReport.Where(a => a.Rank == 1 && a.RequirementID == report.RequirementID).FirstOrDefault();
                        RequirementUsersByRank RevisedUserL1 = revisedReport.Where(a => a.Rank == 1 && a.RequirementID == report.RequirementID).FirstOrDefault();
                        RequirementUsersByRank RevisedUserL2 = revisedReport.Where(a => a.Rank == 2 && a.RequirementID == report.RequirementID).FirstOrDefault();

                        #region Initial L1 START
                        if (InitialUser != null) {
                            report.IL1_UID = InitialUser.UID;
                            report.IL1_RevBasePrice = InitialUser.RevBasePrice;
                            report.IL1_InitBasePrice = InitialUser.InitBasePrice;
                            report.IL1_L1InitialBasePrice = InitialUser.L1InitialBasePrice;
                            report.IL1_VendTotalPrice = InitialUser.VendTotalPrice;
                            report.IL1_Rank = InitialUser.Rank;
                            report.IL1_Quantity = InitialUser.Quantity;
                            report.IL1_CompanyName = InitialUser.CompanyName;
                            report.IL1_SelectedVendorCurrency = InitialUser.SelectedVendorCurrency;
                        }

                        #endregion Initial L1 END

                        #region Revised RL1 START
                        if (RevisedUserL1 != null)
                        {
                            report.RL1_UID = RevisedUserL1.UID;
                            report.RL1_RevBasePrice = RevisedUserL1.RevBasePrice;
                            report.RL1_InitBasePrice = RevisedUserL1.InitBasePrice;
                            report.RL1_L1InitialBasePrice = RevisedUserL1.L1InitialBasePrice;
                            report.RL1_RevVendTotalPrice = RevisedUserL1.RevVendTotalPrice;
                            report.RL1_Rank = RevisedUserL1.Rank;
                            report.RL1_Quantity = RevisedUserL1.Quantity;
                            report.RL1_CompanyName = RevisedUserL1.CompanyName;
                            report.RL1_SelectedVendorCurrency = RevisedUserL1.SelectedVendorCurrency;
                        }

                        #endregion Revised RL1 END

                        #region Revised RL2 START
                        if (RevisedUserL2 != null)
                        {
                            report.RL2_UID = RevisedUserL2.UID;
                            report.RL2_RevBasePrice = RevisedUserL2.RevBasePrice;
                            report.RL2_InitBasePrice = RevisedUserL2.InitBasePrice;
                            report.RL2_L1InitialBasePrice = RevisedUserL2.L1InitialBasePrice;
                            report.RL2_RevVendTotalPrice = RevisedUserL2.RevVendTotalPrice;
                            report.RL2_Rank = RevisedUserL2.Rank;
                            report.RL2_Quantity = RevisedUserL2.Quantity;
                            report.RL2_CompanyName = RevisedUserL2.CompanyName;
                            report.RL2_SelectedVendorCurrency = RevisedUserL2.SelectedVendorCurrency;
                            #endregion Revised RL2 END
                        }

                        report.BasePriceSavings = report.IL1_VendTotalPrice - report.RL1_RevVendTotalPrice;
                        if (report.BasePriceSavings > 0)
                        {
                            report.BasePriceSavings = Math.Round(report.BasePriceSavings, 2);
                        }
                        else
                        {
                            report.BasePriceSavings = 0;
                        }

                        report.SavingsPercentage = (100 - (report.RL1_RevVendTotalPrice / report.IL1_VendTotalPrice) * 100);
                        if (report.SavingsPercentage > 0)
                        {
                            report.SavingsPercentage = Math.Round(report.SavingsPercentage, 2);
                        }
                        else
                        {
                            report.SavingsPercentage = 0;
                        }


                        //report.SavingsPercentage = Math.Round(report.SavingsPercentage, 2);
                        //report.RevisedUserL1 = RevisedUserL1;
                        //report.RevisedUserL2 = RevisedUserL2;

                        consolidatedReports.Add(report);
                    }

                    //consolidatedReports = consolidatedReports.Where(r => r.NoOfVendorsInvited > 0).ToList();





                }
            }
            catch (Exception ex)
            {
                ConsolidatedReport recuirementerror = new ConsolidatedReport();
                recuirementerror.ErrorMessage = ex.Message;
                consolidatedReports.Add(recuirementerror);
            }

            return consolidatedReports;
        }

        public List<LogisticConsolidatedReport> GetLogisticConsolidatedReports(string sessionID, string from, string to, int userID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<LogisticConsolidatedReport> consolidatedReports = new List<LogisticConsolidatedReport>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID, null);
                if (string.IsNullOrEmpty(from))
                {
                    //from = DateTime.MinValue.ToString();
                    from = DateTime.Now.AddDays(-30).ToString();
                }

                if (string.IsNullOrEmpty(to))
                {
                    //to = DateTime.MaxValue.ToString();
                    to = DateTime.Now.ToString();
                }

                DateTime fromDate = Convert.ToDateTime(from);
                DateTime toDate = Convert.ToDateTime(to);
                sd.Add("P_FROM_DATE", fromDate);
                sd.Add("P_TO_DATE", toDate);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("rp_GetLogisticConsolidatedReports", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        LogisticConsolidatedReport report = new LogisticConsolidatedReport();

                        report.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                        report.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        report.ReqPostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                        report.InvoiceNumber = row["INVOICE_NUMBER"] != DBNull.Value ? Convert.ToString(row["INVOICE_NUMBER"]) : string.Empty;
                        report.InvoiceDate = row["INVOICE_DATE"] != DBNull.Value ? Convert.ToDateTime(row["INVOICE_DATE"]) : DateTime.MaxValue;
                        report.ConsigneeName = row["CONSIGNEE_NAME"] != DBNull.Value ? Convert.ToString(row["CONSIGNEE_NAME"]) : string.Empty;
                        report.Destination = row["FINAL_DESTINATION"] != DBNull.Value ? Convert.ToString(row["FINAL_DESTINATION"]) : string.Empty;
                        report.ProductName = row["PROD_ID"] != DBNull.Value ? Convert.ToString(row["PROD_ID"]) : string.Empty;
                        report.ShippingMode = row["MODE_OF_SHIPMENT"] != DBNull.Value ? Convert.ToString(row["MODE_OF_SHIPMENT"]) : string.Empty;
                        // report.QTY = row["NET_WEIGHT"] != DBNull.Value ? Convert.ToInt32(row["NET_WEIGHT"]) : 0;
                        report.ChargeableWt = row["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row["QUANTITY"]) : 0;
                        report.DGFee = row["FORWORD_DG_FEE"] != DBNull.Value ? Convert.ToDouble(row["FORWORD_DG_FEE"]) : 0;
                        report.AMS = row["AMS"] != DBNull.Value ? Convert.ToString(row["AMS"]) : string.Empty;
                        report.MISC = row["MISC"] != DBNull.Value ? Convert.ToString(row["MISC"]) : string.Empty;
                        // report.MenzinesCharges = row["L2_NAME"] != DBNull.Value ? Convert.ToDouble(row["L2_NAME"]) : 0;
                        // report.ChaCharges = row["L1_INITIAL_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row["L1_INITIAL_BASE_PRICE"]) : 0;
                        report.FreightBidAmount = row["REV_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_PRICE"]) : 0;
                        // report.GrandTotal = row["BASE_PRICE_SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["BASE_PRICE_SAVINGS"]) : 0;
                        report.NetWeight = row["NET_WEIGHT"] != DBNull.Value ? Convert.ToDouble(row["NET_WEIGHT"]) : 0;
                        report.ServiceCharges = row["SERVICE_CHARGES"] != DBNull.Value ? Convert.ToDecimal(row["SERVICE_CHARGES"]) : 0;
                        report.CustomClearance = row["CUSTOM_CLEARANCE"] != DBNull.Value ? Convert.ToDecimal(row["CUSTOM_CLEARANCE"]) : 0;
                        report.TerminalHandling = row["TERMINAL_HANDLING"] != DBNull.Value ? Convert.ToDecimal(row["TERMINAL_HANDLING"]) : 0;
                        report.DrawbackAmount = row["DRAWBACK_AMOUNT"] != DBNull.Value ? Convert.ToDouble(row["DRAWBACK_AMOUNT"]) : 0;
                        report.LoadingUnloadingCharges = row["LOADING_UNLOADING"] != DBNull.Value ? Convert.ToDouble(row["LOADING_UNLOADING"]) : 0;
                        report.WarehouseStorages = row["WAREHOUSE_STORAGES"] != DBNull.Value ? Convert.ToDouble(row["WAREHOUSE_STORAGES"]) : 0;
                        report.AdditionalMiscExp = row["ADDITIONAL_MISC_EXP"] != DBNull.Value ? Convert.ToDouble(row["ADDITIONAL_MISC_EXP"]) : 0;
                        report.NarcoticSubcharges = row["NARCOTIC_SUBCHARGES"] != DBNull.Value ? Convert.ToDouble(row["NARCOTIC_SUBCHARGES"]) : 0;
                        report.palletizeNumber = row["PALLETIZE_NUMBER"] != DBNull.Value ? Convert.ToDouble(row["PALLETIZE_NUMBER"]) : 0;
                        report.palletizeQty = row["PALLETIZE_QTY"] != DBNull.Value ? Convert.ToDouble(row["PALLETIZE_QTY"]) : 0;
                        report.QTY = row["QUANTITY"] != DBNull.Value ? Convert.ToInt32(row["QUANTITY"]) : 0;
                        report.IsPalletize = row["IS_PALLETIZE"] != DBNull.Value ? Convert.ToInt32(row["IS_PALLETIZE"]) : 0;

                        consolidatedReports.Add(report);
                    }

                    // consolidatedReports = consolidatedReports.Where(r => r.NoOfVendorsInvited > 0).ToList();
                }
            }
            catch (Exception ex)
            {
                LogisticConsolidatedReport recuirementerror = new LogisticConsolidatedReport();
                recuirementerror.ErrorMessage = ex.Message;
                consolidatedReports.Add(recuirementerror);
            }

            return consolidatedReports;
        }

        public List<ConsolidatedBasePriceReports> GetConsolidatedBasePriceReports(string sessionID, string from, string to, int userID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<ConsolidatedBasePriceReports> consolidatedReports = new List<ConsolidatedBasePriceReports>();
            List<RequirementUsersByRank> revisedReport = new List<RequirementUsersByRank>();
            List<RequirementUsersByRank> initialReport = new List<RequirementUsersByRank>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID, null);
                if (string.IsNullOrEmpty(from))
                {
                    //from = DateTime.MinValue.ToString();
                    from = DateTime.UtcNow.AddDays(-30).ToString();
                }

                if (string.IsNullOrEmpty(to))
                {
                    //to = DateTime.MaxValue.ToString();
                    to = DateTime.UtcNow.ToString();
                }

                //DateTime fromDate = Convert.ToDateTime(from);
                //DateTime toDate = Convert.ToDateTime(to);
                sd.Add("P_FROM_DATE", from);
                sd.Add("P_TO_DATE", to);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("rp_GetConsolidatedBasePriceReports", sd);


                #region Revised Details (Table 1)

                //List<RevisedDetails> revisedDetails = new List<RevisedDetails>();

                if (ds.Tables[1].Rows.Count > 0)
                {
                    foreach (DataRow row1 in ds.Tables[1].Rows)
                    {
                        RequirementUsersByRank revised = new RequirementUsersByRank();
                        revised.UID = row1["U_ID"] != DBNull.Value ? Convert.ToInt32(row1["U_ID"]) : 0;
                        revised.RequirementID = row1["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row1["REQ_ID"]) : 0;
                        revised.RevBasePrice = row1["REV_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["REV_BASE_PRICE"]) : 0;
                        revised.InitBasePrice = row1["INIT_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["INIT_BASE_PRICE"]) : 0;
                        revised.Rank = row1["RN"] != DBNull.Value ? Convert.ToInt32(row1["RN"]) : 0;
                        revised.Quantity = row1["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row1["QUANTITY"]) : 0;
                        revised.CompanyName = row1["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row1["COMPANY_NAME"]) : string.Empty;

                        revisedReport.Add(revised);
                    }
                }

                #endregion Revised Details

                #region initial details (table 2)

                // list<initialdetails> initialdetails = new list<initialdetails>();

                if (ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow row2 in ds.Tables[2].Rows)
                    {
                        RequirementUsersByRank initial = new RequirementUsersByRank();

                        //U_ID, REQ_ID, REV_BASE_PRICE, INIT_BASE_PRICE, RN, QUANTITY, COMPANY_NAME
                        initial.UID = row2["U_ID"] != DBNull.Value ? Convert.ToInt32(row2["U_ID"]) : 0;
                        initial.RequirementID = row2["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row2["REQ_ID"]) : 0;
                        initial.RevBasePrice = row2["REV_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["REV_BASE_PRICE"]) : 0;
                        initial.InitBasePrice = row2["INIT_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["INIT_BASE_PRICE"]) : 0;
                        initial.Rank = row2["RN"] != DBNull.Value ? Convert.ToInt32(row2["RN"]) : 0;
                        initial.Quantity = row2["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row2["QUANTITY"]) : 0;
                        // initial.CompanyName = row2["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row2["COMPANY_NAME"]) : string.Empty;

                        initialReport.Add(initial);
                    }
                }

                #endregion requirement items

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {

                        ConsolidatedBasePriceReports report = new ConsolidatedBasePriceReports();
                        report.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                        report.UID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        report.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        report.ReqPostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                        report.QuotationFreezTime = row["QUOTATION_FREEZ_TIME"] != DBNull.Value ? Convert.ToDateTime(row["QUOTATION_FREEZ_TIME"]) : DateTime.MaxValue;
                        report.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                        report.IsNegotiationEnded = row["IS_NEGOTIATION_ENDED"] != DBNull.Value ? Convert.ToInt32(row["IS_NEGOTIATION_ENDED"]) : 0;
                        // report.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                        report.Closed = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                        report.NoOfVendorsInvited = row["NoOfVendorsInvited"] != DBNull.Value ? Convert.ToInt32(row["NoOfVendorsInvited"]) : 0;
                        report.NoOfvendorsParticipated = row["NoOfvendorsParticipated"] != DBNull.Value ? Convert.ToInt32(row["NoOfvendorsParticipated"]) : 0;
                        report.ReqCategory = row["REQ_CATEGORY"] != DBNull.Value ? Convert.ToString(row["REQ_CATEGORY"]) : string.Empty;


                        RequirementUsersByRank InitialUser = initialReport.Where(a => a.Rank == 1 && a.RequirementID == report.RequirementID).FirstOrDefault();
                        RequirementUsersByRank RevisedUserL1 = revisedReport.Where(a => a.Rank == 1 && a.RequirementID == report.RequirementID).FirstOrDefault();
                        RequirementUsersByRank RevisedUserL2 = revisedReport.Where(a => a.Rank == 2 && a.RequirementID == report.RequirementID).FirstOrDefault();

                        #region Initial L1 START
                        if (InitialUser != null)
                        {
                            report.IL1_UID = InitialUser.UID;
                            report.IL1_RevBasePrice = InitialUser.RevBasePrice;
                            report.IL1_InitBasePrice = InitialUser.InitBasePrice;
                            report.IL1_L1InitialBasePrice = InitialUser.L1InitialBasePrice;
                            report.IL1_Rank = InitialUser.Rank;
                            report.IL1_Quantity = InitialUser.Quantity;
                            report.IL1_CompanyName = InitialUser.CompanyName;
                        }

                        #endregion Initial L1 END

                        #region Revised RL1 START
                        if (RevisedUserL1 != null)
                        {
                            report.RL1_UID = RevisedUserL1.UID;
                            report.RL1_RevBasePrice = RevisedUserL1.RevBasePrice;
                            report.RL1_InitBasePrice = RevisedUserL1.InitBasePrice;
                            report.RL1_L1InitialBasePrice = RevisedUserL1.L1InitialBasePrice;
                            report.RL1_Rank = RevisedUserL1.Rank;
                            report.RL1_Quantity = RevisedUserL1.Quantity;
                            report.RL1_CompanyName = RevisedUserL1.CompanyName;
                        }

                        #endregion Revised RL1 END

                        #region Revised RL2 START
                        if (RevisedUserL2 != null)
                        {
                            report.RL2_UID = RevisedUserL2.UID;
                            report.RL2_RevBasePrice = RevisedUserL2.RevBasePrice;
                            report.RL2_InitBasePrice = RevisedUserL2.InitBasePrice;
                            report.RL2_L1InitialBasePrice = RevisedUserL2.L1InitialBasePrice;
                            report.RL2_Rank = RevisedUserL2.Rank;
                            report.RL2_Quantity = RevisedUserL2.Quantity;
                            report.RL2_CompanyName = RevisedUserL2.CompanyName;
                            #endregion Revised RL2 END
                        }

                        report.BasePriceSavings = report.IL1_InitBasePrice - report.RL1_RevBasePrice;
                        if (report.BasePriceSavings > 0)
                        {
                            report.BasePriceSavings = Math.Round(report.BasePriceSavings, 2);
                        }
                        else
                        {
                            report.BasePriceSavings = 0;
                        }

                        report.SavingsPercentage = (100 - (report.RL1_RevBasePrice / report.IL1_InitBasePrice) * 100);
                        if (report.SavingsPercentage > 0)
                        {
                            report.SavingsPercentage = Math.Round(report.SavingsPercentage, 2);
                        }
                        else
                        {
                            report.SavingsPercentage = 0;
                        }


                        //report.SavingsPercentage = Math.Round(report.SavingsPercentage, 2);
                        //report.RevisedUserL1 = RevisedUserL1;
                        //report.RevisedUserL2 = RevisedUserL2;

                        consolidatedReports.Add(report);
                    }

                    consolidatedReports = consolidatedReports.Where(r => r.NoOfVendorsInvited > 0).ToList();





                }
            }
            catch (Exception ex)
            {
                ConsolidatedBasePriceReports recuirementerror = new ConsolidatedBasePriceReports();
                recuirementerror.ErrorMessage = ex.Message;
                consolidatedReports.Add(recuirementerror);
            }

            return consolidatedReports;
        }

        public string GetUserLoginTemplates(string template, string from, string to, int userID, string sessionID)
        {
            MemoryStream ms = new MemoryStream();
            byte[] contents = null;

            #region Users Login Report

            if (template.ToUpper().Contains("USERS_LOGIN_REPORT"))
            {

                List<ActiveUsers> usersReport = prmservices.GetUsersLoginStatus(userID, from, to, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("USERS_LOGIN_REPORT");

                int lineNumber = 1;

                wsSheet1.Cells["A" + lineNumber].Value = "USERS LOGIN REPORT";
                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Style.Font.Size = 16;
                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));
                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Style.Font.Color.SetColor(Color.White);
                lineNumber++;
                lineNumber++;

                wsSheet1.Cells["A" + lineNumber].Value = "User ID";
                wsSheet1.Cells["B" + lineNumber].Value = "User Name";
                wsSheet1.Cells["C" + lineNumber].Value = "User E-mail";
                wsSheet1.Cells["D" + lineNumber].Value = "User Phone Number";
                wsSheet1.Cells["E" + lineNumber].Value = "User Login Time";


                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(119, 136, 153));
                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Style.Font.Color.SetColor(Color.White);

                lineNumber++;

                wsSheet1.Cells["A:E"].AutoFitColumns();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;

                foreach (var item in usersReport)
                {


                    wsSheet1.Cells["A" + lineNumber].Value = item.UserID;
                    wsSheet1.Cells["B" + lineNumber].Value = item.FirstName + ' ' + item.LastName;
                    wsSheet1.Cells["C" + lineNumber].Value = item.Email;
                    wsSheet1.Cells["D" + lineNumber].Value = item.PhoneNum;
                    wsSheet1.Cells["E" + lineNumber].Value = item.DateCreated.ToString();

                    wsSheet1.Cells.AutoFitColumns();

                    lineNumber++;





                }


                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }

            #endregion Users Login Report



            return Convert.ToBase64String(ms.ToArray());
        }

        public string GetLogsTemplates(string template, string from, string to, int companyID, string sessionid)
        {
            MemoryStream ms = new MemoryStream();
            byte[] contents = null;

            #region Email Logs Report

            if (template.ToUpper().Contains("EMAIL_LOGS_REPORT"))
            {

                List<EmailLogs> emailLogs = prmservices.GetEmailLogs(from,to,companyID, sessionid);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("EMAIL_LOGS_REPORT");

                int lineNumber = 1;

                wsSheet1.Cells["A" + lineNumber].Value = "EMAIL LOGS REPORT";
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Font.Size = 16;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Font.Color.SetColor(Color.White);

                lineNumber++;
                lineNumber++;

                wsSheet1.Cells["A" + lineNumber].Value = "REQ ID";
                wsSheet1.Cells["B" + lineNumber].Value = "REQ TYPE";
                wsSheet1.Cells["C" + lineNumber].Value = "REQ STATUS";
                wsSheet1.Cells["D" + lineNumber].Value = "MESSAGE TYPE";
                wsSheet1.Cells["E" + lineNumber].Value = "NAME";
                wsSheet1.Cells["F" + lineNumber].Value = "EMAIL";
                wsSheet1.Cells["G" + lineNumber].Value = "ATL.EMAIL";
                wsSheet1.Cells["H" + lineNumber].Value = "SUBJECT";
                wsSheet1.Cells["I" + lineNumber + ":J" + lineNumber].Merge = true;
                wsSheet1.Cells["I" + lineNumber + ":J" + lineNumber].Value = "MESSAGE";
                wsSheet1.Cells["K" + lineNumber].Value = "MESSAGE DATE & TIME";


                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(119, 136, 153));
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Font.Color.SetColor(Color.White);


                lineNumber++;

                wsSheet1.Cells["A:K"].AutoFitColumns();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;

                foreach (var item in emailLogs)
                {
                    if (item.ReqStatus == "UNCONFIRMED")
                    {
                        item.ReqStatus = "Quotations pending";
                    }
                    else if (item.ReqStatus == "NOTSTARTED")
                    {
                        item.ReqStatus = "Negotiation scheduled";
                    }
                    else if (item.ReqStatus == "STARTED")
                    {
                        item.ReqStatus = "Negotiation started";
                    }
                    else if (item.ReqStatus == "Negotiation Ended")
                    {
                        item.ReqStatus = "Negotiation Closed";
                    }
                    else if (item.ReqStatus == "DELETED")
                    {
                        item.ReqStatus = "Negotiation Cancelled";
                    }

                    if (item.Requirement == "VENDOR_REGISTER" || item.Requirement == "COMMUNICATIONS" || item.Requirement == "USER_REGISTER")
                    {

                        item.ReqStatus = "--";
                        item.ReqType = -1;

                    }

                    wsSheet1.Cells["A" + lineNumber].Value = item.ReqID;
                    if (item.ReqType == 0)
                    {
                        wsSheet1.Cells["B" + lineNumber].Value = "PRICE";
                    }
                    // wsSheet1.Cells["B" + lineNumber].Value = item.;
                    wsSheet1.Cells["C" + lineNumber].Value = item.ReqStatus;
                    wsSheet1.Cells["D" + lineNumber].Value = item.Requirement;
                    wsSheet1.Cells["E" + lineNumber].Value = item.FirstName + ' ' + item.LastName;
                    wsSheet1.Cells["F" + lineNumber].Value = item.Email;
                    wsSheet1.Cells["G" + lineNumber].Value = item.AltEmail;
                    wsSheet1.Cells["H" + lineNumber].Value = item.Subject;
                    wsSheet1.Cells["I" + lineNumber + ":J" + lineNumber].Merge = true;
                    wsSheet1.Cells["I" + lineNumber + ":J" + lineNumber].Value = item.Message;
                    item.MessageDate = Utilities.ToLocal(item.MessageDate);
                    wsSheet1.Cells["K" + lineNumber].Value = item.MessageDate.ToString();

                    wsSheet1.Cells.AutoFitColumns();

                    lineNumber++;

                }


                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }

            #endregion Email Logs Report


            #region SMS Logs Report

            if (template.ToUpper().Contains("SMS_LOGS_REPORT"))
            {

                List<EmailLogs> emailLogs = prmservices.GetSMSLogs(from, to, companyID, sessionid);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("SMS_LOGS_REPORT");

                int lineNumber = 1;

                wsSheet1.Cells["A" + lineNumber].Value = "SMS LOGS REPORT";
                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Style.Font.Size = 16;
                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));
                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Style.Font.Color.SetColor(Color.White);

                lineNumber++;
                lineNumber++;

                wsSheet1.Cells["A" + lineNumber].Value = "REQ ID";
                wsSheet1.Cells["B" + lineNumber].Value = "REQ TYPE";
                wsSheet1.Cells["C" + lineNumber].Value = "REQ STATUS";
                wsSheet1.Cells["D" + lineNumber].Value = "MESSAGE TYPE";
                wsSheet1.Cells["E" + lineNumber].Value = "NAME";
                wsSheet1.Cells["F" + lineNumber].Value = "EMAIL";
                wsSheet1.Cells["G" + lineNumber].Value = "ALT.EMAIL";
                wsSheet1.Cells["H" + lineNumber].Value = "PHONE NO.";
                wsSheet1.Cells["I" + lineNumber].Value = "ALT.PHONE NO.";
                // wsSheet1.Cells["G" + lineNumber].Value = "SUBJECT";
                wsSheet1.Cells["J" + lineNumber + ":K" + lineNumber].Merge = true;
                wsSheet1.Cells["J" + lineNumber + ":K" + lineNumber].Value = "MESSAGE";
                wsSheet1.Cells["L" + lineNumber].Value = "MESSAGE DATE & TIME";


                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(119, 136, 153));
                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Style.Font.Color.SetColor(Color.White);


                lineNumber++;

                wsSheet1.Cells["A:L"].AutoFitColumns();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;

                var item = emailLogs;

               // foreach (var item in emailLogs)
                    for (int i = 0; i < item.Count; i++)
                    {
                        if (item[i].ReqStatus == "UNCONFIRMED")
                        {
                            item[i].ReqStatus = "Quotations pending";
                        }
                        else if (item[i].ReqStatus == "NOTSTARTED")
                        {
                            item[i].ReqStatus = "Negotiation scheduled";
                        }
                        else if (item[i].ReqStatus == "STARTED")
                        {
                            item[i].ReqStatus = "Negotiation started";
                        }
                        else if (item[i].ReqStatus == "Negotiation Ended")
                        {
                            item[i].ReqStatus = "Negotiation Closed";
                        }
                        else if (item[i].ReqStatus == "DELETED")
                        {
                            item[i].ReqStatus = "Negotiation Cancelled";
                        }

                        wsSheet1.Cells["A" + lineNumber].Value = item[i].ReqID;
                        if (item[i].ReqType == 0)
                        {
                            wsSheet1.Cells["B" + lineNumber].Value = "PRICE";
                        }

                        wsSheet1.Cells["C" + lineNumber].Value = item[i].ReqStatus;
                        wsSheet1.Cells["D" + lineNumber].Value = item[i].Requirement;
                        wsSheet1.Cells["E" + lineNumber].Value = item[i].FirstName + ' ' + item[i].LastName;
                        wsSheet1.Cells["F" + lineNumber].Value = item[i].Email;
                        wsSheet1.Cells["G" + lineNumber].Value = item[i].AltEmail;
                        wsSheet1.Cells["H" + lineNumber].Value = item[i].PhoneNum;
                        wsSheet1.Cells["I" + lineNumber].Value = item[i].AltPhoneNum;
                        //wsSheet1.Cells["G" + lineNumber].Value = item.Subject;
                        wsSheet1.Cells["J" + lineNumber + ":K" + lineNumber].Merge = true;
                        wsSheet1.Cells["J" + lineNumber + ":K" + lineNumber].Value = item[i].Message;

                        item[i].MessageDate = Utilities.ToLocal(item[i].MessageDate);
                        wsSheet1.Cells["L" + lineNumber].Value = item[i].MessageDate.ToString();


                        wsSheet1.Cells.AutoFitColumns();

                        lineNumber++;

                    }


                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }

            #endregion SMS Logs Report


            return Convert.ToBase64String(ms.ToArray());
        }

        public List<AccountingConsolidatedReport> GetAccountingConsolidatedReports(string sessionID, string from, string to, int userID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<AccountingConsolidatedReport> consolidatedReports = new List<AccountingConsolidatedReport>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID, null);
                if (string.IsNullOrEmpty(from))
                {
                    //from = DateTime.MinValue.ToString();
                    from = DateTime.Now.AddDays(-30).ToString();
                }

                if (string.IsNullOrEmpty(to))
                {
                    //to = DateTime.MaxValue.ToString();
                    to = DateTime.Now.ToString();
                }

                DateTime fromDate = Convert.ToDateTime(from);
                DateTime toDate = Convert.ToDateTime(to);
                sd.Add("P_FROM_DATE", fromDate);
                sd.Add("P_TO_DATE", toDate);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("rp_GetAccountingConsolidatedReports", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        AccountingConsolidatedReport report = new AccountingConsolidatedReport();

                        report.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                        report.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        report.ReqPostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                        report.InvoiceNumber = row["INVOICE_NUMBER"] != DBNull.Value ? Convert.ToString(row["INVOICE_NUMBER"]) : string.Empty;
                        report.ConsigneeName = row["CONSIGNEE_NAME"] != DBNull.Value ? Convert.ToString(row["CONSIGNEE_NAME"]) : string.Empty;
                        report.Destination = row["FINAL_DESTINATION"] != DBNull.Value ? Convert.ToString(row["FINAL_DESTINATION"]) : string.Empty;
                        report.ProductName = row["PROD_ID"] != DBNull.Value ? Convert.ToString(row["PROD_ID"]) : string.Empty;
                        report.ShippingMode = row["MODE_OF_SHIPMENT"] != DBNull.Value ? Convert.ToString(row["MODE_OF_SHIPMENT"]) : string.Empty;
                        report.PortOfLanding = row["PORT_OF_LANDING"] != DBNull.Value ? Convert.ToString(row["PORT_OF_LANDING"]) : string.Empty;
                        report.IsPalletize = row["IS_PALLETIZE"] != DBNull.Value ? Convert.ToInt32(row["IS_PALLETIZE"]) : 0;
                        report.FreightBidAmount = row["REV_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_PRICE"]) : 0;
                        report.NetWeight = row["NET_WEIGHT"] != DBNull.Value ? Convert.ToDouble(row["NET_WEIGHT"]) : 0;
                        report.ServiceCharges = row["SERVICE_CHARGES"] != DBNull.Value ? Convert.ToDecimal(row["SERVICE_CHARGES"]) : 0;
                        report.CustomClearance = row["CUSTOM_CLEARANCE"] != DBNull.Value ? Convert.ToDecimal(row["CUSTOM_CLEARANCE"]) : 0;
                        report.TerminalHandling = row["TERMINAL_HANDLING"] != DBNull.Value ? Convert.ToDecimal(row["TERMINAL_HANDLING"]) : 0;
                        report.DrawbackAmount = row["DRAWBACK_AMOUNT"] != DBNull.Value ? Convert.ToDouble(row["DRAWBACK_AMOUNT"]) : 0;
                        report.LoadingUnloadingCharges = row["LOADING_UNLOADING"] != DBNull.Value ? Convert.ToDouble(row["LOADING_UNLOADING"]) : 0;
                        report.WarehouseStorages = row["WAREHOUSE_STORAGES"] != DBNull.Value ? Convert.ToDouble(row["WAREHOUSE_STORAGES"]) : 0;
                        report.AdditionalMiscExp = row["ADDITIONAL_MISC_EXP"] != DBNull.Value ? Convert.ToDouble(row["ADDITIONAL_MISC_EXP"]) : 0;
                        report.NarcoticSubcharges = row["NARCOTIC_SUBCHARGES"] != DBNull.Value ? Convert.ToDouble(row["NARCOTIC_SUBCHARGES"]) : 0;
                        report.palletizeNumber = row["PALLETIZE_NUMBER"] != DBNull.Value ? Convert.ToDouble(row["PALLETIZE_NUMBER"]) : 0;
                        report.palletizeQty = row["PALLETIZE_QTY"] != DBNull.Value ? Convert.ToDouble(row["PALLETIZE_QTY"]) : 0;
                        report.QTY = row["QUANTITY_IN"] != DBNull.Value ? Convert.ToString(row["QUANTITY_IN"]) : string.Empty;
                        if (report.IsPalletize == 1)
                        {
                            report.Pallet = "Palletize";
                        }
                        else if (report.IsPalletize == 0)
                        {
                            report.Pallet = "Non - Palletize";
                        }

                        consolidatedReports.Add(report);
                    }

                    // consolidatedReports = consolidatedReports.Where(r => r.NoOfVendorsInvited > 0).ToList();
                }
            }
            catch (Exception ex)
            {
                AccountingConsolidatedReport recuirementerror = new AccountingConsolidatedReport();
                recuirementerror.ErrorMessage = ex.Message;
                consolidatedReports.Add(recuirementerror);
            }

            return consolidatedReports;
        }

        public string GetConsolidatedTemplates(string template, string from, string to, int userID, string sessionID)
        {
            MemoryStream ms = new MemoryStream();
            byte[] contents = null;

            #region Accounting Consolidated Report

            if (template.ToUpper().Contains("ACCOUNTING_CONSOLIDATED_REPORT"))
            {

                List<AccountingConsolidatedReport> accountingconsolidateReport = GetAccountingConsolidatedReports(sessionID, from, to, userID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("ACCOUNTING_CONSOLIDATED_REPORT");

                int lineNumber = 1;

                wsSheet1.Cells["A" + lineNumber].Value = "ACCOUNTING CONSOLIDATED REPORT";
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Font.Size = 16;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Font.Color.SetColor(Color.White);
                lineNumber++;
                lineNumber++;

                wsSheet1.Cells["A" + lineNumber].Value = "Req ID";
                wsSheet1.Cells["B" + lineNumber].Value = "Invoice No.";
                wsSheet1.Cells["C" + lineNumber].Value = "Posted Date";
                wsSheet1.Cells["D" + lineNumber].Value = "Consignee Name";
                wsSheet1.Cells["E" + lineNumber].Value = "Final Destination";
                wsSheet1.Cells["F" + lineNumber].Value = "Port of Destination";
                wsSheet1.Cells["G" + lineNumber].Value = "Product Name";
                wsSheet1.Cells["H" + lineNumber].Value = "Shipping Mode";
                wsSheet1.Cells["I" + lineNumber].Value = "Division";
                wsSheet1.Cells["J" + lineNumber].Value = "Net Weight";
                wsSheet1.Cells["K" + lineNumber].Value = "Grand Total";

                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(119, 136, 153));
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Font.Color.SetColor(Color.White);

                lineNumber++;

                wsSheet1.Cells["A:N"].AutoFitColumns();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;

                foreach (var item in accountingconsolidateReport)
                {
                    if (item.IsPalletize == 1)
                    {
                        item.Pallet = "Palletize";
                    }
                    else if (item.IsPalletize == 0)
                    {
                        item.Pallet = "Non - Palletize";
                    }

                    wsSheet1.Cells["A" + lineNumber].Value = item.RequirementID;
                    wsSheet1.Cells["B" + lineNumber].Value = item.InvoiceNumber;
                    wsSheet1.Cells["C" + lineNumber].Value = item.ReqPostedOn.ToString();
                    wsSheet1.Cells["D" + lineNumber].Value = item.ConsigneeName;
                    wsSheet1.Cells["E" + lineNumber].Value = item.Destination;
                    wsSheet1.Cells["F" + lineNumber].Value = item.PortOfLanding;
                    wsSheet1.Cells["G" + lineNumber].Value = item.ProductName;
                    wsSheet1.Cells["H" + lineNumber].Value = item.ShippingMode;
                    wsSheet1.Cells["I" + lineNumber].Value = item.Pallet;
                    wsSheet1.Cells["J" + lineNumber].Value = item.NetWeight + " " + item.QTY;

                    if (item.IsPalletize == 1)
                    {
                        item.GrandTotal = (item.FreightBidAmount + Convert.ToDouble(item.ServiceCharges) + Convert.ToDouble(item.CustomClearance) + Convert.ToDouble(item.TerminalHandling) + item.DrawbackAmount + item.LoadingUnloadingCharges +
                            item.WarehouseStorages + item.AdditionalMiscExp + item.NarcoticSubcharges + (item.palletizeQty * item.palletizeNumber));
                    }
                    else if (item.IsPalletize == 0)
                    {
                        item.GrandTotal = (item.FreightBidAmount + Convert.ToDouble(item.ServiceCharges) + Convert.ToDouble(item.CustomClearance) + Convert.ToDouble(item.TerminalHandling) + item.DrawbackAmount + item.LoadingUnloadingCharges +
                            item.WarehouseStorages + item.AdditionalMiscExp + item.NarcoticSubcharges);
                    }

                    wsSheet1.Cells["K" + lineNumber].Value = item.GrandTotal;


                    wsSheet1.Cells.AutoFitColumns();

                    lineNumber++;





                }


                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }

            #endregion Accounting Consolidated Report

            #region Consolidated Report

            if (template.ToUpper().Contains("CONSOLIDATED_REPORT"))
            {

                List<LogisticConsolidatedReport> consolidateReport = GetLogisticConsolidatedReports(sessionID, from, to, userID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("CONSOLIDATED_REPORT");

                int lineNumber = 1;

                wsSheet1.Cells["A" + lineNumber].Value = "CONSOLIDATED REPORT";
                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Style.Font.Size = 16;
                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));
                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Style.Font.Color.SetColor(Color.White);
                lineNumber++;
                lineNumber++;

                wsSheet1.Cells["A" + lineNumber].Value = "Req ID";
                wsSheet1.Cells["B" + lineNumber].Value = "Requirement Title";
                wsSheet1.Cells["C" + lineNumber].Value = "Posted Date";
                wsSheet1.Cells["D" + lineNumber].Value = "Invoice Number";
                wsSheet1.Cells["E" + lineNumber].Value = "Invoice Date";
                wsSheet1.Cells["F" + lineNumber].Value = "Consignee Name";
                wsSheet1.Cells["G" + lineNumber].Value = "Destination";
                wsSheet1.Cells["H" + lineNumber].Value = "Product Name";
                wsSheet1.Cells["I" + lineNumber].Value = "Shipping Mode AIR/SEA";
                wsSheet1.Cells["J" + lineNumber].Value = "Quantity";
                wsSheet1.Cells["K" + lineNumber].Value = "Chargeable Wt";
                wsSheet1.Cells["L" + lineNumber].Value = "DG Fee";
                wsSheet1.Cells["M" + lineNumber].Value = "AMS";
                wsSheet1.Cells["N" + lineNumber].Value = "MISC";
                wsSheet1.Cells["O" + lineNumber].Value = "Menzines Charges";
                wsSheet1.Cells["P" + lineNumber].Value = "CHA Charges";
                wsSheet1.Cells["Q" + lineNumber].Value = "Freight/Bid Amount";
                wsSheet1.Cells["R" + lineNumber].Value = "Grand Total";

                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(119, 136, 153));
                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Style.Font.Color.SetColor(Color.White);

                lineNumber++;

                wsSheet1.Cells["A:R"].AutoFitColumns();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;

                foreach (var item in consolidateReport)
                {
                    //if (item.IsPalletize == 1)
                    //{
                    //    item.Pallet = "Palletize";
                    //}
                    //else if (item.IsPalletize == 0)
                    //{
                    //    item.Pallet = "Non - Palletize";
                    //}

                    wsSheet1.Cells["A" + lineNumber].Value = item.RequirementID;
                    wsSheet1.Cells["B" + lineNumber].Value = item.Title;
                    wsSheet1.Cells["C" + lineNumber].Value = item.ReqPostedOn.ToString();
                    wsSheet1.Cells["D" + lineNumber].Value = item.InvoiceNumber;
                    wsSheet1.Cells["E" + lineNumber].Value = item.InvoiceDate;
                    wsSheet1.Cells["F" + lineNumber].Value = item.ConsigneeName;
                    wsSheet1.Cells["G" + lineNumber].Value = item.Destination;
                    wsSheet1.Cells["H" + lineNumber].Value = item.ProductName;
                    wsSheet1.Cells["I" + lineNumber].Value = item.ShippingMode;
                    wsSheet1.Cells["J" + lineNumber].Value = item.QTY;
                    wsSheet1.Cells["K" + lineNumber].Value = item.ChargeableWt;
                    wsSheet1.Cells["L" + lineNumber].Value = item.DGFee;
                    wsSheet1.Cells["M" + lineNumber].Value = item.AMS;
                    wsSheet1.Cells["N" + lineNumber].Value = item.MISC;
                    wsSheet1.Cells["O" + lineNumber].Value = item.MenzinesCharges;
                    wsSheet1.Cells["P" + lineNumber].Value = 2500;
                    wsSheet1.Cells["Q" + lineNumber].Value = item.FreightBidAmount;


                    if (item.IsPalletize == 1)
                    {
                        item.GrandTotal = (item.FreightBidAmount + Convert.ToDouble(item.ServiceCharges) + Convert.ToDouble(item.CustomClearance) + Convert.ToDouble(item.TerminalHandling) + item.DrawbackAmount + item.LoadingUnloadingCharges +
                            item.WarehouseStorages + item.AdditionalMiscExp + item.NarcoticSubcharges + (item.palletizeQty * item.palletizeNumber));
                    }
                    else if (item.IsPalletize == 0)
                    {
                        item.GrandTotal = (item.FreightBidAmount + Convert.ToDouble(item.ServiceCharges) + Convert.ToDouble(item.CustomClearance) + Convert.ToDouble(item.TerminalHandling) + item.DrawbackAmount + item.LoadingUnloadingCharges +
                            item.WarehouseStorages + item.AdditionalMiscExp + item.NarcoticSubcharges);
                    }

                    wsSheet1.Cells["R" + lineNumber].Value = item.GrandTotal;


                    wsSheet1.Cells.AutoFitColumns();

                    lineNumber++;





                }


                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }

            #endregion Consolidated Report

            return Convert.ToBase64String(ms.ToArray());
        }

        public string GetQCSTemplate(int reqId, int userId, string qcsType, string sessionId)
        {
            Utilities.ValidateSession(sessionId, null);
            MemoryStream ms = new MemoryStream();
            byte[] contents = null;

            try
            {
                var requirementData = prmservices.GetRequirementData(reqId, userId, sessionId);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("QCS_" + qcsType);
                ExcelWorksheet wsSheet2 = ExcelPkg.Workbook.Worksheets.Add("Vendor Score Cards");

                if (requirementData != null && requirementData.ListRequirementItems != null && requirementData.ListRequirementItems.Count > 0 &&
                    requirementData.AuctionVendors != null && requirementData.AuctionVendors.Count > 0)
                {
                    wsSheet2.Cells[1, 1].Value = "Benchmark Compliance Score";
                    wsSheet2.Cells[1, 2].Value = "Benchmark Cost Competitiveness";
                    wsSheet2.Cells[1, 3].Value = "Benchmark Design Support";
                    wsSheet2.Cells[1, 4].Value = "Benchmark Lead Time";
                    wsSheet2.Cells[1, 5].Value = "Benchmark Otif";
                    wsSheet2.Cells[1, 6].Value = "Benchmark PreProduction Approval";
                    wsSheet2.Cells[1, 7].Value = "Benchmark Q2Rejection";
                    wsSheet2.Cells[1, 8].Value = "Benchmark Qtm";
                    wsSheet2.Cells[1, 9].Value = "Benchmark Return percentage";
                    wsSheet2.Cells[1, 10].Value = "Benchmark Service";
                    wsSheet2.Cells[1, 11].Value = "Benchmark Sgc";
                    wsSheet2.Cells[1, 12].Value = "Bucket Tier";
                    wsSheet2.Cells[1, 13].Value = "Compliance Score";
                    wsSheet2.Cells[1, 14].Value = "Cost Competitiveness";
                    wsSheet2.Cells[1, 15].Value = "Design Support";
                    wsSheet2.Cells[1, 16].Value = "Lead Time";
                    wsSheet2.Cells[1, 17].Value = "Operational Total Score";
                    wsSheet2.Cells[1, 18].Value = "Otif";
                    wsSheet2.Cells[1, 19].Value = "Pre Production Approval";
                    wsSheet2.Cells[1, 20].Value = "Q2 Rejection";
                    wsSheet2.Cells[1, 21].Value = "Qtm";
                    wsSheet2.Cells[1, 22].Value = "Return percentage";
                    wsSheet2.Cells[1, 23].Value = "Service";
                    wsSheet2.Cells[1, 24].Value = "Sgc";
                    wsSheet2.Cells[1, 25].Value = "Tier Rating";
                    wsSheet2.Cells[1, 26].Value = "Weighted Benchmark Compliance Score";
                    wsSheet2.Cells[1, 27].Value = "Weighted Benchmark Cost Competitiveness";
                    wsSheet2.Cells[1, 28].Value = "Weighted Benchmark Design Support";
                    wsSheet2.Cells[1, 29].Value = "Weighted Benchmark Lead Time";
                    wsSheet2.Cells[1, 30].Value = "Weighted Benchmark Otif";
                    wsSheet2.Cells[1, 31].Value = "Weighted Benchmark Pre Production Approval";
                    wsSheet2.Cells[1, 32].Value = "Weighted Benchmark Q2 Rejection";
                    wsSheet2.Cells[1, 33].Value = "Weighted Benchmark Qtm";
                    wsSheet2.Cells[1, 34].Value = "Weighted Benchmark Return percentage";
                    wsSheet2.Cells[1, 35].Value = "Weighted Benchmark Service";
                    wsSheet2.Cells[1, 36].Value = "Weighted Benchmark Sgc";
                    foreach (var vendor in requirementData.AuctionVendors)
                    {
                        List<KeyValuePair> keyValuePairs = prmservices.GetVendorCapacityAndScoreCard(vendor.VendorID, sessionId);
                    }
                }

                int lineNumber = 1;
                int emptyCells = 10;
                int dataStartCells = emptyCells + 1;
                //wsSheet1.Cells.Style.Border.Top.Style = ExcelBorderStyle.Medium;
                for (int i = 1; i <= emptyCells; i++)
                {
                    wsSheet1.Cells[lineNumber, i].Value = "";
                }


                requirementData.AuctionVendors = requirementData.AuctionVendors.OrderBy(r => r.Rank == 0).ThenBy(r => r.Rank).ToList();

                int count = 0;
                var highestColorsArray = new List<dynamic>();
                var highestPrices = new List<dynamic>();
                if (requirementData != null && requirementData.ListRequirementItems != null && requirementData.ListRequirementItems.Count > 0 &&
                    requirementData.AuctionVendors != null && requirementData.AuctionVendors.Count > 0)
                {
                    foreach (var vendor in requirementData.AuctionVendors)
                    {
                        var dynamicValues = vendor.ListRequirementItems.Select(i => new { i.ItemRank, i.ItemID, vendor.VendorID }).ToArray();
                        highestColorsArray.AddRange(dynamicValues);
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Value = vendor.CompanyName;
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 242, 204));
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        count++;
                    }
                }

                if (highestColorsArray != null && highestColorsArray.Count() > 0) 
                {
                    highestPrices = highestColorsArray.OrderByDescending(b=>b.ItemRank).GroupBy(a => a.ItemID).Select(g => g.FirstOrDefault()).ToList();
                }

                if (requirementData != null && requirementData.ListRequirementItems != null && requirementData.ListRequirementItems.Count > 0 &&
                    requirementData.AuctionVendors != null && requirementData.AuctionVendors.Count > 0)
                {
                    foreach (var vendor in requirementData.AuctionVendors)
                    {
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Value = vendor.CompanyName;
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(221, 235, 247));
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        
                        count++;
                    }
                }

                for (int i = dataStartCells + count; i <= (dataStartCells + count)  + 2; i++)
                {
                    wsSheet1.Cells[lineNumber, i].Value = "";
                }

                lineNumber++;


                wsSheet1.Cells[lineNumber, 1].Value = "Item Id";
                wsSheet1.Cells[lineNumber, 2].Value = "Article Type";
                wsSheet1.Cells[lineNumber, 3].Value = "Article Name";
                wsSheet1.Cells[lineNumber, 4].Value = "Article Number";
                wsSheet1.Cells[lineNumber, 5].Value = "Style ID";
                wsSheet1.Cells[lineNumber, 6].Value = "Brand";
                wsSheet1.Cells[lineNumber, 7].Value = "Gender";
                wsSheet1.Cells[lineNumber, 8].Value = "Quantity";
                wsSheet1.Cells[lineNumber, 9].Value = "Season";
                wsSheet1.Cells[lineNumber, 10].Value = "Target Cost";

                count = 0;
                if (requirementData != null && requirementData.ListRequirementItems != null && requirementData.ListRequirementItems.Count > 0 &&
                    requirementData.AuctionVendors != null && requirementData.AuctionVendors.Count > 0)
                {
                    int colorCount = 1;
                    foreach (var vendor in requirementData.AuctionVendors)
                    {
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Value = vendor.VendorName;
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        //if (colorCount == 1)
                        //{
                        //    wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(0, 255, 0));
                        //}
                        //else if (colorCount == 2)
                        //{
                        //    wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(0, 0, 255));
                        //}
                        //else if (colorCount == 3)
                        //{
                        //    wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 255, 0));
                        //}
                        //else
                        //{
                        //    wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 242, 204));
                        //}
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 242, 204));
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        count++;
                        colorCount++;
                    }
                }

                if (requirementData != null && requirementData.ListRequirementItems != null && requirementData.ListRequirementItems.Count > 0 &&
                    requirementData.AuctionVendors != null && requirementData.AuctionVendors.Count > 0)
                {
                    foreach (var vendor in requirementData.AuctionVendors)
                    {
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Value = $"Final Order Placement ({vendor.VendorID})";
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(221, 235, 247));
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        count++;
                    }
                }


                wsSheet1.Cells[lineNumber, dataStartCells + count].Value = "Total";
                wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(221, 235, 247));
                wsSheet1.Cells[lineNumber, dataStartCells + count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells[lineNumber, dataStartCells + count].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                
                wsSheet1.Cells[lineNumber, dataStartCells + count + 1].Value = "Remark";
                wsSheet1.Cells[lineNumber, dataStartCells + count + 2].Value = "Approve/Reject";


                bool validationsAdded = false;
                //Data Fill
                lineNumber = lineNumber + 1;
                foreach (var item in requirementData.ListRequirementItems)
                {
                    wsSheet1.Cells[lineNumber, 1].Value = item.ItemID;
                    wsSheet1.Cells[lineNumber, 2].Value = item.ARTICLE_TYPE;
                    wsSheet1.Cells[lineNumber, 3].Value = item.ProductIDorName;
                    wsSheet1.Cells[lineNumber, 4].Value = item.ProductCode;
                    wsSheet1.Cells[lineNumber, 5].Value = item.ProductNo;
                    wsSheet1.Cells[lineNumber, 6].Value = item.BRANDS;
                    wsSheet1.Cells[lineNumber, 7].Value = item.GENDER;
                    wsSheet1.Cells[lineNumber, 8].Value = item.ProductQuantity;
                    wsSheet1.Cells[lineNumber, 9].Value = item.SEASON;
                    wsSheet1.Cells[lineNumber, 10].Value = item.BuyerTargetCost;

                    count = 0;
                    foreach (var vendor in requirementData.AuctionVendors)
                    {
                        if (vendor.ListRequirementItems.Any(l => l.ItemID == item.ItemID))
                        {
                            wsSheet1.Cells[lineNumber, dataStartCells + count].Value = vendor.ListRequirementItems.Where(l => l.ItemID == item.ItemID).First().RevUnitPrice;
                        }
                        else
                        {
                            wsSheet1.Cells[lineNumber, dataStartCells + count].Value = 0;
                        }

                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;

                        var itemColor = vendor.ListRequirementItems.Where(l => l.ItemID == item.ItemID).ToList();
                        if (itemColor != null && itemColor.Count() > 0) 
                        {
                            var found = highestPrices.Where(a => a.ItemID == itemColor[0].ItemID && a.VendorID == vendor.VendorID).ToList();
                            if (found != null && found.Count() > 0)
                            {
                                wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 0, 0));
                            } else 
                            {
                                var rankColor = 0;
                                rankColor = itemColor[0].ItemRank;
                                if (rankColor > 0)
                                {
                                    if (rankColor == 1)
                                    {
                                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(0, 255, 0));
                                    }
                                    else if (rankColor == 2)
                                    {
                                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(0, 0, 255));
                                    }
                                    else if (rankColor == 3)
                                    {
                                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 255, 0));
                                    }
                                    else if (rankColor == 4)
                                    {
                                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 165, 0));
                                    }
                                }
                                else
                                {
                                    wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 242, 204));
                                }
                            }
                        }
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        count++;
                    }

                    string startCellFormula = ((char)(65 + dataStartCells + count - 1)).ToString();
                    foreach (var vendor in requirementData.AuctionVendors)
                    {
                        if (vendor.ListRequirementItems.Any(l => l.ItemID == item.ItemID))
                        {
                            var currentVendorItem = vendor.ListRequirementItems.Where(l => l.ItemID == item.ItemID).First();
                            wsSheet1.Cells[lineNumber, dataStartCells + count].Value = currentVendorItem.QCSAllocatedQuantity;
                        }
                        else
                        {
                            wsSheet1.Cells[lineNumber, dataStartCells + count].Value = 0;
                        }

                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(221, 235, 247));
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        count++;
                    }

                    string endCellFormula = ((char)(65 + dataStartCells + count - 2)).ToString();
                    string tempColumn = ((char)(65 + dataStartCells + count)).ToString();
                    wsSheet1.Cells[lineNumber, dataStartCells + count].Formula = $"=SUM({startCellFormula}{lineNumber}:{endCellFormula}{lineNumber})";
                    string alphabet = ExcelCellAddress.GetColumnLetter(dataStartCells + count + 1);
                    if (!validationsAdded)
                    {
                        var validation = wsSheet1.DataValidations.AddListValidation($"{alphabet}{lineNumber}:{alphabet}1000");
                        validation.ShowErrorMessage = true;
                        validation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                        validation.ErrorTitle = "Invalid remarks";
                        validation.Error = "Invalid remarks";
                        validation.Formula.Values.Add("OTIF");
                        validation.Formula.Values.Add("Same vendor repeat");
                        validation.Formula.Values.Add("Pre-Production Approval");
                        validation.Formula.Values.Add("Quality");
                        validation.Formula.Values.Add("Capacity");
                        validation.Formula.Values.Add("Capability");
                        validation.Formula.Values.Add("Best Price");
                    }
                    
                    wsSheet1.Cells[lineNumber, dataStartCells + count + 1].Value = "";

                    tempColumn = ((char)(65 + dataStartCells + count + 1)).ToString();
                    alphabet = ExcelCellAddress.GetColumnLetter(dataStartCells + count + 2);
                    if (!validationsAdded)
                    {
                        var validation1 = wsSheet1.DataValidations.AddListValidation($"{alphabet}{lineNumber}:{alphabet}1000");
                        validation1.ShowErrorMessage = true;
                        validation1.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                        validation1.ErrorTitle = "Invalid status";
                        validation1.Error = "Invalid status";
                        validation1.Formula.Values.Add("Approved");
                        validation1.Formula.Values.Add("Rejected");
                    }
                    wsSheet1.Cells[lineNumber, dataStartCells + count + 2].Value = "";

                    validationsAdded = true;

                    lineNumber++;
                }

                //^Data Fill


                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

              return Convert.ToBase64String(ms.ToArray());
        }

        public string GetImportQCSTemplate(int reqId, string qcsType, string sessionId)
        {
            Utilities.ValidateSession(sessionId, null);
            MemoryStream ms = new MemoryStream();
            byte[] contents = null;

            try
            {
                ExcelRequirement excelRequirement = GetReqReportForExcel(reqId, sessionId);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("QCS_" + qcsType);
                ExcelWorksheet wsSheet2 = ExcelPkg.Workbook.Worksheets.Add("Vendor Score Cards");


                if (excelRequirement != null && excelRequirement.ReqItems != null && excelRequirement.ReqItems.Length > 0 &&
                    excelRequirement.ReqItems[0].ReqVendors != null && excelRequirement.ReqItems[0].ReqVendors.Length > 0)
                {
                    wsSheet2.Cells[1, 1].Value = "Benchmark Compliance Score";
                    wsSheet2.Cells[1, 2].Value = "Benchmark Cost Competitiveness";
                    wsSheet2.Cells[1, 3].Value = "Benchmark Design Support";
                    wsSheet2.Cells[1, 4].Value = "Benchmark Lead Time";
                    wsSheet2.Cells[1, 5].Value = "Benchmark Otif";
                    wsSheet2.Cells[1, 6].Value = "Benchmark PreProduction Approval";
                    wsSheet2.Cells[1, 7].Value = "Benchmark Q2Rejection";
                    wsSheet2.Cells[1, 8].Value = "Benchmark Qtm";
                    wsSheet2.Cells[1, 9].Value = "Benchmark Return percentage";
                    wsSheet2.Cells[1, 10].Value = "Benchmark Service";
                    wsSheet2.Cells[1, 11].Value = "Benchmark Sgc";
                    wsSheet2.Cells[1, 12].Value = "Bucket Tier";
                    wsSheet2.Cells[1, 13].Value = "Compliance Score";
                    wsSheet2.Cells[1, 14].Value = "Cost Competitiveness";
                    wsSheet2.Cells[1, 15].Value = "Design Support";
                    wsSheet2.Cells[1, 16].Value = "Lead Time";
                    wsSheet2.Cells[1, 17].Value = "Operational Total Score";
                    wsSheet2.Cells[1, 18].Value = "Otif";
                    wsSheet2.Cells[1, 19].Value = "Pre Production Approval";
                    wsSheet2.Cells[1, 20].Value = "Q2 Rejection";
                    wsSheet2.Cells[1, 21].Value = "Qtm";
                    wsSheet2.Cells[1, 22].Value = "Return percentage";
                    wsSheet2.Cells[1, 23].Value = "Service";
                    wsSheet2.Cells[1, 24].Value = "Sgc";
                    wsSheet2.Cells[1, 25].Value = "Tier Rating";
                    wsSheet2.Cells[1, 26].Value = "Weighted Benchmark Compliance Score";
                    wsSheet2.Cells[1, 27].Value = "Weighted Benchmark Cost Competitiveness";
                    wsSheet2.Cells[1, 28].Value = "Weighted Benchmark Design Support";
                    wsSheet2.Cells[1, 29].Value = "Weighted Benchmark Lead Time";
                    wsSheet2.Cells[1, 30].Value = "Weighted Benchmark Otif";
                    wsSheet2.Cells[1, 31].Value = "Weighted Benchmark Pre Production Approval";
                    wsSheet2.Cells[1, 32].Value = "Weighted Benchmark Q2 Rejection";
                    wsSheet2.Cells[1, 33].Value = "Weighted Benchmark Qtm";
                    wsSheet2.Cells[1, 34].Value = "Weighted Benchmark Return percentage";
                    wsSheet2.Cells[1, 35].Value = "Weighted Benchmark Service";
                    wsSheet2.Cells[1, 36].Value = "Weighted Benchmark Sgc";
                    foreach (var vendor in excelRequirement.ReqItems[0].ReqVendors)
                    {
                        List<KeyValuePair> keyValuePairs = prmservices.GetVendorCapacityAndScoreCard(vendor.VendorID, sessionId);
                    }
                }

                int lineNumber = 1;
                int emptyCells = 10;
                int dataStartCells = emptyCells + 1;
                //wsSheet1.Cells.Style.Border.Top.Style = ExcelBorderStyle.Medium;
                for (int i = 1; i <= emptyCells; i++)
                {
                    wsSheet1.Cells[lineNumber, i].Value = "";
                }

                int count = 0;
                if (excelRequirement != null && excelRequirement.ReqItems != null && excelRequirement.ReqItems.Length > 0 &&
                    excelRequirement.ReqItems[0].ReqVendors != null && excelRequirement.ReqItems[0].ReqVendors.Length > 0)
                {
                    foreach (var vendor in excelRequirement.ReqItems[0].ReqVendors)
                    {
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Value = vendor.CompanyName;
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 242, 204));
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        count++;
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Value = "";
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 242, 204));
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        count++;
                    }
                }

                count++;
                if (excelRequirement != null && excelRequirement.ReqItems != null && excelRequirement.ReqItems.Length > 0 &&
                    excelRequirement.ReqItems[0].ReqVendors != null && excelRequirement.ReqItems[0].ReqVendors.Length > 0)
                {
                    foreach (var vendor in excelRequirement.ReqItems[0].ReqVendors)
                    {
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Value = vendor.CompanyName;
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(221, 235, 247));
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        count++;
                    }
                }

                for (int i = dataStartCells + count; i <= (dataStartCells + count) + 2; i++)
                {
                    wsSheet1.Cells[lineNumber, i].Value = "";
                }

                lineNumber++;


                wsSheet1.Cells[lineNumber, 1].Value = "Item Id";
                wsSheet1.Cells[lineNumber, 2].Value = "Article Type";
                wsSheet1.Cells[lineNumber, 3].Value = "Article Name";
                wsSheet1.Cells[lineNumber, 4].Value = "Article Number";
                wsSheet1.Cells[lineNumber, 5].Value = "Style ID";
                wsSheet1.Cells[lineNumber, 6].Value = "Brand";
                wsSheet1.Cells[lineNumber, 7].Value = "Gender";
                wsSheet1.Cells[lineNumber, 8].Value = "Quantity";
                wsSheet1.Cells[lineNumber, 9].Value = "Season";
                wsSheet1.Cells[lineNumber, 10].Value = "Target Cost";

                count = 0;
                if (excelRequirement != null && excelRequirement.ReqItems != null && excelRequirement.ReqItems.Length > 0 &&
                    excelRequirement.ReqItems[0].ReqVendors != null && excelRequirement.ReqItems[0].ReqVendors.Length > 0)
                {
                    foreach (var vendor in excelRequirement.ReqItems[0].ReqVendors)
                    {
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Value = $"{vendor.SelectedVendorCurrency}";
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 242, 204));
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        count++;
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Value = "INR";
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 242, 204));
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        count++;
                    }
                }

                wsSheet1.Cells[lineNumber, dataStartCells + count].Value = "Transportation";
                wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(0, 176, 240));
                wsSheet1.Cells[lineNumber, dataStartCells + count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells[lineNumber, dataStartCells + count].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                count++;

                if (excelRequirement != null && excelRequirement.ReqItems != null && excelRequirement.ReqItems.Length > 0 &&
                    excelRequirement.ReqItems[0].ReqVendors != null && excelRequirement.ReqItems[0].ReqVendors.Length > 0)
                {
                    foreach (var vendor in excelRequirement.ReqItems[0].ReqVendors)
                    {
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Value = $"Final Order Placement ({vendor.VendorID})";
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(221, 235, 247));
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        wsSheet1.Cells[lineNumber, dataStartCells + count].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        count++;
                    }
                }


                wsSheet1.Cells[lineNumber, dataStartCells + count].Value = "Total";
                wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(221, 235, 247));
                wsSheet1.Cells[lineNumber, dataStartCells + count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells[lineNumber, dataStartCells + count].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                wsSheet1.Cells[lineNumber, dataStartCells + count + 1].Value = "Remark";
                wsSheet1.Cells[lineNumber, dataStartCells + count + 2].Value = "Approve/Reject";


                bool validationsAdded = false;
                //Data Fill
                lineNumber = lineNumber + 1;
                foreach (var item in excelRequirement.ReqItems)
                {
                    wsSheet1.Cells[lineNumber, 1].Value = item.ItemID;
                    wsSheet1.Cells[lineNumber, 2].Value = item.ArticleType;
                    wsSheet1.Cells[lineNumber, 3].Value = item.ProductIDorName;
                    wsSheet1.Cells[lineNumber, 4].Value = item.ProductCode;
                    wsSheet1.Cells[lineNumber, 5].Value = item.ProductNo;
                    wsSheet1.Cells[lineNumber, 6].Value = item.ProductBrand;
                    wsSheet1.Cells[lineNumber, 7].Value = "";
                    wsSheet1.Cells[lineNumber, 8].Value = item.ProductQuantity;
                    wsSheet1.Cells[lineNumber, 9].Value = item.SEASON;
                    wsSheet1.Cells[lineNumber, 10].Value = item.BuyerTargetCost;

                    count = 0;
                    if (item.ReqVendors.Length > 0)
                    {
                        //item.ReqVendors = item.ReqVendors.OrderBy(o => o.QuotationPrices.RevUnitPrice == 0).ThenBy(o => o.QuotationPrices.RevUnitPrice).ToArray();
                        //int count = 1;
                        foreach (var vendor in item.ReqVendors)
                        {
                            wsSheet1.Cells[lineNumber, dataStartCells + count].Value = vendor.QuotationPrices.RevUnitPrice;
                            wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 242, 204));
                            wsSheet1.Cells[lineNumber, dataStartCells + count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            wsSheet1.Cells[lineNumber, dataStartCells + count].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            count++;

                            wsSheet1.Cells[lineNumber, dataStartCells + count].Value = (vendor.QuotationPrices.RevUnitPrice * 10);
                            wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 242, 204));
                            wsSheet1.Cells[lineNumber, dataStartCells + count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            wsSheet1.Cells[lineNumber, dataStartCells + count].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            count++;

                        }
                    }

                    wsSheet1.Cells[lineNumber, dataStartCells + count].Value = "";
                    wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(0, 176, 240));
                    wsSheet1.Cells[lineNumber, dataStartCells + count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    wsSheet1.Cells[lineNumber, dataStartCells + count].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    count++;

                    string startCellFormula = ((char)(65 + dataStartCells + count - 1)).ToString();
                    if (item.ReqVendors.Length > 0)
                    {
                        foreach (var vendor in excelRequirement.ReqItems[0].ReqVendors)
                        {
                            wsSheet1.Cells[lineNumber, dataStartCells + count].Value = vendor.QuotationPrices.QCSAllocatedQuantity;
                            wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            wsSheet1.Cells[lineNumber, dataStartCells + count].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(221, 235, 247));
                            wsSheet1.Cells[lineNumber, dataStartCells + count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            wsSheet1.Cells[lineNumber, dataStartCells + count].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            count++;
                        }
                    }

                    string endCellFormula = ((char)(65 + dataStartCells + count - 2)).ToString();
                    string tempColumn = ((char)(65 + dataStartCells + count)).ToString();
                    wsSheet1.Cells[lineNumber, dataStartCells + count].Formula = $"=SUM({startCellFormula}{lineNumber}:{endCellFormula}{lineNumber})";

                    if (!validationsAdded)
                    {
                        var validation = wsSheet1.DataValidations.AddListValidation($"{tempColumn}{lineNumber}:{tempColumn}1000");
                        validation.ShowErrorMessage = true;
                        validation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                        validation.ErrorTitle = "Invalid remarks";
                        validation.Error = "Invalid remarks";
                        validation.Formula.Values.Add("OTIF");
                        validation.Formula.Values.Add("Same vendor repeat");
                        validation.Formula.Values.Add("Pre-Production Approval");
                        validation.Formula.Values.Add("Quality");
                        validation.Formula.Values.Add("Capacity");
                        validation.Formula.Values.Add("Capability");
                    }

                    wsSheet1.Cells[lineNumber, dataStartCells + count + 1].Value = "";

                    tempColumn = ((char)(65 + dataStartCells + count + 1)).ToString();
                    if (!validationsAdded)
                    {
                        var validation1 = wsSheet1.DataValidations.AddListValidation($"{tempColumn}{lineNumber}:{tempColumn}1000");
                        validation1.ShowErrorMessage = true;
                        validation1.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                        validation1.ErrorTitle = "Invalid status";
                        validation1.Error = "Invalid status";
                        validation1.Formula.Values.Add("Approved");
                        validation1.Formula.Values.Add("Rejected");
                    }
                    wsSheet1.Cells[lineNumber, dataStartCells + count + 2].Value = "";

                    validationsAdded = true;

                    lineNumber++;
                }

                //^Data Fill


                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return Convert.ToBase64String(ms.ToArray());
        }

        #endregion

        #region Post

        #endregion


        #region Private

        private string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["MySQLConnectionString"].ConnectionString;
        }

        private int ValidateSession(string sessionId)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            if (string.IsNullOrEmpty(sessionId))
            {
                throw new Exception("Session ID is null");
            }
            
            int isValid = 1;
            try
            {
                sd.Add("P_SESSION_ID", sessionId);
                DataSet ds = sqlHelper.SelectList("cp_ValidateSession", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    isValid = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            //catch (MySqlException ex)
            //{
            //    HttpContext.Current.Server.Transfer("/prm360.html#/login");
            //}
            catch (Exception ex)
            {
                isValid = 0;
            }

            if (isValid <= 0)
            {
                throw new Exception("Invalid Session ID passed");
            }

            return isValid;
        }

        private void SaveFile(string fileName, byte[] fileContent)
        {
            Utilities.SaveFile(fileName, fileContent);
            //PTMGenericServicesClient ptmClient = new PTMGenericServicesClient();
            //ptmClient.SaveFileBytes(fileContent, fileName);
        }

        private Response SaveAttachment(string path)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {   
                sd.Add("P_PATH", path);
                DataSet ds = sqlHelper.SelectList("cp_SaveAttachment", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        #endregion

        #region QCS

        public List<QCSDetails> GetQCSList(int uid, int reqid, string sessionid)
        {
            List<QCSDetails> details = new List<QCSDetails>();

            try
            {
                Utilities.ValidateSession(sessionid, null);
                string query = string.Format("SELECT * FROM qcs_details WHERE REQ_ID = {0} ORDER BY REQ_ID DESC", reqid);
                var dt = sqlHelper.SelectQuery(query);
                CORE.DataNamesMapper<QCSDetails> mapper = new CORE.DataNamesMapper<QCSDetails>();
                details = mapper.Map(dt).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;

        }

        public QCSDetails GetQCSDetails(int qcsid, string sessionid)
        {
            QCSDetails detail = new QCSDetails();

            try
            {
                Utilities.ValidateSession(sessionid, null);
                string query = string.Format("SELECT * FROM qcs_details WHERE QCS_ID = {0}", qcsid);
                var dt = sqlHelper.SelectQuery(query);
                CORE.DataNamesMapper<QCSDetails> mapper = new CORE.DataNamesMapper<QCSDetails>();
                detail = mapper.Map(dt).FirstOrDefault();
            }
            catch (Exception ex)
            {
                detail.ErrorMessage = ex.Message;
                logger.Error(ex, "Error in retrieving Data");
            }

            return detail;

        }

        public Response SaveQCSDetails(QCSDetails qcsdetails, string sessionid)
        {

            Response details = new Response();
            try
            {
                Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };

                sd.Add("P_QCS_ID", qcsdetails.QCS_ID);
                sd.Add("P_U_ID", qcsdetails.U_ID);
                sd.Add("P_REQ_ID", qcsdetails.REQ_ID);
                sd.Add("P_QCS_CODE", qcsdetails.QCS_CODE);
                sd.Add("P_QCS_TYPE", qcsdetails.QCS_TYPE);
                sd.Add("P_PO_CODE", qcsdetails.PO_CODE);
                sd.Add("P_RECOMMENDATIONS", qcsdetails.RECOMMENDATIONS);
                sd.Add("P_UNIT_CODE", qcsdetails.UNIT_CODE);
                sd.Add("P_WF_ID", qcsdetails.WF_ID);
                sd.Add("P_REQ_JSON", qcsdetails.REQ_JSON);

                CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();
                var dataset = sqlHelper.SelectList("qcs_SaveQCSDetails", sd);
                //details = mapper.Map(dataset.Tables[0]).FirstOrDefault();
                if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0 && dataset.Tables[0].Rows[0][0] != null)
                {
                    details.ObjectID = dataset.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(dataset.Tables[0].Rows[0][0].ToString()) : -1;
                    details.ErrorMessage = dataset.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(dataset.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }

                if (details.ObjectID > 0 && qcsdetails.WF_ID > 0)
                {
                    PRMWFService pRMWF = new PRMWFService();
                    Response res2 = pRMWF.AssignWorkflow(qcsdetails.WF_ID, Convert.ToInt32(details.ObjectID), qcsdetails.U_ID, sessionid);
                }



            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        #endregion QCS
    }
}
