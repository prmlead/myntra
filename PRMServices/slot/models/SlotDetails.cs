﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class SlotDetails : Entity
    {
        [DataMember(Name = "slot_id")]
        public int Slot_id { get; set; }

        [DataMember(Name = "slot_description")]
        public string Slot_description { get; set; }

        [DataMember(Name = "slot_entity_id")]
        public int Slot_entity_id { get; set; }

        [DataMember(Name = "slot_start_time")]
        public DateTime? Slot_Start_Time { get; set; }

        [DataMember(Name = "slot_end_time")]
        public DateTime? Slot_End_Time { get; set; }

        [DataMember(Name = "slot_is_valid")]
        public int Slot_is_valid { get; set; }

        [DataMember(Name = "slot_entity_type")]
        public string Slot_entity_type { get; set; }

        [DataMember(Name = "slot_comp_id")]
        public int Slot_comp_id { get; set; }

        [DataMember(Name = "slot_created_by")]
        public int Slot_created_by { get; set; }

        [DataMember(Name = "slot_modified_by")]
        public int Slot_modified_by { get; set; }

        [DataMember(Name = "slot_created_date")]
        public DateTime? Slot_created_date { get; set; }

        [DataMember(Name = "slot_modified_date")]
        public DateTime? Slot_modified_date { get; set; }

        [DataMember(Name = "slot_no_of_vendors")]
        public int Slot_no_of_vendors { get; set; }

        [DataMember(Name = "slot_user")]
        public int Slot_user { get; set; }

        [DataMember(Name = "slot_flag")]
        public string Slot_flag { get; set; }

        [DataMember(Name = "slot_item_id")]
        public int Slot_item_id { get; set; }

        [DataMember(Name = "vendorSlot")]
        public bool VendorSlot { get; set; }

        [DataMember(Name = "isVendorBooked")]
        public bool IsVendorBooked { get; set; }

        [DataMember(Name = "slot_booked_count")]
        public int Slot_booked_count { get; set; }

        

        [DataMember(Name = "VENDORS_SLOT_BOOKED")]
        public string VENDORS_SLOT_BOOKED { get; set; }

        [DataMember(Name = "VENDORS_SLOT_CANCELED")]
        public string VENDORS_SLOT_CANCELED { get; set; }

        [DataMember(Name = "VENDORS_SLOT_REJECTED")]
        public string VENDORS_SLOT_REJECTED { get; set; }

        [DataMember(Name = "VENDORS_SLOT_APPROVED")]
        public string VENDORS_SLOT_APPROVED { get; set; }

        [DataMember(Name = "VENDOR_SLOT_STATUS")]
        public string VENDOR_SLOT_STATUS { get; set; }

    }
}