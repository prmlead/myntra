﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class FileType : Entity
    {
        [DataMember(Name = "fileArray")]
        public byte[] FileArray { get; set; }

        [DataMember(Name = "fileBase64")]
        public String FileBase64 { get; set; }
    }
}