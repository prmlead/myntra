﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class ApparelProcess : Entity
    {

        [DataMember(Name = "WASH_ID")]
        [DataNames("WASH_ID")]
        public string WASH_ID { get; set; }

        [DataMember(Name = "PRINT_ID")]
        [DataNames("PRINT_ID")]
        public string PRINT_ID { get; set; }

        [DataMember(Name = "EMB_ID")]
        [DataNames("EMB_ID")]
        public string EMB_ID { get; set; }

        [DataMember(Name = "APP_ID")]
        [DataNames("APP_ID")]
        public string APP_ID { get; set; }

        [DataMember(Name = "E_ID")]
        [DataNames("E_ID")]
        public string E_ID { get; set; }

        [DataMember(Name = "COLORS")]
        [DataNames("COLORS")]
        public string COLORS { get; set; }

        [DataMember(Name = "SIZES")]
        [DataNames("SIZES")]
        public string SIZES { get; set; }

        [DataMember(Name = "COST")]
        [DataNames("COST")]
        public double? COST { get; set; }

        
            [DataMember(Name = "PRINT_TYPE")]
        [DataNames("PRINT_TYPE")]
        public string PRINT_TYPE { get; set; }

        [DataMember(Name = "PROCESS_TYPE")]
        [DataNames("PROCESS_TYPE")]
        public string PROCESS_TYPE { get; set; }

        [DataMember(Name = "PROCESS_CATEGORY")]
        [DataNames("PROCESS_CATEGORY")]
        public string PROCESS_CATEGORY { get; set; }

        [DataMember(Name = "PROCESS_DISCRIPTION")]
        [DataNames("PROCESS_DISCRIPTION")]
        public string PROCESS_DISCRIPTION { get; set; }

        [DataMember(Name = "WASH_TYPE")]
        [DataNames("WASH_TYPE")]
        public string WASH_TYPE { get; set; }

        [DataMember(Name = "TYPE_OF_GARMENT")]
        [DataNames("TYPE_OF_GARMENT")]
        public string TYPE_OF_GARMENT { get; set; }


        [DataMember(Name = "TYPE_OF_PRODUCT")]
        [DataNames("TYPE_OF_PRODUCT")]
        public string TYPE_OF_PRODUCT { get; set; }

        [DataMember(Name = "COST_PER_GARMENT")]
        [DataNames("COST_PER_GARMENT")]
        public double COST_PER_GARMENT { get; set; }

        [DataMember(Name = "IS_PROCESS_NOMINATED")]
        [DataNames("IS_PROCESS_NOMINATED")]
        public int IS_PROCESS_NOMINATED { get; set; }

        [DataMember(Name = "UOM")]
        [DataNames("UOM")]
        public string UOM { get; set; }

        [DataMember(Name = "UNIT_PRICE")]
        [DataNames("UNIT_PRICE")]
        public double? UNIT_PRICE { get; set; }

        [DataMember(Name = "selectedWashTypes")]
        [DataNames("selectedWashTypes")]
        public string selectedWashTypes { get; set; }
        

        [DataMember(Name = "CONSUMPTION")]
        [DataNames("CONSUMPTION")]
        public double? CONSUMPTION { get; set; }

        [DataMember(Name = "REV_UNIT_PRICE")]
        [DataNames("REV_UNIT_PRICE")]
        public double? REV_UNIT_PRICE { get; set; }

        [DataMember(Name = "REV_CONSUMPTION")]
        [DataNames("REV_CONSUMPTION")]
        public double? REV_CONSUMPTION { get; set; }

        [DataMember(Name = "FILE_ID")]
        [DataNames("FILE_ID")]
        public byte[] FILE_ID { get; set; }

        [DataMember(Name = "FILE_IMAGE_ID")]
        [DataNames("FILE_IMAGE_ID")]
        public int FILE_IMAGE_ID { get; set; }

        [DataMember(Name = "FILE_NAME")]
        [DataNames("FILE_NAME")]
        public string FILE_NAME { get; set; }

        [DataMember(Name = "NOMINATED_SUPPLIER")]
        [DataNames("NOMINATED_SUPPLIER")]
        public string NOMINATED_SUPPLIER { get; set; }

        [DataMember(Name = "EMBROIDERY_NAME")]
        [DataNames("EMBROIDERY_NAME")]
        public string EMBROIDERY_NAME { get; set; }

        [DataMember(Name = "APPLIQUE_NAME")]
        [DataNames("APPLIQUE_NAME")]
        public string APPLIQUE_NAME { get; set; }

        [DataMember(Name = "EMBELLISHMENT_NAME")]
        [DataNames("EMBELLISHMENT_NAME")]
        public string EMBELLISHMENT_NAME { get; set; }

        [DataMember(Name = "TOTAL_SUB_ITEM_PRICE")]
        [DataNames("TOTAL_SUB_ITEM_PRICE")]
        public double? TOTAL_SUB_ITEM_PRICE { get; set; }

        [DataMember(Name = "WASH_TYPES_PRICE")]
        [DataNames("WASH_TYPES_PRICE")]
        public double? WASH_TYPES_PRICE { get; set; }

        [DataMember(Name = "TYPE")]
        [DataNames("TYPE")]
        public string TYPE { get; set; }

        [DataMember(Name = "typeOfGarmentList")]
        [DataNames("typeOfGarmentList")]
        public string typeOfGarmentList { get; set; }

        [DataMember(Name = "IS_NEW_OBJ")]
        [DataNames("IS_NEW_OBJ")]
        public int IS_NEW_OBJ { get; set; }

        [DataMember(Name = "washTypes")]
        public List<washTypes> washTypes { get; set; }
    }

    public class washTypes
    {
        [DataMember(Name = "washType")]
        [DataNames("washType")]
        public string washType { get; set; }

        [DataMember(Name = "isChecked")]
        [DataNames("isChecked")]
        public bool isChecked { get; set; }
    }
}