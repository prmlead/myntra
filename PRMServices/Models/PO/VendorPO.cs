﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using MySql.Data.MySqlClient;
using PRMServices.Models;

namespace PRMServices
{
    [DataContract]
    public class VendorPO : ResponseAudit
    {
        [DataMember(Name = "vendor")]
        public UserDetails Vendor { get; set; }

        [DataMember(Name = "poFile")]
        public FileUpload POFile { get; set; }

        [DataMember(Name = "req")]
        public Requirement Req { get; set; }

        [DataMember(Name = "listPOItems")]
        public List<POItems> ListPOItems { get; set; }

        string poLink = string.Empty;
        [DataMember(Name = "poLink")]
        public string POLink
        {
            get
            {
                if (!string.IsNullOrEmpty(poLink))
                { return poLink; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { poLink = value; }
            }
        }

        [DataMember(Name = "common")]
        public bool Common { get; set; }


        string purchaseOrderID = string.Empty;
        [DataMember(Name = "purchaseOrderID")]
        public string PurchaseOrderID
        {
            get
            {
                if (!string.IsNullOrEmpty(purchaseOrderID))
                { return purchaseOrderID; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { purchaseOrderID = value; }
            }
        }

    }
}