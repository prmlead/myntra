﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class ApparelSundries : Entity
    {
        // CATEGORY, POLYBAG, CARTON, PHILOSPHY_LABLE, NOMINATED_SUPPLIER

        [DataMember(Name = "SEWING_ID")]
        [DataNames("SEWING_ID")]
        public string SEWING_ID { get; set; }

        [DataMember(Name = "PACK_ID")]
        [DataNames("PACK_ID")]
        public string PACK_ID { get; set; }

        [DataMember(Name = "SEWING_NAME")]
        [DataNames("SEWING_NAME")]
        public string SEWING_NAME { get; set; }

        [DataMember(Name = "PACKAGING_NAME")]
        [DataNames("PACKAGING_NAME")]
        public string PACKAGING_NAME { get; set; }

        [DataMember(Name = "CATEGORY")]
        [DataNames("CATEGORY")]
        public string CATEGORY { get; set; }

        [DataMember(Name = "POLYBAG")]
        [DataNames("POLYBAG")]
        public string POLYBAG { get; set; }

       
            [DataMember(Name = "CARTON")]
        [DataNames("CARTON")]
        public string CARTON { get; set; }

        [DataMember(Name = "PHILOSPHY_LABLE")]
        [DataNames("PHILOSPHY_LABLE")]
        public string PHILOSPHY_LABLE { get; set; }

        [DataMember(Name = "TYPE_OF_SUNDRIES")]
        [DataNames("TYPE_OF_SUNDRIES")]
        public string PROCESS_CATEGORY { get; set; }

        [DataMember(Name = "STYLING")]
        [DataNames("STYLING")]
        public string STYLING { get; set; }

        [DataMember(Name = "ARTICLE_TYPE")]
        [DataNames("ARTICLE_TYPE")]
        public string ARTICLE_TYPE { get; set; }

        [DataMember(Name = "BRANDS")]
        [DataNames("BRANDS")]
        public string BRANDS { get; set; }

        [DataMember(Name = "GENDER")]
        [DataNames("GENDER")]
        public string GENDER { get; set; }

        [DataMember(Name = "SEWING_COST")]
        [DataNames("SEWING_COST")]
        public double? SEWING_COST { get; set; }

        [DataMember(Name = "PACKAGING_COST")]
        [DataNames("PACKAGING_COST")]
        public double? PACKAGING_COST { get; set; }

        [DataMember(Name = "REV_SEWING_COST")]
        [DataNames("REV_SEWING_COST")]
        public double? REV_SEWING_COST { get; set; }

        [DataMember(Name = "REV_PACKAGING_COST")]
        [DataNames("REV_PACKAGING_COST")]
        public double? REV_PACKAGING_COST { get; set; }

        [DataMember(Name = "NOMINATED_SUPPLIER")]
        [DataNames("NOMINATED_SUPPLIER")]
        public string NOMINATED_SUPPLIER { get; set; }

        [DataMember(Name = "TOTAL_SUB_ITEM_PRICE")]
        [DataNames("TOTAL_SUB_ITEM_PRICE")]
        public double? TOTAL_SUB_ITEM_PRICE { get; set; }

        [DataMember(Name = "TYPE")]
        [DataNames("TYPE")]
        public string TYPE { get; set; }

        [DataMember(Name = "PACKAGING_CONSUMPTION")]
        [DataNames("PACKAGING_CONSUMPTION")]
        public double? PACKAGING_CONSUMPTION { get; set; }

        [DataMember(Name = "SEWING_CONSUMPTION")]
        [DataNames("SEWING_CONSUMPTION")]
        public double? SEWING_CONSUMPTION { get; set; }

        [DataMember(Name = "REV_PACKAGING_CONSUMPTION")]
        [DataNames("REV_PACKAGING_CONSUMPTION")]
        public double? REV_PACKAGING_CONSUMPTION { get; set; }

        [DataMember(Name = "REV_SEWING_CONSUMPTION")]
        [DataNames("REV_SEWING_CONSUMPTION")]
        public double? REV_SEWING_CONSUMPTION { get; set; }

        [DataMember(Name = "IS_SUNDRIES_NOMINATED")]
        [DataNames("IS_SUNDRIES_NOMINATED")]
        public int IS_SUNDRIES_NOMINATED { get; set; }

        [DataMember(Name = "IS_NEW_OBJ")]
        [DataNames("IS_NEW_OBJ")]
        public int IS_NEW_OBJ { get; set; }

        [DataMember(Name = "FILE_ID")]
        [DataNames("FILE_ID")]
        public byte[] FILE_ID { get; set; }

        [DataMember(Name = "FILE_IMAGE_ID")]
        [DataNames("FILE_IMAGE_ID")]
        public int FILE_IMAGE_ID { get; set; }

        [DataMember(Name = "FILE_NAME")]
        [DataNames("FILE_NAME")]
        public string FILE_NAME { get; set; }

        [DataMember(Name = "UOM")]
        [DataNames("UOM")]
        public string UOM { get; set; }
    }
}