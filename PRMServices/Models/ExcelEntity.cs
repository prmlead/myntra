﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRMServices.Models
{
    public class ExcelEntity
    {
        public ExcelPackage excelPackage { get; set; }
        public bool isDataUpdated { get; set; }
    }
}