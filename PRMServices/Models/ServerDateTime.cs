﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class ServerDateTime : Entity
    {

        [DataMember(Name = "YEAR")]
        public int YEAR { get; set; }

        [DataMember(Name = "MONTH")]
        public int MONTH { get; set; }

        [DataMember(Name = "DAY")]
        public int DAY { get; set; }

        [DataMember(Name = "HOUR")]
        public int HOUR { get; set; }

        [DataMember(Name = "MIN")]
        public int MIN { get; set; }

        [DataMember(Name = "SEC")]
        public int SEC { get; set; }

    }
}