﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class ApparelFabric : Entity
    {
        

            [DataMember(Name = "FABRIC_ID")]
        [DataNames("FABRIC_ID")]
        public string FABRIC_ID { get; set; }

        [DataMember(Name = "NAME")]
        [DataNames("NAME")]
        public string NAME { get; set; }

        [DataMember(Name = "WEAVE_KNITS")]
        [DataNames("WEAVE_KNITS")]
        public string WEAVE_KNITS { get; set; }

        [DataMember(Name = "PATTERN_TYPE")]
        [DataNames("PATTERN_TYPE")]
        public string PATTERN_TYPE { get; set; }

        [DataMember(Name = "COMPOSITION")]
        [DataNames("COMPOSITION")]
        public string COMPOSITION { get; set; }

        [DataMember(Name = "COUNT")]
        [DataNames("COUNT")]
        public string COUNT { get; set; }

        [DataMember(Name = "CONSTRUCTION")]
        [DataNames("CONSTRUCTION")]
        public string CONSTRUCTION { get; set; }

        [DataMember(Name = "WT_GSM_GLM_OZ")]
        [DataNames("WT_GSM_GLM_OZ")]
        public string WT_GSM_GLM_OZ { get; set; }

        [DataMember(Name = "WIDTH")]
        [DataNames("WIDTH")]
        public string WIDTH { get; set; }

        [DataMember(Name = "AW19_PRICE")]
        [DataNames("AW19_PRICE")]
        public string AW19_PRICE { get; set; }

        [DataMember(Name = "FINAL_SS20_PRICE")]
        [DataNames("FINAL_SS20_PRICE")]
        public double? FINAL_SS20_PRICE { get; set; }

        [DataMember(Name = "CONTENT")]
        [DataNames("CONTENT")]
        public string CONTENT { get; set; }

        [DataMember(Name = "CUTTABLE_WIDTH")]
        [DataNames("CUTTABLE_WIDTH")]
        public string CUTTABLE_WIDTH { get; set; }

        [DataMember(Name = "SHRINKAGE")]
        [DataNames("SHRINKAGE")]
        public string SHRINKAGE { get; set; }

        [DataMember(Name = "DESCRIPTION")]
        [DataNames("DESCRIPTION")]
        public string DESCRIPTION { get; set; }

        [DataMember(Name = "IS_FABRIC_NOMINATED")]
        [DataNames("IS_FABRIC_NOMINATED")]
        public int IS_FABRIC_NOMINATED { get; set; }

        [DataMember(Name = "UOM")]
        [DataNames("UOM")]
        public string UOM { get; set; }

        [DataMember(Name = "WEIGHT_UOM")]
        [DataNames("WEIGHT_UOM")]
        public string WEIGHT_UOM { get; set; }


        [DataMember(Name = "UNIT_PRICE")]
        [DataNames("UNIT_PRICE")]
        public double? UNIT_PRICE { get; set; }

        [DataMember(Name = "REV_UNIT_PRICE")]
        [DataNames("REV_UNIT_PRICE")]
        public double? REV_UNIT_PRICE { get; set; }

        [DataMember(Name = "REV_CONSUMPTION")]
        [DataNames("REV_CONSUMPTION")]
        public double? REV_CONSUMPTION { get; set; }

        [DataMember(Name = "CONSUMPTION")]
        [DataNames("CONSUMPTION")]
        public double? CONSUMPTION { get; set; }

        [DataMember(Name = "MILL")]
        [DataNames("MILL")]
        public string MILL { get; set; }

        [DataMember(Name = "FILE_ID")]
        [DataNames("FILE_ID")]
        public byte[] FILE_ID { get; set; }

        [DataMember(Name = "FILE_IMAGE_ID")]
        [DataNames("FILE_IMAGE_ID")]
        public int FILE_IMAGE_ID { get; set; }

        [DataMember(Name = "FILE_NAME")]
        [DataNames("FILE_NAME")]
        public string FILE_NAME { get; set; }

        [DataMember(Name = "NOMINATED_SUPPLIER")]
        [DataNames("NOMINATED_SUPPLIER")]
        public string NOMINATED_SUPPLIER { get; set; }

        [DataMember(Name = "DESIGN_STYLE")]
        [DataNames("DESIGN_STYLE")]
        public string DESIGN_STYLE { get; set; }

        [DataMember(Name = "TOTAL_SUB_ITEM_PRICE")]
        [DataNames("TOTAL_SUB_ITEM_PRICE")]
        public double? TOTAL_SUB_ITEM_PRICE { get; set; }

        [DataMember(Name = "IS_NEW_OBJ")]
        [DataNames("IS_NEW_OBJ")]
        public int IS_NEW_OBJ { get; set; }

        [DataMember(Name = "TYPE")]
        [DataNames("TYPE")]
        public string TYPE { get; set; }

    }
}