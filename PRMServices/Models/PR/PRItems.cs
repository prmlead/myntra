﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class PRItems : Entity
    {

        [DataMember] [DataNames("ITEM_ID")] public int ITEM_ID { get; set; }
        [DataMember] [DataNames("PR_ID")] public int PR_ID { get; set; }
        [DataMember] [DataNames("PR_NUMBER")] public string PR_NUMBER { get; set; }
        [DataMember] [DataNames("ITEM_NAME")] public string ITEM_NAME { get; set; }
        [DataMember] [DataNames("ITEM_CODE")] public string ITEM_CODE { get; set; }
        [DataMember] [DataNames("ITEM_NUM")] public string ITEM_NUM { get; set; }
        //ITEM_NUM
        [DataMember] [DataNames("HSN_CODE")] public string HSN_CODE { get; set; }
        [DataMember] [DataNames("ITEM_DESCRIPTION")] public string ITEM_DESCRIPTION { get; set; }
        [DataMember] [DataNames("BRAND")] public string BRAND { get; set; }
        [DataMember] [DataNames("UNITS")] public string UNITS { get; set; }
        [DataMember] [DataNames("EXIST_QUANTITY")] public decimal EXIST_QUANTITY { get; set; }
        [DataMember] [DataNames("REQUIRED_QUANTITY")] public decimal REQUIRED_QUANTITY { get; set; }
        [DataMember] [DataNames("UNIT_PRICE")] public decimal UNIT_PRICE { get; set; }
        [DataMember] [DataNames("C_GST_PERCENTAGE")] public decimal C_GST_PERCENTAGE { get; set; }
        [DataMember] [DataNames("S_GST_PERCENTAGE")] public decimal S_GST_PERCENTAGE { get; set; }
        [DataMember] [DataNames("I_GST_PERCENTAGE")] public decimal I_GST_PERCENTAGE { get; set; }
        [DataMember] [DataNames("TOTAL_PRICE")] public decimal TOTAL_PRICE { get; set; }
        [DataMember] [DataNames("COMMENTS")] public string COMMENTS { get; set; }
        [DataMember] [DataNames("ATTACHMENTS")] public string ATTACHMENTS { get; set; }
        [DataMember] [DataNames("REQ_ID")] public int REQ_ID { get; set; }

        [DataMember] [DataNames("U_ID")] public int U_ID { get; set; }

        [DataMember] [DataNames("PRODUCT_ID")] public int PRODUCT_ID { get; set; }


        [DataMember] [DataNames("itemAttachment")] public byte[] itemAttachment { get; set; }
        [DataMember] [DataNames("attachmentName")] public string attachmentName { get; set; }

        [DataMember] [DataNames("DEPARTEMENT")] public string DEPARTEMENT { get; set; }
        [DataMember] [DataNames("DEPT_CODE")] public string DEPT_CODE { get; set; }
        [DataMember] [DataNames("PLANT")] public string PLANT { get; set; }
        [DataMember] [DataNames("DOC_TYPE")] public string DOC_TYPE { get; set; }
        [DataMember] [DataNames("DELIVERY_DATE")] public DateTime? DELIVERY_DATE { get; set; }
        [DataMember] [DataNames("DELIVERY_TEXT")] public string DELIVERY_TEXT { get; set; }
        [DataMember] [DataNames("U_NAME")] public string U_NAME { get; set; }
        [DataMember] [DataNames("CREATED_DATE")] public DateTime? CREATED_DATE { get; set; }
        [DataMember] [DataNames("PURCHASE_GROUP")] public string PURCHASE_GROUP { get; set; }
        [DataMember] [DataNames("CategoryCode")] public string CategoryCode { get; set; }
        [DataMember] [DataNames("CreatedBy")] public string CreatedBy { get; set; }
        [DataMember] [DataNames("ITEM_TEXT")] public string ITEM_TEXT { get; set; }
        [DataMember] [DataNames("PO_TEXT")] public string PO_TEXT { get; set; }
        [DataMember] [DataNames("ProductId")] public int ProductId { get; set; }

        [DataMember] [DataNames("CategoryName")] public string CategoryName { get; set; }
        [DataMember] [DataNames("IS_VALID")] public int IS_VALID { get; set; }
    }
}