﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class ReqDesignations : Entity
    {        

        [DataMember(Name = "reqDesigID")]
        public int ReqDesigID { get; set; }

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "reqID")]
        public int ReqID { get; set; }

        [DataMember(Name = "isValid")]
        public bool IsValid { get; set; }
        
        [DataMember(Name = "companyDesignations")]
        public CompanyDesignations CompanyDesignations { get; set; }

        [DataMember(Name = "createdBy")]
        public int CreatedBy { get; set; }

        [DataMember(Name = "user")]
        public User User { get; set; }
                
    }
}