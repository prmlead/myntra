﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class NegotiationSettings : Entity
    {
        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "minReductionAmount")]
        public double MinReductionAmount { get; set; }

        [DataMember(Name = "minReductionPercent")]
        public double MinReductionPercent { get; set; }

        [DataMember(Name = "rankComparision")]
        public double RankComparision { get; set; }

        [DataMember(Name = "negotiationDuration")]
        public string NegotiationDuration { get; set; }

        [DataMember(Name = "compInterest")]
        public Decimal CompInterest { get; set; } 
    }
}