﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class cr_CategoryProducts : Entity
    {

        [DataMember] [DataNames("CAT_ID")] public int CAT_ID { get; set; }

        [DataMember] [DataNames("PROD_ID")] public string PROD_ID { get; set; }
        [DataMember] [DataNames("PROD_CODE")] public string PROD_CODE { get; set; }
        [DataMember] [DataNames("PROD_NAME")] public string PROD_NAME { get; set; }
        [DataMember] [DataNames("PROD_HSN")] public string PROD_HSN { get; set; }
        [DataMember] [DataNames("PROD_NO")] public string PROD_NO { get; set; }
        [DataMember] [DataNames("PROD_UNITS")] public string PROD_UNITS { get; set; }
        [DataMember] [DataNames("PROD_DESC")] public string PROD_DESC { get; set; }
        [DataMember] [DataNames("PROD_ALT_UNITS")] public string PROD_ALT_UNITS { get; set; }
        [DataMember] [DataNames("PROD_SHELF_LIFE")] public string PROD_SHELF_LIFE { get; set; }
        [DataMember] [DataNames("PROD_VOL")] public string PROD_VOL { get; set; }

        [DataMember] [DataNames("CATEGORIES")] public string CATEGORIES { get; set; }
        [DataMember] [DataNames("SUB_CATEGORIES")] public string SUB_CATEGORIES { get; set; }

        [DataMember] [DataNames("PROD_OCCUPATION")] public decimal PROD_OCCUPATION { get; set; }
        [DataMember] [DataNames("ALL_PROD_OCCUPATION")] public decimal ALL_PROD_OCCUPATION { get; set; }
        [DataMember] [DataNames("CAT_PROD_OCCUPATION")] public decimal CAT_PROD_OCCUPATION { get; set; }

        #region Call2
        [DataMember] [DataNames("PENDING_REQ")] public int PENDING_REQ { get; set; }
        [DataMember] [DataNames("PENDING_PO")] public int PENDING_PO { get; set; }
        [DataMember] [DataNames("CLOSED_PO")] public int CLOSED_PO { get; set; }
        [DataMember] [DataNames("TOTAL_ASSIGNED_VENDORS")] public int TOTAL_ASSIGNED_VENDORS { get; set; }

        [DataMember] [DataNames("BEST_PRICE")] public decimal BEST_PRICE { get; set; }
        [DataMember] [DataNames("BEST_PRICE_DATE")] public DateTime? BEST_PRICE_DATE { get; set; }
        [DataMember] [DataNames("BEST_PRICE_REQ")] public int BEST_PRICE_REQ { get; set; }

        [DataMember] [DataNames("LAST_PRICE")] public decimal LAST_PRICE { get; set; }
        [DataMember] [DataNames("LAST_PRICE_DATE")] public DateTime? LAST_PRICE_DATE { get; set; }
        [DataMember] [DataNames("LAST_PRICE_REQ")] public int LAST_PRICE_REQ { get; set; }
        [DataMember] [DataNames("LAST_PRICE_PPS")] public int LAST_PRICE_PPS { get; set; }

        #endregion Call2
    }
}