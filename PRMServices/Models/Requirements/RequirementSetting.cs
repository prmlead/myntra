﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class RequirementSetting : Entity
    {
        [DataMember] [DataNames("REQ_ID")] public int REQ_ID { get; set; }
        [DataMember] [DataNames("REQ_SETTING")] public string REQ_SETTING { get; set; }
        [DataMember] [DataNames("REQ_SETTING_VALUE")] public string REQ_SETTING_VALUE { get; set; }
        [DataMember] [DataNames("USER")] public int USER { get; set; }
    }
}