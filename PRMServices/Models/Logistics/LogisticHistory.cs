﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class LogisticHistory : Entity
    {
        [DataMember(Name = "requirementID")]
        public int RequirementID { get; set; }

        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "commentText")]
        public string CommentText { get; set; }

        [DataMember(Name = "replyCommentID")]
        public int ReplyCommentID { get; set; }

        [DataMember(Name = "commentID")]
        public int CommentID { get; set; }

        [DataMember(Name = "createdTime")]
        public DateTime CreatedTime { get; set; }

        [DataMember(Name = "bidAmount")]
        public double BidAmount { get; set; }

        [DataMember(Name = "currency")]
        public string Currency { get; set; }

        [DataMember(Name = "rejectReson")]
        public string RejectReson { get; set; }

        [DataMember(Name = "revRejectReson")]
        public string RevRejectReson { get; set; }
    }
}