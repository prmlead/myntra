﻿using PRM.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models.Catalog
{
    [DataContract]
    public class Product : EntityExt
    {
        [DataMember(Name = "prodId")]
        public int ProductId { get; set; }

        [DataMember(Name = "compId")]
        public int CompanyId { get; set; }

        //[DataMember(Name = "prodCode")]
        //public string ProductCode { get; set; }


        string prodCode = string.Empty;
        [DataMember(Name = "prodCode")]
        public string ProductCode
        {
            get
            {
                return prodCode;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    prodCode = value;
                }
            }
        }

        [DataMember(Name = "prodName")]
        public string ProductName { get; set; }

        [DataMember(Name = "articleType")]
        public string ArticleType { get; set; }

        [DataMember(Name = "prodHSNCode")]
        public string ProductHSNCode { get; set; }

        [DataMember(Name = "prodNo")]
        public string ProductNo { get; set; }

        [DataMember(Name = "prodQty")]
        public string ProdQty { get; set; }

        [DataMember(Name = "prodDesc")]
        public string ProductDesc { get; set; }
        
        [DataMember(Name = "isValid")]
        public int IsValid { get; set; }

        [DataMember(Name = "prodAlternateUnits")]
        public string ProdAlternativeUnits { get; set; }

        [DataMember(Name = "unitConversion")]
        public string UnitConversion { get; set; }

        [DataMember(Name = "shelfLife")]
        public string ShelfLife { get; set; }

        [DataMember(Name = "productVolume")]
        public string ProductVolume { get; set; }

        [DataMember(Name = "isCoreProductCategory")]
        public int IsCoreProductCategory { get; set; } = 1;

        //[DataMember(Name = "prodSelected")]
        //public int ProductSelected { get; set; }

        //[DataMember(Name = "prodChecked")]
        //public bool ProductChecked { get; set; }

        [DataMember(Name = "productGST")]
        public decimal ProductGST { get; set; }

        [DataMember(Name = "prefferedBrand")]
        public string PrefferedBrand { get; set; }

        [DataMember(Name = "alternateBrand")]
        public string AlternateBrand { get; set; }

        [DataMember(Name = "totPurchaseQty")]
        public int TotPurchaseQty { get; set; }

        [DataMember(Name = "inTransit")]
        public int InTransit { get; set; }

        [DataMember(Name = "leadTime")]
        public string LeadTime { get; set; }

        [DataMember(Name = "departments")]
        public string Departments { get; set; }

        [DataMember(Name = "deliveryTerms")]
        public string DeliveryTerms { get; set; }

        [DataMember(Name = "termsConditions")]
        public string TermsConditions { get; set; }

        [DataMember(Name = "listVendorDetails")]
        public List<ProductVendorDetails> ListVendorDetails { get; set; }

        [DataMember(Name = "multipleAttachments")]
        public List<FileUpload> MultipleAttachments { get; set; }


        string itemAttachments = string.Empty;
        [DataMember(Name = "itemAttachments")]
        public string ItemAttachments
        {
            get
            {
                return this.itemAttachments;
            }
            set
            {
                this.itemAttachments = value;
            }
        }

        [DataMember(Name = "contractManagement")]
        public List<ContractManagementDetails> ContractManagement { get; set; }

    }

    [DataContract]
    public class ContractManagementDetails
    {
        [DataMember(Name = "PC_ID")]
        public int PC_ID { get; set; }

        [DataMember(Name = "number")]
        public string Number { get; set; }

        [DataMember(Name = "value")]
        public int Value { get; set; }

        [DataMember(Name = "ProductId")]
        public int ProductId { get; set; }

        [DataMember(Name = "U_ID")]
        public int U_ID { get; set; }

        [DataMember(Name = "document")]
        public string Document { get; set; }

        [DataMember(Name = "quantity")]
        public int Quantity { get; set; }

        [DataMember(Name = "availedQuantity")]
        public int AvailedQuantity { get; set; }

        [DataMember(Name = "vendorId")]
        public int VendorId { get; set; }

        [DataMember(Name = "startTime")]
        public DateTime? StartTime { get; set; }

        [DataMember(Name = "endTime")]
        public DateTime? EndTime { get; set; }

        [DataMember(Name = "isValid")]
        public int IsValid { get; set; }

        [DataMember(Name = "companyName")]
        public string CompanyName { get; set; }
    }

    [DataContract]
    public class ImportEntity : Entity
    {
        [DataMember(Name = "entityName")]
        public string EntityName
        {
            get;
            set;
        }

        [DataMember(Name = "userid")]
        public int UserID
        {
            get;
            set;
        }

        [DataMember(Name = "attachment")]
        public byte[] Attachment
        {
            get;
            set;
        }

        [DataMember(Name = "attachmentFileName")]
        public string AttachmentFileName
        {
            get;
            set;
        }
    }

    [DataContract]
    public class ProductFilters
    {
        [DataMember(Name = "filterType")]
        public string FilterType
        {
            get;
            set;
        }

        [DataMember(Name = "filterField")]
        public string FilterField
        {
            get;
            set;
        }

        [DataMember(Name = "filterCond")]
        public string FilterCond
        {
            get;
            set;
        }

        [DataMember(Name = "filterValue")]
        public string FilterValue
        {
            get;
            set;
        }

        [DataMember(Name = "filterFieldDataType")]
        public string filterFieldDataType
        {
            get;
            set;
        }
    }

    [DataContract]
    public class CompanyConfiguration : Entity
    {

        [DataMember(Name = "compConfigID")]
        public int CompConfigID { get; set; }

        [DataMember(Name = "compID")]
        public int CompID { get; set; }

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "configKey")]
        public string ConfigKey { get; set; }

        [DataMember(Name = "configValue")]
        public string ConfigValue { get; set; }

        [DataMember(Name = "isValid")]
        public bool IsValid { get; set; }

        [DataMember(Name = "configText")]
        public string ConfigText { get; set; }

    }
}