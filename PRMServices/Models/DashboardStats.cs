﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class DashboardStats : Entity
    {
        [DataMember(Name = "totalAuctions")]
        public int TotalAuctions { get; set; }

        [DataMember(Name = "totalAuctionsPending")]
        public int TotalAuctionsPending { get; set; }

        [DataMember(Name = "totalAuctionsCompleted")]
        public int TotalAuctionsCompleted { get; set; }

        [DataMember(Name = "totalTurnaroundTime")]
        public double TotalTurnaroundTime { get; set; }


        [DataMember(Name="totalTurnover")]
        public double TotalTurnover { get; set; }

        [DataMember(Name = "totalSaved")]
        public double TotalSaved { get; set; }

        [DataMember(Name = "totalOrders")]
        public int TotalOrders { get; set; }

        [DataMember(Name = "requirementsList")]
        public List<Requirement> RequirementsList { get; set; }

        [DataMember(Name = "stats")]
        public List<CArrayKeyValue> CArrayKeyValue { get; set; }


        [DataMember(Name = "noOfIndentsReceived")]
        public int NoOfIndentsReceived { get; set; }
        //[DataMember(Name = "price")]
        //public List<Requirement> price { get; set; }


        //string status = string.Empty;
        //[DataMember(Name = "status")]
        //public string Status
        //{
        //    get
        //    {
        //        if (!string.IsNullOrEmpty(status))
        //        { return status; }
        //        else
        //        { return ""; }
        //    }
        //    set
        //    {
        //        if (!string.IsNullOrEmpty(value))
        //        { status = value; }
        //    }
        //}




        [DataMember(Name = "noOfPendingRFQ")]
        public int NoOfPendingRFQ { get; set; }

        [DataMember(Name = "noOfClosedRFQ")]
        public int NoOfClosedRFQ { get; set; }



    }
}