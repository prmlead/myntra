﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class ApparelRequirement : Entity
    {

        [DataMember(Name = "REQ_ID")]
        [DataNames("REQ_ID")]
        public int REQ_ID { get; set; }

        [DataMember(Name = "ARTICLE_TYPE")]
        [DataNames("ARTICLE_TYPE")]
        public string ARTICLE_TYPE { get; set; }

        [DataMember(Name = "BRANDS")]
        [DataNames("BRANDS")]
        public string BRANDS { get; set; }

        [DataMember(Name = "GENDER")]
        [DataNames("GENDER")]
        public string GENDER { get; set; }

        [DataMember(Name = "STYLE_ID")]
        [DataNames("STYLE_ID")]
        public string STYLE_ID { get; set; }

        [DataMember(Name = "ARTICLE_ID")]
        [DataNames("ARTICLE_ID")]
        public string ARTICLE_ID { get; set; }

        [DataMember(Name = "ARTICLE_NUMBER")]
        [DataNames("ARTICLE_NUMBER")]
        public string ARTICLE_NUMBER { get; set; }

        [DataMember(Name = "QUANTITY")]
        [DataNames("QUANTITY")]
        public int QUANTITY { get; set; }

        [DataMember(Name = "MOQ")]
        [DataNames("MOQ")]
        public string MOQ { get; set; }

        [DataMember(Name = "SEASON")]
        [DataNames("SEASON")]
        public string SEASON { get; set; }

        [DataMember(Name = "BUY_PLAN_MONTH")]
        [DataNames("BUY_PLAN_MONTH")]
        public string BUY_PLAN_MONTH { get; set; }

        [DataMember(Name = "HSN_CODE")]
        [DataNames("HSN_CODE")]
        public double HSN_CODE { get; set; }

        [DataMember(Name = "PRODUCT_DESCRIPTION")]
        [DataNames("PRODUCT_DESCRIPTION")]
        public double PRODUCT_DESCRIPTION { get; set; }

        [DataMember(Name = "PROCESS_DESCRIPTION")]
        [DataNames("PROCESS_DESCRIPTION")]
        public string PROCESS_DESCRIPTION { get; set; }

        [DataMember(Name = "FABRIC_DESCRIPTION")]
        [DataNames("FABRIC_DESCRIPTION")]
        public string FABRIC_DESCRIPTION { get; set; }

        [DataMember(Name = "IS_NEW_OBJ")]
        [DataNames("IS_NEW_OBJ")]
        public int IS_NEW_OBJ { get; set; }


    }
}