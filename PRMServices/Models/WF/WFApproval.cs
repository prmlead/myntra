﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class WFApproval : Entity
    {

        [DataMember] [DataNames("REFER_MODULE_LINK_NAME")] public string REFER_MODULE_LINK_NAME { get; set; }
        [DataMember] [DataNames("REFER_MODULE_LINK_ID")] public int REFER_MODULE_LINK_ID { get; set; }
        [DataMember] [DataNames("REFER_MODULE_NAME")] public string REFER_MODULE_NAME { get; set; }



        [DataMember] [DataNames("MODULE_TYPE")] public string MODULE_TYPE { get; set; }
        [DataMember] [DataNames("MODULE_NAME")] public string MODULE_NAME { get; set; }
        [DataMember] [DataNames("WF_STATUS")] public string WF_STATUS { get; set; }
        [DataMember] [DataNames("MODULE_ID")] public int MODULE_ID { get; set; }
        [DataMember] [DataNames("WF_ID")] public int WF_ID { get; set; }
        [DataMember] [DataNames("DESIG_ID")] public int DESIG_ID { get; set; }
        [DataMember] [DataNames("DEPT_ID")] public int DEPT_ID { get; set; }
        [DataMember] [DataNames("USER_ID")] public int USER_ID { get; set; }
        [DataMember] [DataNames("APPROVAL_DATE")] public DateTime? APPROVAL_DATE { get; set; }

    }

}