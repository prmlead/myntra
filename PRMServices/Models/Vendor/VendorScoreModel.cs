﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models.Vendor
{
    [DataContract]
    public class VendorScoreModel
    {
        [DataMember]
        public string Field { get; set; }

        [DataMember]
        public int Value { get; set; }

        [DataMember]
        public int DefaultValue { get; set; }
    }
}