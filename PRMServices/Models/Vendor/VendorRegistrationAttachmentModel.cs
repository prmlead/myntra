﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRMServices.Models.Vendor
{
    public class VendorRegistrationAttachmentModel
    {
        public string Field { get; set; }

        public  IList<Attachemnts> Files { get; set; }

    }

    public class Attachemnts
    {
        public string name { get; set; }
        public byte[] fileStream { get; set; }
        public string fileType { get; set; }
    }
}