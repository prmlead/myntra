﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class QCSDetails : Entity
    {

        [DataMember] [DataNames("QCS_ID")] public int QCS_ID { get; set; }
        [DataMember] [DataNames("QCS_CODE")] public string QCS_CODE { get; set; }
        [DataMember] [DataNames("QCS_TYPE")] public string QCS_TYPE { get; set; }
        [DataMember] [DataNames("REQ_ID")] public int REQ_ID { get; set; }
        [DataMember] [DataNames("PO_CODE")] public string PO_CODE { get; set; }
        [DataMember] [DataNames("RECOMMENDATIONS")] public string RECOMMENDATIONS { get; set; }
        [DataMember] [DataNames("UNIT_CODE")] public string UNIT_CODE { get; set; }
        [DataMember] [DataNames("U_ID")] public int U_ID { get; set; }
        [DataMember] [DataNames("CREATED_USER")] public string CREATED_USER { get; set; }
        [DataMember] [DataNames("CREATED_DATE")] public DateTime CREATED_DATE { get; set; }
        [DataMember] [DataNames("WF_ID")] public int WF_ID { get; set; }
        [DataMember] [DataNames("REQ_JSON")] public string REQ_JSON { get; set; }
        [DataMember] [DataNames("CREATED_BY")] public int CREATED_BY { get; set; }
        [DataMember] [DataNames("MODIFIED_BY")] public int MODIFIED_BY { get; set; }
    }
}