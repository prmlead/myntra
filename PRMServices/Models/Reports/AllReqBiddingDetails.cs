﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class AllReqBiddingDetails : Entity
    {

        [DataMember] [DataNames("REQ_POSTED_BY")] public string REQ_POSTED_BY { get; set; }
        [DataMember] [DataNames("VENDOR_RANK")] public int VENDOR_RANK { get; set; }
        [DataMember] [DataNames("VENDOR_NAME")] public string VENDOR_NAME { get; set; }
        [DataMember] [DataNames("IS_QUOTATION_REJECTED")] public int IS_QUOTATION_REJECTED { get; set; }
        [DataMember] [DataNames("REQ_ID")] public int REQ_ID { get; set; }
        [DataMember] [DataNames("U_ID")] public int U_ID { get; set; }
        [DataMember] [DataNames("REV_UNIT_PRICE")] public double REV_UNIT_PRICE { get; set; }
        [DataMember] [DataNames("DATE_MODIFIED")] public DateTime? DATE_MODIFIED { get; set; }
        [DataMember] [DataNames("ITEM_ID")] public int ITEM_ID { get; set; }
        [DataMember] [DataNames("ARTICLE_NAME")] public string ARTICLE_NAME { get; set; }
        [DataMember] [DataNames("ARTICLE_NUMBER")] public string ARTICLE_NUMBER { get; set; }
        [DataMember] [DataNames("STYLE_ID")] public string STYLE_ID { get; set; }
        [DataMember] [DataNames("IS_REGRET")] public int IS_REGRET { get; set; }
        [DataMember] [DataNames("FABRIC_JSON")] public string FABRIC_JSON { get; set; }
        [DataMember] [DataNames("PROCESS_JSON")] public string PROCESS_JSON { get; set; }
        [DataMember] [DataNames("ACCESSORIES_JSON")] public string ACCESSORIES_JSON { get; set; }
        [DataMember] [DataNames("FABRIC_COST")] public double FABRIC_COST { get; set; }
        [DataMember] [DataNames("PROCESS_COST")] public double PROCESS_COST { get; set; }
        [DataMember] [DataNames("TRIMS_COST")] public double TRIMS_COST { get; set; }
        [DataMember] [DataNames("UNIT_PRICE")] public double UNIT_PRICE { get; set; }
        [DataMember] [DataNames("PRICE")] public double PRICE { get; set; }
        [DataMember] [DataNames("REVICED_PRICE")] public double REVICED_PRICE { get; set; }
        [DataMember] [DataNames("ITEM_RANK")] public string ITEM_RANK { get; set; }
        [DataMember] [DataNames("QUANTITY")] public double QUANTITY { get; set; }
        [DataMember] [DataNames("QUANTITY_IN")] public string QUANTITY_IN { get; set; }
        [DataMember] [DataNames("QUOTED_UNIT_PRICE")] public double QUOTED_UNIT_PRICE { get; set; }
        [DataMember] [DataNames("FINAL_BID_PRICE")] public double FINAL_BID_PRICE { get; set; }
        [DataMember] [DataNames("BUYER_TARGET_COST")] public double BUYER_TARGET_COST { get; set; }
        [DataMember] [DataNames("LEAD_TIME")] public string LEAD_TIME { get; set; }
        [DataMember] [DataNames("TOTAL_QUOTED_PRICE")] public double TOTAL_QUOTED_PRICE { get; set; }
        [DataMember] [DataNames("TOTAL_BID_PRICE")] public double TOTAL_BID_PRICE { get; set; }
        [DataMember] [DataNames("SAVINGS_PERCENTAGE")] public double SAVINGS_PERCENTAGE { get; set; }
        [DataMember] [DataNames("ITEM_SAVINGS_VALUE")] public double ITEM_SAVINGS_VALUE { get; set; }
        [DataMember] [DataNames("VENDOR_SAVINGS_PERCENTAGE")] public double VENDOR_SAVINGS_PERCENTAGE { get; set; }
        [DataMember] [DataNames("VENDOR_SAVINGS_VALUE")] public double VENDOR_SAVINGS_VALUE { get; set; }
        [DataMember] [DataNames("REGRET_COMMENTS")] public string REGRET_COMMENTS { get; set; }

        [DataMember] [DataNames("ApparelFabric")] public List<ApparelFabric> ApparelFabric { get; set; }
        [DataMember] [DataNames("ApparelProcess")] public List<ApparelProcess> ApparelProcess { get; set; }
        [DataMember] [DataNames("ApparelSundries")] public List<ApparelSundries> ApparelSundries { get; set; }
        [DataMember] [DataNames("ARTICLE_TYPE")] public string ARTICLE_TYPE { get; set; }
        [DataMember] [DataNames("BRAND")] public string BRAND { get; set; }
        [DataMember] [DataNames("SEASON")] public string SEASON { get; set; }
        [DataMember] [DataNames("QUOT_ID")] public int QUOT_ID { get; set; }
        [DataMember] [DataNames("SUB_ITEM_COST")] public double SUB_ITEM_COST { get; set; }
        [DataMember] [DataNames("ITEM_COST_WITH_REJECTION")] public double ITEM_COST_WITH_REJECTION { get; set; }
        [DataMember] [DataNames("ITEM_COST_WITH_MARGIN")] public double ITEM_COST_WITH_MARGIN { get; set; }
        [DataMember] [DataNames("SUB_TOTAL_COST")] public double SUB_TOTAL_COST { get; set; }
        [DataMember] [DataNames("ITEM_UNIT_PRICE")] public double ITEM_UNIT_PRICE { get; set; }
        [DataMember] [DataNames("ITEM_REV_UNIT_PRICE")] public double ITEM_REV_UNIT_PRICE { get; set; }
        [DataMember] [DataNames("TOTAL_COST")] public double TOTAL_COST { get; set; }
        [DataMember] [DataNames("CUT_MAKE")] public double CUT_MAKE { get; set; }
        [DataMember] [DataNames("REJECTION_PERCENTAGE")] public double REJECTION_PERCENTAGE { get; set; }
        [DataMember] [DataNames("MARGIN_PERCENTAGE")] public double MARGIN_PERCENTAGE { get; set; }


    }
}