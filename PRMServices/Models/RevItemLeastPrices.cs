﻿using PRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class RevItemLeastPrices 
    {
        [DataMember] [DataNames("REQ_ID")] public int REQ_ID { get; set; }
        [DataMember] [DataNames("REV_UNIT_PRICE")] public double REV_UNIT_PRICE { get; set; }
        [DataMember] [DataNames("ARTICLE_TYPE")] public string ARTICLE_TYPE { get; set; }
        [DataMember] [DataNames("ARTICLE_NUMBER")] public string ARTICLE_NUMBER { get; set; }
        [DataMember] [DataNames("VENDOR_CODE")] public string VENDOR_CODE { get; set; }
        [DataMember] [DataNames("SAP_VENDOR_ID")] public int SAP_VENDOR_ID { get; set; }
        [DataMember] [DataNames("ERROR_MESSAGE")] public string ERROR_MESSAGE { get; set; }
        [DataMember] [DataNames("STYLE_ID")] public string STYLE_ID { get; set; }
    }
    public class RevAllItemPrices 
    {
        [DataMember] [DataNames("errorMessage")] public string errorMessage { get; set; }
        [DataMember] [DataNames("reqList")] public List<RFQ> reqList { get; set; }
    }

    public class RFQ
    {
        [DataMember] [DataNames("reqId")] public int reqId { get; set; }
        [DataMember] [DataNames("vanPrices")] public List<GroupedItems> vanPrices { get; set; }
    }

    public class ItemRevisedPrices
    {
        [IgnoreDataMember()] [DataNames("reqId")] public int reqId { get; set; }
        [DataMember] [DataNames("revUnitPrice")] public double revUnitPrice { get; set; }
        [DataMember] [DataNames("articleNumber")] public string articleNumber { get; set; }
        [DataMember] [DataNames("vendorCode")] public string vendorCode { get; set; }
        [DataMember] [DataNames("allocatedQty")] public double allocatedQty { get; set; }
        [DataMember] [DataNames("sapVendorId")] public int sapVendorId { get; set; }
        [DataMember] [DataNames("styleId")] public string styleId { get; set; }
        [DataMember] [DataNames("dateModified")] public string dateModified { get; set; }
    }

    public class GroupedItems
    {
        [DataMember] [DataNames("articleNumber")] public string articleNumber { get; set; }
        [DataMember] [DataNames("styleId")] public string styleId { get; set; }
        [DataMember] [DataNames("prices")] public List<RevisedPrices> prices { get; set; }
        [DataMember] [DataNames("remarks")] public string remarks { get; set; }
        [DataMember] [DataNames("approvalStatus")] public string approvalStatus { get; set; }
        [IgnoreDataMember()] [DataMember] [DataNames("pricesTemp")] public List<ItemRevisedPrices> pricesTemp { get; set; }
    }

    public class RevisedPrices
    {
        [DataMember] [DataNames("revUnitPrice")] public double revUnitPrice { get; set; }
        [DataMember] [DataNames("vendorCode")] public string vendorCode { get; set; }
        [DataMember] [DataNames("sapVendorId")] public int sapVendorId { get; set; }
        [DataMember] [DataNames("dateModified")] public string dateModified { get; set; }
        [DataMember] [DataNames("allocatedQty")] public double allocatedQty { get; set; }
    }

}