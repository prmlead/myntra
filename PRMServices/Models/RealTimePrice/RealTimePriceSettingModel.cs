﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRMServices.Models.RealTimePrice
{
    public class RealTimePriceSettingModel
    {

        public IList<RealTimePriceAllowProductModel> AllowedProducts { get; set; }

        public bool AllowThresholdNotification { get; set; }

        public bool AllowPriceDipNotification { get; set; }
    }
}