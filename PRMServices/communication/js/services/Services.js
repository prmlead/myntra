prmApp.constant('PRMCommunicationServiceDomain', 'communication/svc/PRMCommunication.svc/REST/');
prmApp.service('PRMCommunicationService', ["PRMCommunicationServiceDomain", "userService", "httpServices",
    function (PRMCommunicationServiceDomain, userService, httpServices) {
        var PRMCommunicationService = this;

        PRMCommunicationService.getCatalogueVendors = function (catalogue, uid, value) {
            let url = PRMCommunicationServiceDomain + 'getcataloguevendors?catalogue=' + catalogue + '&uid=' + uid + '&sessionid=' + userService.getUserToken() + '&isCatalogue=' + value;
            return httpServices.get(url);
        };

        PRMCommunicationService.getCategoryVendors = function (categories, uid) {
            let url = PRMCommunicationServiceDomain + 'getCategoryVendors?categories=' + categories + '&sessionid=' + userService.getUserToken() + '&uid=' + uid + '&evalID=' + 0;
            return httpServices.get(url);
        };

        PRMCommunicationService.SendCommunications = function (params) {
            let url = PRMCommunicationServiceDomain + 'sendcommunication';
            return httpServices.post(url, params);
        };

        PRMCommunicationService.getConfigKey = function () {
            let url = PRMCommunicationServiceDomain + 'getConfigKey?sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        return PRMCommunicationService;
}]);