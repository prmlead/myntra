﻿using Microsoft.AspNet.SignalR;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using OfficeOpenXml;
using PRM.Core.Common;
using COMM = PRMServices.Common;
using PRMServices.Models;
using PRMServices.SignalR;
using PRMServices.SQLHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.ServiceModel.Activation;
using System.Configuration;
using System.Net;

namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]

    public class PRMCommunication : IPRMCommunication
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private MySQLBizClass sqlHelper = new MySQLBizClass();
        private PRMServices prmServices = new PRMServices();

        public List<UserDetails> GetCatalogueVendors(string catalogue, int uid, string sessionID,bool isCatalogue)
        {
            List<UserDetails> ListUser = new List<UserDetails>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", uid);
                sd.Add("P_CATEGORY", catalogue);
                sd.Add("P_IS_CATALOGUE", isCatalogue == true ? 1 : 0);
                DataSet ds = sqlHelper.SelectList("cm_getCategoryBasedVendors", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        UserDetails User = new UserDetails();
                        User.UserID = row["U_ID"] != DBNull.Value ? Convert.ToInt16(row["U_ID"]) : -1;
                        User.FirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                        User.LastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                        User.PhoneNum = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                        User.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                        User.Category = row["V_CATEGORY"] != DBNull.Value ? Convert.ToString(row["V_CATEGORY"]) : string.Empty;
                        User.IsValid = row["IS_VALID"] != DBNull.Value ? (Convert.ToInt32(row["IS_VALID"]) == 1 ? true : false) : false;
                        User.Email = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                        User.AltEmail = row["ALL_EMAILS"] != DBNull.Value ? Convert.ToString(row["ALL_EMAILS"]) : string.Empty;
                        User.AltPhoneNum = row["ALL_PHONES"] != DBNull.Value ? Convert.ToString(row["ALL_PHONES"]) : string.Empty;
                        ListUser.Add(User);
                    }
                }
            }
            catch (Exception ex)
            {
                UserDetails User = new UserDetails();
                User.ErrorMessage = ex.Message;
                ListUser.Add(User);
            }

            return ListUser;
        }

        public Response GetConfigKey(string sessionID)
        {
            Response response = new Response();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID, null);
                string value = null;
                value = ConfigurationManager.AppSettings["IS_CATALOGUE"].ToString();
                response.ErrorMessage = !string.IsNullOrEmpty(value) ? value: string.Empty;
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SendCommunication(List<UserDetails> vendors, string subject, string body, bool sendSMS, bool sendEmail, string sessionID, byte [] commattachments, string commattachmentName)
        {
            Response response = new Response();
            try
            {
                Attachment singleAttachment = null;
                if (commattachmentName !=null) {
                    var fileData = DownloadFile("", sessionID, commattachments, commattachmentName);
                    if (fileData.FileStream != null && !string.IsNullOrEmpty(fileData.FileName))
                    {
                        singleAttachment = new Attachment(fileData.FileStream, fileData.FileName);
                    }
                }

                if (vendors!=null && vendors.Count > 0 && (sendSMS || sendEmail))
                {
                    foreach (var vendor in vendors)
                    {
                        try
                        {
                            if (sendSMS)
                            {
                                string tempBody = body;
                                tempBody = tempBody.Replace("<br />", Environment.NewLine);
                                string sms = subject + Environment.NewLine + tempBody;
                                //Utilities.SendSMS(string.Empty, vendor.AltPhoneNum, sms, 0, vendor.UserID, "COMMUNICATIONS", null).ConfigureAwait(false); 
                                prmServices.SendSMS(string.Empty, vendor.AltPhoneNum, sms,0,0, "COMMUNICATIONS").ConfigureAwait(false);
                            }

                            if (sendEmail)
                            {
                                //Utilities.SendEmail(vendor.AltEmail, subject, body, 0, vendor.UserID, "COMMUNICATIONS", sessionID).ConfigureAwait(false);
                                prmServices.SendEmail(vendor.AltEmail, subject, body,0,0, "COMMUNICATIONS", sessionID, singleAttachment).ConfigureAwait(false);
                            }

                            response.ObjectID++;
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }                
            }
            catch(Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }


        public FileData DownloadFile(string path, string sessioID, byte[] attachment = null, string fileName = "")
        {
            FileData fd = new FileData();
            try
            {
                fd.FileName = fileName;
                fd.FileStream = new MemoryStream(attachment);
            }
            catch
            {
                FileData fd1 = new FileData();
                return fd1;
            }

            return fd;

        }
    }
}