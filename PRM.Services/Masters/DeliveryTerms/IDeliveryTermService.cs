﻿using PRM.Core.Domain.Masters.DeliveryTerms;
using PRM.Core.Pagers;
using System.Collections.Generic;

namespace PRM.Services.Masters.DeliveryTerms
{
    public interface IDeliveryTermService
    {
        void InsertDeliveryTerm(DeliveryTerm entity);

        void UpdateDeliveryTerm(DeliveryTerm entity);

        void DeleteDeliveryTerm(DeliveryTerm entity);

        DeliveryTerm GetDeliveryTermById(int id);

        IList<DeliveryTerm> GetDeliveryTermList(object filter);

        IPagedList<DeliveryTerm> GetDeliveryTermList(Pager pager,int companyId);
    }
}