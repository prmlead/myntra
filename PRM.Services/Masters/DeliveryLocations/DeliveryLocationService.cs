﻿using Dapper;
using PRM.Core.Domain.Acl;
using PRM.Core.Domain.Masters.DeliveryLocations;
using PRM.Core.Pagers;
using PRM.Data;
using PRM.Services.Acl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Services.Masters.DeliveryLocations
{
   public class DeliveryLocationService : IDeliveryLocationService
    {
        public DeliveryLocationService(IRepository<DeliveryLocation> deliveryLocationService,
      IDbProvider dbProvider,
      IAclMappingService aclMappingService)
        {
            _deliveryLocationService = deliveryLocationService;
            _dbProvider = dbProvider;
            _aclMappingService = aclMappingService;
        }

        private readonly IRepository<DeliveryLocation> _deliveryLocationService;
        private readonly IDbProvider _dbProvider;
        private readonly IAclMappingService _aclMappingService;
        public void DeleteDeliveryLocation(DeliveryLocation entity)
        {
            _deliveryLocationService.Delete(entity);
        }

        public DeliveryLocation GetDeliveryLocationById(int id)
        {

          var  entity = _deliveryLocationService.GetById(id);

            var mapper = _aclMappingService.GetAclMappingList(new { EntityName = "DeliveryLocation", EntityId = entity.Id });
            if (entity.LimitedToDepartment)
                entity.AllowedDepartments = mapper.Where(e => e.Type == (int)AclMappingType.Department).Select(e => e.Value).ToArray();

            if (entity.LimitedToProject)
                entity.AllowedProjects = mapper.Where(e => e.Type == (int)AclMappingType.Project).Select(e => e.Value).ToArray();

            if (entity.LimitedToUser)
                entity.AllowedUsers = mapper.Where(e => e.Type == (int)AclMappingType.User).Select(e => e.Value).ToArray();

            return entity;
        }

        public IPagedList<DeliveryLocation> GetDeliveryLocationList(Pager pager,int companyId)
        {
            using (var con = _dbProvider.Connection())
            {
                var query = "SELECT SQL_CALC_FOUND_ROWS  * FROM master_deliverylocation";

                List<String> WhereColumns = new List<string>();

                WhereColumns.Add(string.Format(" Company_Id = {0} ", companyId));
                WhereColumns.Add(string.Format(" Deleted = {0}", 0));

                if (!string.IsNullOrEmpty(pager.Criteria.SearchTerm))
                {
                    WhereColumns.Add(string.Format(" Tttle Like %{0}%", pager.Criteria.SearchTerm));
                    WhereColumns.Add(string.Format(" Address Like %{0}%", pager.Criteria.SearchTerm));


                }

                if (pager.Criteria.Filter.Any())
                {

                }

                if (WhereColumns.Any())
                    query = query + string.Format(" WHERE {0}", string.Join(" AND ", WhereColumns));

                query = query + " ORDER BY CreatedOnUtc DESC ";

                if (pager.PageIndex >= 0 && pager.PageSize > 0)
                {
                    query = query + string.Format(" LIMIT {0},{1} ", pager.PageIndex * pager.PageSize, pager.PageSize);
                }

                query += " ; SELECT FOUND_ROWS()";

                var multi = con.QueryMultiple(query);

                var lst = multi.Read<DeliveryLocation>().ToList();
                int total = multi.Read<int>().SingleOrDefault();

                return new PagedList<DeliveryLocation>(lst, pager.PageIndex, pager.PageSize, total);
            }
        }

        public IList<DeliveryLocation> GetDeliveryLocationList(object filter)
        {
            return _deliveryLocationService.GetList(filter).ToList();
        }

        public void InsertDeliveryLocation(DeliveryLocation entity)
        {
            var id =  _deliveryLocationService.Insert(entity);
            entity.Id = id;
            InsertMapping(entity);

        }

        public void UpdateDeliveryLocation(DeliveryLocation entity)
        {
            _deliveryLocationService.Update(entity);

            var mapper = _aclMappingService.GetAclMappingList(new { EntityName = "DeliveryLocation", EntityId = entity.Id });

            foreach (var map in mapper)
                _aclMappingService.DeleteAclMapping(map);

            InsertMapping(entity);

        }

        private void InsertMapping(DeliveryLocation entity)
        {
            if (entity.LimitedToDepartment)
            {
                _aclMappingService.InsertDepartmentMapping(entity.Company_Id, "DeliveryLocation", entity.Id, entity.AllowedDepartments);
            }

            if (entity.LimitedToProject)
            {
                _aclMappingService.InsertProjectMapping(entity.Company_Id, "DeliveryLocation", entity.Id, entity.AllowedProjects);

            }

            if (entity.LimitedToUser)
            {
                _aclMappingService.InsertUserMapping(entity.Company_Id, "DeliveryLocation", entity.Id, entity.AllowedUsers);

            }
        }

    }
}
