﻿using PRM.Core.Domain.Masters.ContactDetails;
using PRM.Core.Pagers;
using System.Collections.Generic;

namespace PRM.Services.Masters.ContactDetails
{
    public interface IContactDetailService
    {

        void InsertContactDetail(ContactDetail entity);

        void UpdateContactDetail(ContactDetail entity);

        void DeleteContactDetail(ContactDetail entity);

        ContactDetail GetContactDetailById(int id);

        IList<ContactDetail> GetContactDetailList(object filter);

        IPagedList<ContactDetail> GetContactDetailList(Pager pager,int companyId);

    }
}