﻿using PRM.Core.Domain.Configurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRM.Services.Configurations
{
    public interface IRegistrationFormService
    {

        IList<RegistrationForm> GetRegistrationForm(int companyId);

        void InsertRegistrationFormField(RegistrationForm entity);

        void UpdateRegistrationFormField(RegistrationForm entity);

        void DeleteRegistrationFormField(RegistrationForm entity);

        RegistrationForm GetRegistrationFormField(int id);



    }
}