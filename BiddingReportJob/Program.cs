﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BiddingReportJob
{
    class Program
    {
        static void Main(string[] args)
        {
            string html = string.Empty;
            int days = Convert.ToInt32(ConfigurationManager.AppSettings["BiddingReportDays"]);
            string url = string.Format(ConfigurationManager.AppSettings["BiddingURL"], DateTime.Now.AddDays(-days).ToString("yyyy-MM-dd"), DateTime.Now.ToString("yyyy-MM-dd"));
            
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.AutomaticDecompression = DecompressionMethods.GZip;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                html = reader.ReadToEnd();
            }

        }
    }
}
