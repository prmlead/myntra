﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace PRM.Core.Storages
{
   public class StorageHelper
    {
        public StorageHelper()
        {

        }

        public string BasePath()
        {
            var appDomain = System.AppDomain.CurrentDomain;
            return appDomain.RelativeSearchPath ?? appDomain.BaseDirectory;
        }

        public string MapPath(string virtualPath)
        {
            return HostingEnvironment.MapPath(virtualPath);
        }

        public string SaveFile(string virtualPath, byte[] stream, string name, string extention = "")
        {
           try
            {
                var filename = name;
                //if (!string.IsNullOrEmpty(extention))
                 //   filename = string.Format("{0}.{1}", name, extention);

                //var filePath = Path.Combine(MapPath(virtualPath), filename);
                File.WriteAllBytes(virtualPath, stream); // Requires System.IO

                return name;
            }
            catch(Exception ex)
            {
                return "";
            }
        }
    }
}
