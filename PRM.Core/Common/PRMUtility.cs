﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

using OfficeOpenXml;
using PRM.Core.Models;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.ComponentModel;

namespace PRM.Core.Common
{
    public static class PRMUtility
    {
        public static CustomProperty GetCustPropObject(DataRow row)
        {
            CustomProperty detail = new CustomProperty();
            detail.CustPropID = Convert.ToInt32(row.GetColumnValue("CUST_PROP_ID", detail.CustPropID.GetType()));
            detail.CompanyID = Convert.ToInt32(row.GetColumnValue("COMP_ID", detail.CompanyID.GetType()));
            detail.CustPropCode = Convert.ToString(row.GetColumnValue("CUST_PROP_CODE", detail.CustPropCode.GetType()));
            detail.CustPropDesc = Convert.ToString(row.GetColumnValue("CUST_PROP_DESC", detail.CustPropDesc.GetType()));
            detail.CustPropType = Convert.ToString(row.GetColumnValue("CUST_PROP_TYPE", detail.CustPropType.GetType()));
            detail.CustPropDefault = Convert.ToString(row.GetColumnValue("CUST_PROP_DEFAULT", detail.CustPropDefault.GetType()));
            detail.ModuleName = Convert.ToString(row.GetColumnValue("CUST_MODULE", detail.ModuleName.GetType()));
            detail.EntityID = Convert.ToInt32(row.GetColumnValue("ENTITY_ID", detail.EntityID.GetType()));
            detail.CustPropValue = Convert.ToString(row.GetColumnValue("CUST_PROP_VALUE", detail.CustPropValue.GetType()));

            return detail;
        }

        public static DataTable WorksheetToDataTable(ExcelWorksheet oSheet)
        {
            int totalRows = oSheet.Dimension.End.Row;
            int totalCols = oSheet.Dimension.End.Column;
            DataTable dt = new DataTable(oSheet.Name);
            DataRow dr = null;
            for (int i = 1; i <= totalRows; i++)
            {
                if (i > 1) dr = dt.Rows.Add();
                for (int j = 1; j <= totalCols; j++)
                {
                    if (i == 1)
                    {
                        dt.Columns.Add(oSheet.Cells[i, j].Value.ToString());
                    }
                    else
                    {
                        if (oSheet.Cells[i, j] != null && oSheet.Cells[i, j].Value != null)
                        {
                            dr[j - 1] = oSheet.Cells[i, j].Value.ToString();
                        }
                        else
                        {
                            dr[j - 1] = string.Empty;
                        }
                    }
                }
            }
            return dt;
        }

        public static DataSet SaveCompanyCustProp(CustomProperty companyProp, MySqlCommand cmd)
        {
            cmd.CommandText = "cp_SaveCompanyCustProp";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("P_CUST_PROP_ID", companyProp.CustPropID));
            cmd.Parameters.Add(new MySqlParameter("P_COMP_ID", companyProp.CompanyID));
            cmd.Parameters.Add(new MySqlParameter("P_CUST_PROP_CODE", companyProp.CustPropCode));
            cmd.Parameters.Add(new MySqlParameter("P_CUST_PROP_DESC", companyProp.CustPropDesc));
            cmd.Parameters.Add(new MySqlParameter("P_CUST_PROP_TYPE", companyProp.CustPropType));
            cmd.Parameters.Add(new MySqlParameter("P_CUST_PROP_DEFAULT", companyProp.CustPropDefault));
            cmd.Parameters.Add(new MySqlParameter("P_CUST_MODULE", companyProp.ModuleName));
            cmd.Parameters.Add(new MySqlParameter("P_USER", companyProp.ModifiedBY));

            MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            myDA.Fill(ds);

            return ds;
        }

        public static DataSet SaveCompanyPropValue(CustomProperty companyProp, MySqlCommand cmd)
        {
            cmd.CommandText = "cp_SaveCompanyCustPropValues";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("P_CUST_PROP_ID", companyProp.CustPropID));
            cmd.Parameters.Add(new MySqlParameter("P_ENTITY_ID", companyProp.EntityID));
            cmd.Parameters.Add(new MySqlParameter("P_CUST_PROP_VALUE", companyProp.CustPropValue));
            cmd.Parameters.Add(new MySqlParameter("P_USER", companyProp.ModifiedBY));

            MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            myDA.Fill(ds);

            return ds;
        }
        
        public static DataSet ExecuteQuery(string query, MySqlCommand cmd)
        {
            cmd.Parameters.Clear();
            cmd.CommandText = query;
            cmd.CommandType = CommandType.Text;

            MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            myDA.Fill(ds);

            return ds;
        }

        public static Requirement GetFullReqEntity(DataRow row, MySqlCommand cmd)
        {
            Requirement req = new Requirement();

            req.Title = Convert.ToString(row.GetColumnValue("REQ_TITLE", req.Title.GetType()));
            req.Description = Convert.ToString(row.GetColumnValue("REQ_DESC", req.Description.GetType()));
            req.PostedOn = Convert.ToDateTime(row.GetColumnValue("REQ_POSTED_ON", req.PostedOn.GetType()));
            string cat = Convert.ToString(row.GetColumnValue("REQ_CATEGORY", req.Category.GetType()));
            req.Category = new string[] { cat };
            req.Urgency = Convert.ToString(row.GetColumnValue("REQ_URGENCY", req.Urgency.GetType()));
            req.Budget = Convert.ToString(row.GetColumnValue("REQ_BUDGET", req.Budget.GetType()));
            req.DeliveryLocation = Convert.ToString(row.GetColumnValue("REQ_DELIVERY_LOC", req.DeliveryLocation.GetType()));
            req.Taxes = Convert.ToString(row.GetColumnValue("REQ_TAXES", req.Taxes.GetType()));
            req.PaymentTerms = Convert.ToString(row.GetColumnValue("REQ_PAYMENT_TERMS", req.PaymentTerms.GetType()));
            req.Status = Convert.ToString(row.GetColumnValue("CLOSED", req.Status.GetType()));
            req.StartTime = Convert.ToDateTime(row.GetColumnValue("START_TIME", req.StartTime.GetType()));
            req.CustomerID = Convert.ToInt32(row.GetColumnValue("U_ID", req.CustomerID.GetType()));
            req.CustFirstName = Convert.ToString(row.GetColumnValue("U_FNAME", req.CustFirstName.GetType()));
            req.CustLastName = Convert.ToString(row.GetColumnValue("U_LNAME", req.CustLastName.GetType()));
            req.DeliveryTime = Convert.ToString(row.GetColumnValue("REQ_DELIVERY_TIME", req.DeliveryTime.GetType()));
            req.AttachmentName = Convert.ToString(row.GetColumnValue("REQ_ATTACHMENT", req.AttachmentName.GetType()));
            req.EndTime = Convert.ToDateTime(row.GetColumnValue("END_TIME", req.EndTime.GetType()));
            req.SuperUserID = Convert.ToInt32(row.GetColumnValue("SUPER_U_ID", req.SuperUserID.GetType()));
            req.IsStopped = Convert.ToBoolean(row.GetColumnValue("SELECTED_VENDOR_ID", req.IsStopped.GetType()));
            req.Savings = Convert.ToDouble(row.GetColumnValue("SAVINGS", req.Savings.GetType()));
            req.CustomerCompanyName = Convert.ToString(row.GetColumnValue("COMPANY_NAME", req.CustomerCompanyName.GetType()));
            req.Currency = Convert.ToString(row.GetColumnValue("REQ_CURRENCY", req.Currency.GetType()));
            req.TimeZone = Convert.ToString(row.GetColumnValue("REQ_TIMEZONE", req.TimeZone.GetType()));
            req.CustCompID = Convert.ToInt32(row.GetColumnValue("CUST_COMP_ID", req.CustCompID.GetType()));
            req.IsQuotationPriceLimit = Convert.ToBoolean(row.GetColumnValue("IS_QUOTATION_PRICE_LIMIT", req.IsQuotationPriceLimit.GetType()));
            req.QuotationPriceLimit = Convert.ToInt32(row.GetColumnValue("QUOTATION_PRICE_LIMIT", req.QuotationPriceLimit.GetType()));
            req.NoOfQuotationReminders = Convert.ToInt32(row.GetColumnValue("NO_OF_QUOTATION_REMINDERS", req.NoOfQuotationReminders.GetType()));
            req.RemindersTimeInterval = Convert.ToInt32(row.GetColumnValue("REMINDERS_TIME_INTERVAL", req.RemindersTimeInterval.GetType()));
            req.RequirementID = Convert.ToInt32(row.GetColumnValue("REQ_ID", req.RequirementID.GetType()));
            req.Subcategories = Convert.ToString(row.GetColumnValue("REQ_SUBCATEGORIES", req.Subcategories.GetType()));
            req.POLink = Convert.ToString(row.GetColumnValue("PURCHASE_ORDER_LINK", req.POLink.GetType()));
            req.InclusiveTax = Convert.ToBoolean(row.GetColumnValue("REQ_INCLUSIVE_TAX", req.InclusiveTax.GetType()));
            req.IncludeFreight = Convert.ToBoolean(row.GetColumnValue("REQ_INCLUDE_FREIGHT", req.IncludeFreight.GetType()));
            req.MinReduceAmount = Convert.ToInt32(row.GetColumnValue("MIN_REDUCE_AMOUNT", req.MinReduceAmount.GetType()));
            req.QuotationFreezTime = Convert.ToDateTime(row.GetColumnValue("QUOTATION_FREEZ_TIME", req.QuotationFreezTime.GetType()));
            req.IsTabular = Convert.ToBoolean(row.GetColumnValue("REQ_IS_TABULAR", req.IsTabular.GetType()));
            req.ReqComments = Convert.ToString(row.GetColumnValue("REQ_COMMENTS", req.ReqComments.GetType()));
            req.IsNegotiationEnded = Convert.ToInt32(row.GetColumnValue("IS_NEGOTIATION_ENDED", req.IsNegotiationEnded.GetType()));
            req.NegotiationDuration = Convert.ToString(row.GetColumnValue("NEGOTIATION_DURATION", req.NegotiationDuration.GetType()));
            req.MinVendorComparision = Convert.ToInt32(row.GetColumnValue("VENDOR_COMPARISION_MIN_AMOUNT", req.MinVendorComparision.GetType()));
            req.MinVendorComparision = Convert.ToInt32(row.GetColumnValue("VENDOR_COMPARISION_MIN_AMOUNT", req.MinVendorComparision.GetType()));
            req.MinVendorComparision = Convert.ToInt32(row.GetColumnValue("VENDOR_COMPARISION_MIN_AMOUNT", req.MinVendorComparision.GetType()));
            req.ReqPDF = Convert.ToInt32(row.GetColumnValue("REQ_PDF", req.ReqPDF.GetType()));
            req.ReqPDFCustomer = Convert.ToInt32(row.GetColumnValue("REQ_PDF_CUSTOMER", req.ReqPDFCustomer.GetType()));
            req.ReportItemWise = Convert.ToInt32(row.GetColumnValue("REPORT_ITEM_WISE", req.ReportItemWise.GetType()));
            req.ReqType = Convert.ToString(row.GetColumnValue("PARAM_REQ_TYPE", req.ReqType.GetType()));
            req.PriceCapValue = Convert.ToDouble(row.GetColumnValue("PARAM_PRICE_CAP_VALUE", req.PriceCapValue.GetType()));
            req.PriceCapValue = Convert.ToDouble(row.GetColumnValue("CUST_COMP_ID", req.PriceCapValue.GetType()));
            req.SuperUser = new User();
            req.PostedUser = new User();
            req.PostedUser.UserInfo = new UserInfo();
            req.SuperUser.FirstName = Convert.ToString(row.GetColumnValue("SUPER_USER_NAME", req.SuperUser.FirstName.GetType()));
            req.SuperUser.Email = Convert.ToString(row.GetColumnValue("SUPER_USER_EMAIL", req.SuperUser.Email.GetType()));
            req.SuperUser.PhoneNum = Convert.ToString(row.GetColumnValue("SUPER_USER_PHONE", req.SuperUser.PhoneNum.GetType()));
            req.PostedUser.PhoneNum = Convert.ToString(row.GetColumnValue("U_PHONE", req.SuperUser.PhoneNum.GetType()));
            req.PostedUser.FirstName = Convert.ToString(row.GetColumnValue("U_FNAME", req.SuperUser.FirstName.GetType()));
            req.PostedUser.LastName = Convert.ToString(row.GetColumnValue("U_LNAME", req.SuperUser.LastName.GetType()));
            req.PostedUser.UserInfo.Address = Convert.ToString(row.GetColumnValue("ADDRESS", req.SuperUser.UserInfo.Address.GetType()));
            req.MaterialDispachmentLink = Convert.ToInt32(row.GetColumnValue("MAT_DIS_LINK", req.MaterialDispachmentLink.GetType()));
            req.MaterialReceivedLink = Convert.ToInt32(row.GetColumnValue("MAT_DIS_LINK", req.MaterialReceivedLink.GetType()));
            
            NegotiationSettings ns = new NegotiationSettings();
            ns.MinReductionAmount = Convert.ToDouble(row.GetColumnValue("UD_MIN_REDUCE_AMOUNT", ns.MinReductionAmount.GetType()));
            ns.RankComparision = Convert.ToDouble(row.GetColumnValue("UD_VENDOR_COMPARISION_MIN_AMOUNT", ns.RankComparision.GetType()));
            ns.NegotiationDuration = Convert.ToString(row.GetColumnValue("UD_NEGOTIATION_DURATION", ns.NegotiationDuration.GetType()));
            req.NegotiationSettings = ns;

            

            return req;
        }
        
        public static VendorDetails GetVendorDetailsEntity(DataRow row, MySqlCommand cmd)
        {
            VendorDetails vendor = new VendorDetails();

            vendor.PO = new RequirementPO();
            vendor.Vendor = new User();
            vendor.VendorID = Convert.ToInt32(row.GetColumnValue("U_ID", vendor.VendorID.GetType()));
            vendor.VendorName = Convert.ToString(row.GetColumnValue("VENDOR_NAME", vendor.VendorName.GetType()));
            vendor.InitialPrice = Convert.ToDouble(row.GetColumnValue("VEND_INIT_PRICE", vendor.InitialPrice.GetType()));
            vendor.RunningPrice = Convert.ToDouble(row.GetColumnValue("VEND_RUN_PRICE", vendor.RunningPrice.GetType()));
            vendor.TotalInitialPrice = Convert.ToDouble(row.GetColumnValue("VEND_TOTAL_PRICE", vendor.TotalInitialPrice.GetType()));
            vendor.TotalRunningPrice = Convert.ToDouble(row.GetColumnValue("VEND_TOTAL_PRICE_RUNNING", vendor.TotalRunningPrice.GetType()));
            vendor.City = Convert.ToString(row.GetColumnValue("UAD_CITY", vendor.City.GetType()));
            vendor.Rating = Convert.ToDouble(row.GetColumnValue("RATING", vendor.Rating.GetType()));
            vendor.CompanyName = Convert.ToString(row.GetColumnValue("COMPANY_NAME", vendor.CompanyName.GetType()));
            vendor.InitialPriceWithOutTaxFreight = Convert.ToDouble(row.GetColumnValue("VEND_INIT_PRICE_WITHOUT_TAX", vendor.InitialPriceWithOutTaxFreight.GetType()));
            vendor.VendorFreight = Convert.ToDouble(row.GetColumnValue("VEND_FREIGHT", vendor.VendorFreight.GetType()));
            vendor.Warranty = Convert.ToString(row.GetColumnValue("WARRANTY", vendor.Warranty.GetType()));
            vendor.Payment = Convert.ToString(row.GetColumnValue("PAYMENT", vendor.Payment.GetType()));
            vendor.Duration = Convert.ToString(row.GetColumnValue("DURATION", vendor.Duration.GetType()));
            vendor.OtherProperties = Convert.ToString(row.GetColumnValue("OTHER_PROPERTIES", vendor.OtherProperties.GetType()));
            vendor.IsQuotationRejected = Convert.ToInt32(row.GetColumnValue("IS_QUOTATION_REJECTED", vendor.IsQuotationRejected.GetType()));
            vendor.QuotationRejectedComment = Convert.ToString(row.GetColumnValue("QUOTATION_REJECTED_REASON", vendor.QuotationRejectedComment.GetType()));
            vendor.OtherProperties = Convert.ToString(row.GetColumnValue("OTHER_PROPERTIES", vendor.OtherProperties.GetType()));
            vendor.PO.POLink = Convert.ToString(row.GetColumnValue("PO_ATT_ID", vendor.PO.POLink.GetType()));
            vendor.QuotationUrl = Convert.ToString(row.GetColumnValue("QUOTATION_URL", vendor.QuotationUrl.GetType()));
            vendor.RevQuotationUrl = Convert.ToString(row.GetColumnValue("REV_QUOTATION_URL", vendor.RevQuotationUrl.GetType()));
            vendor.RevPrice = Convert.ToDouble(row.GetColumnValue("REV_PRICE", vendor.RevPrice.GetType()));
            vendor.RevVendorFreight = Convert.ToDouble(row.GetColumnValue("REV_VEND_FREIGHT", vendor.RevVendorFreight.GetType()));
            vendor.RevVendorTotalPrice = Convert.ToDouble(row.GetColumnValue("REV_VEND_TOTAL_PRICE", vendor.RevVendorTotalPrice.GetType()));
            vendor.IsRevQuotationRejected = Convert.ToInt32(row.GetColumnValue("IS_REV_QUOTATION_REJECTED", vendor.IsRevQuotationRejected.GetType()));
            vendor.RevQuotationRejectedComment = Convert.ToString(row.GetColumnValue("REV_QUOTATION_REJECTED_REASON", vendor.RevQuotationRejectedComment.GetType()));
            vendor.LastActiveTime = Convert.ToString(row.GetColumnValue("LAST_ACTIVE_TIME", vendor.LastActiveTime.GetType()));
            vendor.Discount = Convert.ToDouble(row.GetColumnValue("DISCOUNT", vendor.Discount.GetType()));
            vendor.ReductionPercentage = Convert.ToDouble(row.GetColumnValue("REDUCTION_PERCENTAGE", vendor.ReductionPercentage.GetType()));
            vendor.Vendor.Email = Convert.ToString(row.GetColumnValue("U_EMAIL", vendor.Vendor.Email.GetType()));
            vendor.Vendor.PhoneNum = Convert.ToString(row.GetColumnValue("U_PHONE", vendor.Vendor.PhoneNum.GetType()));
            vendor.PO.MaterialDispachmentLink = Convert.ToInt32(row.GetColumnValue("MAT_DIS_LINK", vendor.PO.MaterialDispachmentLink.GetType()));
            vendor.PO.MaterialReceivedLink = Convert.ToInt32(row.GetColumnValue("MR_LINK", vendor.PO.MaterialReceivedLink.GetType()));
            vendor.ReductionPercentage = Math.Round(vendor.ReductionPercentage, 3);

            return vendor;
        }

        public static RequirementItems GetReqItemsEntity(DataRow row, MySqlCommand cmd)
        {
            RequirementItems item = new RequirementItems();
            
            item.ItemID = Convert.ToInt32(row.GetColumnValue("ITEM_ID", item.ItemID.GetType()));
            item.RequirementID = Convert.ToInt32(row.GetColumnValue("REQ_ID", item.RequirementID.GetType()));
            item.ProductIDorName = Convert.ToString(row.GetColumnValue("PROD_ID", item.ProductIDorName.GetType()));
            item.ProductDescription = Convert.ToString(row.GetColumnValue("DESCRIPTION", item.ProductDescription.GetType()));
            item.ProductQuantity = Convert.ToDouble(row.GetColumnValue("QUANTITY", item.ProductQuantity.GetType()));
            item.ProductBrand = Convert.ToString(row.GetColumnValue("BRAND", item.ProductBrand.GetType()));
            item.OthersBrands = Convert.ToString(row.GetColumnValue("OTHER_BRAND", item.OthersBrands.GetType()));
            item.ProductImageID = Convert.ToInt32(row.GetColumnValue("IMAGE_ID", item.ProductImageID.GetType()));
            item.IsDeleted = Convert.ToInt32(row.GetColumnValue("IS_DELETED", item.IsDeleted.GetType()));
            item.ProductQuantityIn = Convert.ToString(row.GetColumnValue("QUANTITY_IN", item.ProductQuantityIn.GetType()));
            item.SelectedVendorID = Convert.ToInt32(row.GetColumnValue("SELECTED_VENDOR_ID", item.SelectedVendorID.GetType()));
            item.ProductIDorNameCustomer = Convert.ToString(row.GetColumnValue("PROD_ID", item.ProductIDorNameCustomer.GetType()));
            item.ProductNoCustomer = Convert.ToString(row.GetColumnValue("PROD_NO", item.ProductNoCustomer.GetType()));
            item.ProductDescriptionCustomer = Convert.ToString(row.GetColumnValue("DESCRIPTION", item.ProductDescriptionCustomer.GetType()));
            item.ProductBrandCustomer = Convert.ToString(row.GetColumnValue("BRAND", item.ProductBrandCustomer.GetType()));
            
            return item;
        }

        private static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["MySQLConnectionString"].ConnectionString;
        }

        public static string GetEnumDesc<T>(string value) where T : struct, IConvertible
        {
            var type = typeof(T);
            var memInfo = type.GetMember(value);
            var attributes = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
            var description = ((DescriptionAttribute)attributes[0]).Description;
            return description;
        }

        public static void SaveFile(string fileName, byte[] fileContent)
        {
            
            //PTMGenericServicesClient ptmClient = new PTMGenericServicesClient();
            //ptmClient.SaveFileBytes(fileContent, fileName);
            //File.WriteAllBytes(fileName, attachment);
        }

        public static Response SaveAttachment(string path, MySqlCommand cmd = null)
        {
            Response response = new Response();
            try
            {

                if (cmd == null)
                {
                    cmd = new MySqlCommand();
                    cmd.Connection = new MySqlConnection(GetConnectionString());
                }
                if (cmd.Connection.State != ConnectionState.Open)
                {
                    cmd.Connection.Open();
                }
                cmd.Parameters.Clear();
                cmd.CommandText = "cp_SaveAttachment";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("P_PATH", path));
                MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                myDA.Fill(ds);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }
            finally
            {
                //cmd.Connection.Close();
                cmd.Parameters.Clear();
            }
            return response;
        }

        public static int ValidateSession(string sessionId, MySqlCommand cmd)
        {
            if (string.IsNullOrEmpty(sessionId))
            {
                throw new Exception("Session ID is null");
            }
            cmd.Parameters.Clear();
            int isValid = 1;
            try
            {
                if (cmd == null)
                {
                    cmd = new MySqlCommand();
                    cmd.Connection = new MySqlConnection(GetConnectionString());
                }
                cmd.CommandText = "cp_ValidateSession";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("P_SESSION_ID", sessionId));
                if (cmd.Connection.State != ConnectionState.Open)
                {
                    cmd.Connection.Open();
                }
                MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                myDA.Fill(ds);

                cmd.Parameters.Clear();
                //cmd.Connection.Close();

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    isValid = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (MySqlException ex)
            {
                HttpContext.Current.Server.Transfer("/prm360.html#/login");
            }
            catch (Exception ex)
            {
                isValid = 0;
            }

            if (isValid <= 0)
            {
                throw new Exception("Invalid Session ID passed");
            }

            return isValid;
        }        
    }
}