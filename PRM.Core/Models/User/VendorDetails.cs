﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRM.Core.Models
{
    [DataContract]
    public class VendorDetails : Entity
    {
        [DataMember(Name = "vendorID")]
        public int VendorID { get; set; }

        [DataMember(Name = "requirementID")]
        public int RequirementID { get; set; }

        string vendorName = string.Empty;
        [DataMember(Name = "vendorName")]
        public string VendorName
        {
            get
            {
                return this.vendorName;
            }
            set
            {
                this.vendorName = value;
            }
        }
        
        [DataMember(Name = "initialPrice")]
        public double InitialPrice { get; set; }

        [DataMember(Name = "runningPrice")]
        public double RunningPrice { get; set; }

        [DataMember(Name = "totalInitialPrice")]
        public double TotalInitialPrice { get; set; }

        [DataMember(Name = "totalRunningPrice")]
        public double TotalRunningPrice { get; set; }

        [DataMember(Name = "rank")]
        public int Rank { get; set; }

        [DataMember(Name = "city")]
        public string City { get; set; }

        [DataMember(Name = "taxes")]
        public double Taxes { get; set; }

        [DataMember(Name = "quotationUrl")]
        public string QuotationUrl { get; set; }


        [DataMember(Name = "rating")]
        public double Rating { get; set; }

        string companyName = string.Empty;
        [DataMember(Name = "companyName")]
        public string CompanyName
        {
            get
            {
                return this.companyName;
            }
            set
            {
                this.companyName = value;
            }
        }
        

        [DataMember(Name = "initialPriceWithOutTaxFreight")]
        public double InitialPriceWithOutTaxFreight { get; set; }

        [DataMember(Name = "vendorFreight")]
        public double VendorFreight { get; set; }

        [DataMember(Name = "warranty")]
        public string Warranty { get; set; }

        [DataMember(Name = "payment")]
        public string Payment { get; set; }

        [DataMember(Name = "duration")]
        public string Duration { get; set; }

        [DataMember(Name = "validity")]
        public string Validity { get; set; }

        [DataMember(Name = "isQuotationRejected")]
        public int IsQuotationRejected { get; set; }

        [DataMember(Name = "quotationRejectedComment")]
        public string QuotationRejectedComment { get; set; }

        [DataMember(Name = "PO")]
        public RequirementPO PO { get; set; }


        

        //  REV_QUOTATION_URL, REV_PRICE, REV_TAX, REV_VEND_FREIGHT, REV_VEND_TOTAL_PRICE

        [DataMember(Name = "revquotationUrl")]
        public string RevQuotationUrl { get; set; }

        [DataMember(Name = "revPrice")]
        public double RevPrice { get; set; }

        [DataMember(Name = "revVendorFreight")]
        public double RevVendorFreight { get; set; }

        [DataMember(Name = "revVendorTotalPrice")]
        public double RevVendorTotalPrice { get; set; }

        [DataMember(Name = "isRevQuotationRejected")]
        public int IsRevQuotationRejected { get; set; }

        [DataMember(Name = "revQuotationRejectedComment")]
        public string RevQuotationRejectedComment { get; set; }

        [DataMember(Name = "lastActiveTime")]
        public string LastActiveTime { get; set; }

        [DataMember(Name = "discount")]
        public double Discount { get; set; }

        [DataMember(Name = "reductionPercentage")]
        public double ReductionPercentage { get; set; }

        [DataMember(Name = "otherProperties")]
        public string OtherProperties { get; set; }

        [DataMember(Name = "vendor")]
        public User Vendor { get; set; }

        [DataMember(Name = "materialDispachmentLink")]
        public int MaterialDispachmentLink { get; set; }

        [DataMember(Name = "materialReceivedLink")]
        public int MaterialReceivedLink { get; set; }
    }
}