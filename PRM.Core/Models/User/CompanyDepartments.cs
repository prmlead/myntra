﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRM.Core.Models
{
    [DataContract]
    public class CompanyDepartments : Entity
    {        
        //DEPT_ID, DEPT_CODE, DEPT_DESC, COMP_ID, CREATED_BY, MODIFIED_BY, DATE_CREATED, DATE_MODIFIED 

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "deptID")]
        public int DeptID { get; set; }

        [DataMember(Name = "deptCode")]
        public string DeptCode { get; set; }

        [DataMember(Name = "deptDesc")]
        public string DeptDesc { get; set; }

        [DataMember(Name = "compID")]
        public int CompID { get; set; }

        [DataMember(Name = "deptAdmin")]
        public int DeptAdmin { get; set; }

        [DataMember(Name = "deptAdminName")]
        public string DeptAdminName { get; set; }

        [DataMember(Name = "isValid")]
        public int IsValid { get; set; }
    }
}