﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRM.Core.Models
{
    [DataContract]
    public class ReqDepartments : Entity
    {        
        [DataMember(Name = "reqDeptID")]
        public int ReqDeptID { get; set; }

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "reqID")]
        public int ReqID { get; set; }
        
        [DataMember(Name = "isValid")]
        public bool IsValid { get; set; }

        [DataMember(Name = "companyDepartments")]
        public CompanyDepartments CompanyDepartments { get; set; }

        [DataMember(Name = "createdBy")]
        public int CreatedBy { get; set; }

        [DataMember(Name = "user")]
        public User User { get; set; }
        
    }
}