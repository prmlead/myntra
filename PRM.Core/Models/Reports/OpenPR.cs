﻿using PRM.Core.Common;
using System;
using System.Runtime.Serialization;

namespace PRM.Core.Models.Reports
{
    [DataContract]
    public class OpenPR : ResponseAudit
    {
        [DataMember]
        [DataNames("COMP_ID")]
        public string COMP_ID { get; set; }

        [DataMember]
        [DataNames("PLANT")]
        public string PLANT { get; set; }

        [DataMember]
        [DataNames("PURCHASING_GROUP")]
        public string PURCHASING_GROUP { get; set; }

        [DataMember]
        [DataNames("PURCHASE_REQUISITION")]
        public string PURCHASE_REQUISITION { get; set; }

        [DataMember]
        [DataNames("ITEM_OF_REQUISITION")]
        public string ITEM_OF_REQUISITION { get; set; }

        [DataMember]
        [DataNames("REQUISITION_DATE")]
        public string REQUISITION_DATE { get; set; }

        [DataMember]
        [DataNames("MATERIAL")]
        public string MATERIAL { get; set; }

        [DataMember]
        [DataNames("SHORT_TEXT")]
        public string SHORT_TEXT { get; set; }

        [DataMember]
        [DataNames("UNIT_OF_MEASURE")]
        public string UNIT_OF_MEASURE { get; set; }

        [DataMember]
        [DataNames("QUANTITY_REQUESTED")]
        public string QUANTITY_REQUESTED { get; set; }

        [DataMember]
        [DataNames("TOTAL_VALUE")]
        public string TOTAL_VALUE { get; set; }

        [DataMember]
        [DataNames("DELIV_DATE_FROM_TO")]
        public string DELIV_DATE_FROM_TO { get; set; }

        [DataMember]
        [DataNames("VENDOR")]
        public string VENDOR { get; set; }

        [DataMember]
        [DataNames("PURCHASE_ORDER_DATE")]
        public string PURCHASE_ORDER_DATE { get; set; }

        [DataMember]
        [DataNames("PURCHASE_ORDER")]
        public string PURCHASE_ORDER { get; set; }

        [DataMember]
        [DataNames("DELIVERY_DATE")]
        public string DELIVERY_DATE { get; set; }

        [DataMember]
        [DataNames("RELEASE_DATE")]
        public string RELEASE_DATE { get; set; }

        [DataMember]
        [DataNames("STATUSCODE")]
        public string STATUSCODE { get; set; }

        [DataMember]
        [DataNames("DELETION_INDICATOR")]
        public string DELETION_INDICATOR { get; set; }

        [DataMember]
        [DataNames("PURCHASE_ORDER_ITEM")]
        public string PURCHASE_ORDER_ITEM { get; set; }

        [DataMember]
        [DataNames("SUPPLIER")]
        public string SUPPLIER { get; set; }

        [DataMember]
        [DataNames("BLOCKING_INDICATOR")]
        public string BLOCKING_INDICATOR { get; set; }

        [DataMember]
        [DataNames("CLOSED")]
        public string CLOSED { get; set; }

        [DataMember]
        [DataNames("QUANTITY_ORDERED")]
        public string QUANTITY_ORDERED { get; set; }

        [DataMember]
        [DataNames("GOODS_RECEIPT")]
        public string GOODS_RECEIPT { get; set; }

        [DataMember]
        [DataNames("PLANT_NAME")]
        public string PLANT_NAME { get; set; }

        [DataMember]
        [DataNames("PURCHASER")]
        public string PURCHASER { get; set; }

        [DataMember]
        [DataNames("API_NAME")]
        public string API_NAME { get; set; }

        [DataMember]
        [DataNames("LEAD_TIME")]
        public string LEAD_TIME { get; set; }
    }


    [DataContract]
    public class OpenPO : ResponseAudit
    {
        [DataMember]
        [DataNames("PLANT_NAME")]
        public string PLANT_NAME { get; set; }

        [DataMember]
        [DataNames("PURCHASER")]
        public string PURCHASER { get; set; }

        [DataMember]
        [DataNames("PO_NO")]
        public string PO_NO { get; set; }

        [DataMember]
        [DataNames("GRN_QTY")]
        public string GRN_QTY { get; set; }

        [DataMember]
        [DataNames("PLANT")]
        public string PLANT { get; set; }

        [DataMember]
        [DataNames("PUR_ORG")]
        public string PUR_ORG { get; set; }

        [DataMember]
        [DataNames("PUR_GRP")]
        public string PUR_GRP { get; set; }

        [DataMember]
        [DataNames("PR_NUMBER")]
        public string PR_NUMBER { get; set; }

        [DataMember]
        [DataNames("PR_ITM_NO")]
        public string PR_ITM_NO { get; set; }

        [DataMember]
        [DataNames("PR_CREATE_DATE")]
        public string PR_CREATE_DATE { get; set; }

        [DataMember]
        [DataNames("MATERIAL")]
        public string MATERIAL { get; set; }

        [DataMember]
        [DataNames("MAT_DESCRIPTION")]
        public string MAT_DESCRIPTION { get; set; }

        [DataMember]
        [DataNames("API_NAME")]
        public string API_NAME { get; set; }

        [DataMember]
        [DataNames("QUANTITY")]
        public string QUANTITY { get; set; }

        [DataMember]
        [DataNames("UOM")]
        public string UOM { get; set; }

        [DataMember]
        [DataNames("LEAD_TIME")]
        public string LEAD_TIME { get; set; }

        [DataMember]
        [DataNames("GRN_DT")]
        public string GRN_DT { get; set; }

        [DataMember]
        [DataNames("PO_DELV_DATE")]
        public string PO_DELV_DATE { get; set; }

        [DataMember]
        [DataNames("PO_DATE")]
        public string PO_DATE { get; set; }

        [DataMember]
        [DataNames("PO_ITEM_TEXT")]
        public string PO_ITEM_TEXT { get; set; }

        [DataMember]
        [DataNames("VENDOR")]
        public string VENDOR { get; set; }

        [DataMember]
        [DataNames("VENDOR_NAME")]
        public string VENDOR_NAME { get; set; }

        [DataMember]
        [DataNames("VENDORS_COUNTRY")]
        public string VENDORS_COUNTRY { get; set; }

        [DataMember]
        [DataNames("PO_TYPE")]
        public string PO_TYPE { get; set; }

        [DataMember]
        [DataNames("USER_ID")]
        public string USER_ID { get; set; }

        [DataMember]
        [DataNames("GRN_NET_VALUE")]
        public string GRN_NET_VALUE { get; set; }

        [DataMember]
        [DataNames("PO_VALUE")]
        public string PO_VALUE { get; set; }        
    }
}
