﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class Register : Entity
    {
        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        [DataMember(Name = "phoneNum")]
        public string PhoneNum { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "category")]
        public string Category { get; set; }

        [DataMember(Name = "panNumber")]
        public string PanNumber { get; set; }

        [DataMember(Name = "vatNumber")]
        public string VatNumber { get; set; }

        [DataMember(Name = "stnNumber")]
        public string StnNumber { get; set; }

        [DataMember(Name = "knownSince")]
        public string KnownSince { get; set; }

        [DataMember(Name = "username")]
        public string Username { get; set; }

        [DataMember(Name = "password")]
        public string Password { get; set; }

        [DataMember(Name = "companyName")]
        public string CompanyName { get; set; }

        [DataMember(Name = "department")]
        public string Department { get; set; }

        [DataMember(Name = "userType")]
        public string UserType { get; set; }

        [DataMember(Name="referringUserID")]
        public int ReferringUserID { get; set; }

        [DataMember(Name = "subcategories")]
        public int[] Subcategories { get; set; }

        [DataMember(Name = "currency")]
        public int Currency { get; set; }

        [DataMember(Name = "timeZone")]
        public int TimeZone { get; set; }

        [DataMember(Name = "altEmail")]
        public string AltEmail { get; set; }

        [DataMember(Name = "altPhoneNum")]
        public string AltPhoneNum { get; set; }
    }
}