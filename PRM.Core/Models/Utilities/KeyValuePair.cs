﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class KeyValuePair : Entity
    {
        [DataMember(Name = "key")]
        public int Key { get; set; }

        [DataMember(Name = "key1")]
        public string Key1 { get; set; }

        [DataMember(Name = "value")]
        public string Value { get; set; }

        [DataMember(Name = "value1")]
        public int Value1 { get; set; }

        [DataMember(Name = "decimalVal")]
        public decimal DecimalVal { get; set; }

        [DataMember(Name = "arrayVal")]
        public object[] ArrayVal { get; set; }
    }
}