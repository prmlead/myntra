﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Core.Domain.RealTimePrices
{
   public class RealTimePriceReport
    {
        public DateTime? PriceDate { get; set; }
        public decimal Price { get; set; }
    }
}
